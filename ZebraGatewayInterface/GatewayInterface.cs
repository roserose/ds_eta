using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Net;
using System.Net.Sockets;

namespace ZebraGatewayInterface
{
    public enum REMOTECMD
    {
        TOTALVEHS,
        GETVEHSLIST,
        REGISTERTOLOG,
        VEHICLELOG,
        VEHICLECOMMAND,
        VEHICLETRIGGER
    };
    public enum enumReciveMsgType { Normal = 0, Zebra = 1 };
    [Serializable]
    public struct GATEWAYINFO
    {
        public string ipaddr;
        public int port;
        public int total_vehicle;
    }

    [Serializable]
    public struct GATEWAYCMDINFO
    {
        public int veh_id;
        public string cmd;
    }

    public struct REMOTEENDPPOINTINFO
    {
        public int veh_id;
        public string veh_reg;
        public IPEndPoint remote_endpoint;
        public bool connected;
    }

    public struct VEHICLELISTINFO
    {
        public int veh_id;
        public REMOTEENDPPOINTINFO tagInfo;
    }

    public struct RECEIVESOCKETINFO
    {
        public int veh_id;
        public string cmd;
        public string data;
        public byte[] buf;
        public Socket socket;
        public int ttl;
    }
    public struct REMOTECMDINFO
    {
        public int veh_id;
        public string cmd;
        public string data;
        public Socket socket;
        public DateTime expire;
    }

    public struct RemoteTcpInfo
    {
        public int veh_id;
        public string cmd;
        public string data;
        public TcpClient tcp;
        public DateTime expire;
        public TransactionStatus Ts;
    }
    public struct TransactionStatus
    {
        public STATUS StatusTrigger;
        public STATUS StatusGateWay;
        public STATUS StatusBox;
        public STATUS StatusReport;
    }

    public enum STATUS
    {
        NONE,
        TRIGGER_OK,
        TRIGGER_ERROR,
        GATEWAY_OK,
        GATEWAY_ERROR,
        BOX_OK,
        BOX_ERROR,
        REPORT_OK,
        REPORT_ERROR
    };

    public enum ProcessState 
    {
        ADD, 
        SELECT, 
        UPDATE,
        DELETE, 
        LOSTSIGNAL 
    };

    /*Socket*/
    public delegate void ClientReceiveMsgHandler(Socket socket, string data);
    public delegate void ServerReceiveMsgHandler(Socket socket, int veh_id, string cmd, string data);
    public delegate void RemoteMsgHandler(Socket socket, int veh_id, string cmd, string data);
    public delegate void ConnectedHandler(Socket socket, String name);
    public delegate void DisconnectedHandler(Socket socket, String name);

    /*TCPClient && TcpListener*/
    public delegate void TcpConnectedMsg(TcpClient tcp, string data);
    public delegate void TcpDisconnectedMsg(TcpClient tcp, string data);
    public delegate void TcpReceiveMsg(TcpClient tcp, int veh_id, string cmd, string data);
    public delegate void TcpReceiveMsg2(TcpClient tcp, string data);

    public delegate void RemoteMessage(string data);
  
    public interface GatewayInterface
    {
        void Close();
        void TestPost();

        bool SendDeviceCmd(int veh_id, string cmd);
        int GetNumberOfConnection();

        String Alive();
        ArrayList GetActiveConnectionList();
        GATEWAYINFO GetIPStr();
    }
}
