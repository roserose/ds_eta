﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZebraDataCentre;
using ZebraEncryption;
using ZebraDataCentre;

namespace ZebraPortViewer
{      
    public partial class ConfigFrm : Form
    {
        private ZebraPortMonitor.CFG _objCFG;
        public ZebraPortMonitor.CFG objCFG
        {
            set { _objCFG = value; }
            get { return _objCFG; }
        }

      //  private 
        private DataTable cmdEncryptionTable = new DataTable();
        private DataTable cmdBoxTable = new DataTable();
        private DataTable cmdOutputType = new DataTable();
        public DataTable dtCMDList;

        public ConfigFrm()
        {
            InitializeComponent();
        }

        private void ConfigFrm_Load(object sender, EventArgs e)
        {
            InitializeCombobox();
            InitializeText();
        }
        private void InitializeCombobox()
        {
            InitCmdTable();

            cbEncryption.DataSource = cmdEncryptionTable;
            cbEncryption.DisplayMember = "cmd_display";
            cbEncryption.ValueMember = "cmd_algorithm";
            cbEncryption.SelectedIndex = 0;

            cbBoxType.DataSource = cmdBoxTable;
            cbBoxType.DisplayMember = "cmd_display";
            cbBoxType.ValueMember = "cmd_version";
            cbBoxType.SelectedIndex = 0;

            cbOutputType.DataSource = cmdOutputType;
            cbOutputType.DisplayMember = "cmd_display";
            cbOutputType.ValueMember = "cmd_output";
            cbOutputType.SelectedIndex = 0;
        }
        private void InitCmdTable()
        {
            cmdEncryptionTable.Columns.Add("cmd_display", typeof(String));
            cmdEncryptionTable.Columns.Add("cmd_algorithm", typeof(EncryptionAlgorithm));
            foreach (EncryptionAlgorithm suit in Enum.GetValues(typeof(EncryptionAlgorithm)))
            {
                cmdEncryptionTable.Rows.Add(new object[] { suit.ToString(), suit });
            }

            cmdBoxTable.Columns.Add("cmd_display", typeof(String));
            cmdBoxTable.Columns.Add("cmd_version", typeof(BoxVersions));
            foreach (BoxVersions suit in Enum.GetValues(typeof(BoxVersions)))
            {
                cmdBoxTable.Rows.Add(new object[] { suit.ToString(), suit });
            }

            cmdOutputType.Columns.Add("cmd_display", typeof(String));
            cmdOutputType.Columns.Add("cmd_output", typeof(EncryptionDataType));
            foreach (EncryptionDataType suit in Enum.GetValues(typeof(EncryptionDataType)))
            {
                cmdOutputType.Rows.Add(new object[] { suit.ToString(), suit });
            }
        }
        private void InitializeText()
        {
            try
            {
                tbConDatabase.Text = _objCFG.DbHost;
                tbConLocalIP.Text = _objCFG.Host_ip;
                tbConLocalPort.Text = _objCFG.Local_port.ToString();
                tbConForwadPort.Text = _objCFG.Forward_Port.ToString();
                tbConRemoteIP.Text = _objCFG.Remote_ip;
                tbConRemotePort.Text = _objCFG.Remote_port.ToString();

                tbRawDataQ.Text = _objCFG.RawDataQ;
                tbReadQueue.Text = _objCFG.ReadQ;
                tbTagMsg.Text = _objCFG.Msg_tag;
                tbConTriggerIP.Text = _objCFG.TriggerIP;
                tbConTriggerPort.Text = _objCFG.TriggerPort.ToString();

                rtConSystemQueue.Text = _objCFG.Max2PushQ.Replace(',', '\n');

                ckConRecDB.Checked = _objCFG.EnableRecVeh;
                ckEnableTrigger.Checked = _objCFG.EnableTrigger;
                ckEnableRemoteObj.Checked = _objCFG.EnableRemoteObj;
                ckEnableLogQueue.Checked = _objCFG.EnableLoqQueue;
            }
            catch
            { }
        }

        private void btConfig_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do You Want to Save.", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {

                _objCFG.DbHost = tbConDatabase.Text;
                _objCFG.Host_ip = tbConLocalIP.Text;
                _objCFG.Local_port = Convert.ToInt32(tbConLocalPort.Text);
                _objCFG.Forward_Port = Convert.ToInt32(tbConForwadPort.Text);
                _objCFG.Remote_ip = tbConRemoteIP.Text;
                _objCFG.Remote_port = Convert.ToInt32(tbConRemotePort.Text);

                _objCFG.RawDataQ = tbRawDataQ.Text;
                _objCFG.ReadQ = tbReadQueue.Text;
                _objCFG.Msg_tag = tbTagMsg.Text;
                _objCFG.TriggerIP = tbConTriggerIP.Text;
                _objCFG.TriggerPort = Convert.ToInt32(tbConTriggerPort.Text);

                _objCFG.Max2PushQ = rtConSystemQueue.Text.Replace('\n', ',');

                _objCFG.EnableRecVeh = ckConRecDB.Checked;
                _objCFG.EnableTrigger = ckEnableTrigger.Checked;
                _objCFG.EnableRemoteObj = ckEnableRemoteObj.Checked;
                _objCFG.EnableLoqQueue = ckEnableLogQueue.Checked;
            }
        }
  
    }
}
