using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Remoting.Channels.Tcp;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using System.Runtime.Remoting;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Messaging;
using System.Runtime.Remoting.Messaging;

using ZebraNet;
using ZebraPortMonitor;
using ZebraGatewayInterface;
using ZebraRemoteInterface;
using ZebraDataCentre;

namespace ZebraPortViewer
{
    public partial class ViewerFrm : Form
    {
        private System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        private System.Threading.Semaphore tvNodeBusy = new System.Threading.Semaphore(1, 1);
        private System.Threading.Semaphore wrLogBusy = new System.Threading.Semaphore(1, 1);
        private System.Threading.Semaphore gTrigerSemaphore = new System.Threading.Semaphore(1, 1);
        private System.Threading.Semaphore updateMsgQSemaphore = new System.Threading.Semaphore(1, 1);
        private System.Threading.Semaphore gExpireSemaphore = new System.Threading.Semaphore(1, 1);
     
        private delegate int MyDelegate(int s);

        #region Properties
        private Point tvNodeListMousePoint = new Point();
        private RemoteSeverInterface remoteServerInterface = null;
        private TCPClient objTcpClient = null;
        private Monitors objPortMon = null;

        private ArrayList regLogMsg = new ArrayList();
        private Hashtable gHsClientSendCmd = new Hashtable();
        private Hashtable gHsTrigerCmdList = new Hashtable();
        private Hashtable gHsUserSendCmd = new Hashtable();
        private Queue gQTriggerExpire = new Queue();
        private Queue gQTrigger = new Queue();

        private Queue<string> _updateMsgQ = new Queue<string>();
        public Queue<string> UpdateMsgQ
        {
            get
            {
                return _updateMsgQ;
            }
            set
            {
                _updateMsgQ = value;
            }
        }
        private DateTime TimeDisPlayMonitor = DateTime.Now;
        
        #endregion

        public ViewerFrm()
        {
            this.Name = "MainFrm";
            try
            {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
               
                InitializeComponent();
                InitCfg();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ViewerFrm_Load(object sender, EventArgs e)
        {
            tmTriggerExpire.Start();
            tmChkProcessHealth.Start();
            tmTrigger.Start();
            tmUpdateMsg.Start();
        }

        private void InitCfg()
        {
            try
            {
                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                // TCP Listening
                objPortMon = new Monitors();
                objPortMon.OnMonitorsMsg2 += new MonitorsMsgHandler2(objPortMon_OnMonitorsMsg2);
                objPortMon.OnDisconnectFromClient += new ClientDisconectedHandler(objPortMon_OnDisconnectFromClient);

                try
                {
                    IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
                    foreach (IPAddress ip in host.AddressList)
                    {
                        IPAddress ipaddress = IPAddress.Parse(objPortMon.CFG.Host_ip); ;
                        if (ip.ToString() == ipaddress.ToString())
                        {
                            break;
                        }
                    }
                }
                catch
                { }

                InitRemoteConfig();

                tmTriggerExpire.Interval = 1000 * 30;
                tmTrigger.Interval = 1000;

                this.Text = "Zebra Port Viewer V." + System.Reflection.Assembly.GetEntryAssembly().GetName().Version;
                
                toolStripStatusPortNumber2.Text = objPortMon.CFG.Local_port.ToString();
                toolStripStatusQName2.Text = objPortMon.CFG.Max2PushQ;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Caption");
            }
        }
     
        private void InitRemoteConfig()
        {
            try
            {
                remoteServerInterface = new RemoteSeverInterface(Convert.ToInt32(objPortMon.CFG.Forward_Port));
                remoteServerInterface.Init();
                remoteServerInterface.OnRemoteMsg += new RemoteMsgHandler(remoteServerInterface_OnRemoteMsg);

                if (objPortMon.CFG.EnableTrigger)
                {
                    objTcpClient = new TCPClient();
                    objTcpClient.OnConnected += new TcpConnectedMsg(remoteTcpClient_OnConnected);  //+= new RemoteMsgHandler(remoteClientInterface_OnRemoteMsg);
                    objTcpClient.OnRemoteMsg += new TcpReceiveMsg(remoteTcpClient_OnRemoteMsg);
                    objTcpClient.OnDisConnected += new TcpDisconnectedMsg(remoteTcpClient_OnDisConnected);
                    objTcpClient.Connect(objPortMon.CFG.TriggerIP, objPortMon.CFG.TriggerPort, objPortMon.CFG.Host_ip, objPortMon.CFG.Local_port, true, string.Empty, 60, enumReciveMsgType.Zebra);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region RemoteServer
        private void remoteServerInterface_OnRemoteMsg(Socket socket, int veh_id, string cmd, string data)
        {
            if (socket == null) return;

            try
            {
                REMOTECMDINFO info = new REMOTECMDINFO();
                info.veh_id = veh_id;
                info.cmd = cmd;
                info.data = data.Trim(new char[] { '\0' });
                info.socket = socket;

                if (cmd.Trim().ToUpper() == REMOTECMD.TOTALVEHS.ToString())
                {
                    return;
                }
                else if (cmd.Trim().ToUpper() == REMOTECMD.GETVEHSLIST.ToString())
                {
                    GetAllVehicles(info);
                    return;
                }
                else if (cmd.Trim().ToUpper() == REMOTECMD.REGISTERTOLOG.ToString())
                {
                    RegisterTologMsg(socket);
                    return;
                }
                else
                {
                    // Write
                    // 1 "*>BW0"
                    // 2 "**********ABC"
                    // 3 "*>BW0**********ABC\n"

                    //READ
                    //"*>BR0"
                    string Command = cmd;
                    if(data.Length > 0 && !(data.StartsWith("\0\0\0\0")))
                    {
                        string _data = data.Substring(0, data.IndexOf('\0'));
                        Command += _data;
                    }

                    if (SendCommandTreeNodeToDevice(Command, veh_id))
                    {
                        if (!gHsClientSendCmd.ContainsKey(veh_id))
                        {
                            gHsClientSendCmd.Add(veh_id, info);
                        }
                        else
                        {
                            gHsClientSendCmd[veh_id] = info;
                        }
                    }
                    else
                    {
                        if (gHsClientSendCmd.ContainsKey(veh_id))
                        {
                            gHsClientSendCmd.Remove(veh_id);
                        }
                    }
                    return;
                }
            }
            catch
            {
            }
        }
        private bool SendCommandTreeNodeToDevice(string cmd, int vid)
        {
            semaphor.WaitOne();
            bool is_suscess = false;
            try
            {
                string cmdMsgStr = cmd;
                IPEndPoint rmEP = null;
                int veh_id = vid;
                if (TvNodeList != null && TvNodeList.Nodes != null && TvNodeList.Nodes.Count > 0)
                {
                    foreach (TreeNode node in TvNodeList.Nodes)
                    {
                        REMOTEENDPPOINTINFO nodeInfo = (REMOTEENDPPOINTINFO)node.Tag;
                        if (nodeInfo.veh_id == veh_id)
                        {
                            rmEP = nodeInfo.remote_endpoint;
                            break;
                        }
                    }
                }
                ClientManager clnt1 = null;
                if (rmEP != null)
                {
                    foreach (ClientManager mngr in objPortMon.tcp.ClientList)
                    {
                        try
                        {
                            if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                                (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                            {
                                clnt1 = mngr;
                                break;
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
                if (clnt1 != null)
                {
                    if (clnt1.SendNetMsgToClient(cmdMsgStr + "\n"))
                    {
                        is_suscess = true;
                    }
                }
            }
            catch { }
            semaphor.Release();
            return is_suscess;
        }
        #endregion
        #region RemoteClient
        private void remoteTcpClient_OnConnected(TcpClient tcp, string data) { }
        private void remoteTcpClient_OnDisConnected(TcpClient tcp, string data) { }
        private void remoteTcpClient_OnRemoteMsg(TcpClient tcp, int veh_id, string cmd, string data)
        {
            try
            {
                if (cmd.Trim().ToUpper() == REMOTECMD.VEHICLECOMMAND.ToString() || cmd.Trim().ToUpper() == REMOTECMD.VEHICLETRIGGER.ToString())
                {
                    RemoteTcpInfo info = new RemoteTcpInfo();
                    info.veh_id = veh_id;
                    info.cmd = REMOTECMD.VEHICLECOMMAND.ToString();//cmd.Trim().ToUpper();
                    data = data.Trim(new char[] { '\0', '\r' });
                    data = data.Replace('#', ',');
                    info.data = data;
                    info.tcp = tcp;
                    EnqueueTrigger(info);
                    return;
                }
                else
                { }
            }
            catch { }
        }
        private void tmTrigger_Tick(object sender, EventArgs e)
        {
            tmTrigger.Stop();
            try
            {
                while (gQTrigger.Count > 0)
                {
                    RemoteTcpInfo info = (RemoteTcpInfo)gQTrigger.Dequeue();

                    if (info.cmd.ToUpper() == REMOTECMD.VEHICLECOMMAND.ToString())
                    {
                        if (SendCommandTreeNodeToDevice(info.data, info.veh_id))
                        {
                            info.cmd = REMOTECMD.VEHICLELOG.ToString();
                            info.data = STATUS.GATEWAY_OK.ToString();
                            info.Ts.StatusGateWay = STATUS.GATEWAY_OK;
                            info.expire = DateTime.Now.AddSeconds(60);
                        }
                        else
                        {
                            info.cmd = REMOTECMD.VEHICLELOG.ToString();
                            info.data = STATUS.GATEWAY_ERROR.ToString();
                            info.Ts.StatusGateWay = STATUS.GATEWAY_ERROR;
                        }

                        MangerHashTriggerBuffer(ProcessState.ADD, info);
                        EnqueueTrigger(info);
                    }
                    else if (info.cmd.ToUpper() == REMOTECMD.VEHICLELOG.ToString())
                    {
                        string[] zMsg = info.data.Split(' ');
                        int index = info.veh_id;

                        if (gHsTrigerCmdList.ContainsKey(index))
                        {
                            bool isDel = true;
                            RemoteTcpInfo hash_info = (RemoteTcpInfo)gHsTrigerCmdList[index];
                            if (info.Ts.StatusGateWay == STATUS.GATEWAY_ERROR)
                            {
                                SendReportToServer(hash_info);
                                isDel = true;
                            }
                            else if (info.Ts.StatusGateWay == STATUS.GATEWAY_OK)
                            {
                                if (SendReportToServer(hash_info)) { isDel = false; }
                                else isDel = true;
                            }
                            else if (info.data.StartsWith("Report"))
                            {
                                hash_info.data = info.data;
                                hash_info.cmd = info.cmd;
                                SendReportToServer(hash_info);

                                if (hash_info.Ts.StatusBox != STATUS.BOX_OK)
                                {
                                    hash_info.Ts.StatusReport = STATUS.REPORT_OK;
                                    MangerHashTriggerBuffer(ProcessState.ADD, hash_info);
                                    isDel = false;
                                }
                                else
                                    isDel = true;
                            }
                            else if (info.data[0] == 'W' || info.data[0] == 'R')
                            {
                                hash_info.data = info.data;
                                hash_info.cmd = info.cmd;
                                SendReportToServer(hash_info);

                                if (info.data[0] == 'W')
                                {
                                    isDel = true;
                                }
                                else if (info.data[0] == 'R')
                                {
                                    if (hash_info.Ts.StatusReport != STATUS.REPORT_OK)
                                    {
                                        hash_info.Ts.StatusBox = STATUS.BOX_OK;
                                        MangerHashTriggerBuffer(ProcessState.ADD, hash_info);
                                        isDel = false;
                                    }
                                    else
                                        isDel = true;
                                }
                            }

                            if (isDel)
                            {
                                MangerHashTriggerBuffer(ProcessState.DELETE, info);
                            }
                        }
                    }
                    Thread.Sleep(10);
                }
            }
            catch { }
            tmTrigger.Start();
        }
        private void EnqueueTrigger(RemoteTcpInfo msg)
        {
            gTrigerSemaphore.WaitOne();
            try
            {
                gQTrigger.Enqueue(msg);
            }
            catch { }
            gTrigerSemaphore.Release();
        }
        private bool SendReportToServer(RemoteTcpInfo info)
        {
            bool isDel = false;
            try
            {
                IPEndPoint ep = (IPEndPoint)info.tcp.Client.RemoteEndPoint;
                string key = ep.ToString();

                if (objTcpClient.gHsServerList.ContainsKey(key))
                {
                    string msg = info.veh_id.ToString() + "," + info.cmd + "," + info.data;
                    isDel = objTcpClient.SendData(key, msg);
                }
            }
            catch
            { }
            return isDel;
        }
        private void tmTriggerExpire_Tick(object sender, EventArgs e)
        {
            tmTriggerExpire.Stop();
            try
            {
                MangerHashTriggerBuffer(ProcessState.LOSTSIGNAL, new RemoteTcpInfo());
            }
            catch
            { }
            tmTriggerExpire.Start();
        }
        private void MangerHashTriggerBuffer(ProcessState state, RemoteTcpInfo info)
        {
            gExpireSemaphore.WaitOne();
            try
            {
                switch (state)
                {
                    case ProcessState.ADD:
                        {
                            if (!gHsTrigerCmdList.ContainsKey(info.veh_id))
                            {
                                gHsTrigerCmdList.Add(info.veh_id, info);
                            }
                            else
                            {
                                gHsTrigerCmdList[info.veh_id] = info;
                            }
                        }
                        break;
                    case ProcessState.DELETE:
                        {
                            if (gHsTrigerCmdList.ContainsKey(info.veh_id))
                            {
                                gHsTrigerCmdList.Remove(info.veh_id);
                            }
                        }
                        break;
                    case ProcessState.LOSTSIGNAL:
                        {
                            if (gHsTrigerCmdList.Count > 0)
                            {
                                ArrayList arRemove = new ArrayList();
                                foreach (DictionaryEntry entry in gHsTrigerCmdList)
                                {
                                    RemoteTcpInfo e = (RemoteTcpInfo)gHsTrigerCmdList[entry.Key];

                                    if (DateTime.Now >= e.expire)
                                    {
                                        arRemove.Add(entry.Key);
                                    }
                                }
                                foreach (int id in arRemove)
                                {
                                    if (gHsTrigerCmdList.ContainsKey(id))
                                    {
                                        RemoteTcpInfo e = (RemoteTcpInfo)gHsTrigerCmdList[id];
                                        e.data = "TIMEOUT";
                                        SendReportToServer(e);
                                        gHsTrigerCmdList.Remove(id);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            catch { }
            gExpireSemaphore.Release();
        }
        #endregion
        #region Other
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            String errMsg = string.Format("Fatal Error : {0} \n {1}\n", ex.Message, ex.StackTrace);
            WriteLogEvent(errMsg);
        }
        private void objPortMon_OnDisconnectFromClient(NetDisConnectedEvent e)
        {
            RemoveTreeListViewNode(e.ep.ToString());
        }
        private void ViewerFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (objTcpClient != null)
            {
                objTcpClient.Disconnect();
            }
            Process.GetCurrentProcess().Kill();
        }
        private void ViewerFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (objTcpClient != null)
            {
                objTcpClient.Disconnect();
            }
            Process.GetCurrentProcess().Kill();
        }
        private void CloseSession()
        {
            if (remoteServerInterface != null)
            {
                remoteServerInterface.Dispose();
            }

            tmChkProcessHealth.Stop();
            objPortMon.Close();

            WriteLogEvent("Application is exited");
            Application.Exit();
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            CloseSession();
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ConfigFrm frm = new ConfigFrm();
            frm.objCFG = objPortMon.CFG;
        
            if (frm.ShowDialog() == DialogResult.OK)
            {
                createXml(frm.objCFG);
                if (MessageBox.Show("Restart System Now.", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConfigFrm frm = new ConfigFrm();
            frm.objCFG = objPortMon.CFG;

            if (frm.ShowDialog() == DialogResult.OK)
            {
                createXml(frm.objCFG);
            }

        }

        private bool createXml(CFG _cfg)
        {
            try
            {
                String path = "";
                String fullPath = this.GetType().Assembly.GetName().CodeBase.ToString().ToUpper();
                String fileName = this.GetType().Assembly.GetName().Name + ".EXE";
                fileName = fileName.ToUpper();
                int iFound = fullPath.IndexOf(fileName);
                if (iFound > -1)
                {
                    path = fullPath.Substring(0, iFound);
                }
                else
                {
                    path = fullPath;
                }

                XmlSerializer serializer = new XmlSerializer(typeof(CFG));
                Stream fs = new FileStream("PortMonitorCfg.xml", FileMode.Create);
                XmlWriter writer = new XmlTextWriter(fs, Encoding.Unicode);
                serializer.Serialize(writer, _cfg);
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message");
            }
            return false;
        }
        private void ViewerFrm_Shown(object sender, EventArgs e)
        {

        }
        #endregion
        #region RemoteServer
        private void tvNodeList_MouseMove(object sender, MouseEventArgs e)
        {
            tvNodeListMousePoint.X = e.X;
            tvNodeListMousePoint.Y = e.Y;
        }
        private DataTable CreateVehInfoTable()
        {
            DataTable tvTable = new DataTable();

            tvTable.Columns.Add("registration", typeof(String));
            tvTable.Columns.Add("ip_endpoint", typeof(String));

            foreach (TreeNode n in TvNodeList.Nodes)
            {
                REMOTEENDPPOINTINFO info = (REMOTEENDPPOINTINFO)n.Tag;
                tvTable.Rows.Add(new object[] { info.veh_reg.Trim(), info.remote_endpoint.ToString() });
            }

            return tvTable;
        }
        private void ReplyRemoteBack(REMOTECMDINFO info, string msg)
        {
            try
            {
                remoteServerInterface.SendCmd(info.socket, info.veh_id, info.cmd, msg);
                WriteLogEvent3(Convert.ToString(info.socket.RemoteEndPoint) + "  " + Convert.ToString(info.socket.LocalEndPoint));
            }
            catch { }
        }
        private void GetTotalVehs(REMOTECMDINFO info)
        {
            int total = 0;
            if (TvNodeList != null && TvNodeList.Nodes != null)
            {
                total = TvNodeList.Nodes.Count;
            }
            remoteServerInterface.SendCmd(info.socket, info.veh_id, info.cmd, total.ToString());
        }
        private void GetAllVehicles(REMOTECMDINFO info)
        {
            try
            {
                string vehs = "";
                if (TvNodeList != null && TvNodeList.Nodes != null)
                {
                    foreach (TreeNode node in TvNodeList.Nodes)
                    {
                        REMOTEENDPPOINTINFO nodeInfo = (REMOTEENDPPOINTINFO)node.Tag;
                        vehs += nodeInfo.veh_id.ToString() + "#";
                        Thread.Sleep(1);
                    }
                    if (vehs.Trim().Length > 1)
                    {
                        vehs = vehs.Substring(0, vehs.Length - 1);
                        remoteServerInterface.SendCmd(info.socket, info.veh_id, info.cmd, vehs);
                    }
                }
            }
            catch
            {
            }
        }
        private void RegisterTologMsg(Socket socket)
        {
            bool found = false;
            foreach (Socket s in regLogMsg)
            {
                if (socket == s)
                {
                    found = true;
                }
            }
            if (!found)
            {
                regLogMsg.Add(socket);
            }
        }
        private void SendRemoteMsg(string msg)
        {
            try
            {
                if (regLogMsg.Count > 0)
                {
                    foreach (Socket sck in regLogMsg)
                    {
                        if (sck.Connected)
                        {
                            msg = msg.Replace(',', '#');
                            remoteServerInterface.SendCmd(sck, 0, REMOTECMD.REGISTERTOLOG.ToString(), msg);
                        }
                        else
                        {
                            regLogMsg.Remove(sck);
                            SendRemoteMsg(msg);
                            break;
                        }
                    }
                }
            }
            catch
            {
            }
        }
        #endregion

        #region TreeView && RichTextBox
        private void OnMonitorsMsg(MonitorEvtArgs e)
        {
            if (e.msg != null && e.msg.Length > 0)
            {
                UpdateRichTxt(e.msg);
            }
        }
        private void objPortMon_OnMonitorsMsg2(MonitorEvtArgs e)
        {
            if (e.msg != null && e.msg.Length > 0)
            {
                if (e.msg.StartsWith("Report"))
                {
                    RemoteTcpInfo info = new RemoteTcpInfo();
                    info.cmd = REMOTECMD.VEHICLELOG.ToString();
                    info.data = e.msg;
                    info.veh_id = Convert.ToInt32((e.msg.Split('@'))[1]);
                    info.tcp = null;
                    EnqueueTrigger(info);
                }
                else if (e.msg[0] == 'W' || e.msg[0] == 'R')
                {
                    string[] tmp = e.msg.Split(' '); 
                    RemoteTcpInfo info = new RemoteTcpInfo();
                    info.cmd = REMOTECMD.VEHICLELOG.ToString();
                    info.data = e.msg;
                    info.veh_id = Convert.ToInt32(tmp[2].Split('\r','\n')[0]);
                    info.tcp = null;
                    EnqueueTrigger(info);
                }
                else
                {
                    UpdateRichTxt(e.msg);
                }
            }
        }
        private void UpdateRichTxt(String msg)
        {
            updateMsgQSemaphore.WaitOne();
            try
            {
                UpdateMsgQ.Enqueue(msg);
            }
            catch { }
            updateMsgQSemaphore.Release();
        }
        private void tmUpdateMsg_Tick(object sender, EventArgs e)
        {
            tmUpdateMsg.Stop();
            try
            {
                while (UpdateMsgQ.Count > 0)
                {
                    string msg = UpdateMsgQ.Dequeue();
                    ShowLogMsg(msg);
                    Thread.Sleep(5);
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("ViewFrm():error #5: " + ex.Message);
            }
            tmUpdateMsg.Start();
        }
        private void ShowLogMsg(String msg)
        {
            try
            {
                String txt = "";
                int eTag = msg.IndexOf(",EP:");
                if (eTag < 0)
                {
                    txt = msg.Trim();
                }
                else
                {
                    txt = msg.Substring(0, eTag);
                }

                if (txt.StartsWith("$"))
                {
                    richTextBox1.Text = txt + richTextBox1.Text;
                    SendRemoteMsg(txt);

                    int nRow = ((int)(richTextBox1.ClientSize.Height / richTextBox1.Font.GetHeight()));
                    if (richTextBox1.Lines.Length > nRow)
                    {
                        int iPos = 0;
                        int iIdx = -1;
                        for (int i = 0; i < nRow; i++)
                        {
                            if (iPos < richTextBox1.Text.Length)
                            {
                                iIdx = richTextBox1.Text.IndexOf('\n', iPos);
                                if (iIdx > -1)
                                {
                                    iPos = iIdx + 1;
                                }
                            }
                        }
                        if (iPos > 0)
                        {
                            richTextBox1.Text = richTextBox1.Text.Remove(iPos - 1);
                        }
                    }

                    ShowReturnMsg(msg.Trim());

                    tvNodeBusy.WaitOne();
                    UpdateTreeListView(msg.Trim());
                    tvNodeBusy.Release();

                    TimeDisPlayMonitor = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("ViewFrm():error #4.4: " + ex.Message);
            }
        }
        private void ShowReturnMsg(String msg)
        {
            String[] fldStr = msg.Split(new char[] { ',' });
            if (fldStr.Length >= 11)
            {
                if (fldStr[11].Substring(0, 1).ToUpper() == "R")
                {
                    string vid = fldStr[0].Substring(1, fldStr[0].Length - 1);
                    string txt = fldStr[12].Trim(new char[] { '~', '\0', '\n', '\r' });

                    int veh_id = Convert.ToInt32(vid);
                    if (gHsClientSendCmd.ContainsKey(veh_id))
                    {
                        ReplyRemoteBack((REMOTECMDINFO)gHsClientSendCmd[veh_id], txt);
                        gHsClientSendCmd.Remove(veh_id);
                    }
                    if (gHsUserSendCmd.ContainsKey(veh_id))
                    {
                        ShowResultsFrm frm = new ShowResultsFrm();
                        frm.lbVID.Text = vid;
                        frm.lbResult.Text = txt;

                        frm.Show();
                        gHsUserSendCmd.Remove(veh_id);
                    }
                }
            }
        }
        private void UpdateTreeListView(String msg)
        {
            try
            {
                if (msg.Length <= 50) return;

                String[] str = msg.Split(new char[] { ',' });
                if (str != null && str.Length > 1)
                {
                    str[str.Length - 1] = str[str.Length - 1].Trim();
                    if (str[str.Length - 1].Length > 1)
                    {
                        String[] epStr = str[str.Length - 1].Split(new char[] { ':' });
                        if (epStr.Length > 2)
                        {
                            str[0] = str[0].Substring(1, str[0].Length - 1);

                            REMOTEENDPPOINTINFO epTag = new REMOTEENDPPOINTINFO();
                            epTag.veh_id = Convert.ToInt32(str[0]);
                            epTag.veh_reg = str[0].Trim();
                            epTag.remote_endpoint = new IPEndPoint(IPAddress.Parse(epStr[1]), Convert.ToInt32(epStr[2]));
                            epTag.connected = true;

                            if (epTag.veh_id > 0)
                            {
                                int keyIndex = TvNodeList.Nodes.IndexOfKey(str[0]);
                                if (keyIndex < 0)
                                {
                                    TreeNode n = TvNodeList.Nodes.Add(str[0], str[0]);

                                    n.Tag = epTag;
                                    lbOnlineVehs.Update();

                                    TvNodeList.Update();
                                }
                                else
                                {
                                    TvNodeList.Nodes[keyIndex].Tag = epTag;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent2("ViewFrm():error #1: " + ex.Message);
            }
        }
        private void RemoveTreeListViewNode(String msg)
        {
            try
            {
                if (this.TvNodeList.InvokeRequired)
                {
                    SetTextCallback proc = new SetTextCallback(RemoveTreeListViewNode);
                    Invoke(proc, new object[] { msg });
                }
                else
                {
                    String[] ep = msg.Split(new char[] { ':' });
                    if (ep.Length > 1)
                    {
                        if (TvNodeList.Nodes.IndexOfKey(ep[0] + ":" + ep[1]) >= 0)
                        {
                            TreeNode n = TvNodeList.Nodes[TvNodeList.Nodes.IndexOfKey(ep[0] + ":" + ep[1])];
                            TvNodeList.Nodes.Remove(n);

                            lbOnlineVehs.Text = TvNodeList.Nodes.Count.ToString() + " " + "Vehicle(s)";
                            lbOnlineVehs.Update();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("ViewFrm():error #2: " + ex.Message);
            }
        }
        #endregion
        #region Write/ReadBox
        private void WriteBoxConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = TvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                String paramStr = "";

                if (!((REMOTEENDPPOINTINFO)curSelectedNode.Tag).connected)
                {
                    MessageBox.Show("The connection is disconnected !!!");
                    return;
                }

                IPEndPoint rmEP = ((REMOTEENDPPOINTINFO)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                WrDrvCfgFrm frm = new WrDrvCfgFrm();
                frm.VidTable = CreateVehInfoTable();
                if (frm.VidTable == null)
                {
                    MessageBox.Show("Cannot setup vehicle table", "Error!!!");
                    return;
                }
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }

                cmdMsgStr = frm.SelectedCmd;
                paramStr = frm.DataToCfg;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });

                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.ClientList)
                {
                    try
                    {
                        if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                            (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                        {
                            clnt = mngr;
                            break;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Write config. to device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        cmdMsgStr = cmdMsgStr + paramStr + "\n";
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Cannot connect to the remote host");
                }
                frm.VidTable.Dispose();
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!", ex.Message);
            }
        }
        private void ReadBoxConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = TvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";

                if (!((REMOTEENDPPOINTINFO)curSelectedNode.Tag).connected)
                {
                    MessageBox.Show("The connection is disconnected !!!");
                    return;
                }

                IPEndPoint rmEP = ((REMOTEENDPPOINTINFO)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                ReadDrvCfgFrm frm = new ReadDrvCfgFrm();
                frm.VidTable = CreateVehInfoTable();
                if (frm.VidTable == null)
                {
                    MessageBox.Show("Cannot setup vehicle table", "Error!!!");
                    return;
                }
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }
                cmdMsgStr = frm.SelectedCmd;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });
                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.ClientList)
                {
                    try
                    {
                        if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                            (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                        {
                            clnt = mngr;
                            break;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Read config. from device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr + "\n");
                        if (bSuccess || !gHsUserSendCmd.ContainsKey(Convert.ToInt32(frm.Vid)))
                        {
                            int key = Convert.ToInt32(frm.Vid);
                            if (!gHsUserSendCmd.ContainsKey(key))
                            {
                                gHsUserSendCmd.Add(key, 1);
                            }
                        }
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Cannot connect to the remote host");
                }

                frm.VidTable.Dispose();
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!", ex.Message);
            }
        }
        private void WriteCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = TvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                String paramStr = "";

                if (!((REMOTEENDPPOINTINFO)curSelectedNode.Tag).connected)
                {
                    MessageBox.Show("The connection is disconnected !!!");
                    return;
                }

                IPEndPoint rmEP = ((REMOTEENDPPOINTINFO)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                WrCardFrm frm = new WrCardFrm();
                frm.VidTable = CreateVehInfoTable();
                if (frm.VidTable == null)
                {
                    MessageBox.Show("Cannot setup vehicle table", "Error!!!");
                    return;
                }
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }

                // 1 "*>BW0"
                // 2 "**********ABC"
                // 3 "*>BW0**********ABC\n"
                cmdMsgStr = frm.SelectedCmd; // "*>BW0"
                paramStr = frm.DataToCfg; //"**********ABC"
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });

                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.ClientList)
                {
                    try
                    {
                        if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                            (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                        {
                            clnt = mngr;
                            break;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Write config. to device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        cmdMsgStr = cmdMsgStr + paramStr + "\n";
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr); //"*>BW0**********ABC\n"
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Cannot connect to the remote host");
                }

                frm.VidTable.Dispose();
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!", ex.Message);
            }
        }
        private void ReadCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = TvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";

                if (!((REMOTEENDPPOINTINFO)curSelectedNode.Tag).connected)
                {
                    MessageBox.Show("The connection is disconnected !!!");
                    return;
                }

                IPEndPoint rmEP = ((REMOTEENDPPOINTINFO)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                ReadCardFrm frm = new ReadCardFrm();
                frm.VidTable = CreateVehInfoTable();
                if (frm.VidTable == null)
                {
                    MessageBox.Show("Cannot setup vehicle table", "Error!!!");
                    return;
                }
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }
                cmdMsgStr = frm.SelectedCmd;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });
                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.ClientList)
                {
                    try
                    {
                        if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                            (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                        {
                            clnt = mngr;
                            break;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Read config. from device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr + "\n");
                        if (bSuccess || !gHsUserSendCmd.ContainsKey(Convert.ToInt32(frm.Vid)))
                        {
                            int key = Convert.ToInt32(frm.Vid);
                            if (!gHsUserSendCmd.ContainsKey(key))
                            {
                                gHsUserSendCmd.Add(key, 1);
                            }
                        }
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Cannot connect to the remote host");
                }

                frm.VidTable.Dispose();
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!", ex.Message);
            }
        }
        private void WriteTemperatureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = TvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                String paramStr = "";

                if (!((REMOTEENDPPOINTINFO)curSelectedNode.Tag).connected)
                {
                    MessageBox.Show("The connection is disconnected !!!");
                    return;
                }

                IPEndPoint rmEP = ((REMOTEENDPPOINTINFO)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                WrTemperatureFrm frm = new WrTemperatureFrm();
                frm.VidTable = CreateVehInfoTable();
                if (frm.VidTable == null)
                {
                    MessageBox.Show("Cannot setup vehicle table", "Error!!!");
                    return;
                }
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }

                cmdMsgStr = frm.SelectedCmd;
                paramStr = frm.DataToCfg;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });

                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.ClientList)
                {
                    try
                    {
                        if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                            (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                        {
                            clnt = mngr;
                            break;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Write config. to device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        cmdMsgStr = cmdMsgStr + paramStr + "\n";
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Cannot connect to the remote host");
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!", ex.Message);
            }
        }
        private void ReadTemperatureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = TvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";

                if (!((REMOTEENDPPOINTINFO)curSelectedNode.Tag).connected)
                {
                    MessageBox.Show("The connection is disconnected !!!");
                    return;
                }

                IPEndPoint rmEP = ((REMOTEENDPPOINTINFO)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                ReadTemperatureFrm frm = new ReadTemperatureFrm();
                frm.VidTable = CreateVehInfoTable();
                if (frm.VidTable == null)
                {
                    MessageBox.Show("Cannot setup vehicle table", "Error!!!");
                    return;
                }
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }
                cmdMsgStr = frm.SelectedCmd;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });
                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.ClientList)
                {
                    try
                    {
                        if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                            (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                        {
                            clnt = mngr;
                            break;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Read config. from device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr + "\n");
                        if (bSuccess || !gHsUserSendCmd.ContainsKey(Convert.ToInt32(frm.Vid)))
                        {
                            int key = Convert.ToInt32(frm.Vid);
                            if (!gHsUserSendCmd.ContainsKey(key))
                            {
                                gHsUserSendCmd.Add(key, 1);
                            }
                        }
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Cannot connect to the remote host");
                }

                frm.VidTable.Dispose();
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!", ex.Message);
            }
        }
        #endregion
        private void WriteLogEvent(String msg)
        {
            /*
            wrLogBusy.WaitOne();
            try
            {
                msg = DateTime.Now.ToString() + " : " + msg;
                String fileName = "ViewFrmLogEvent.txt";
                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch { }
            wrLogBusy.Release();
            */
        }
        private void WriteLogEvent2(String msg)
        {
            try
            {
                msg = DateTime.Now.ToString() + " : " + msg;
                String fileName = DateTime.Now.ToString("yyyyMMdd_") + "MonitorLogEvent.txt";
                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch { }
        }
        private void WriteLogEvent3(String msg)
        {
            try
            {
                msg = DateTime.Now.ToString() + " : " + msg;
                String fileName = DateTime.Now.ToString("yyyyMMdd_") + "test.txt";
                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch { }
        }
        private void tmChkProcessHealth_Tick(object sender, EventArgs e)
        {
            System.Windows.Forms.Timer tm = (System.Windows.Forms.Timer)sender;
            tm.Stop();
            try
            {
                Int64 maxBytesLimit = 1024 * 1024 * 1024;
                Process p = Process.GetCurrentProcess();
                if ((int)p.WorkingSet64 > maxBytesLimit)
                {
                    p.Kill();
                }

                if (objTcpClient != null)
                {
                    toolStripStatusTrigger.Text =objPortMon.CFG.EnableTrigger + " : " + objTcpClient.gHsServerList.Count.ToString();
                    StatusLabelHashCommand.Text = gHsTrigerCmdList.Count.ToString();
                }
                else
                {
                    toolStripStatusTrigger.Text = objPortMon.CFG.EnableTrigger + " : -";
                    StatusLabelHashCommand.Text = "-";
                }
                if (this.objPortMon.tcp.ClientList.Count > 0)
                {
                    if (DateTime.Now.AddMinutes(-4) > TimeDisPlayMonitor && this.objPortMon.tcp.ClientList.Count > 10)
                    {
                        if (objTcpClient != null)
                        {
                            objTcpClient.Disconnect();
                        }
                        Process.GetCurrentProcess().Kill();
                    }
                    else if (DateTime.Now.AddMinutes(-7) > TimeDisPlayMonitor && this.objPortMon.tcp.ClientList.Count >= 5 && this.objPortMon.tcp.ClientList.Count <= 10)
                    {
                        if (objTcpClient != null)
                        {
                            objTcpClient.Disconnect();
                        }
                        Process.GetCurrentProcess().Kill();
                    }
                    else if (DateTime.Now.AddMinutes(-12) > TimeDisPlayMonitor)
                    {
                        if (objTcpClient != null)
                        {
                            objTcpClient.Disconnect();
                        }
                        Process.GetCurrentProcess().Kill();
                    }
                }
                lbOnlineVehs.Text = TvNodeList.Nodes.Count.ToString() + " " + "Vehicle" + " : " + this.objPortMon.tcp.ClientList.Count + " Online";
            }
            catch { }
            tm.Start();
        }
    }
}