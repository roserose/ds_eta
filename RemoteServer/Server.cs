using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Collections;
using System.Diagnostics;
using System.IO;

using ZebraGatewayInterface;
using ZebraRemoteInterface;
using System.Net;

namespace RemoteServer
{
    public partial class Server : Form
    {
        private System.Threading.Semaphore updateMsgQSemaphore = new System.Threading.Semaphore(1, 1);
        private System.Threading.Semaphore gExpireSemaphore = new System.Threading.Semaphore(1, 1);

        public enum HashType { Add, ContainsKey, Remove };
        public enum CommandType { FORCE, TRIGGER };
        public enum CommadLelve { TRIGGER_SEND, GATEWAY_SEND };
        public enum ExpireType { NONE, TriggerExpire, ZebraManagerExpire};
     
        public struct RemoteSourceInfo
        {
            public int id_rec;
            public int veh_id;
            public string cmd;
            public string data;
            public string command;
            public IPEndPoint Source;
            public IPEndPoint Destination;
            public CommandType cmd_type;
            public CommadLelve cmd_level;
            public ExpireType ExpireType;
            public int ExpireCount;
            public DateTime NextSendTime;
            public TransactionStatus Th;
        }
        private TCPServer objTcpGateway = null;
        private TCPServer objTcpZbManager = null;
        private SQL gSql = new SQL();
        private ArrayList arRegisterServerMsg = new ArrayList();
        private Queue gQlog = new Queue();
        private Queue gTriggerQ = new Queue();
        private Queue gBufferQ = new Queue();
        private Hashtable gHsConnecInfo = new Hashtable();
        private int gGatewayPort = -1;
        private int gZebraManagerPort = 0;
        private string gStringConectStatus = "";

        public Server()
        {
            InitializeComponent();

            if (LISTENING())
            {
                Process p = Process.GetCurrentProcess();
                p.Kill();
            }

            tmRepeat.Interval = 1000 * 30;
            tmSendReceive.Interval = 1000;
            tmShowLog.Interval = 1000;
            tmRepeat.Start();
        }
        private bool LISTENING()
        {
            bool is_error = false;
            try
            {
                this.rtStatus.Text = string.Empty;
             
                RemoteServer.Properties.Settings st = new RemoteServer.Properties.Settings();
                gGatewayPort = st.GatewayPort;
                gZebraManagerPort = st.ZebraManagerPort;

                objTcpGateway = new TCPServer(gGatewayPort);
                objTcpGateway.OnRemoteMsg += new TcpReceiveMsg(objTcpGateway_OnRemoteMsg);
                objTcpGateway.ServerStart();
                gStringConectStatus += "GateWay Port : " + gGatewayPort.ToString() + " . . Begin Connect ";

                objTcpZbManager = new TCPServer(gZebraManagerPort);
                objTcpZbManager.OnRemoteMsg += new TcpReceiveMsg(objTcpZbManager_OnRemoteMsg);
                objTcpZbManager.ServerStart();
                gStringConectStatus += "\nZebraManager Port : " + gZebraManagerPort.ToString() + " . . Begin Connect ";
                
            }
            catch
            {
                is_error = true;
            }
            return is_error;
        }
        #region remoteTcpZebraManager
        void objTcpZbManager_OnRemoteMsg(TcpClient tcp, int veh_id, string cmd, string data)
        {
            try
            {
                if (cmd.Trim().ToUpper() == REMOTECMD.VEHICLECOMMAND.ToString())
                {
                    //Recieve From Zebra Manager : Send to ZebraPortViewer
                    string dataTemp = data.Trim(new char[] { '\0','\r' });
                    string[] tmp = dataTemp.Split('#');
                    if (tmp.Length == 2)
                    {
                        RemoteSourceInfo remoteInfo = new RemoteSourceInfo();
                        remoteInfo.id_rec = -1;
                        remoteInfo.cmd_type = CommandType.FORCE;
                        remoteInfo.veh_id = veh_id;
                        remoteInfo.cmd = REMOTECMD.VEHICLECOMMAND.ToString();
                        remoteInfo.Destination = null;
                        remoteInfo.Source = (IPEndPoint)tcp.Client.RemoteEndPoint;
                        remoteInfo.data = tmp[0];
                        remoteInfo.ExpireType = ExpireType.ZebraManagerExpire;
                        remoteInfo.ExpireCount = Convert.ToInt32(tmp[1]);
                        remoteInfo.NextSendTime = DateTime.Now.AddMinutes(5);
                        remoteInfo.command = tmp[0];
                        recCommand(remoteInfo);
                    }
                }
            }
            catch
            {
            }
        }
        private void btManager_Click(object sender, EventArgs e)
        {
            try
            {
                //Recieve From Zebra Manager : Send to ZebraPortViewer
                RemoteSourceInfo remoteInfo = new RemoteSourceInfo();
                remoteInfo.id_rec = -1;
                remoteInfo.cmd_type = CommandType.FORCE;
                remoteInfo.veh_id = 888;
                remoteInfo.cmd = REMOTECMD.VEHICLECOMMAND.ToString();
                remoteInfo.Destination = null;
                remoteInfo.Source = null;
                remoteInfo.data = "*>R001010";
                remoteInfo.ExpireType = ExpireType.ZebraManagerExpire;
                remoteInfo.ExpireCount = Convert.ToInt32("3");
                remoteInfo.NextSendTime = DateTime.Now.AddMinutes(5);
                remoteInfo.command = "*>R001010";
                recCommand(remoteInfo);
            }
            catch
            { }
        }
        private bool SendCommandToZebraManager(int veh_id, string cmd, string data)
        {
            bool isSend = false;
            try
            {
                string msg = "VID" + veh_id.ToString() + "," + cmd + "," + data;
                objTcpZbManager.SendDataBroadCast(msg);
            }
            catch
            { }
            return isSend;
        }
        #endregion
        #region remoteTcpGateway
        private void objTcpGateway_OnRemoteMsg(TcpClient tcp, int veh_id, string cmd, string data)
        {
            try
            {
                if (cmd.Trim().ToUpper() == REMOTECMD.VEHICLELOG.ToString())
                {
                    //Recieve From ZebraPortViewer : Send to ZebraManager
                    RemoteSourceInfo remoteInfo = new RemoteSourceInfo();
                    if (gHsConnecInfo.ContainsKey(veh_id))
                    {
                        remoteInfo = (RemoteSourceInfo)gHsConnecInfo[veh_id];
                        remoteInfo.data = data.Trim(new char[] { '\0','\r' });
                    }
                    else
                    {
                        remoteInfo.cmd_type = CommandType.TRIGGER;
                        remoteInfo.veh_id = veh_id;
                        remoteInfo.cmd = cmd;
                        remoteInfo.Destination = null;
                        remoteInfo.Source = (IPEndPoint)tcp.Client.RemoteEndPoint;
                        remoteInfo.data = data.Trim(new char[] { '\0', '\r' });
                        remoteInfo.ExpireType = ExpireType.ZebraManagerExpire;
                        remoteInfo.ExpireCount = 0;
                        remoteInfo.NextSendTime = DateTime.Now.AddMinutes(5);
                        remoteInfo.command = cmd;
                  
                        MangerHashTriggerBuffer(ProcessState.ADD, remoteInfo);
                    }
                    recCommand(remoteInfo);
                    return;
                }
            }
            catch
            {
            }
        }
        #endregion

        private void tmSendReceive_Tick(object sender, EventArgs e)
        {
            tmSendReceive.Stop();
            try
            {
                if (gTriggerQ.Count > 0)
                {
                    RemoteSourceInfo info = (RemoteSourceInfo)gTriggerQ.Dequeue();
                    if (info.cmd.Trim().ToUpper() == REMOTECMD.VEHICLECOMMAND.ToString())
                    {
                        #region VEHICLECOMMAND

                        DataTable dt = gSql.GetVehInAddress(info.veh_id);
                        if (dt.Rows.Count > 0)
                        {
                            string zIP = (string)dt.Rows[0]["local_ip"];
                            int zPort = (int)dt.Rows[0]["local_port"];
                            info.Destination = new IPEndPoint(IPAddress.Parse(zIP), zPort);

                            if (SendCommandToGateway(info.Destination.ToString(), info.veh_id, info.cmd, info.data))
                            {
                                info.Th.StatusTrigger = STATUS.TRIGGER_OK;
                                info.data = STATUS.TRIGGER_OK.ToString();
                                info.cmd = REMOTECMD.VEHICLELOG.ToString();
                                info.cmd_level = CommadLelve.TRIGGER_SEND;
                            }
                            else
                            {
                                info.Th.StatusTrigger = STATUS.TRIGGER_ERROR;
                                info.data = STATUS.TRIGGER_ERROR.ToString();
                                info.cmd = REMOTECMD.VEHICLELOG.ToString();
                                info.cmd_level = CommadLelve.TRIGGER_SEND;
                            }

                            if (info.id_rec == -1)
                            {
                                string strHostName = System.Net.Dns.GetHostName();
                                IPEndPoint ipEntry = (IPEndPoint)info.Source;
                                IPAddress addr = ipEntry.Address;

                                info.id_rec = gSql.InsertLogCommand(info.veh_id, info.command, false, DateTime.Now, addr.ToString(), "ZM");
                            }
                            MangerHashTriggerBuffer(ProcessState.ADD, info);
                        }
                        #endregion
                    }
                    else if (info.cmd.Trim().ToUpper() == REMOTECMD.VEHICLETRIGGER.ToString())
                    {
                        #region VEHICLETRIGGER
                        DataTable dt = gSql.GetVehInAddress(info.veh_id);
                        if (dt.Rows.Count > 0)
                        {
                            string zIP = (string)dt.Rows[0]["local_ip"];
                            int zPort = (int)dt.Rows[0]["local_port"];
                            info.Destination = new IPEndPoint(IPAddress.Parse(zIP), zPort);

                            if (SendCommandToGateway(info.Destination.ToString(), info.veh_id, info.cmd, info.data))
                            {
                                info.Th.StatusTrigger = STATUS.TRIGGER_OK;
                                info.data = STATUS.TRIGGER_OK.ToString();
                                info.cmd = REMOTECMD.VEHICLELOG.ToString();
                                info.cmd_level = CommadLelve.TRIGGER_SEND;
                            }
                            else
                            {
                                info.Th.StatusTrigger = STATUS.TRIGGER_ERROR;
                                info.data = STATUS.TRIGGER_ERROR.ToString();
                                info.cmd = REMOTECMD.VEHICLELOG.ToString();
                                info.cmd_level = CommadLelve.TRIGGER_SEND;
                            }

                            if (info.id_rec == -1)
                            {
                                string strHostName = System.Net.Dns.GetHostName();
                                IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
                                IPAddress[] addr = ipEntry.AddressList;

                                info.id_rec = gSql.InsertLogCommand(info.veh_id, info.command, false, DateTime.Now, addr[0].ToString(), "TG");
                            }

                            MangerHashTriggerBuffer(ProcessState.ADD, info);
                        }
                        #endregion
                    }
                    else if (info.cmd.Trim().ToUpper() == REMOTECMD.VEHICLELOG.ToString())
                    {
                        string data = "Unknown Command : " + info.veh_id;
                        if (gHsConnecInfo.ContainsKey(info.veh_id))
                        {
                            RemoteSourceInfo infoHash = (RemoteSourceInfo)gHsConnecInfo[info.veh_id];
                            #region COMMANDTYPE.TRIGGER
                            if (info.cmd_type == CommandType.TRIGGER)
                            {
                                //Not Waiting . . Triger Send OK
                                //if (info.cmd_level == CommadLelve.TRIGGER_SEND && info.data == STATUS.TRIGGER_OK.ToString())
                                if (info.Th.StatusTrigger == STATUS.TRIGGER_OK && infoHash.cmd_level == CommadLelve.TRIGGER_SEND)
                                {
                                    data = "TG1 - " + info.data + " : " + info.veh_id.ToString() + " : " + info.command;
                                    info.cmd_level = CommadLelve.GATEWAY_SEND;
                                    info.Th.StatusGateWay = STATUS.GATEWAY_ERROR;
                                    info.data = STATUS.GATEWAY_ERROR.ToString();
                                    MangerHashTriggerBuffer(ProcessState.UPDATE, info);
                                }
                                else
                                {   //  GateWay Send OK
                                    if (infoHash.cmd_level == CommadLelve.GATEWAY_SEND)
                                    {   /* ࡵ���������*/
                                        if (info.data == STATUS.GATEWAY_OK.ToString())
                                        {
                                            data = "TG2 - " + infoHash.cmd_level.ToString() + " : " + info.veh_id.ToString() + " : " + info.data;
                                            infoHash.Th.StatusGateWay = STATUS.GATEWAY_OK;
                                            MangerHashTriggerBuffer(ProcessState.UPDATE, infoHash);
                                        }
                                        else if (info.data.StartsWith("Report"))
                                        {
                                            data = "TG3 - " + infoHash.cmd_level + " : " + info.veh_id + " : " + info.data;
                                            string[] report = info.data.Split('@');
                                            gSql.UpdateLogCommandReport(infoHash.id_rec, infoHash.veh_id, report[3]);

                                            if (infoHash.Th.StatusBox != STATUS.BOX_OK)
                                            {
                                                infoHash.Th.StatusReport = STATUS.REPORT_OK;
                                                MangerHashTriggerBuffer(ProcessState.UPDATE, infoHash);
                                            }
                                            else
                                            {
                                                MangerHashTriggerBuffer(ProcessState.DELETE, infoHash);
                                            }
                                        }
                                        #region /* ��ҹ������¹�����*/
                                        else if (info.data[0] == 'R' || info.data[0] == 'W')
                                        {
                                            data = "TG4 - " + infoHash.cmd_level + " : " + info.veh_id + " : " + info.data;
                                            gSql.UpdateLogCommand(infoHash.id_rec, infoHash.veh_id, true);

                                            if (info.data[0] == 'R')
                                            {
                                                if (infoHash.Th.StatusReport != STATUS.REPORT_OK)
                                                {
                                                    infoHash.Th.StatusBox = STATUS.BOX_OK;
                                                    MangerHashTriggerBuffer(ProcessState.UPDATE, infoHash);
                                                }
                                                else
                                                {
                                                    MangerHashTriggerBuffer(ProcessState.DELETE, infoHash);
                                                }
                                            }
                                            else if (info.data[0] == 'W')
                                            {
                                                MangerHashTriggerBuffer(ProcessState.DELETE, infoHash);
                                            }
                                        }
                                        #endregion
                                        else if (info.data == STATUS.GATEWAY_ERROR.ToString())/* ����������*/
                                        {
                                            data = "TG5 - " + infoHash.cmd_level.ToString() + " : " + info.veh_id.ToString() + " : " + info.data + " ReSend : " + info.ExpireCount;
                                        }
                                        else if (info.data == "TIMEOUT")
                                        {
                                            data = "TG6 - " + infoHash.cmd_level.ToString() + " : " + info.veh_id.ToString() + " : " + info.data;
                                        }
                                    }
                                    else  // �Դ���ࡵ�������
                                    {
                                        data = "TG7 - " + info.cmd_level + " : " + info.veh_id + " : " + info.data + " : " + info.Destination + " Send : " + info.ExpireCount + " NotConnect Gateway";
                                    }
                                }
                            }
                            #endregion
                            #region COMMANDTYPE.FORCE
                            else if (info.cmd_type == CommandType.FORCE)
                            {
                                //Not Waiting . . Triger Send OK
                                if (info.Th.StatusTrigger == STATUS.TRIGGER_OK && infoHash.cmd_level == CommadLelve.TRIGGER_SEND)
                                {
                                    SendCommandToZebraManager(info.veh_id, REMOTECMD.VEHICLELOG.ToString(), info.data);
                                    data = "ZM1 - " + info.data + " : " + info.veh_id.ToString() + " : " + info.command;
                                    info.cmd_level = CommadLelve.GATEWAY_SEND;
                                    info.Th.StatusGateWay = STATUS.GATEWAY_ERROR;
                                    info.data = STATUS.GATEWAY_ERROR.ToString();
                                    MangerHashTriggerBuffer(ProcessState.UPDATE, info);
                                }
                                else
                                {   //  GateWay Send OK
                                    if (infoHash.cmd_level == CommadLelve.GATEWAY_SEND)
                                    {   /* ࡵ���������*/
                                        if (info.data == STATUS.GATEWAY_OK.ToString())
                                        {
                                            SendCommandToZebraManager(info.veh_id, REMOTECMD.VEHICLELOG.ToString(), info.data);
                                            data = "ZM2 - " + infoHash.cmd_level.ToString() + " : " + info.veh_id.ToString() + " : " + info.data;
                                        } /* ��ҹ������¹�����*/
                                        else if (info.data.StartsWith("Report"))
                                        {
                                            data = "ZM3 - " + infoHash.cmd_level + " : " + info.veh_id + " : " + info.data;
                                            SendCommandToZebraManager(info.veh_id, REMOTECMD.VEHICLELOG.ToString(), info.data);

                                            string[] report = info.data.Split('@');
                                            gSql.UpdateLogCommandReport(infoHash.id_rec, infoHash.veh_id, report[3]);

                                            if (infoHash.Th.StatusBox != STATUS.BOX_OK)
                                            {
                                                infoHash.Th.StatusReport = STATUS.REPORT_OK;
                                                MangerHashTriggerBuffer(ProcessState.UPDATE, infoHash);
                                            }
                                            else
                                            {
                                                MangerHashTriggerBuffer(ProcessState.DELETE, infoHash);
                                            }
                                        }
                                        else if (info.data[0] == 'R' || info.data[0] == 'W')
                                        {
                                            data = "ZM4 - " + infoHash.cmd_level + " : " + info.veh_id + " : " + info.data;
                                            SendCommandToZebraManager(info.veh_id, REMOTECMD.VEHICLELOG.ToString(), info.data);
                                            gSql.UpdateLogCommand(infoHash.id_rec, infoHash.veh_id, true);

                                            if (info.data[0] == 'R')
                                            {
                                                if (infoHash.Th.StatusReport != STATUS.REPORT_OK)
                                                {
                                                    infoHash.Th.StatusBox = STATUS.BOX_OK;
                                                    MangerHashTriggerBuffer(ProcessState.UPDATE, infoHash);
                                                }
                                                else
                                                {
                                                    MangerHashTriggerBuffer(ProcessState.DELETE, infoHash);
                                                }
                                            }
                                            else if (info.data[0] == 'W')
                                            {
                                                MangerHashTriggerBuffer(ProcessState.DELETE, infoHash);
                                            }
                                        }
                                        else if (info.data == STATUS.GATEWAY_ERROR.ToString())/* ����������*/
                                        {
                                            data = "ZM5 - " + infoHash.cmd_level.ToString() + " : " + info.veh_id.ToString() + " : " + info.data + " CountDown Send : " + info.ExpireCount;
                                            SendCommandToZebraManager(info.veh_id, REMOTECMD.VEHICLELOG.ToString(), info.data);
                                        }
                                        else if (info.data == "TIMEOUT")
                                        {
                                            data = "ZM6 - " + infoHash.cmd_level.ToString() + " : " + info.veh_id.ToString() + " : " + info.data;
                                            SendCommandToZebraManager(info.veh_id, REMOTECMD.VEHICLELOG.ToString(), info.data);
                                        }
                                    }
                                    else  // �Դ���ࡵ�������
                                    {
                                        data = "ZM7 - " + info.cmd_level + " : " + info.veh_id + " : " + info.data + " : " + info.Destination + " CountDown Send : " + info.ExpireCount;
                                        SendCommandToZebraManager(info.veh_id, REMOTECMD.VEHICLELOG.ToString(), info.data);
                                    }
                                }
                            }
                            #endregion
                        }
                        gQlog.Enqueue(DateTime.Now.ToString("HH:mm:ss - ") + data);
                    }
                }
                if (objTcpGateway != null || objTcpZbManager != null)
                {
                    this.rtStatus.Text = gStringConectStatus +
                        "\nGateway Connect Total : " + objTcpGateway.gHsClientList.Count.ToString() +
                        "\nManager Connect Total : " + objTcpZbManager.gHsClientList.Count.ToString()+ 
                        "\nTotal Buffer : " + gHsConnecInfo.Count.ToString();
                    LabelTime.Text = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                }
            }
            catch
            { }
            tmSendReceive.Start();
        }
        private void MangerHashTriggerBuffer(ProcessState state, RemoteSourceInfo info)
        {
            gExpireSemaphore.WaitOne();
            try
            {
                switch (state)
                {
                    case ProcessState.ADD:
                        {
                            if (!gHsConnecInfo.ContainsKey(info.veh_id))
                            {
                                gHsConnecInfo.Add(info.veh_id, info);
                            }
                            else
                            {
                                gHsConnecInfo[info.veh_id] = info;
                            }
                            recCommand(info);
                        }
                        break;
                    case ProcessState.UPDATE:
                        {
                            if (gHsConnecInfo.ContainsKey(info.veh_id))
                            {
                                gHsConnecInfo[info.veh_id] = info;
                            }
                        }
                        break;
                    case ProcessState.DELETE:
                        {
                            if (gHsConnecInfo.ContainsKey(info.veh_id))
                            {
                                gHsConnecInfo.Remove(info.veh_id);
                            }
                        }
                        break;
                    case ProcessState.LOSTSIGNAL:
                        {
                            if (gHsConnecInfo.Count > 0)
                            {
                                ArrayList arRemove = new ArrayList();
                                foreach (DictionaryEntry entry in gHsConnecInfo)
                                {
                                    RemoteSourceInfo e = (RemoteSourceInfo)gHsConnecInfo[entry.Key];
                                    switch (e.ExpireType)
                                    {
                                        case ExpireType.TriggerExpire:
                                            {
                                                if (DateTime.Now > e.NextSendTime)
                                                {
                                                    if (e.ExpireCount > 1)
                                                    {
                                                        ///GEN
                                                        RemoteSourceInfo remoteInfo = (RemoteSourceInfo)e;
                                                        remoteInfo.cmd = REMOTECMD.VEHICLETRIGGER.ToString();
                                                        remoteInfo.data = e.command;
                                                        remoteInfo.id_rec = e.id_rec;
                                                        remoteInfo.ExpireCount -= 1;

                                                        if (remoteInfo.ExpireCount > 1) remoteInfo.NextSendTime = DateTime.Now.AddMinutes(5);
                                                        else remoteInfo.NextSendTime = DateTime.Now.AddSeconds(75);

                                                        recCommand(remoteInfo);
                                                    }
                                                    else
                                                    {
                                                        arRemove.Add(e.veh_id);
                                                    }
                                                }
                                            }
                                            break;
                                        case ExpireType.ZebraManagerExpire:
                                            {
                                                if (DateTime.Now > e.NextSendTime)
                                                {
                                                    if (e.ExpireCount > 1)
                                                    {
                                                        ///GEN
                                                        RemoteSourceInfo remoteInfo = (RemoteSourceInfo)e;
                                                        remoteInfo.cmd = REMOTECMD.VEHICLECOMMAND.ToString();
                                                        remoteInfo.data = e.command;
                                                        remoteInfo.id_rec = e.id_rec;
                                                        remoteInfo.ExpireCount -= 1;

                                                        if (remoteInfo.ExpireCount > 1) remoteInfo.NextSendTime = DateTime.Now.AddMinutes(5);
                                                        else remoteInfo.NextSendTime = DateTime.Now.AddSeconds(75);

                                                        SendCommandToZebraManager(remoteInfo.veh_id, REMOTECMD.VEHICLELOG.ToString(), "Resend:" + remoteInfo.ExpireCount);
                                                        recCommand(remoteInfo);
                                                    }
                                                    else
                                                    {
                                                        SendCommandToZebraManager(e.veh_id, REMOTECMD.VEHICLELOG.ToString(), "Expire");
                                                        arRemove.Add(e.veh_id);
                                                    }
                                                }
                                            }
                                            break;
                                    }
                                }
                                foreach (int id in arRemove)
                                {
                                    if (gHsConnecInfo.ContainsKey(id))
                                    {
                                        gHsConnecInfo.Remove(id);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            catch { }
            gExpireSemaphore.Release();
        }
        private void recCommand(object info)
        {
            updateMsgQSemaphore.WaitOne();
            try
            {
                gTriggerQ.Enqueue(info);
            }
            catch { }
            updateMsgQSemaphore.Release();
        }
        private bool SendCommandToGateway(string Key, int veh_id, string cmd, string data)
        {
            bool isSend = false;
            try
            {
                string msg = "VID" + veh_id.ToString() + "," + cmd + "," + data;
                isSend = objTcpGateway.SendData(Key.ToString(), msg);
            }
            catch
            { }
            return isSend;
        }
        private void CreateTrigerEvent(int veh_id,string cmd)
        {
            try
            {
                RemoteSourceInfo remoteInfo = new RemoteSourceInfo();
                remoteInfo.id_rec = -1;
                remoteInfo.cmd_type = CommandType.TRIGGER;
                remoteInfo.veh_id = veh_id;
                remoteInfo.cmd = REMOTECMD.VEHICLETRIGGER.ToString();
                remoteInfo.Destination = null;
                remoteInfo.Source = null;
                remoteInfo.data = cmd;
                remoteInfo.ExpireType = ExpireType.TriggerExpire;
                remoteInfo.ExpireCount = 3;
                remoteInfo.NextSendTime = DateTime.Now.AddMinutes(2);
                remoteInfo.command = cmd;
                recCommand(remoteInfo);
            }
            catch { }
        }
        private void btSend_Click(object sender, EventArgs e)
        {
            CreateTrigerEvent(Convert.ToInt32(tbVid.Text), "*>R001010");
        }
        private void btZBManager_Click(object sender, EventArgs e)
        {
            try
            { 
                /*TRIGGER_OK
                        TRIGGER_Error
                        GatewayOK
                        GatewayError
                        TIMEOUT
                        R OK 767
                        W OK 767
                 */
                string data = "R OK";
                objTcpZbManager.SendDataBroadCast("767," + REMOTECMD.VEHICLELOG.ToString() + ',' + data);
            }
            catch { }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void Server_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (objTcpGateway != null || objTcpZbManager != null)
                {
                    objTcpGateway.ApplicationExit();
                    objTcpZbManager.ApplicationExit();
                }
                Process p = Process.GetCurrentProcess();
                p.Kill();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        private void tmShowLog_Tick(object sender, EventArgs e)
        {
            tmShowLog.Stop();
            if (gQlog.Count > 0)
            {
                string st = (string)gQlog.Dequeue();
                UpdateLogMsg(st);
                WriteNetFile(DateTime.Now.ToString("HH:mm:ss") + " : " + st);
            }
            tmShowLog.Start();
        }
        private void UpdateLogMsg(string msg)
        {
            try
            {
                richTextBox1.Text = msg + "\n" + richTextBox1.Text;

                int nRow = ((int)(richTextBox1.ClientSize.Height / richTextBox1.Font.GetHeight()));
                if (richTextBox1.Lines.Length > nRow)
                {
                    int iPos = 0;
                    int iIdx = -1;
                    for (int i = 0; i < nRow; i++)
                    {
                        if (iPos < richTextBox1.Text.Length)
                        {
                            iIdx = richTextBox1.Text.IndexOf('\n', iPos);
                            if (iIdx > -1)
                            {
                                iPos = iIdx + 1;
                            }
                        }
                    }
                    if (iPos > 0)
                    {
                        richTextBox1.Text = richTextBox1.Text.Remove(iPos - 1);
                    }
                }
            }
            catch { }
        }
        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        private void WriteNetFile(String msg)
        {
            semaphor.WaitOne();
            try
            {
                String logPath = Directory.GetCurrentDirectory() + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                String fileName = logPath + "\\" + "DataLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch
            { }
            semaphor.Release();
        }
        private void tmRepeat_Tick(object sender, EventArgs e)
        {
            tmRepeat.Stop();
            try
            {
                MangerHashTriggerBuffer(ProcessState.LOSTSIGNAL, new RemoteSourceInfo());
            }
            catch
            { }
            tmRepeat.Start();
        }
    }
}
