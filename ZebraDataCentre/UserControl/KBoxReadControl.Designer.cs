namespace ZebraDataCentre
{
    partial class KBoxReadControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboCommandList = new System.Windows.Forms.ComboBox();
            this.comboVehList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboCommandList
            // 
            this.comboCommandList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCommandList.FormattingEnabled = true;
            this.comboCommandList.Location = new System.Drawing.Point(79, 42);
            this.comboCommandList.Name = "comboCommandList";
            this.comboCommandList.Size = new System.Drawing.Size(171, 21);
            this.comboCommandList.TabIndex = 0;
            // 
            // comboVehList
            // 
            this.comboVehList.FormattingEnabled = true;
            this.comboVehList.Location = new System.Drawing.Point(79, 12);
            this.comboVehList.Name = "comboVehList";
            this.comboVehList.Size = new System.Drawing.Size(171, 21);
            this.comboVehList.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Veh ID :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Command :";
            // 
            // KBoxReadControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboVehList);
            this.Controls.Add(this.comboCommandList);
            this.Name = "KBoxReadControl";
            this.Size = new System.Drawing.Size(277, 85);
            this.Load += new System.EventHandler(this.KBoxReadControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboCommandList;
        private System.Windows.Forms.ComboBox comboVehList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
