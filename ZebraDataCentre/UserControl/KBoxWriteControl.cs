using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace ZebraDataCentre
{
    public partial class KBoxWriteControl : UserControl
    {
        public event ExcampleHandler EventMsg;

        ArrayList arTempTextBox = new ArrayList();
        private TranslateCommand gTranCom = new TranslateCommand();
        private DataTable _dtReadCommand = new DataTable();
        public DataTable gDataTableCommand
        {
            set
            {
                _dtReadCommand = value;
            }
        }

        private DataTable _dtVeh_list = new DataTable();
        public DataTable gDatatableVehList
        {
            set
            {
                _dtVeh_list = value;
            }
        }

        private int _cmd_veh_id = -1;
        public int gVehID
        {
            get
            {
                return _cmd_veh_id;
            }
        }

        private string _cmdCommand = "";
        public string gCommand
        {
            get
            {
                return _cmdCommand;
            }
        }

        public KBoxWriteControl()
        {
            InitializeComponent();
            this.lbPara1.MouseClick += new MouseEventHandler(lb_MouseClick);
            this.lbPara2.MouseClick += new MouseEventHandler(lb_MouseClick);
            this.lbPara3.MouseClick += new MouseEventHandler(lb_MouseClick);
            this.lbPara4.MouseClick += new MouseEventHandler(lb_MouseClick);
            this.lbPara5.MouseClick += new MouseEventHandler(lb_MouseClick);

            tb1.KeyPress += new KeyPressEventHandler(tb_KeyPress);
            tb2.KeyPress += new KeyPressEventHandler(tb_KeyPress);
            tb3.KeyPress += new KeyPressEventHandler(tb_KeyPress);
            tb4.KeyPress += new KeyPressEventHandler(tb_KeyPress);
            tb5.KeyPress += new KeyPressEventHandler(tb_KeyPress);
        }

        void tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                TextBox tb = (TextBox)sender;
                if (e.KeyChar != 8)
                {
                    COMMANDSYNTAX info = (COMMANDSYNTAX)tb.Tag;
                    string output = "";
                    string msg = tb.Text + e.KeyChar.ToString();
                    if (!gTranCom.CheckSyntax(info.type.ToUpper(), info.condition, msg, out output))
                    {
                        e.Handled = true;
                    }
                }
            }
            catch { }
        }

        void lb_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                Label lb = (Label)sender;
                if (((COMMANDSYNTAX)lb.Tag).condition.Length == 0) { return; }
                toolTip1.UseFading = true;
                toolTip1.UseAnimation = true;
                toolTip1.IsBalloon = true;

                toolTip1.ShowAlways = true;

                toolTip1.AutoPopDelay = 5000;
                toolTip1.InitialDelay = 1000;
                toolTip1.ReshowDelay = 1000;

                toolTip1.SetToolTip(lb, ((COMMANDSYNTAX)lb.Tag).condition);
            }
            catch { }
        }

        private void KBoxWriteControl_Load(object sender, EventArgs e)
        {
            InitializeComponance();
            this.comboCommandList.SelectedIndexChanged += new EventHandler(comboCommandList_SelectedIndexChanged);
            this.comboVehList.SelectedIndexChanged += new EventHandler(comboVehList_SelectedIndexChanged);

            try
            {
                if (comboVehList.Items.Count > 0 && !int.TryParse(comboVehList.SelectedValue.ToString(), out _cmd_veh_id)) { }
                _cmdCommand = comboCommandList.SelectedValue.ToString();
            }
            catch { }

            lbPara1.Visible = lbPara2.Visible = lbPara3.Visible = lbPara4.Visible = lbPara5.Visible = false;
            tb1.Visible = tb2.Visible = tb3.Visible = tb4.Visible = tb5.Visible = false;

        }

        private void InitializeComponance()
        {
            try
            {
                if (_dtVeh_list.Rows.Count > 0)
                {
                    this.comboVehList.DataSource = _dtVeh_list;
                    this.comboVehList.DisplayMember = "registration";
                    this.comboVehList.ValueMember = "veh_id";
                }

                if (_dtReadCommand.Rows.Count > 0)
                {
                    this.comboCommandList.DataSource = _dtReadCommand;
                    this.comboCommandList.DisplayMember = "cmd_display";
                    this.comboCommandList.ValueMember = "cmd_message";
                    this.comboCommandList.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message");
            }
        }

        private void comboVehList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboVehList.Items.Count > 0 && !int.TryParse(comboVehList.SelectedValue.ToString(), out _cmd_veh_id))
                { }
            }
            catch
            { }
        }

        private void comboCommandList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboCommandList.Focus())
                {
                    
                    arTempTextBox = new ArrayList();
                    if (comboCommandList.SelectedValue.ToString().Length > 0)
                    {
                         _cmdCommand = comboCommandList.SelectedValue.ToString();
                    }
                    DataRow[] dr = _dtReadCommand.Select(string.Format(@"cmd_display = '{0}'", comboCommandList.Text));
                   // DataRow[] dr = _dtReadCommand.Select(string.Format(@"cmd_message = '{0}'", _cmdCommand));
                    if (dr.Length > 0)
                    {
                        EnnableTextBox((ArrayList)((object)dr[0]["obj"]));
                        DataRowView data = (DataRowView)comboCommandList.SelectedItem;
                        OnMessasge(data.Row[2].ToString());
                    }
                }
            }
            catch
            { }
        }

        void OnMessasge(string msg)
        {
            if (EventMsg != null)
            {
                EventMsg(msg);
            }
        }
        private void EnnableTextBox(ArrayList ar)
        {
            try
            {
                tb1.Text = tb2.Text = tb3.Text = tb4.Text = tb5.Text = string.Empty;
                lbPara1.Visible = lbPara2.Visible = lbPara3.Visible = lbPara4.Visible = lbPara5.Visible = false;
                tb1.Visible = tb2.Visible = tb3.Visible = tb4.Visible = tb5.Visible = false;
                string tag = " : ";

                for (int i = 1; i < ar.Count; i++)
                {
                    if (i == 1)
                    {
                        tb1.Visible = lbPara1.Visible = true;
                        tb1.Tag = lbPara1.Tag = (COMMANDSYNTAX)ar[i];
                        lbPara1.Text = ((COMMANDSYNTAX)ar[i]).cmd_name + tag;
                        arTempTextBox.Add(tb1);
                    }
                    else if (i == 2)
                    {
                        tb2.Visible = lbPara2.Visible = true;
                        tb2.Tag = lbPara2.Tag = (COMMANDSYNTAX)ar[i];
                        lbPara2.Text = ((COMMANDSYNTAX)ar[i]).cmd_name + tag;
                        arTempTextBox.Add(tb2);
                    }
                    else if (i == 3)
                    {
                        tb3.Visible = lbPara3.Visible = true;
                        tb3.Tag = lbPara3.Tag = (COMMANDSYNTAX)ar[i];
                        lbPara3.Text = ((COMMANDSYNTAX)ar[i]).cmd_name + tag;
                        arTempTextBox.Add(tb3);
                    }
                    else if (i == 4)
                    {
                        tb4.Visible = lbPara4.Visible = true;
                        tb4.Tag = lbPara4.Tag = (COMMANDSYNTAX)ar[i];
                        lbPara4.Text = ((COMMANDSYNTAX)ar[i]).cmd_name + tag;
                        arTempTextBox.Add(tb4);
                    }
                    else if (i == 5)
                    {
                        tb5.Visible = lbPara5.Visible = true;
                        tb5.Tag = lbPara5.Tag = (COMMANDSYNTAX)ar[i];
                        lbPara5.Text = ((COMMANDSYNTAX)ar[i]).cmd_name + tag;
                        arTempTextBox.Add(tb5);
                    }

                }
            }
            catch
            { }
        }

        public void GetCommand()
        {
            if (arTempTextBox.Count > 0)
            {
                for (int i = 0; i < arTempTextBox.Count; i++)
                {
                    if (((TextBox)arTempTextBox[i]).Text.Length > 0)
                        _cmdCommand += "," + ((TextBox)arTempTextBox[i]).Text;
                }
            }
            else
            {
                _cmdCommand = string.Empty;
            }
        }

    }
}
