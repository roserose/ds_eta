namespace ZebraDataCentre
{
    partial class TimeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.Box3 = new System.Windows.Forms.TextBox();
            this.Box2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Box1 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Box3);
            this.panel1.Controls.Add(this.Box2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Box1);
            this.panel1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(89, 18);
            this.panel1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(8, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = ".";
            // 
            // Box3
            // 
            this.Box3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Box3.Location = new System.Drawing.Point(64, 2);
            this.Box3.MaxLength = 3;
            this.Box3.Name = "Box3";
            this.Box3.Size = new System.Drawing.Size(20, 13);
            this.Box3.TabIndex = 3;
            this.Box3.TabStop = false;
            this.Box3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Box2
            // 
            this.Box2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Box2.Location = new System.Drawing.Point(32, 2);
            this.Box2.MaxLength = 3;
            this.Box2.Name = "Box2";
            this.Box2.Size = new System.Drawing.Size(20, 13);
            this.Box2.TabIndex = 2;
            this.Box2.TabStop = false;
            this.Box2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(56, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(8, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = ".";
            // 
            // Box1
            // 
            this.Box1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Box1.Location = new System.Drawing.Point(4, 2);
            this.Box1.MaxLength = 3;
            this.Box1.Name = "Box1";
            this.Box1.Size = new System.Drawing.Size(20, 13);
            this.Box1.TabIndex = 1;
            this.Box1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TimeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "TimeControl";
            this.Size = new System.Drawing.Size(92, 19);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Box3;
        private System.Windows.Forms.TextBox Box2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Box1;

    }
}
