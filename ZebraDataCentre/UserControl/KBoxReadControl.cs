using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ZebraDataCentre
{
    public delegate void ExcampleHandler(string msg);
    public partial class KBoxReadControl : UserControl
    {
        public event ExcampleHandler EventMsg;

        private DataTable _dtReadCommand = new DataTable();
        public DataTable gDataTableCommand
        {
            set
            {
                _dtReadCommand = value;
            }
        }

        private DataTable _dtVeh_list = new DataTable();
        public DataTable gDataTableVehList
        {
            set 
            {
                _dtVeh_list = value; 
            }
        }

        private int _cmd_veh_id = -1;
        public int gVehID
        {
            get
            {
                return _cmd_veh_id;
            }
        }

        private string _cmdCommand = "";
        public string gCommand
        {
            get
            {
                return _cmdCommand;
            }
        }

        public KBoxReadControl()
        {
            InitializeComponent();
        }

        private void InitializeComponance()
        {
            try
            {
                if (_dtVeh_list.Rows.Count > 0)
                {
                    this.comboVehList.DataSource = _dtVeh_list;
                    this.comboVehList.DisplayMember = "registration";
                    this.comboVehList.ValueMember = "veh_id";
                }

                if (_dtReadCommand.Rows.Count > 0)
                {
                    this.comboCommandList.DataSource = _dtReadCommand;
                    this.comboCommandList.DisplayMember = "cmd_display";
                    this.comboCommandList.ValueMember = "cmd_message";
                    this.comboCommandList.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message");
            }
        }

        private void KBoxReadControl_Load(object sender, EventArgs e)
        {
            InitializeComponance();
            this.comboCommandList.SelectedIndexChanged += new EventHandler(comboCommandList_SelectedIndexChanged);
            this.comboVehList.SelectedIndexChanged += new EventHandler(comboVehList_SelectedIndexChanged);
            comboVehList.TextChanged += new EventHandler(comboVehList_TextChanged);
            try
            {
                if (comboVehList.Items.Count > 0 && !int.TryParse(comboVehList.SelectedValue.ToString(), out _cmd_veh_id)) { }
                _cmdCommand = comboCommandList.SelectedValue.ToString();
            }
            catch { }
        }

        void comboVehList_TextChanged(object sender, EventArgs e)
        {
            if (!int.TryParse(comboVehList.Text, out _cmd_veh_id))
            {
                _cmd_veh_id = -1;
            }
        }

        void comboVehList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboVehList.Items.Count > 0 && !int.TryParse(comboVehList.SelectedValue.ToString(), out _cmd_veh_id))
            { }
        }

        void comboCommandList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboCommandList.SelectedValue.ToString().Length > 0)
            {
                _cmdCommand = comboCommandList.SelectedValue.ToString();
                DataRowView data = (DataRowView)comboCommandList.SelectedItem;
                OnMessasge(data.Row[2].ToString());
            }
        }
        void OnMessasge(string msg)
        {
            if (EventMsg != null)
            {
                EventMsg(msg);
            }
        }
    }
}
