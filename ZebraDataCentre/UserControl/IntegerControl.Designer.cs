namespace ZebraDataCentre
{
    partial class IntegerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mkTb = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // mkTb
            // 
            this.mkTb.HidePromptOnLeave = true;
            this.mkTb.Location = new System.Drawing.Point(1, 0);
            this.mkTb.Mask = "00000";
            this.mkTb.Name = "mkTb";
            this.mkTb.PromptChar = ' ';
            this.mkTb.Size = new System.Drawing.Size(107, 20);
            this.mkTb.TabIndex = 0;
            this.mkTb.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.mkTb_MaskInputRejected);
            this.mkTb.TextChanged += new System.EventHandler(this.mkTb_TextChanged);
            // 
            // IntegerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mkTb);
            this.Name = "IntegerControl";
            this.Size = new System.Drawing.Size(108, 21);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.MaskedTextBox mkTb;
    }
}
