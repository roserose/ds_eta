using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraDataCentre
{
    public partial class ReadTemperatureFrm : Form
    {
        private DataTable vidTable = new DataTable();
        public DataTable VidTable
        {
            get { return vidTable; }
            set { vidTable = value; }
        }

        private DataTable cmdTable = new DataTable();
        public ReadTemperatureFrm()
        {
            InitializeComponent();            
        }

        private String selectedEP = "?";
        public String SelectedEP
        {
            get { return selectedEP; }
            set { selectedEP = value; }
        }

        private String selectedCmd = "?";
        public String SelectedCmd
        {
            get { return selectedCmd; }
            set { selectedCmd = value; }
        }

        private String vid = "?";
        public String Vid
        {
            get { return vid; }
            set { vid = value; }
        }
        private String commandName = "?";
        public String CommmandName
        {
            get { return commandName; }
            set { commandName = value; }
        }

        private void IntObj()
        {
            InitCmdTable();

            cbVIDList.DataSource    =   vidTable ;
            cbVIDList.DisplayMember =   "registration" ;
            cbVIDList.ValueMember   =   "ip_endpoint" ;

            cbCmdList.DataSource    =   cmdTable ;
            cbCmdList.DisplayMember =   "cmd_display" ;
            cbCmdList.ValueMember   =   "cmd_message" ;
            cbCmdList.SelectedIndex =   0 ;
        }

        public string GetCmdDesc(string cmd)
        {
            string desc = "";
            if (!(cmdTable != null && cmdTable.Rows.Count > 0)) return desc;
            DataRow[] row = cmdTable.Select(string.Format("cmd_message='{0}'", cmd));
            if (row.Length > 0)
            {
                desc = (string)row[0]["cmd_display"];
            }

            return desc;
        }

        public void InitCmdTable()
        {
            cmdTable.Columns.Add("cmd_display", typeof(String));
            cmdTable.Columns.Add("cmd_message", typeof(String));

            cmdTable.Rows.Add(new object[] { "Reads Temperature Sensor #0", "*>BT0" });
            cmdTable.Rows.Add(new object[] { "Reads Temperature Sensor #1", "*>BT1" });
            cmdTable.Rows.Add(new object[] { "Reads Temperature Sensor #2", "*>BT2" });

            cmdTable.Rows.Add(new object[] { "Reads Temperature Limits & Loop Time #0", "*>R256264" });
            cmdTable.Rows.Add(new object[] { "Reads Temperature Limits & Loop Time #1", "*>R265273" });
            cmdTable.Rows.Add(new object[] { "Reads Temperature Limits & Loop Time #2", "*>R274282" });
        }

        private void ReadTemperatureFrm_Load(object sender, EventArgs e)
        {
            IntObj();
            DataRow[] row = vidTable.Select(String.Format("ip_endpoint = '{0}'",SelectedEP));
            if (row.Length > 0)
            {
                cbVIDList.Text = row[0]["registration"].ToString();
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (cbVIDList.SelectedValue != null)
            {
                SelectedEP = cbVIDList.SelectedValue.ToString();
            }

            if (cbCmdList.SelectedValue != null)
            {
                SelectedCmd = cbCmdList.SelectedValue.ToString();
                CommmandName = cbCmdList.Text;
            }

            if (cbVIDList.Text != null)
            {
                Vid = cbVIDList.Text.ToString().Trim();
            }
        }
    }
}