using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace ZebraDataCentre
{
    public partial class WrTemperatureFrm : Form
    {
        private DataTable vidTable = new DataTable();
        public DataTable VidTable
        {
            get { return vidTable; }
            set { vidTable = value; }
        }

        private DataTable cmdTable = new DataTable();

        private String selectedEP = "?";
        public String SelectedEP
        {
            get { return selectedEP; }
            set { selectedEP = value; }
        }

        private String selectedCmd = "?";
        public String SelectedCmd
        {
            get { return selectedCmd; }
            set { selectedCmd = value; }
        }

        private String dataToCfg = "";
        public String DataToCfg
        {
            get { return dataToCfg; }
            set { dataToCfg = value; }
        }

        private String vid = "?";
        public String Vid
        {
            get { return vid; }
            set { vid = value; }
        }
        private String commandName = "?";
        public String CommmandName
        {
            get { return commandName; }
            set { commandName = value; }
        }
        private void IntObj()
        {
            InitCmdTable();

            cbVIDList.DataSource = vidTable;
            cbVIDList.DisplayMember = "registration";
            cbVIDList.ValueMember = "ip_endpoint";

            cbCmdList.DataSource = cmdTable;
            cbCmdList.DisplayMember = "cmd_display";
            cbCmdList.ValueMember = "cmd_message";
            cbCmdList.SelectedIndex = 0;
        }
        public void InitCmdTable()
        {
            cmdTable.Columns.Add("cmd_display", typeof(String));
            cmdTable.Columns.Add("cmd_message", typeof(String));

            cmdTable.Rows.Add(new object[] { "Write Temperature Sensor #0", "*>W256264" });
            cmdTable.Rows.Add(new object[] { "Write Temperature Sensor #1", "*>W265273" });
            cmdTable.Rows.Add(new object[] { "Write Temperature Sensor #2", "*>W274282" });
        }

        public string GetCmdDesc(string cmd)
        {
            string desc = "";
            if (!(cmdTable != null && cmdTable.Rows.Count > 0)) return desc;
            DataRow[] row = cmdTable.Select(string.Format("cmd_message='{0}'", cmd));
            if (row.Length > 0)
            {
                desc = (string)row[0]["cmd_display"];
            }

            return desc;
        }

        public WrTemperatureFrm()
        {
            InitializeComponent();
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            if (cbVIDList.SelectedValue != null)
            {
                SelectedEP = cbVIDList.SelectedValue.ToString();
            }

            if (cbCmdList.SelectedValue != null)
            {
                SelectedCmd = cbCmdList.SelectedValue.ToString();
                CommmandName = cbCmdList.Text;
            }

            if (cbVIDList.Text != null)
            {
                Vid = cbVIDList.Text.ToString().Trim();
            }

            DataToCfg = tbCfgData.Text.Trim();
            DialogResult = DialogResult.OK;
        }

        private void WrTemperatureFrm_Load(object sender, EventArgs e)
        {
            IntObj();
            DataRow[] row = vidTable.Select(String.Format("ip_endpoint = '{0}'", SelectedEP));
            if (row.Length > 0)
            {
                cbVIDList.Text = row[0]["registration"].ToString();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void tbCfgData_Validating(object sender, CancelEventArgs e)
        {
            String data = tbCfgData.Text.Trim();
            e.Cancel = false;

            switch (cbCmdList.SelectedIndex)
            {
                case 0:
                case 1:
                case 2:
                    if (data.Length > 10)
                    {
                        MessageBox.Show("Data should not over 10 characters.");
                        e.Cancel = true;
                    }
                    break;
            }
        }
    }
}