﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZebraDataCentre
{
    public enum BoxVersions
    {
        None = 0,
        GBox = 1,
        MBox = 2,
        KBox = 3,
        GPSTracker = 4,
        Meitrack = 5
    }
    public enum ServiceStatus
    {
        None,
        Running,
        Stop,
        Stop_TimeOut,
        Not_Install,

    }
    public enum Q_BROADCASTTYPE 
    { 
        MAX2 = 1, 
        BROADCAST = 2 ,
        Log = 3,
        DistanceOver = 4
    }
}
