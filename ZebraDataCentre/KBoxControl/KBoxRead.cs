using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraDataCentre
{
    public partial class KBoxRead : Form
    {
        delegate void SetTextCallback(string obj);

        private int _cmd_veh_id = -1;
        public int gVehID
        {
            get
            {
                return _cmd_veh_id;
            }
        }

        private string _cmdCommand = "";
        public string gCommand
        {
            get
            {
                return _cmdCommand;
            }
        }

        private DataTable _dtReadCommand = new DataTable();
        private DataTable _dtVeh_list = new DataTable();
  
        public KBoxRead(DataTable VehList, DataTable Command)
        {
            InitializeComponent();

            this.kBoxReadControl.EventMsg += new ZebraDataCentre.ExcampleHandler(kBoxReadControl_EventMsg);
            this.kBoxReadControl.gDataTableVehList = VehList;
            this.kBoxReadControl.gDataTableCommand = Command;
        }

        void kBoxReadControl_EventMsg(string msg)
        {
            UpdateExampleStatus(msg);
        }
        private void UpdateExampleStatus(string obj)
        {
            if (this.rtExample.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(UpdateExampleStatus);
                this.Invoke(d, new object[] { obj });
            }
            else
                rtExample.Text = obj.Replace('%', '\n');
        }

        private void KBoxRead_Load(object sender, EventArgs e)
        {
         
        }

        private void btKboxRead_Click(object sender, EventArgs e)
        {
            if (this.kBoxReadControl.gVehID > -1 && this.kBoxReadControl.gCommand.Length > 0)
            {
                _cmd_veh_id = this.kBoxReadControl.gVehID;
                _cmdCommand = this.kBoxReadControl.gCommand;
            }

            this.Dispose();
        }
    }
}