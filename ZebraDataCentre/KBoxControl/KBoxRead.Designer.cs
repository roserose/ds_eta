namespace ZebraDataCentre
{
    partial class KBoxRead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KBoxRead));
            this.btKboxRead = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.kBoxReadControl = new ZebraDataCentre.KBoxReadControl();
            this.rtExample = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btKboxRead
            // 
            this.btKboxRead.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btKboxRead.Location = new System.Drawing.Point(252, 135);
            this.btKboxRead.Name = "btKboxRead";
            this.btKboxRead.Size = new System.Drawing.Size(75, 23);
            this.btKboxRead.TabIndex = 1;
            this.btKboxRead.Text = "Read";
            this.btKboxRead.UseVisualStyleBackColor = true;
            this.btKboxRead.Click += new System.EventHandler(this.btKboxRead_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.kBoxReadControl);
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 127);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Read Command";
            // 
            // kBoxReadControl
            // 
            this.kBoxReadControl.Location = new System.Drawing.Point(20, 19);
            this.kBoxReadControl.Name = "kBoxReadControl";
            this.kBoxReadControl.Size = new System.Drawing.Size(261, 85);
            this.kBoxReadControl.TabIndex = 0;
            // 
            // rtExample
            // 
            this.rtExample.BackColor = System.Drawing.SystemColors.Control;
            this.rtExample.Location = new System.Drawing.Point(2, 130);
            this.rtExample.Name = "rtExample";
            this.rtExample.ReadOnly = true;
            this.rtExample.Size = new System.Drawing.Size(244, 80);
            this.rtExample.TabIndex = 6;
            this.rtExample.Text = "";
            // 
            // KBoxRead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(334, 212);
            this.Controls.Add(this.rtExample);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btKboxRead);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(350, 250);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(350, 250);
            this.Name = "KBoxRead";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "KBoxReadCommand";
            this.Load += new System.EventHandler(this.KBoxRead_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ZebraDataCentre.KBoxReadControl kBoxReadControl;
        private System.Windows.Forms.Button btKboxRead;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtExample;




    }
}