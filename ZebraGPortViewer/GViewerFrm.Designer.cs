namespace ZebraGPortViewer
{
    partial class GViewerFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GViewerFrm));
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusPortNumber1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusPortNumber2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusQName1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusQName2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lbOnlineVehs = new System.Windows.Forms.Label();
            this.tvNodeList = new System.Windows.Forms.TreeView();
            this.ctDeviceCfgMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toWriteConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toReadConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smartCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toWriteDataToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toReadDataFromToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.temperatureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.tmChkConnAlive = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ctDeviceCfgMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.richTextBox1.ForeColor = System.Drawing.Color.Lime;
            this.richTextBox1.Location = new System.Drawing.Point(0, 27);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(440, 282);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(643, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(108, 36);
            this.toolStripButton2.Text = "&Configuration";
            this.toolStripButton2.Visible = false;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(63, 36);
            this.toolStripButton1.Text = "&Quit";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusPortNumber1,
            this.toolStripStatusPortNumber2,
            this.toolStripStatusQName1,
            this.toolStripStatusQName2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 381);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(643, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusPortNumber1
            // 
            this.toolStripStatusPortNumber1.Name = "toolStripStatusPortNumber1";
            this.toolStripStatusPortNumber1.Size = new System.Drawing.Size(67, 17);
            this.toolStripStatusPortNumber1.Text = "Port Number";
            // 
            // toolStripStatusPortNumber2
            // 
            this.toolStripStatusPortNumber2.AutoSize = false;
            this.toolStripStatusPortNumber2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusPortNumber2.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.toolStripStatusPortNumber2.Name = "toolStripStatusPortNumber2";
            this.toolStripStatusPortNumber2.Size = new System.Drawing.Size(50, 17);
            this.toolStripStatusPortNumber2.Text = "?";
            // 
            // toolStripStatusQName1
            // 
            this.toolStripStatusQName1.Name = "toolStripStatusQName1";
            this.toolStripStatusQName1.Size = new System.Drawing.Size(77, 17);
            this.toolStripStatusQName1.Text = "System Queue";
            // 
            // toolStripStatusQName2
            // 
            this.toolStripStatusQName2.AutoSize = false;
            this.toolStripStatusQName2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusQName2.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.toolStripStatusQName2.Name = "toolStripStatusQName2";
            this.toolStripStatusQName2.Size = new System.Drawing.Size(100, 17);
            this.toolStripStatusQName2.Text = "?";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(2, 42);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(640, 338);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(632, 312);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Panel";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 2);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lbOnlineVehs);
            this.splitContainer1.Panel1.Controls.Add(this.tvNodeList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.richTextBox1);
            this.splitContainer1.Size = new System.Drawing.Size(631, 308);
            this.splitContainer1.SplitterDistance = 190;
            this.splitContainer1.TabIndex = 1;
            // 
            // lbOnlineVehs
            // 
            this.lbOnlineVehs.AutoSize = true;
            this.lbOnlineVehs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbOnlineVehs.Location = new System.Drawing.Point(7, 7);
            this.lbOnlineVehs.Name = "lbOnlineVehs";
            this.lbOnlineVehs.Size = new System.Drawing.Size(103, 13);
            this.lbOnlineVehs.TabIndex = 1;
            this.lbOnlineVehs.Text = "Online Vehicle(s)";
            // 
            // tvNodeList
            // 
            this.tvNodeList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvNodeList.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.tvNodeList.ContextMenuStrip = this.ctDeviceCfgMenu;
            this.tvNodeList.ForeColor = System.Drawing.Color.Lime;
            this.tvNodeList.Location = new System.Drawing.Point(0, 27);
            this.tvNodeList.Margin = new System.Windows.Forms.Padding(0);
            this.tvNodeList.Name = "tvNodeList";
            this.tvNodeList.Size = new System.Drawing.Size(188, 281);
            this.tvNodeList.TabIndex = 0;
            this.tvNodeList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tvNodeList_MouseMove);
            // 
            // ctDeviceCfgMenu
            // 
            this.ctDeviceCfgMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gPSToolStripMenuItem,
            this.smartCardToolStripMenuItem,
            this.temperatureToolStripMenuItem});
            this.ctDeviceCfgMenu.Name = "ctDeviceCfgMenu";
            this.ctDeviceCfgMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ctDeviceCfgMenu.Size = new System.Drawing.Size(137, 70);
            // 
            // gPSToolStripMenuItem
            // 
            this.gPSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toWriteConfigToolStripMenuItem,
            this.toReadConfigToolStripMenuItem});
            this.gPSToolStripMenuItem.Name = "gPSToolStripMenuItem";
            this.gPSToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.gPSToolStripMenuItem.Text = "&GPS Box";
            // 
            // toWriteConfigToolStripMenuItem
            // 
            this.toWriteConfigToolStripMenuItem.Name = "toWriteConfigToolStripMenuItem";
            this.toWriteConfigToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.toWriteConfigToolStripMenuItem.Text = "&Write Config.";
            this.toWriteConfigToolStripMenuItem.Click += new System.EventHandler(this.WriteBoxConfigToolStripMenuItem_Click);
            // 
            // toReadConfigToolStripMenuItem
            // 
            this.toReadConfigToolStripMenuItem.Name = "toReadConfigToolStripMenuItem";
            this.toReadConfigToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.toReadConfigToolStripMenuItem.Text = "&Read Config.";
            this.toReadConfigToolStripMenuItem.Click += new System.EventHandler(this.ReadBoxConfigToolStripMenuItem_Click);
            // 
            // smartCardToolStripMenuItem
            // 
            this.smartCardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toWriteDataToToolStripMenuItem,
            this.toReadDataFromToolStripMenuItem});
            this.smartCardToolStripMenuItem.Name = "smartCardToolStripMenuItem";
            this.smartCardToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.smartCardToolStripMenuItem.Text = "Smart &Card";
            // 
            // toWriteDataToToolStripMenuItem
            // 
            this.toWriteDataToToolStripMenuItem.Name = "toWriteDataToToolStripMenuItem";
            this.toWriteDataToToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.toWriteDataToToolStripMenuItem.Text = "&Write Config";
            this.toWriteDataToToolStripMenuItem.Click += new System.EventHandler(this.WriteCardToolStripMenuItem_Click);
            // 
            // toReadDataFromToolStripMenuItem
            // 
            this.toReadDataFromToolStripMenuItem.Name = "toReadDataFromToolStripMenuItem";
            this.toReadDataFromToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.toReadDataFromToolStripMenuItem.Text = "&Read Data";
            this.toReadDataFromToolStripMenuItem.Click += new System.EventHandler(this.ReadCardToolStripMenuItem_Click);
            // 
            // temperatureToolStripMenuItem
            // 
            this.temperatureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeConfigToolStripMenuItem,
            this.readDataToolStripMenuItem});
            this.temperatureToolStripMenuItem.Name = "temperatureToolStripMenuItem";
            this.temperatureToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.temperatureToolStripMenuItem.Text = "Temperature";
            // 
            // writeConfigToolStripMenuItem
            // 
            this.writeConfigToolStripMenuItem.Name = "writeConfigToolStripMenuItem";
            this.writeConfigToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.writeConfigToolStripMenuItem.Text = "&Write Config.";
            this.writeConfigToolStripMenuItem.Click += new System.EventHandler(this.WriteTemperatureToolStripMenuItem_Click);
            // 
            // readDataToolStripMenuItem
            // 
            this.readDataToolStripMenuItem.Name = "readDataToolStripMenuItem";
            this.readDataToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.readDataToolStripMenuItem.Text = "&Read Data";
            this.readDataToolStripMenuItem.Click += new System.EventHandler(this.ReadTemperatureToolStripMenuItem_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(9, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Latest Message";
            // 
            // tmChkConnAlive
            // 
            this.tmChkConnAlive.Interval = 5000;
            this.tmChkConnAlive.Tick += new System.EventHandler(this.tmChkConnAlive_Tick);
            // 
            // ViewerFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 403);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewerFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zebra GPort Monitor V1.20(Build 200908110824)";
            this.Shown += new System.EventHandler(this.ViewerFrm_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ViewerFrm_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.ctDeviceCfgMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusPortNumber1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tvNodeList;
        private System.Windows.Forms.Label lbOnlineVehs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tmChkConnAlive;
        private System.Windows.Forms.ContextMenuStrip ctDeviceCfgMenu;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusPortNumber2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusQName1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusQName2;
        private System.Windows.Forms.ToolStripMenuItem gPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toWriteConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toReadConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smartCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toWriteDataToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toReadDataFromToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem temperatureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readDataToolStripMenuItem;
    }
}

