using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraGPortViewer
{
    public partial class ReadDrvCfgFrm : Form
    {
        private DataTable vidTable = new DataTable();
        public DataTable VidTable
        {
            get { return vidTable; }
            set { vidTable = value; }
        }

        private DataTable cmdTable = new DataTable();
        public ReadDrvCfgFrm()
        {
            InitializeComponent();            
        }

        private String selectedEP = "?";
        public String SelectedEP
        {
            get { return selectedEP; }
            set { selectedEP = value; }
        }

        private String selectedCmd = "?";
        public String SelectedCmd
        {
            get { return selectedCmd; }
            set { selectedCmd = value; }
        }

        private String vid = "?";
        public String Vid
        {
            get { return vid; }
            set { vid = value; }
        }


        private void IntObj()
        {
            InitCmdTable();

            cbVIDList.DataSource    =   vidTable ;
            cbVIDList.DisplayMember =   "registration" ;
            cbVIDList.ValueMember   =   "ip_endpoint" ;

            cbCmdList.DataSource    =   cmdTable ;
            cbCmdList.DisplayMember =   "cmd_display" ;
            cbCmdList.ValueMember   =   "cmd_message" ;
            cbCmdList.SelectedIndex =   0 ;
        }

        private void InitCmdTable()
        {
            cmdTable.Columns.Add("cmd_display", typeof(String));
            cmdTable.Columns.Add("cmd_message", typeof(String));

            cmdTable.Rows.Add(new object[] { "Request Report Immediately",                                  "*>R000000"});
            cmdTable.Rows.Add(new object[] { "Reads FRAM Logger Header Character",                          "*>R000001" });
            cmdTable.Rows.Add(new object[] { "Reads Firmware Version",                                      "*>R011015" });
            cmdTable.Rows.Add(new object[] { "Reads Vehcle ID.",                                            "*>R001010" });
            cmdTable.Rows.Add(new object[] { "Reads The Limit for The Watchdog Timer",                      "*>R235239" });
            cmdTable.Rows.Add(new object[] { "Read The APN",                                                "*>R016047" });
            cmdTable.Rows.Add(new object[] { "Read The IP/URL Address And Port",                            "*>R048095" });
            cmdTable.Rows.Add(new object[] { "Reads The SMS Center Number",                                 "*>R144159" });
            cmdTable.Rows.Add(new object[] { "Reads The Speed Limit",                                       "*>R208210" });
            cmdTable.Rows.Add(new object[] { "Reads The Set SMS Loop Time",                                 "*>R211212" });
            cmdTable.Rows.Add(new object[] { "Reads The Set GPRS Loop Time",                                "*>R213215" });
            cmdTable.Rows.Add(new object[] { "Reads Output Status",                                         "*>R216217" });
            cmdTable.Rows.Add(new object[] { "Reads Back The Acceleration/Decelertion Limit Parameter",     "*>R228229" });
            cmdTable.Rows.Add(new object[] { "Reads The Preset Limit for The OFFLINE Report Time",          "*>R224225" });
            cmdTable.Rows.Add(new object[] { "Reads The Maximumn Allowed Swerve Angle or Suddenly Change",  "*>R226227" });
          //cmdTable.Rows.Add(new object[] { "Reads The Internal System Status for Factory Use Only",       "*>R218222" });
            cmdTable.Rows.Add(new object[] { "Reads Speed Warning toggle on Output 4",                      "*>R223224" });
            cmdTable.Rows.Add(new object[] { "Reads The Value of The Speed Timer Function",                 "*>R233234" });

            cmdTable.Rows.Add(new object[] { "Reads Back Logger Interval",                                  "*>R240242" });
            cmdTable.Rows.Add(new object[] { "Reads the Speed Warning Toggle on Output 4",                  "*>R223223" });

            cmdTable.Rows.Add(new object[] { "Reads Back Buffer Mode",                                      "*>R248249" });
            cmdTable.Rows.Add(new object[] { "Reads The 4 Digit Reconnection Delay Timer for GPRS Mode",    "*>R249252" });
            cmdTable.Rows.Add(new object[] { "Reads Back The System Status",                                "*>R253255" });
            cmdTable.Rows.Add(new object[] { "To Reboot Immediately",                                       "*>R999999" });
        }

        private void ReadDrvCfgFrm_Load(object sender, EventArgs e)
        {
            IntObj();
            DataRow[] row = vidTable.Select(String.Format("ip_endpoint = '{0}'",SelectedEP));
            if (row.Length > 0)
            {
                cbVIDList.Text = row[0]["registration"].ToString();
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (cbVIDList.SelectedValue != null)
            {
                SelectedEP = cbVIDList.SelectedValue.ToString();
            }

            if (cbCmdList.SelectedValue != null)
            {
                SelectedCmd = cbCmdList.SelectedValue.ToString();
            }

            if (cbVIDList.Text != null)
            {
                Vid = cbVIDList.Text.ToString().Trim();
            }
        }
    }
}