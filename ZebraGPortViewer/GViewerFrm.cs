using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Remoting.Channels;
using System.Collections;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Remoting.Channels.Tcp;
using System.Diagnostics;
using ZebraDataCentre;
using System.Xml;
using ZebraGPortViewer.Properties;
using System.Configuration;
using ZebraPortMonitor;
using System.Runtime.Remoting;
using ZebraNet;
using System.Net;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.IO;
using System.Globalization;

namespace ZebraGPortViewer
{
    public partial class GViewerFrm : Form
    {
        public struct RemoteEndpPointInfo
        {
            public int          veh_id;
            public string       veh_reg;
            public IPEndPoint   remote_endpoint;
        }

        private Boolean     bCanClose = false ;
        private Point       tvNodeListMousePoint = new Point() ;

        System.Threading.Semaphore tvNodeBusy = new System.Threading.Semaphore(1, 1);

        #region Properties

        private string _remote_ip = "127.0.0.1";
        public string Remote_ip
        {
            get { return _remote_ip; }
            set { _remote_ip = value; }
        }

        private int _remote_port = 6000;
        public int Remote_port
        {
            get { return _remote_port; }
            set { _remote_port = value; }
        }

        private int _local_port = 6005;
        public int Local_port
        {
            get { return _local_port; }
            set { _local_port = value; }
        }

        private string _msg_tag = "???";
        public string Msg_tag
        {
            get { return _msg_tag; }
            set { _msg_tag = value; }
        }

        MonitorsInterface remoteObj = null;

        #endregion

        ZebraPortMonitor.Monitors objPortMon = new Monitors(); 
        public GViewerFrm()
        {
            try
            {
                InitializeComponent();
                objPortMon.OnMonitorsMsg2           +=  new MonitorsMsgHandler2(objPortMon_OnMonitorsMsg2);
                objPortMon.OnDisconnectFromClient   +=  new ClientDisconectedHandler(objPortMon_OnDisconnectFromClient);

                this.Text = String.Format("Zebra GPort Monitor V{0} (Build {1})", "1.2", DateTime.Now.ToString("yyyyMMddHHmm", new CultureInfo( "en-US" )));
                InitCfg();                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text,  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InitCfg()
        {
            try
            {
                String path = Directory.GetCurrentDirectory();

                XmlSerializer s     =   new XmlSerializer(typeof(CFG));
                XmlTextReader xmlRd =   new XmlTextReader(path + "\\GPortMonitorCfg.xml");
                CFG cfg = (CFG)s.Deserialize(xmlRd);

                _remote_ip      =   cfg.Remote_ip ;
                _remote_port    =   cfg.Remote_port ;
                _local_port     =   cfg.Local_port ;
                _msg_tag        =   cfg.Msg_tag ;

                toolStripStatusPortNumber2.Text =   _local_port.ToString();
                toolStripStatusQName2.Text      =   cfg.ReadQ ;
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message, "Caption");
            }
        }
        private bool InitRemoteServiceObj()
        {
            try
            {
                //BinaryClientFormatterSinkProvider clientProvider2 = new BinaryClientFormatterSinkProvider();
                //BinaryServerFormatterSinkProvider serverProvider2 = new BinaryServerFormatterSinkProvider();
                //serverProvider2.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

                //IDictionary props2 = new Hashtable();
                //props2["port"] = 0;
                //string s = System.Guid.NewGuid().ToString();
                //props2["name"] = s;
                //props2["typeFilterLevel"] = TypeFilterLevel.Full;
                //TcpChannel chan2 = new TcpChannel(
                //    props2, clientProvider2, serverProvider2);

                //ChannelServices.RegisterChannel(chan2);
                //Type typeofRI = typeof(MonitorsInterface);

                //String connStr = String.Format("tcp://{0}:{1}/MonitorObj", Remote_ip, Remote_port);

                //remoteObj = (MonitorsInterface)Activator.GetObject(
                //    typeof(MonitorsInterface),
                //    connStr);

                //if (remoteObj.Alive() == "OK")
                //{
                //    return true;
                //}
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //toolStripStatusPortNumber1.Text = "Server disconnected !!!";
            }
            return false;
        }
        private void InitRemoteMonitorObj()
        {
            try
            {
                //TcpChannel chan = new TcpChannel();
                //RemotingConfiguration.RegisterWellKnownServiceType(
                //    Type.GetType("ZebraPortMonitor.Monitors,ZebraPortMonitor"),
                //    "MonitorObj",
                //    WellKnownObjectMode.Singleton);
            }
            catch (Exception ex)
            {
                //WriteLogEvent("Zebra Sevice Error #3" + ex.Message);
            }
        }
        private void OnMonitorsMsg(MonitorEvtArgs e)
        {
            if (e.msg != null && e.msg.Length > 0)
            {
                UpdateRichTxt(e.msg);
            }
        }
        private void tmChkConnAlive_Tick(object sender, EventArgs e)
        {
            Timer tm = (Timer)sender;
            tm.Stop();
            tvNodeBusy.WaitOne();

            try
            {
                for (int i = 0; i < this.tvNodeList.Nodes.Count; i++)
                {
                    RemoteEndpPointInfo info = (RemoteEndpPointInfo)this.tvNodeList.Nodes[i].Tag;
                    bool ret = objPortMon.ChkConnAlive(info.remote_endpoint);
                    if (!ret)
                    {
                        this.tvNodeList.Nodes.Remove(this.tvNodeList.Nodes[i]);
                        lbOnlineVehs.Text = this.tvNodeList.Nodes.Count.ToString() + " " + "Online Vehicle(s)";
                        lbOnlineVehs.Update();

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("ViewFrm():error #3: " + ex.Message);
            }

            tvNodeBusy.Release();
            tm.Start();
        }

        void objPortMon_OnDisconnectFromClient(NetDisConnectedEvent e)
        {
            RemoveTreeListViewNode(e.ep.ToString());
        }
        void objPortMon_OnMonitorsMsg2(MonitorEvtArgs e)
        {
            if (e.msg != null && e.msg.Length > 0)
            {
                UpdateRichTxt(e.msg);
            }
        }

        private void UpdateRichTxt(String msg)
        {
            if (this.richTextBox1.InvokeRequired)
            {
                if (!bCanClose)
                {
                    SetTextCallback proc = new SetTextCallback(UpdateRichTxt);
                    Invoke(proc, new object[] { msg });
                }
                else
                {
                    Application.Exit();
                }
            }
            else
            {
                String txt = "";
                int eTag = msg.IndexOf(",EP:");
                if (eTag < 0)
                {
                    txt = msg.Trim();
                }
                else
                {
                    txt = msg.Substring(0, eTag);
                }
                richTextBox1.Text = txt + "\n" +richTextBox1.Text;

                int nRow = ((int)(richTextBox1.ClientSize.Height / richTextBox1.Font.GetHeight()));    
                if (richTextBox1.Lines.Length > nRow)
                {
                    int iPos = 0;
                    int iIdx = -1;
                    for (int i = 0; i < nRow; i++)
                    {
                        if (iPos < richTextBox1.Text.Length)
                        {
                            iIdx = richTextBox1.Text.IndexOf('\n', iPos);
                            if (iIdx > -1)
                            {
                                iPos = iIdx + 1;
                            }
                        }
                    }
                    if (iPos > 0)
                    {
                        richTextBox1.Text = richTextBox1.Text.Remove(iPos-1);
                    }
                }

                ShowReturnMsg(msg.Trim());

                tvNodeBusy.WaitOne();
                UpdateTreeListView(msg.Trim());
                tvNodeBusy.Release();
            }
        }
        private void ShowReturnMsg(String msg)
        {
            String[] fldStr = msg.Split(new char[] { ',' });
            if (fldStr.Length >= 11)
            {
                if (fldStr[11].Substring(0,1).ToUpper() == "R")
                {
                    ShowResultsFrm frm = new ShowResultsFrm();
                    frm.lbVID.Text      =   fldStr[0].Substring(1, fldStr[0].Length - 1);
                    frm.lbResult.Text   =   fldStr[12].Trim(new char[] {'~', '\0', '\n', '\r'});
                    frm.Show();
                }
            }
        }
        private void UpdateTreeListView(String msg)
        {
            try
            {
                String[] str = msg.Split(new char[] { ',' });
                if (str != null && str.Length > 0)
                {
                    str[str.Length - 1] = str[str.Length - 1].Trim();
                    if (str[str.Length - 1].Length > 1)
                    {
                        String[] epStr = str[str.Length - 1].Split(new char[] { ':' });
                        if (epStr.Length > 2)
                        {
                            if (tvNodeList.Nodes.IndexOfKey(epStr[1] + ":" + epStr[2]) < 0)
                            {
                                str[0] = str[0].Substring(1, str[0].Length - 1);
                                foreach (TreeNode tn in tvNodeList.Nodes)
                                {
                                    if (tn.Text.Trim().ToUpper() == str[0].Trim().ToUpper())
                                    {
                                        tvNodeList.Nodes.Remove(tn);
                                        break;
                                    }
                                }

                                TreeNode n = tvNodeList.Nodes.Add(epStr[1] + ":" + epStr[2], str[0]);
                                n.Nodes.Add(epStr[1]);
                                n.Nodes.Add(epStr[2]);

                                RemoteEndpPointInfo epTag = new RemoteEndpPointInfo();
                                epTag.veh_id  = Convert.ToInt32(str[0]) ;
                                epTag.veh_reg = str[0].Trim() ;
                                epTag.remote_endpoint = new IPEndPoint(IPAddress.Parse(epStr[1]), Convert.ToInt32(epStr[2])) ;
                                n.Tag = epTag;

                                lbOnlineVehs.Text = tvNodeList.Nodes.Count.ToString() + " " + "Online Vehicle(s)";
                                lbOnlineVehs.Update();

                                tvNodeList.Update();
                            }
                            else
                            {
                                str[0] = str[0].Substring(1, str[0].Length - 1);
                                TreeNode n = tvNodeList.Nodes[(tvNodeList.Nodes.IndexOfKey(epStr[1] + ":" + epStr[2]))];
                                if (n.Text != str[0] && str[0].IndexOf("OK") == -1)
                                {                                    
                                    n.Text = str[0];

                                    RemoteEndpPointInfo info = (RemoteEndpPointInfo)n.Tag ;
                                    info.veh_id  = Convert.ToInt32( str[0] );
                                    info.veh_reg = str[0];

                                    n.Tag = info;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("ViewFrm():error #1: " + ex.Message);
            }
        }
        private void RemoveTreeListViewNode(String msg)
        {
            try
            {
                if (this.tvNodeList.InvokeRequired)
                {
                    SetTextCallback proc = new SetTextCallback(RemoveTreeListViewNode);
                    Invoke(proc, new object[] { msg });
                }
                else
                {
                    String[] ep = msg.Split(new char[] { ':' });
                    if (ep.Length > 1)
                    {
                        if (tvNodeList.Nodes.IndexOfKey(ep[0] + ":" + ep[1]) >= 0)
                        {
                            TreeNode n = tvNodeList.Nodes[tvNodeList.Nodes.IndexOfKey(ep[0] + ":" + ep[1])];
                            tvNodeList.Nodes.Remove(n);

                            lbOnlineVehs.Text = tvNodeList.Nodes.Count.ToString() + " " + "Online Vehicle(s)";
                            lbOnlineVehs.Update();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                WriteLogEvent("ViewFrm():error #2: " + ex.Message);
            }
        }

        private void WriteLogEvent(String msg)
        {
            if (!EventLog.SourceExists("Zebra"))
            {
                EventLog.CreateEventSource("Zebra", "ServerLog");
            }
            EventLog myLog = new EventLog();
            myLog.Source = "Zebra";
            myLog.WriteEntry(msg);
        }

        private void ViewerFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseSession();
        }

        private void CloseSession()
        {
            tmChkConnAlive.Stop();

            objPortMon.Close();
            Application.Exit();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            CloseSession();
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            CfgFrm frm = new CfgFrm();
            frm.Show();
        }
        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {            
        }
        private void ViewerFrm_Shown(object sender, EventArgs e)
        {
            tmChkConnAlive.Start();
        }
        private void writeMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        private void readMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        private void tvNodeList_MouseMove(object sender, MouseEventArgs e)
        {
            tvNodeListMousePoint.X = e.X;
            tvNodeListMousePoint.Y = e.Y;
        }

        private void WriteBoxConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = tvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                String paramStr = "";

                IPEndPoint rmEP = ((RemoteEndpPointInfo)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                WrDrvCfgFrm frm = new WrDrvCfgFrm();
                frm.VidTable = CreateVehInfoTable(); 
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }

                cmdMsgStr = frm.SelectedCmd;
                paramStr = frm.DataToCfg;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });

                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.clientList)
                {
                    if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                        (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                    {
                        clnt = mngr;
                        break;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Write config. to device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        cmdMsgStr = cmdMsgStr + paramStr + "\n";
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr);
                        return;
                    }
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!");
            }
        }
        private void ReadBoxConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = tvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                IPEndPoint rmEP = ((RemoteEndpPointInfo)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                ReadDrvCfgFrm frm = new ReadDrvCfgFrm();
                frm.VidTable = CreateVehInfoTable(); 
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }
                cmdMsgStr = frm.SelectedCmd;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });
                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.clientList)
                {
                    if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                        (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                    {
                        clnt = mngr;
                        break;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Read config. from device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr + "\n");
                        return;
                    }
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!");
            }
        }

        private void WriteCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = tvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                String paramStr = "";

                IPEndPoint rmEP = ((RemoteEndpPointInfo)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                WrCardFrm frm = new WrCardFrm();
                frm.VidTable = CreateVehInfoTable(); 
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }

                cmdMsgStr = frm.SelectedCmd;
                paramStr = frm.DataToCfg;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });

                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.clientList)
                {
                    if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                        (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                    {
                        clnt = mngr;
                        break;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Write config. to device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        cmdMsgStr = cmdMsgStr + paramStr + "\n";
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr);
                        return;
                    }
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!");
            }
        }
        private void ReadCardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = tvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                IPEndPoint rmEP = ((RemoteEndpPointInfo)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                ReadCardFrm frm = new ReadCardFrm();
                frm.VidTable = CreateVehInfoTable(); 
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }
                cmdMsgStr = frm.SelectedCmd;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });
                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.clientList)
                {
                    if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                        (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                    {
                        clnt = mngr;
                        break;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Read config. from device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr + "\n");
                        return;
                    }
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!");
            }
        }

        private void WriteTemperatureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = tvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                String paramStr = "";

                IPEndPoint rmEP = ((RemoteEndpPointInfo)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                WrTemperatureFrm frm = new WrTemperatureFrm();
                frm.VidTable = CreateVehInfoTable(); 
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }

                cmdMsgStr = frm.SelectedCmd;
                paramStr = frm.DataToCfg;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });

                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.clientList)
                {
                    if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                        (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                    {
                        clnt = mngr;
                        break;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Write config. to device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        cmdMsgStr = cmdMsgStr + paramStr + "\n";
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr);
                        return;
                    }
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!");
            }
        }
        private void ReadTemperatureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                TreeViewHitTestInfo ht = tvNodeList.HitTest(tvNodeListMousePoint);
                if (ht.Node == null || ht.Node.Tag == null)
                {
                    return;
                }

                TreeNode curSelectedNode = ht.Node;

                String cmdMsgStr = "";
                IPEndPoint rmEP = ((RemoteEndpPointInfo)curSelectedNode.Tag).remote_endpoint;
                ClientManager clnt = null;

                ReadTemperatureFrm frm = new ReadTemperatureFrm();
                frm.VidTable = CreateVehInfoTable(); 
                frm.SelectedEP = rmEP.Address.ToString() + ":" + rmEP.Port.ToString();
                if (frm.ShowDialog() == DialogResult.Cancel)
                {
                    return;
                }

                if (frm.SelectedEP.Length < 1)
                {
                    return;
                }
                cmdMsgStr = frm.SelectedCmd;
                String[] tmp = frm.SelectedEP.Split(new char[] { ':' });
                frm.Close();

                rmEP = new IPEndPoint(IPAddress.Parse(tmp[0]), Convert.ToInt32(tmp[1]));
                foreach (ClientManager mngr in objPortMon.tcp.clientList)
                {
                    if ((((IPEndPoint)mngr.socket.RemoteEndPoint).Address.ToString() == ((IPEndPoint)rmEP).Address.ToString()) &&
                        (((IPEndPoint)mngr.socket.RemoteEndPoint).Port == ((IPEndPoint)rmEP).Port))
                    {
                        clnt = mngr;
                        break;
                    }
                }

                if (clnt != null)
                {
                    String txtMsg = "VID:" + frm.Vid.Trim() + ", IP:" + clnt.IP.ToString() + ":" + clnt.Port.ToString();
                    if (MessageBox.Show(txtMsg, "Read config. from device?", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Boolean bSuccess = clnt.SendNetMsgToClient(cmdMsgStr + "\n");
                        return;
                    }
                }
            }
            catch (SocketException ex)
            {
                MessageBox.Show("Error : Socket Error !!!");
            }
        }

        private DataTable CreateVehInfoTable()
        {
            DataTable tvTable = new DataTable();

            tvTable.Columns.Add("registration", typeof(String));
            tvTable.Columns.Add("ip_endpoint", typeof(String));

            foreach (TreeNode n in tvNodeList.Nodes)
            {
                RemoteEndpPointInfo info = (RemoteEndpPointInfo)n.Tag;
                tvTable.Rows.Add(new object[] { info.veh_reg.Trim(), info.remote_endpoint.ToString() });
            }

            return tvTable;
        }
    }
}