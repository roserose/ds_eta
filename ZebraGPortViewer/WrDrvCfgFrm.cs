using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace ZebraGPortViewer
{
    public partial class WrDrvCfgFrm : Form
    {
        private DataTable vidTable = new DataTable();
        public DataTable VidTable
        {
            get { return vidTable; }
            set { vidTable = value; }
        }

        private DataTable cmdTable = new DataTable();

        private String selectedEP = "?";
        public String SelectedEP
        {
            get { return selectedEP; }
            set { selectedEP = value; }
        }

        private String selectedCmd = "?";
        public String SelectedCmd
        {
            get { return selectedCmd; }
            set { selectedCmd = value; }
        }

        private String dataToCfg = "";
        public String DataToCfg
        {
            get { return dataToCfg; }
            set { dataToCfg = value; }
        }

        private String vid = "?";
        public String Vid
        {
            get { return vid; }
            set { vid = value; }
        }

        private void IntObj()
        {
            InitCmdTable();

            cbVIDList.DataSource = vidTable;
            cbVIDList.DisplayMember = "registration";
            cbVIDList.ValueMember = "ip_endpoint";

            cbCmdList.DataSource = cmdTable;
            cbCmdList.DisplayMember = "cmd_display";
            cbCmdList.ValueMember = "cmd_message";
            cbCmdList.SelectedIndex = 0;
        }
        private void InitCmdTable()
        {
            cmdTable.Columns.Add("cmd_display", typeof(String));
            cmdTable.Columns.Add("cmd_message", typeof(String));

            cmdTable.Rows.Add(new object[] { "Set The FRAM Logger Download Header",                         "*>W000000" });
            cmdTable.Rows.Add(new object[] { "Sets The Vehicle ID",                                         "*>W001010" });
            cmdTable.Rows.Add(new object[] { "Sets The Limit for The Watchdog Timer",                       "*>W235239" });
            cmdTable.Rows.Add(new object[] { "Sets The Maximum Time The Vehicle Can Stop",                  "*>W229231" });
            cmdTable.Rows.Add(new object[] { "Sets The APN",                                                "*>W016047" });
            cmdTable.Rows.Add(new object[] { "Sets The IP Address/URL And Port",                            "*>W048095" });
            cmdTable.Rows.Add(new object[] { "Sets The SMS Center Number",                                  "*>R144159" });
            cmdTable.Rows.Add(new object[] { "Sets The Speed Limit",                                        "*>W208210" });
            cmdTable.Rows.Add(new object[] { "Sets The Set SMS Loop Time",                                  "*>W211212" });
            cmdTable.Rows.Add(new object[] { "Sets The Set GPRS Loop Time",                                 "*>W213215" });
            cmdTable.Rows.Add(new object[] { "Sets Output Status",                                          "*>W216217" });
            cmdTable.Rows.Add(new object[] { "Sets Back The Acceleration/Decelertion Limit Parameter",      "*>W228229" });
            cmdTable.Rows.Add(new object[] { "Set The Preset Limit for The OFFLINE Report Time",            "*>W224225" });
            cmdTable.Rows.Add(new object[] { "Sets The Maximum Allowed Swerve Angle or Sudden Change Angle","*>W226227" });
            cmdTable.Rows.Add(new object[] { "Sets up The Logger Interval",                                 "*>W240242" });
            cmdTable.Rows.Add(new object[] { "Sets up The Internal System Status", "*>W218222" });
            cmdTable.Rows.Add(new object[] { "Sets up The Speed Warning to Toggle Output 4 of The MAX2",    "*>W223223" });
            cmdTable.Rows.Add(new object[] { "Sets up The Over Speed Timer Function",                       "*>W233234" });
            cmdTable.Rows.Add(new object[] { "Sets FRAM Mode",                                              "*>W248248" });
            cmdTable.Rows.Add(new object[] { "Sets The 4 Digit Reconnection Delay Timer for GPRS",          "*>W249252" });
            cmdTable.Rows.Add(new object[] { "Sets The System Status Byte #2",                              "*>W253255" });
            cmdTable.Rows.Add(new object[] { "Sets The Special Command to Set APN, IP:PORT, Username, Password", "*>W016143" }); // APN must be 32 chrs, IP:PORT to be 32 chrs, Usr 16, Pwd 16. The space can be 00H or Space Character.
        }

        public WrDrvCfgFrm()
        {
            InitializeComponent();
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            if (cbVIDList.SelectedValue != null)
            {
                SelectedEP = cbVIDList.SelectedValue.ToString();
            }

            if (cbCmdList.SelectedValue != null)
            {
                SelectedCmd = cbCmdList.SelectedValue.ToString();
            }

            if (cbVIDList.Text != null)
            {
                Vid = cbVIDList.Text.ToString().Trim();
            }

            DataToCfg = tbCfgData.Text.Trim();
            DialogResult = DialogResult.OK;
        }

        private void WrDrvCfgFrm_Load(object sender, EventArgs e)
        {
            IntObj();
            DataRow[] row = vidTable.Select(String.Format("ip_endpoint = '{0}'", SelectedEP));
            if (row.Length > 0)
            {
                cbVIDList.Text = row[0]["registration"].ToString();
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void tbCfgData_Validating(object sender, CancelEventArgs e)
        {
            String data = tbCfgData.Text.Trim();
            e.Cancel = false;

            switch (cbCmdList.SelectedIndex)
            {
                case 0:
                    break;

                case 1:                    
                    if (data.Length > 10)
                    {
                        MessageBox.Show("Data should not over 10 characters.");
                        e.Cancel = true;
                    }
                    break;

                case 2:
                    if (data.Length != 5)
                    {
                        MessageBox.Show("Data to get 5 characters only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                        }
                    }
                    break;

                case 3:
                    if (data.Length != 3)
                    {
                        MessageBox.Show("Data to get 3 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                        else
                        {
                            if (Convert.ToInt32(data) < 0 || Convert.ToInt32(data) > 999)
                            {
                                MessageBox.Show("Data out of range 0 - 999");
                                e.Cancel = true;
                            }
                        }
                    }
                    break;

                case 4:
                    if (data.Length > 32)
                    {
                        MessageBox.Show("Data over 32 characters.");
                        e.Cancel = true;
                    }
                    break;

                case 5:
                    String[] tmp =  data.Split(new char[] {':'});
                    if (tmp.Length != 2)
                    {
                        MessageBox.Show("Data should be xxx.xxx.xxx.xxx:yyyy");
                        e.Cancel = true;
                    }
                    else
                    {
                        IPAddress ipTmp ;
                        if (!IPAddress.TryParse(tmp[0], out ipTmp))
                        {
                            MessageBox.Show("Wrong type of IP Address");
                            e.Cancel = true;
                        }
                        else
                        {
                            int iTmp;
                            if( !int.TryParse(tmp[1], out iTmp))
                            {
                                MessageBox.Show("Port Number be Incorrect.");
                                e.Cancel = true;
                            }
                        }
                    }
                    break;

                case 6:
                    int iTmp2;
                    if (!int.TryParse( data, out iTmp2))
                    {
                        MessageBox.Show("Data should be numbers only.");
                        e.Cancel = true;
                    }
                    break;

                case 7:
                    if (data.Length != 3)
                    {
                        MessageBox.Show("Data to get 3 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 8:
                    if (data.Length != 2)
                    {
                        MessageBox.Show("Data to get 3 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 9:
                    if (data.Length != 3)
                    {
                        MessageBox.Show("Data to get 3 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 10:
                    if (data.Length != 3)
                    {
                        MessageBox.Show("Data to get 3 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 11:
                case 12:
                case 13:
                case 14:
                    if (data.Length != 2)
                    {
                        MessageBox.Show("Data to get 2 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 15:
                    if (data.Length != 3)
                    {
                        MessageBox.Show("Data to get 3 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 16:
                    if (data.Length != 5)
                    {
                        MessageBox.Show("Data to get 5 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 17:
                    if (data.Length != 2)
                    {
                        MessageBox.Show("Data to get 2 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 18:
                    if (data.Length != 1)
                    {
                        MessageBox.Show("Data to get 1 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 19:
                    if (data.Length != 4)
                    {
                        MessageBox.Show("Data to get 4 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;

                case 20:
                    if (data.Length != 3)
                    {
                        MessageBox.Show("Data to get 3 digits only.");
                        e.Cancel = true;
                    }
                    else
                    {
                        int iTmp = 0;
                        if (!int.TryParse(data, out iTmp))
                        {
                            MessageBox.Show("Data should be numbers only.");
                            e.Cancel = true;
                        }
                    }
                    break;
            }
        }
    }
}