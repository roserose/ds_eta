﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace ZebraLogTools
{
    public class TextCls
    {
        public void WriteSystemLogEvent(String Msg)
        {
            try
            {
                Msg = DateTime.Now.ToString() + " " + Msg;
                if (File.Exists("SystemLog.txt"))
                {
                    StreamWriter sr = File.AppendText("SystemLog.txt");
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText("SystemLog.txt");
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch { }
        }
        public void WriteLogError(String Msg)
        {
            try
            {
                String logPath = Directory.GetCurrentDirectory() + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                Msg = DateTime.Now.ToString() + "_" + Msg;
                String fileName = logPath + "\\" + "Error" + DateTime.Now.ToString("yyyyMM") + ".txt";

                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch
            {
            }
        }
        public void WriteTextFile(String Patch,String Msg)
        {
            try
            {
                String logPath = Patch + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                String fileName = logPath + "\\" + "DataLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch
            { }
        }
        public void WriteTextFileLiftTime(String Patch, String Msg,int Day)
        {
            try
            {
                String logPath = Patch + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                String fileName = logPath + "\\" + "DataLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();

                    FileCls objFile = new FileCls();
                    objFile.DeleteFileOldDay(logPath, Day, ".txt");
                }
            }
            catch
            { }
        }
    }
}
