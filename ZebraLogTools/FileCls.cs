﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO;

namespace ZebraLogTools
{
    public class FileCls
    {
        public void DeleteFileOldDay(string Patch, int Day, string Extension)
        {
            try
            {
                foreach (string data in Directory.GetFiles(Patch))
                {
                    FileInfo file = new FileInfo(data);
                    if (file.LastWriteTime < DateTime.Now.AddDays(-Day))
                    {
                        if (Extension == ".*")
                        {
                            File.Delete(data);
                        }
                        else if (Path.GetExtension(data) == Extension)
                        {
                            File.Delete(data);
                        }
                    }
                }
            }
            catch { }
        }
    }
}
