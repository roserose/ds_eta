﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Threading;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Net;

using ZebraLogTools;
using ZebraDataCentre;
using ZebraNoSQLLib;
using ZrebraFilterInterface;

namespace ZebraWriteLog
{
    public partial class MainForm : Form
    {
        private CommonQeue MQ;
        private MongoDBCls objMongoCls = null;
        private int LogType = 0;
        private Int64 TotalMessage = 0;
        private string mq = string.Empty;
        private string mongo_svr = string.Empty;
        private string mongo_DB = string.Empty;
        private string mongo_collection = string.Empty;
        public MainForm()
        {
            InitializeComponent();
            InitializeSystem();
        }
        private void InitializeSystem()
        {
            try
            {
                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                this.Text = "ZebraWriteLog V." + System.Reflection.Assembly.GetEntryAssembly().GetName().Version;
                ZebraWriteLog.Properties.Settings pf = new Properties.Settings();
                MQ = new CommonQeue(pf.SystemQueue.Trim());
                MQ.OnReceiveMQ += new ReceiveMQ(mq_OnReceiveMQ);
                MQ.InitReceiveQMsg();
                mq = pf.SystemQueue.Trim();
                mongo_svr = pf.MongoServerPort.Trim();
                mongo_DB = pf.DataBaseName.Trim();
                mongo_collection = pf.CollectionName.Trim();
               
                objMongoCls = new MongoDBCls(pf.MongoServerPort.Trim(),pf.DataBaseName.Trim(),pf.CollectionName.Trim());
                LogType = pf.LogType;
            }
            catch { }
        }
     
        private void MainForm_Load(object sender, EventArgs e)
        {
            MQ.BeginReceiveQMsg();
        }

        void mq_OnReceiveMQ(object owner, XmlReader data)
        {
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(LogInfo));
                LogInfo msg = (LogInfo)ser.Deserialize(data);
                switch (LogType)
                {
                    case 0:
                        GateProcessLog(msg);
                        break;
                }
                Thread.Sleep(1);
                TotalMessage++;
            }
            catch (Exception ex)
            { }
            MQ.BeginReceiveQMsg();
        }
        private void GateProcessLog(LogInfo msg)
        {
            try
            {
                Gateway_Log info = new Gateway_Log();
                info.data = msg.data;
                info.d_ip= GetIntIP(msg.destination);
                info.d_port = GetIntPort(msg.destination);
                info.s_ip = GetIntIP(msg.source);
                info.s_port = GetIntPort(msg.source);
                info.time = msg.time;
                DateTime d = Convert.ToDateTime(msg.time);
                info.date_id = Int64.Parse((d.ToString("yyyyMMddHHmmss")));
                info.vid = GetVehID(msg.data, int.Parse(msg.data_type));
                info.type = int.Parse(msg.data_type);
                info.number = CreateShardKey(info.d_ip.ToString(),info.type.ToString(),info.vid.ToString());

                objMongoCls.InsertLog(info);
                objMongoCls.InsertLog2(info);
            }
            catch
            {
            }
        }

        private Double CreateShardKey(string d_ip, string type, string vid)
        {
            Double key = 0.0;
            try
            {
                string tm_type = type;
                string tm_vid = "0000";
                if (int.Parse(vid) > 0)
                {
                    if (vid.Length < 4)
                        tm_vid = vid.PadLeft(4, '0');
                    else
                        tm_vid = vid.Substring((vid.Length - 4), 4);
                }
                string tm_ip = "00000";
                string temp_address = d_ip;
                tm_ip = temp_address.Substring((temp_address.Length - 4), 4);

                key = Convert.ToDouble((tm_type + tm_vid + tm_ip));
                key += Convert.ToDouble("0." + DateTime.Now.ToString("yyyyMMdd"));
            }
            catch { }
            return key;
        }

        private int GetVehID(string msg ,int type)
        {
            int vid = 0;
            try
            {
                switch (type)
                {
                    case 0://None
                        break;
                    case (int)BoxVersions.GBox://G
                        {
                            string[] splitStr = { "\r\n" };
                            string[] data = msg.Split(splitStr,StringSplitOptions.None);

                            foreach (string st in data)
                            {
                                if (st.IndexOf('$') > -1)
                                {
                                    string[] v = st.Split(',');
                                    vid = int.Parse(v[(v.Length - 1)]);
                                    break;
                                }
                            }
                        }
                        break;
                    case (int)BoxVersions.MBox:
                        {
                            string[] splitStr = { "\r\n" };
                            string[] data = msg.Split(splitStr, StringSplitOptions.None);

                            foreach (string st in data)
                            {
                                if (st.IndexOf('$') > -1)
                                {
                                    string[] v = st.Split(',');
                                    string temp = v[0].Substring(1, v[0].Length - 1);
                                    vid = int.Parse(temp);
                                    break;
                                }
                                else if (st.IndexOf("VID:") > -1)
                                {
                                   // M2Mguru Copyright Reserved  2009, VID:42605     , Ver:90613
                                    string[] v = st.Split(',');
                                    string temp_vid = v[1].Trim();
                                    string temp = temp_vid.Substring(temp_vid.IndexOf(':') + 1, temp_vid.Length - (temp_vid.IndexOf(':') + 1));
                                    vid = int.Parse(temp);
                                    break;
                                }
                            }
                        }
                        break;
                    case (int)BoxVersions.KBox://K
                        {
                            string[] data = msg.Split('\r');

                            foreach (string st in data)
                            {
                                if (st.IndexOf('#') > -1 || st.IndexOf('$') > -1)
                                {
                                    string[] v  = st.Split(',');
                                    vid = int.Parse(v[(v.Length - 1)]);
                                    break;
                                }
                            }
                        }
                        break;
                }
            }
            catch { }
            return vid;
        }
        private int GetIntIP(string ip) 
        {
            string[] st = ip.Split(':');
            int intAddress = GetIntAddress(get_ip(st[0]).ToString());
            return intAddress;
        }
        private int GetIntAddress(string ip)
        {
            return BitConverter.ToInt32(IPAddress.Parse(ip).GetAddressBytes(), 0);
        }
        private int GetIntPort(string ip)
        {
            int tmp_port = 0;
            try
            {
                if (int.TryParse(ip.Split(':')[1], out tmp_port))
                { }
            }
            catch { }
            return tmp_port;
        }
        private IPAddress get_ip(string ip)
        {
            IPAddress temp = IPAddress.Parse("0.0.0.0");
            try
            {
                temp = IPAddress.Parse(ip);
            }
            catch { }
            return temp;
        }

        private void tmDisplay_Tick(object sender, EventArgs e)
        {
            try
            {
                rtDisplay.Text = "Queue : " + mq
                    + "\r\nServer : " + mongo_svr
                    + "\r\nDataBase : " + mongo_DB
                    + "\r\nCollection : " + mongo_collection
                    + "\r\nTotal : " + TotalMessage.ToString();
                slTime.Text = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
            }
            catch { }
        }
    }
}
