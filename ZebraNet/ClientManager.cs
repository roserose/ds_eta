using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.ComponentModel;
using ZebraNet;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace ZebraNet
{
    public delegate void ThreadExpiredHandler(object obj);
    public class ClientManager
    {
        public IPAddress IP
        {
            get
            {
                try
                {
                    if (this.socket != null)
                        return ((IPEndPoint)this.socket.RemoteEndPoint).Address;
                    else
                        return IPAddress.None;
                }
                catch
                {
                    return IPAddress.None;
                }
            }
        }
        public const int RECEIVETIMEOUT = 1000 * 60 * 5;
      //  private System.Timers.Timer tmIsAlive;
      //  private int gResend = 3;

        public int Port
        {
            get
            {
                if ( this.socket != null)
                    return ( (IPEndPoint)this.socket.RemoteEndPoint ).Port;
                else
                    return -1;
            }
        }
        public bool Connected
        {
            get
            {
                if ( this.socket != null )
                    return this.socket.Connected;
                else
                    return false;
            }
        }

        public struct CONNECTINFO
        {
            public Socket socket;
            public byte[] buffer;
        }

        public          bool            IsThreadRunning = true ;
        public          Socket          socket ;
        private         NetworkStream   networkStream ;
        private         Thread          sckThrd ;

        #region Constructor
        public ClientManager(Socket clientSocket)
       {
           try
           {
               this.socket = clientSocket ;
               this.networkStream = new NetworkStream(this.socket) ;

               this.sckThrd = new Thread(new ThreadStart(ReceiveData));
               this.sckThrd.Start();

               //tmIsAlive = new System.Timers.Timer();
               //tmIsAlive.Interval = 1000 * 90 ;
               //tmIsAlive.Elapsed += new System.Timers.ElapsedEventHandler(tmIsAlive_Elapsed);
               //tmIsAlive.Start();
           }
           catch (Exception e)
           {
               WriteLogEvent("ClientManager Error #0:" + e.Message) ;
           }
        }

        //private void tmIsAlive_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //  //  tmIsAlive.Stop();
        //    try
        //    {
        //       // if (this.IsThreadRunning && SendNetMsgToClient("*>T\n")) { }
        //    }
        //    catch
        //    {
        //    }
        //    finally
        //    {
        //        if (this.IsThreadRunning)
        //        {
        //           // tmIsAlive.Start();
        //        }
        //        else
        //        { }
        //    }
        //}

        ~ClientManager()
        {
            ShutdowConnection();
        }

        #endregion

        public void TerminateObj()
        {
            this.sckThrd = null;
            this.socket = null;
            this.networkStream = null;
        }
        public void ShutdowConnection()
        {
            try
            {
                if (this.socket != null)
                {
                    if (this.socket.Connected) { this.socket.Disconnect(false); }
                    this.socket.Close();
                }
                if (this.sckThrd != null) { this.IsThreadRunning = false; this.sckThrd.Abort(); }
                if (this.networkStream != null) { this.networkStream.Dispose(); }

                this.sckThrd = null;
                this.socket = null;
                this.networkStream = null;
            }
            catch (Exception e)
            {
                WriteLogEvent("ClientManager Error #0.5:" + e.Message);
            }
        }

        private void ReceiveData()
        {
            try
            {
                this.socket.ReceiveTimeout = RECEIVETIMEOUT;

                while (this.IsThreadRunning)
                {
                    if (!this.socket.Connected) {
                    //    tmIsAlive.Stop();
                        break; 
                    }

                    CONNECTINFO info = new CONNECTINFO();
                    info.socket = this.socket;
                    info.buffer = new byte[2048];
                    SocketError err;
                    info.socket.Receive(info.buffer, 0, info.buffer.Length, SocketFlags.None, out err);
                    //Console.WriteLine(err.ToString());
                    if (err != SocketError.Success || info.buffer[0] == 0) { this.IsThreadRunning = false; break; }
                    OnDataReceived(info);
                }
            }
            catch (Exception e)
            {
                WriteLogEvent("ClientManager Error #1:" + e.Message);
            }
            finally
            {
                try
                {
                    this.IsThreadRunning = false;
                    if (this.socket.Connected) { this.socket.Disconnect(true); }
                    this.socket.Close();
                    OnThreadExpired();                    
                }
                catch { }                
            }
        }
        private void OnDataReceived(CONNECTINFO info)
        {
            try
            {
                String dataReceived = System.Text.Encoding.ASCII.GetString(info.buffer);
                dataReceived = dataReceived.Substring(0, dataReceived.IndexOf('\0'));
                if (dataReceived.Length > 1)
                {
                    ReceivedMsgEvent evt = new ReceivedMsgEvent();
                    evt.data = info.buffer;
                    evt.remoteIP = info.socket.RemoteEndPoint;
                    this.OnReceiveMsg(evt);
                }
            }
            catch (Exception e)
            {
                WriteLogEvent("ClientManager Error #2:" + e.Message);
            }
        }
        #region Private Methods
        private void bwSender_RunWorkerCompleted(object sender , RunWorkerCompletedEventArgs e)
        {
            if ( !e.Cancelled && e.Error == null && ( (bool)e.Result ) )
                this.OnSentMsg(new EventArgs());
            else
                this.OnFailedMsg(new EventArgs());

            ( (BackgroundWorker)sender ).Dispose();
            GC.Collect();
        }
        private void bwSender_DoWork(object sender , DoWorkEventArgs e)
        {
            String msg = (String)e.Argument;
            e.Result = this.SendNetMsgToClient(msg);
        }

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1 , 1);
        public bool SendNetMsgToClient(String msg)
        {
            try
            {
                semaphor.WaitOne();

                byte[] buffer = Encoding.ASCII.GetBytes(msg.ToCharArray());
                this.networkStream.Write(buffer, 0, msg.Length);
                this.networkStream.Flush();

                semaphor.Release();
                return true;
            }
            catch
            {
                semaphor.Release();
                return false;
            }
        }

        #endregion

        #region Public Methods
        public void SendNetMsg(String msg)
        {
            if ( this.socket != null && this.socket.Connected )
            {
                BackgroundWorker bwSender = new BackgroundWorker();
                bwSender.DoWork += new DoWorkEventHandler(bwSender_DoWork);
                bwSender.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwSender_RunWorkerCompleted);
                bwSender.RunWorkerAsync(msg);
            }
            else
                this.OnFailedMsg(new EventArgs());
        }
       
        public bool Disconnect()
        {
            if (this.socket != null) {
                this.socket.Close();
                this.IsThreadRunning = false;
            }

            if (this.socket != null && this.socket.Connected)
            {
                try {
                    this.socket.Disconnect(true);
                    return true;
                } catch {
                    return false;
                }
            }
            else {
                return true;
            }
        } 
        #endregion

        #region Events
        public event NetMsgReceivedEventHandler ReceiveMsg;
        protected virtual void OnReceiveMsg(ReceivedMsgEvent e)
        {
            try
            {
                if (ReceiveMsg != null)
                    ReceiveMsg(this, e);
            }
            catch { }
        }

        public event NetMsgSentEventHandler SentMsg;
        protected virtual void OnSentMsg(EventArgs e)
        {
            try
            {
                if (SentMsg != null)
                    SentMsg(this, e);
            }
            catch { }
        }
        public event NetMsgSendingFailedEventHandler FailedMsg;
        protected virtual void OnFailedMsg(EventArgs e)
        {
            try
            {
                if (FailedMsg != null)
                    FailedMsg(this, e);
            }
            catch { }
        }

        public event ThreadExpiredHandler ThreadExpired;
        protected void OnThreadExpired()
        {
            try
            {
                if (ThreadExpired != null)
                    ThreadExpired(this);
            }
            catch { }
        }
        #endregion

        protected void WriteLogEvent(String msg)
        {
            /*
            msg = DateTime.Now.ToString() + " : " + msg;
            String fileName = "ClientLogEvent.txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            */ 
        }
    }
}
