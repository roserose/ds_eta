using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using ZebraNet;

namespace ZebraNet
{
    public delegate void NetMsgReceivedEventHandler(object sender, ReceivedMsgEvent e);
    public delegate void NetMsgSentEventHandler(object sender , EventArgs e);
    public delegate void NetMsgSendingFailedEventHandler(object sender , EventArgs e);
    public delegate void NetDisconnetedHandler(object sender, NetDisConnectedEvent e);

    public class ClientEventArgs : EventArgs
    {
        private Socket socket;
        public IPAddress IP
        {
            get { return ( (IPEndPoint)this.socket.RemoteEndPoint ).Address; }
        }
        public int Port
        {
            get{return ((IPEndPoint)this.socket.RemoteEndPoint).Port;}
        }
        public ClientEventArgs(Socket clientManagerSocket)
        {
            this.socket = clientManagerSocket;
        }
    }
    public class NetDisConnectedEvent : EventArgs
    {
        public IPEndPoint ep;
        public int port;
        public byte[] data;
    }    
}
