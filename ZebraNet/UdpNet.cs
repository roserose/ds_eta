using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace ZebraNet
{
    public class UdpNet : ComNet
    {
        private const byte ETX = 0x3;
        public event ReceivedMsgHandler OnReceivedMsg;

        public UdpNet(int port, Boolean listen)
            : base(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp, port, listen)
        {
        }

        public void Close()
        {
            base.CloseCom();
            NetSocket.Close();            
        }

        protected override void ListeningProcThread()
        {
         //   IPHostEntry localHostEntry;

            try
            {
                IPEndPoint localIpEndPoint = new IPEndPoint(IPAddress.Any, NetPort);
                NetSocket.Bind(localIpEndPoint);

                Byte[] received = new Byte[256];
                while (true)
                {
                    IPEndPoint  tmpIpEndPoint   = new IPEndPoint(IPAddress.Any, NetPort);
                    EndPoint    remoteEP        =   (tmpIpEndPoint) ;
                    int         bytesReceived   =   NetSocket.ReceiveFrom(received, ref remoteEP) ;

                    ReceivedMsgEvent evt        =   new ReceivedMsgEvent() ;
                    evt.remoteIP                =   remoteEP ;
                    evt.data                    =   received ;
                    OnReceivedMsg(this, evt);
                }
            }
            catch (SocketException se)
            {
                WriteLogEvent("Udp Error #2:" + se.Message);
            }
        }

        public void Send(EndPoint remoteEP, byte[] data)
        {
            try
            {
                NetSocket.SendTo(data, remoteEP);
            }
            catch(Exception ex)
            {
                WriteLogEvent("Udp Error #3:" + ex.Message);
            }
        }
    }
}
