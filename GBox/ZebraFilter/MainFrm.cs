using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using System.IO;
using System.Messaging;
using System.Diagnostics;
using System.Globalization;
using System.Collections;
using System.Net;
using System.Web;
using ZrebraFilterInterface;
using ZebraDataCentre;

namespace ZebraFilter
{
    public partial class MainFrm : Form
    {
        #region
        public struct CFG
        {
            private string _gboxreadq;
            [XmlElement("GboxReadQ")]
            public string GboxReadQ
            {
                get { return _gboxreadq; }
                set { _gboxreadq = value; }
            }
            private string _gboxsendq;
            [XmlElement("GboxSendQ")]
            public string GboxSendQ
            {
                get { return _gboxsendq; }
                set { _gboxsendq = value; }
            }

            private string _mqreceiver;
            [XmlElement("MQReceiver")]
            public string Receiver
            {
                get { return _mqreceiver; }
                set { _mqreceiver = value; }
            }

            private string _programHeader;
            [XmlElement("ProgramHeader")]
            public string ProgramHeader
            {
                get { return _programHeader; }
                set { _programHeader = value; }
            }

            //TempVehQueue
            private string _TempVehQueue;
            [XmlElement("TempVehQueue")]
            public string TempVehQueue
            {
                get { return _TempVehQueue; }
                set { _TempVehQueue = value; }
            }

            private string _dbHost;
            [XmlElement("DBHost")]
            public string DBHost
            {
                get { return _dbHost; }
                set { _dbHost = value; }
            }

            private string _LocalIp;
            [XmlElement("LocalIp")]
            public string LocalIp
            {
                get { return _LocalIp; }
                set { _LocalIp = value; }
            }

            private string _LocalPort;
            [XmlElement("LocalPort")]
            public string LocalPort
            {
                get { return _LocalPort; }
                set { _LocalPort = value; }
            }
        }

        public enum TimeType { Local, Web, DataBase };

        public struct OBJSTATE
        {
            public XmlReader xmlData;
            public Stream bodyStream;
        }
        public struct FWQINFO
        {
            public string IP;
            public ReceiverInterface gReceiverObj;
        }
        public struct DATAQUEUE
        {
            public RECHECKTYPE type;
            public Max2FmtMsg veh_data;
        }
        public struct VEHINFO
        {
            public string veh_id;
            public DateTime last_connect_server;
            public bool last_engineon;
            public bool is_chang_data; //engine off funtion
            public bool is_recheck;
            public Max2FmtMsg Max2FM;
        }
        public enum RECHECKTYPE
        {
            LOSTSIGNAL = 0,
            RECHECK = 1
        }

        private sqlCls sql = null;
        private CFG gCFG = new CFG();
        private SubClass sClass = null;

        private GBoxQueue GBOXMQR;
        private MessageQueue GBOXFWDMQ;
        private CommonQueueStream VEHINFOQ;
        private CommonQeue MQ;

        private Queue<String> _logmsgQ = new Queue<String>();
        private Queue<DATAQUEUE> _dataQueue = new Queue<DATAQUEUE>();

        private Hashtable vehHashInfo = new Hashtable();
        private ArrayList max2Pushq = new ArrayList();

        private int total_of_data = 0;
        private String WDTextMSMQ_runing = "";
        private bool BeginReceive = true;
        private bool waitStatus = true;
        private string gDBHost = "";
        private string WebServer = "http://10.0.254.20";

        private TimeType timeType = TimeType.DataBase;
        private DateTime gTimeSetver = new DateTime();

        private string testTime = "";

        #endregion

        public MainFrm()
        {
            InitializeComponent();
            InitLocalSystem();
        }

        private bool InitLocalSystem()
        {
            try
            {
                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                max2Pushq.Clear();

                String path = "";
                String fullPath = this.GetType().Assembly.GetName().CodeBase.ToString().ToUpper();
                String fileName = this.GetType().Assembly.GetName().Name + ".EXE";
                fileName = fileName.ToUpper();
                int iFound = fullPath.IndexOf(fileName);
                if (iFound > -1)
                {
                    path = fullPath.Substring(0, iFound);
                }
                else
                {
                    path = fullPath;
                }

                XmlSerializer s = new XmlSerializer(typeof(CFG));
                XmlTextReader xmlRd = new XmlTextReader(path + "PortMonitorCfg.xml");
                gCFG = (CFG)s.Deserialize(xmlRd);

                #region-------assign value
                string max2qList = "";
                String[] mqReceiver = gCFG.Receiver.Split(new char[] { ',', ';' });
                if (mqReceiver.Length > 0)
                {
                    foreach (string mq in mqReceiver)
                    {
                        if (mq.Length == 0) { continue; }
                        MQ = new CommonQeue(mq.Trim());
                        max2Pushq.Add(MQ);
                        max2qList += "\nMax2Send : " + mq;
                    }
                }
                //GBox Queue
                GBOXMQR = new GBoxQueue(gCFG.GboxReadQ);
                GBOXMQR.InitReceiveQMsg();
                GBOXMQR.OnReceiveMQ += new ReceiveMQStream(GBOXMQR_OnReceiveMQ);
                InitGboxForwardQ(gCFG.GboxSendQ);
                //VEH Buffer
                VEHINFOQ = new CommonQueueStream(gCFG.TempVehQueue);
                VEHINFOQ.InitReceiveQMsg();
                VEHINFOQ.OnReceiveMQ += new ReceiveMQStream(GenBufferVehLog);

                gDBHost = gCFG.DBHost;
                #endregion

                WDTextMSMQ_runing =
                    "GboxReadQ : " + gCFG.GboxReadQ.ToString() + "\n" +
                    "GboxSendQ : " + gCFG.GboxSendQ.ToString() + max2qList;

                sql = new sqlCls(gDBHost); //connet database
                sClass = new SubClass(gDBHost);

                rtMSMQ.Text = WDTextMSMQ_runing;

                this.Text = String.Format("Zebra Filter V.{0} {1}", System.Reflection.Assembly.GetEntryAssembly().GetName().Version, gCFG.ProgramHeader);

                PopQBufferVehLog();
                DateTime dtMax = DateTime.Now.AddMinutes(2);
                while (waitStatus && DateTime.Now < dtMax)
                {
                    Thread.Sleep(5);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
            return true;
        }

        private void MainFrm_Shown(object sender, EventArgs e)
        {
            try
            {
                if (GBOXMQR != null)
                {
                    GBOXMQR.BeginReceiveQMsg();
                }

                updateLog.Start();
                tmCheckLostsignal.Start();
                Thread thread = new Thread(new ThreadStart(DataQueueFunction));
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #region Vehicle Buffer
        //����͡
        private void PopQBufferVehLog()
        {
            try
            {
                if (VEHINFOQ.GetAllMessage() > 0)
                {
                    VEHINFOQ.BeginReceiveQMsg();
                }
                else
                {
                    waitStatus = false; 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }
        private void GenBufferVehLog(object owner, XmlReader data, Stream stmData)
        {
            try
            {
                XmlSerializer ser = new XmlSerializer(typeof(VEHINFO));
                VEHINFO msg = (VEHINFO)ser.Deserialize(data);

                string key = msg.veh_id.Trim();
                if (!vehHashInfo.ContainsKey(key))
                {
                    vehHashInfo.Add(key, msg);
                }

                if(VEHINFOQ.GetAllMessage() > 0)
                {
                    VEHINFOQ.BeginReceiveQMsg();
                    Thread.Sleep(5);
                }
                else
                {
                    waitStatus = false;
                }
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }
        //������
        private void PushQBufferVehLog(string[] vid)
        {
            try
            {
                CommonQeue VEHIDQ = new CommonQeue(gCFG.TempVehQueue);
                foreach (string key in vid)
                {
                    if (vehHashInfo.ContainsKey(key))
                    {
                        VEHINFO data = (VEHINFO)vehHashInfo[key];
                        VEHIDQ.PushQ(data);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }
        #endregion
        #region Translat
        private void GBOXMQR_OnReceiveMQ(object owner, XmlReader xmlData, Stream bodyStream)
        {
            try
            {
                TranslatNPutForMax2Msg(xmlData);
                total_of_data++;

                if (BeginReceive)
                {
                    GBOXMQR.BeginReceiveQMsg();
                }
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }
        private void TranslatNPutForMax2Msg(XmlReader xmlData)
        {
            if (xmlData == null) { return; }
            try
            {
                Max2FmtMsg msg = new Max2FmtMsg();
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlData);
                Boolean type_of_fix = true;
                String iReset = "-";
                String gps_date_time = GetTime();
                DateTime timeServer = Convert.ToDateTime(gps_date_time);
                String szUTC = "";

                if (doc.GetElementsByTagName("utc_time")[0] == null)
                {
                    szUTC = timeServer.ToUniversalTime().ToString("HHmmss");//  .UtcNow.ToString("HHmmss");//DateTime.UtcNow.ToString("HHmmss");
                }
                else
                {
                    int iUTC = Convert.ToInt32(doc.GetElementsByTagName("utc_time")[0].InnerXml);
                    if (iUTC <= 0)
                    {
                        iReset = doc.GetElementsByTagName("product_name")[0].InnerXml;
                        if (iReset.Trim() != "GBOX") { return; }
                    }

                    int year = timeServer.ToUniversalTime().Year; //.UtcNow.Year;
                    int month = timeServer.ToUniversalTime().Month;//.UtcNow.Month;
                    int day = timeServer.ToUniversalTime().Day;//DateTime.UtcNow.Day;
                    int hour = (iUTC / 60) / 60;
                    int minute = ((iUTC / 60) - (hour * 60));
                    int second = iUTC - ((minute * 60) + (hour * 60 * 60));

                    if (hour > 23 || minute > 59 || second > 59) { return; }
                    DateTime d = new DateTime(year, month, day, hour, minute, second);

                  //  if (d.ToUniversalTime() > timeServer.UtcNow.ToUniversalTime()) { return; }
                    szUTC = d.ToString("HHmmss");
                }

                ///vid
                if (doc.GetElementsByTagName("veh_id")[0] == null) return;
                msg.vid = doc.GetElementsByTagName("veh_id")[0].InnerXml;
                if (msg.vid == null || msg.vid.Trim().Length < 1) { return; }

                if (timeType == TimeType.Local)
                {
                    ///gps_date_time
                    if (doc.GetElementsByTagName("timestamp")[0] == null) return;
                    gps_date_time = doc.GetElementsByTagName("timestamp")[0].InnerXml;
                }

                /// utc
                if (gps_date_time == null || gps_date_time.Trim().Length < 1) return;
                msg.gps_date_time = Convert.ToDateTime(gps_date_time).ToString("ddMMyy", new CultureInfo("en-US"));
             

                if (iReset.Trim() == "GBOX")
                {
                    msg.utc = Convert.ToDateTime(gps_date_time).ToString("HHmmss", new CultureInfo("en-US"));
                }
                else
                    msg.utc = szUTC;


                msg.gps_date_time = timeServer.AddHours(-7).ToString("ddMMyy");

                DateTime dtCheck = GetTimeStamp(msg.gps_date_time, msg.utc);

                //�ѹ��ǧ˹��
                //if (dtCheck >= timeServer.AddSeconds(10))
                //{
                //    msg.utc = timeServer.AddHours(-7).ToString("HHmmss");
                //    msg.gps_date_time = timeServer.ToString("ddMMyy", new CultureInfo("en-US"));
                //}
                ////�ѹ�����ѧ
                //else if (dtCheck < timeServer.AddMinutes(-10))
                //{
                //    msg.utc = timeServer.AddHours(-7).ToString("HHmmss");
                //    msg.gps_date_time = timeServer.ToString("ddMMyy", new CultureInfo("en-US"));
                //}

                ///lat
                if (doc.GetElementsByTagName("lat")[0] == null) return;
                msg.lat = doc.GetElementsByTagName("lat")[0].InnerXml;
                if (msg.lat == null || msg.lat.Trim().Length < 3)
                {
                    msg.lat = "0.00";
                    type_of_fix = false;
                }

                ///lon
                if (doc.GetElementsByTagName("lon")[0] == null) return;
                msg.lon = doc.GetElementsByTagName("lon")[0].InnerXml;
                if (msg.lon == null || msg.lon.Trim().Length < 3)
                {
                    msg.lon = "0.00";
                    type_of_fix = false;
                }

                ///speed
                if (doc.GetElementsByTagName("speed")[0] == null) return;
                msg.speed = doc.GetElementsByTagName("speed")[0].InnerXml;
                if (msg.lon == null || msg.lon.Trim().Length < 1)
                {
                    msg.speed = "0";
                }
                msg.speed = ((int)Math.Round(Convert.ToDouble(msg.speed), 0)).ToString();

                ///course
                if (doc.GetElementsByTagName("true_course")[0] == null) return;
                msg.course = doc.GetElementsByTagName("true_course")[0].InnerXml;
                if (msg.course == null || msg.course.Trim().Length < 1)
                {
                    msg.course = "0";
                }

                ///no_of_satellite
                if (doc.GetElementsByTagName("satelite")[0] == null) return;
                msg.no_of_satellite = doc.GetElementsByTagName("satelite")[0].InnerXml;
                if (msg.no_of_satellite == null || msg.no_of_satellite.Trim().Length < 1)
                {
                    msg.no_of_satellite = "0";
                    type_of_fix = false;
                }

                ///type_of_fix
                if (type_of_fix)
                {
                    msg.type_of_fix = "1";
                }
                else
                    msg.type_of_fix = "0";

                ///gpio_status
                /// port : 3 = Engine on/off 
                if (doc.GetElementsByTagName("port")[0] == null) return;
                string port_id = doc.GetElementsByTagName("port")[0].InnerXml;
                if (port_id == null || port_id.Trim().Length < 1)
                {
                    port_id = "0";
                }

                ///gpio_status
                if (doc.GetElementsByTagName("port_value")[0] == null) return;
                string port_value = doc.GetElementsByTagName("port_value")[0].InnerXml;
                if (port_value == null || port_value.Trim().Length < 1)
                {
                    port_value = "0";
                }

                if (doc.GetElementsByTagName("power_level")[0] == null)
                {
                    msg.gpio_status = IOPortValue(port_id, port_value, 12);
                }
                else
                {
                    int power_level = Convert.ToInt32(doc.GetElementsByTagName("power_level")[0].InnerXml);
                    msg.gpio_status = IOPortValue(port_id, port_value, power_level);
                }

                //analog_level
                if (doc.GetElementsByTagName("power_level")[0] == null) return;
                string analog_level = "0";
                analog_level = doc.GetElementsByTagName("power_level")[0].InnerXml;
                int iAnalog_level = Convert.ToInt32(analog_level) * 100;
                msg.analog_level = iAnalog_level.ToString();

                //Antena_Status
                int antena_status = 1;
                if (doc.GetElementsByTagName("Antena_Status")[0] == null)
                { }
                else
                {
                    antena_status = Convert.ToInt32(doc.GetElementsByTagName("Antena_Status")[0].InnerXml);
                }
                ///type_of_message
                ///Chang msg.gpio_status
                if (doc.GetElementsByTagName("product_name")[0] != null)
                {
                    msg.type_of_message = "+";
                    if (sql.GetLastEvtEngine(Convert.ToInt32(msg.vid)))
                    {
                        msg.gpio_status = "10";
                    }
                    else
                    {
                        msg.gpio_status = "00";
                    }
                }
                else if (Convert.ToInt32(port_id) > 0)
                {
                    msg.type_of_message = "I";
                }
                else
                {
                    msg.type_of_message = "G";
                }

                //internal power 
                if (doc.GetElementsByTagName("battery_level")[0] != null)
                {
                    if (msg.type_of_message == "G")
                    {
                        msg.internal_power = GetPowerValue(1, doc.GetElementsByTagName("battery_level")[0].InnerXml);
                    }
                    else
                        msg.internal_power = "0";
                }
                //external power 
                if (doc.GetElementsByTagName("power_level")[0] != null)
                {
                    if (msg.type_of_message == "G")
                    {
                        msg.external_power = GetPowerValue(2, doc.GetElementsByTagName("power_level")[0].InnerXml);
                    }
                    else
                        msg.external_power = "0";
                }

                //GSM Data
                string gsm_cell_id = "0";
                string gsm_location_id = "0";
                string gsm_rssi_level = "0";
                if (doc.GetElementsByTagName("gsm_cell_id")[0] != null)
                {
                    gsm_cell_id = doc.GetElementsByTagName("gsm_cell_id")[0].InnerXml;
                }
                if (doc.GetElementsByTagName("gsm_location_id")[0] != null)
                {
                    gsm_location_id = doc.GetElementsByTagName("gsm_location_id")[0].InnerXml;
                }
                if (doc.GetElementsByTagName("gsm_rssi_level")[0] != null)
                {
                    gsm_rssi_level = doc.GetElementsByTagName("gsm_rssi_level")[0].InnerXml;
                }
                msg.tag = string.Format(@"Q,0,0,{1},{0},0,0,{2}", gsm_cell_id, gsm_location_id, gsm_rssi_level);

                msg.conInfo = new ConnectionInfo();
                msg.conInfo.source_ip = "0.0.0.0";
                msg.conInfo.source_port = "0";
                msg.conInfo.destination_ip = gCFG.LocalIp;
                msg.conInfo.destination_port = gCFG.LocalPort;
                msg.conInfo.type_of_box = "G";

                // forward Q to server
                BroadcastQ(msg);
                EnqueueDataQueue(msg, RECHECKTYPE.RECHECK);
            }
            catch
            {
            }
        }
        private string GetTime()
        {
            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                switch (timeType)
                {
                    case TimeType.Local:
                        time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", new System.Globalization.CultureInfo("en-US")); 
                        break;
                    case TimeType.DataBase:
                        DataTable dt = sql.GetServerTeimestamp(); //time server
                        if (dt.Rows.Count > 0)
                        {
                            //utc+7[1]
                            //utc[2]
                            time = (string)dt.Rows[0]["local_timestamp_string"];
                            testTime = time;
                        }
                        break;
                    case TimeType.Web:
                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(WebServer);
                        HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                        DateTime d = Convert.ToDateTime(resp.Headers["Date"]);
                        time = d.ToString("yyyy-MM-dd HH:mm:ss", new System.Globalization.CultureInfo("en-US"));
                        resp.Close();
                        break;
                }
            }
            catch
            { }
            return time;
        }
   
        private DateTime GetTimeStamp(String date, String time)
        {
            DateTime timestamp = new DateTime();
            try
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                timestamp = d.AddHours(7.0);

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
            return timestamp;
        }
        private string GetDexToHex(string zDecStr)
        {
            string retHex = zDecStr;
            try
            {
                int dec = 0;
                if (!(int.TryParse(zDecStr, out dec)))
                {
                    return retHex;
                }
                retHex = string.Format("{0:X}", dec);
            }
            catch
            {
            }
            return retHex;
        }
        private String IOPortValue(String PortID, String PortValue, int power_level)
        {
            String szIO = "00";
            if (power_level < 2)
            {
                szIO = "00";
                switch (PortID)
                {
                    case "3": if (PortValue == "1") szIO = "10"; break;
                }
            }
            else
            {
                szIO = "80";
                switch (PortID)
                {
                    case "3": if (PortValue == "1") szIO = "90"; break;
                }
            }
            return szIO;
        }
        private String GetPowerValue(int leng, string power_level)
        {
            String szIO = "00";
            try
            {
                switch (leng)
                {
                    case 1:
                        {
                            if (power_level.Length == 2)
                            {
                                szIO = power_level + "0";
                            }
                        }
                        break;
                    case 2:
                        {
                            if (power_level.Length == 2)
                            {
                                szIO = power_level + "00";
                            }
                        }
                        break;
                }
            }
            catch
            { }

            return szIO;
        }
        #endregion
        #region Check Signal && Create Event
        private void tmCheckLostsignal_Tick(object sender, EventArgs e)
        {
            tmCheckLostsignal.Stop();
            try
            {
                gTimeSetver = Convert.ToDateTime(GetTime());
                EnqueueDataQueue(new Max2FmtMsg(), RECHECKTYPE.LOSTSIGNAL);
                tmCheckLostsignal.Interval = 1000 * 60 * 2;
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
            tmCheckLostsignal.Start();
        }
        private void DataQueueFunction()
        {
            while (BeginReceive)
            {
                try
                {
                    if (_dataQueue.Count > 0)
                    {
                        DATAQUEUE info = (DATAQUEUE)_dataQueue.Dequeue();
                        switch (info.type)
                        {
                            case RECHECKTYPE.RECHECK:
                                {
                                    UpDateLastConnect(info.veh_data);
                                    break;
                                }
                            case RECHECKTYPE.LOSTSIGNAL:
                                {
                                    RecheckLostSignal();
                                    break;
                                }
                        }
                    }
                    Thread.Sleep(10);
                }
                catch
                {
                }
            }
        }
   
        private void UpDateLastConnect(Max2FmtMsg msg)
        {
            string key = msg.vid.Trim();
            if (!vehHashInfo.ContainsKey(key))
            {
                VEHINFO data = new VEHINFO();
                data.veh_id = msg.vid.Trim(); 
                //*data.last_connect_server = DateTime.Now;
                data.last_connect_server = gTimeSetver;
                data.last_engineon = sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id));
                data.is_chang_data = false;
                data.is_recheck = false;
                data.Max2FM = msg;
                vehHashInfo.Add(key, data);
            }
            else
            {
                VEHINFO data = (VEHINFO)vehHashInfo[key];

                //check last record engine on-off from msg "I","+"
                if (data.is_recheck)
                {
                    data.last_engineon = sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id));
                    data.is_recheck = false;
                }

               //* TimeSpan sp = gTimeSetver - DateTime.Now - data.last_connect_server;
                TimeSpan sp = gTimeSetver - data.last_connect_server;
                if (sp.TotalSeconds >= 0 && sp.TotalMinutes <= 5)
                {
                    #region
                    if (data.last_engineon)
                    {
                        //normal status  add time and end process
                        //*data.last_connect_server = DateTime.Now;
                        data.last_connect_server =gTimeSetver;
                    }
                    //recheck engine off is sure && type of msg not I and reset
                    else if (!data.last_engineon && (msg.type_of_message != "I" && msg.type_of_message != "+"))
                    {
                        //��������ҧ�ͧ���� �����ҡѺ�Դ����ͧ ��������
                        bool bZeroRow = false;
                        bool en = sClass.checkTimeinterval(data.veh_id, 22, out bZeroRow);

                        if (bZeroRow)
                        {
                            data.last_engineon = sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id));
                        }
                        //������ҧ������ҡѺ 2 �ҷ�
                        if (en)
                        {
                            //ʶҹ�����ش��ͧ�Ѻ����ͧ+������ҧ������ҡѺ 2 �ҷ�
                            if (!sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id)))
                            {
                                //create evt_engine on
                                data.Max2FM.type_of_message = "C";
                                data.Max2FM.gpio_status = "10"; //engine on
                                data.last_engineon = true;
                               //* data.last_connect_server = DateTime.Now;
                                data.last_connect_server = gTimeSetver;
                                CreateEngineEvent(data);
                            }
                            else
                            {
                                data.last_engineon = true;
                            }
                        }
                    }
                    #endregion
                }
                else if (sp.TotalMinutes > 27 && sp.TotalMinutes < 33)
                {
                    #region
                    if (!data.last_engineon)
                    {
                        //normal status add time and end process
                        //*data.last_connect_server = DateTime.Now;
                        data.last_connect_server = gTimeSetver;
                    }
                    //recheck engine on is sure && type of msg not I and reset
                    else if (data.last_engineon && msg.type_of_message != "I" && msg.type_of_message != "+")
                    {
                        //��������ҧ�ͧ���� �����ҡѺ�Ѻ����ͧ��������
                        bool bZerorow = false;
                        //�礢�������͹��ѧ������ôѺ����ͧ��ԧ�������
                        bool en = sClass.checkTimeinterval(data.veh_id, 23, out bZerorow);

                        if (bZerorow)
                        {
                            data.last_engineon = sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id));
                        }
                        //ʶҹ�����ش��ͧ�Դ����ͧ + ������ҧ������ҡѺ 30 �ҷ�
                        //recheck engine on is sure
                        if (en)
                        {
                            if (sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id)))
                            {
                                //create evt_engine off
                                data.Max2FM.type_of_message = "C";
                                data.Max2FM.gpio_status = "0"; //engine off
                                data.last_engineon = false;
                                //*data.last_connect_server = DateTime.Now;
                                data.last_connect_server = gTimeSetver;

                                //check interval time
                                CreateEngineEvent(data);
                            }
                            else
                            {
                                data.last_engineon = false;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    if (sp.TotalMinutes > 33 && sp.TotalMinutes < 60)
                    {
                        data.last_engineon = sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id));
                    }
                }

                if (msg.type_of_message == "I" || msg.type_of_message == "+")
                {
                    data.is_recheck = true;
                }
                
              //*  data.last_connect_server = DateTime.Now;
                data.last_connect_server = gTimeSetver;
                data.Max2FM = msg;

                vehHashInfo[key] = data;
            }
        }
        private void RecheckLostSignal()
        {
            try
            {
                if (vehHashInfo.Count < 1) { return; }

                string rcKey = "";
                foreach (string stKey in vehHashInfo.Keys)
                {
                    rcKey += stKey + ",";
                }

                rcKey = rcKey.Substring(0, rcKey.Length - 1);

                foreach (string key in rcKey.Split(new char[] { ',' }))
                {
                    if (vehHashInfo.ContainsKey(key))
                    {
                        VEHINFO data = (VEHINFO)vehHashInfo[key];
                        //last connect server && check from ZBDB
                        //* TimeSpan sp = DateTime.Now - data.last_connect_server;
                        //* TimeSpan sp1 = DateTime.Now - sql.GetLastTimestamp(Convert.ToInt32(data.veh_id));
                        TimeSpan sp = gTimeSetver - data.last_connect_server;
                        TimeSpan sp1 = gTimeSetver - sql.GetLastTimestamp(Convert.ToInt32(data.veh_id));

                        if ((sp.TotalMinutes > 35 && sp.TotalMinutes < 60) && (sp1.TotalMinutes > 35 && sp1.TotalMinutes < 60))
                        {
                            //recheck engine on is sure
                            if (sql.GetLastEvtEngine(Convert.ToInt32(data.veh_id)))
                            {
                                //insert engine off
                                data.Max2FM.type_of_message = "c";
                                data.Max2FM.gpio_status = "0";
                                //  data.last_engineon = false;

                                CreateEngineEvent(data);
                                vehHashInfo.Remove(data.veh_id);
                            }
                        }
                        else if ((sp.TotalMinutes > 60) && (sp1.TotalMinutes > 60))
                        {
                            vehHashInfo.Remove(data.veh_id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }
        private void CreateEngineEvent(VEHINFO data)
        {
            try
            {
                //*DateTime timestamp = DateTime.Now.AddHours(-7);
                DateTime timestamp = gTimeSetver.AddHours(-7);
                String time_gps = timestamp.ToString("ddMMyy,HHmmss", new CultureInfo("en-US"));
                String[] seqMsg = time_gps.Split(',');

                Max2FmtMsg msg = new Max2FmtMsg();

                msg.vid = data.veh_id.ToString();
                msg.gps_date_time = seqMsg[0];
                msg.utc = seqMsg[1];
                msg.lat = data.Max2FM.lat;
                msg.lon = data.Max2FM.lon;
                msg.speed = "0";
                msg.course = data.Max2FM.course;
                msg.type_of_fix = data.Max2FM.type_of_fix;
                msg.no_of_satellite = data.Max2FM.no_of_satellite;
                msg.gpio_status = data.Max2FM.gpio_status;
                msg.analog_level = data.Max2FM.analog_level;
                msg.type_of_message = data.Max2FM.type_of_message;

                BroadcastQ(msg);
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }
        #endregion
        #region EnQueue && Broadcast && Print and write log
        System.Threading.Semaphore dataQueueSemaphor = new System.Threading.Semaphore(1, 1);
        private void EnqueueDataQueue(Max2FmtMsg msg, RECHECKTYPE rcType)
        {
            dataQueueSemaphor.WaitOne();
            DATAQUEUE info = new DATAQUEUE();
            info.type = rcType;
            info.veh_data = msg;
            _dataQueue.Enqueue(info);
            dataQueueSemaphor.Release();
        }
        System.Threading.Semaphore broadSemaphor = new System.Threading.Semaphore(1, 1);
        private void BroadcastQ(Max2FmtMsg msg)
        {
            broadSemaphor.WaitOne();
            try
            {
                String Log_msg = "$" +
                  msg.vid + "," +
                  msg.gps_date_time + "," +
                  msg.utc + "," +
                  msg.lat + "," +
                  msg.lon + "," +
                  msg.speed + "," +
                  msg.course + "," +
                  msg.type_of_fix + "," +
                  msg.no_of_satellite + "," +
                  msg.gpio_status + "," +
                  msg.analog_level + "," +
                  msg.type_of_message + "," +
                  msg.odometor + "," +
                  msg.internal_power + "," +
                  msg.external_power;

                //msg.conInfo.type_of_box = "G";
                //msg.conInfo.source_ip = "0.0.0.0";
                //msg.conInfo.source_port = "0";
                //msg.conInfo.destination_ip = "0.0.0.0";
                //msg.conInfo.destination_port = "0";


                foreach (CommonQeue info in max2Pushq)
                {
                    try
                    {
                        info.PushQ(msg);
                        Thread.Sleep(10);
                    }
                    catch (Exception ex)
                    {
                        sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
                    }
                }
                //sClass.GBoxStatus(msg);
                _logmsgQ.Enqueue(Log_msg + "\n");
            }
            catch (Exception ex)
            {
                sql.WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
            broadSemaphor.Release();
        }
        private void updateLog_Tick(object sender, EventArgs e)
        {
            System.Windows.Forms.Timer tm = (System.Windows.Forms.Timer)sender;
            tm.Stop();
            try
            {
                if (_logmsgQ.Count > 0)
                {
                    String log = _logmsgQ.Dequeue();
                    if (this.WindowState != FormWindowState.Minimized)
                    {
                        UpdateRichTxt(log);
                    }
                    sClass.WriteLog(log);

                    tsTimeServer.Text = "  GetTimeServer : " + testTime;
                }
                if (BeginReceive)
                {
                    if (this.WindowState != FormWindowState.Minimized)
                    {
                        toolStripStatusLabel1.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
                        tsVehOnline.Text = vehHashInfo.Count.ToString();
                        rtTotalData.Text = total_of_data.ToString();
                        rtLogShow.Text = _logmsgQ.Count.ToString();
                    }
                }
                else
                {
                    toolStripStatusLabel1.Text = "Stop BeginReceive Thread. Plase wait. . .";
                }
            }
            catch
            { }
            tm.Start();
        }
        private void UpdateRichTxt(String msg)
        {
            String[] strLn = msg.ToString().Split(new char[] { '\n' });
            foreach (String sLn in strLn)
            {
                string[] str = sLn.Split(new char[] { ',' });
                if (str[0].Length < 1 || str == null || str.Length < 1) return;

                str[0] = str[0].Substring(str[0].IndexOf('$') + 1);
                richTextBox1.Text = sLn + "\n" + richTextBox1.Text;

                int nRow = ((int)(richTextBox1.ClientSize.Height / richTextBox1.Font.GetHeight()));
                if (richTextBox1.Lines.Length > nRow)
                {
                    int iPos = 0;
                    int iIdx = -1;
                    for (int i = 0; i < nRow; i++)
                    {
                        if (iPos < richTextBox1.Text.Length)
                        {
                            iIdx = richTextBox1.Text.IndexOf('\n', iPos);
                            if (iIdx > -1)
                            {
                                iPos = iIdx + 1;
                            }
                        }
                    }
                    if (iPos > 0)
                    {
                        richTextBox1.Text = richTextBox1.Text.Remove(iPos - 1);
                    }
                }
            }
        }
        #endregion
        #region General Function
        private void InitGboxForwardQ(String QPathStr)
        {
            if (QPathStr == null || QPathStr.Trim().Length < 1)
            {
                return;
            }

            String GlobalQPath = String.Format(".\\Private$\\{0}", QPathStr);
            if (MessageQueue.Exists(GlobalQPath))
            {
                GBOXFWDMQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                GBOXFWDMQ = MessageQueue.Create(GlobalQPath);
            }
        }
        private void MainFrm_Load(object sender, EventArgs e)
        {
        }
        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                e.Cancel = BeginReceive;
             

                if (BeginReceive)
                {
                    if (vehHashInfo.Count > 0)
                    {

                        string rcKey = "";
                        foreach (string stKey in vehHashInfo.Keys)
                        {
                            rcKey += stKey + ",";
                        }

                        rcKey = rcKey.Substring(0, rcKey.Length - 1);
                        PushQBufferVehLog(rcKey.Split(','));
                    }
                }

                BeginReceive = false;
            }
            catch
            { }
        }
        private void connecttionToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (BeginReceive)
            {
                Application.Exit();
            }
            else
            {
                Process.GetCurrentProcess().Kill();
            }
        }
        private void openLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string logPath = Directory.GetCurrentDirectory() + "\\Log";
            string windir = Environment.GetEnvironmentVariable("WINDIR");
            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = windir + @"\explorer.exe";
            prc.StartInfo.Arguments = logPath;
            prc.Start();
        }
        private void openConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("notepad.exe", "PortMonitorCfg.xml");
        }
        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InitLocalSystem();
        }
        #endregion
    }
}