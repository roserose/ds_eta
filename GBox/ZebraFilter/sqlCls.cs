using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace ZebraFilter
{
    class sqlCls
    {
        //private string dbConn = "Data Source = 10.0.10.70; Initial Catalog = ZebraDB; uid = sa; pwd = aab-gps-gtt";
        private string dbConn = string.Empty;

        public sqlCls(string db)
        {
            dbConn = string.Format(@"Data Source = {0}; Initial Catalog = ZebraDB; uid = sa; pwd = aab-gps-gtt", db);
        }

        public void InsertNewGBoxStatus(int zVehID, string zIP, int zPort, bool isConnected, int zLocalPort)
        {
            try
            {
                SqlConnection cn = new SqlConnection(dbConn);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "spBox_Gbox_InsertNewStatus";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", zVehID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@ip", zIP);
                cm.Parameters.Add(p);

                p = new SqlParameter("@port", zPort);
                cm.Parameters.Add(p);

                p = new SqlParameter("@isConnected", isConnected);
                cm.Parameters.Add(p);

                p = new SqlParameter("@local_port", zLocalPort);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
            }
        }
        
        public void InsertNewEngineStatus(int zVehID, DateTime zTime, int zEvtID, String remark)
        {
            try
            {
                SqlConnection cn = new SqlConnection(dbConn);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "spLogMsg_InsertCreateEngineStatus";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", zVehID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@timestamp", zTime);
                cm.Parameters.Add(p);

                p = new SqlParameter("@evt_id", zEvtID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@remark", remark);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
                //WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
        }

        public bool GetLastEvtEngine(int zVehID)
        {
            bool last_engine_on = false;

            try
            {
                DataTable dtRet = new DataTable();

                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastEngineOnStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);

                    cn.Close();

                    if (dtRet.Rows.Count > 0)
                    {
                        if ((int)dtRet.Rows[0]["evt_id"] == 22)
                        {
                            last_engine_on = true;
                        }
                    }
                }
            }
            catch
            {
            }

            return last_engine_on;
        }
        //sql2 ok
        public DateTime GetLastTimestamp(int zVehID)
        {
            DateTime local_timestamp = new DateTime();
        
            try
            {
                DataTable dtRet = new DataTable();

                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFT_GetLastTimestamp";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);

                    cn.Close();

                    if (dtRet.Rows.Count > 0)
                    {
                        local_timestamp = (DateTime)dtRet.Rows[0]["local_timestamp"];
                    }
                }
            }
            catch
            {
            }

            return local_timestamp;
        }

        //sql2 ok
        public DataTable GetDataInterval(int zVehID)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFT_GetDataInterval";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);

                    cn.Close();
                }
            }
            catch
            {
            }

            return dtRet;
        }

        public int GetLastIOStatus(int veh_id)
        {
            int state = 0;
            int ref_idx = 0;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetLastIOStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (int)cm.Parameters["@ref_idx"].Value;
                    }
                    catch
                    {
                    }

                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                WriteLogError(ex.TargetSite + "-\n" + ex.StackTrace + "-\n" + ex.Message);
            }
            return state;
        }

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        public void WriteLogError(String ErrorMsg)
        {
            try
            {
                semaphor.WaitOne();
                String logPath = Directory.GetCurrentDirectory() + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                String fileName = logPath + "\\" + DateTime.Now.ToString("yyyyMM") + ".txt";

                String Msg = DateTime.Now.ToString() + "," + ErrorMsg;

                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                semaphor.Release();
            }
            catch { }
        }

        //sql2 ok
        public DataTable GetServerTeimestamp()
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "SP_GetServerTimeStamp";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;
                    try
                    {
                        SqlDataAdapter adp = new SqlDataAdapter(cm);
                        adp.Fill(dtRet);
                    }
                    catch
                    {
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return dtRet;
        }
    }
}
