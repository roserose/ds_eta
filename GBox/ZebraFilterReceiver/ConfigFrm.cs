﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZebraFilterReceiver
{
    public partial class ConfigFrm : Form
    {
        private MainFrm.CFG _CFG = new MainFrm.CFG();
        public MainFrm.CFG CFG
        {
            set { _CFG = value; }
            get { return _CFG; }
        }
         
        public ConfigFrm()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            _CFG.MessageQueue = this.tbQueueuNane.Text;
            _CFG.ReportPort = this.tbPort.Text;
            _CFG.ProgramHeader = this.tbHeader.Text;
        }

        private void ConfigFrm_Load(object sender, EventArgs e)
        {
            this.tbQueueuNane.Text = _CFG.MessageQueue;
            this.tbPort.Text = _CFG.ReportPort;
            this.tbHeader.Text = _CFG.ProgramHeader;
        }
    }
}
