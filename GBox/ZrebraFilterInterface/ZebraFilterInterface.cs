using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace ZrebraFilterInterface
{
    [Serializable]
    public enum EVT_TYPE
    {
        NONE = -999,
        TRUE_IOSTATUS = -7,
        TRUE_ZONEING = -6,
        TRUE_ALERT_ZONE2 = -5,
        TEST_ZONE_ALERT = -4,
        TRUEMSG2 = -3,
        TRUEMSG = -2,
        GENERIC2 = -1,
        GENERIC = 0,
        RESET = 1,
        SPEEDING = 2,
        IDLE = 3,
        HAZARD_SPEED = 4,
        HAZARD_ANGLE = 5,
        DIGITAL_SENSOR = 8,
        ANALOG_SENSOR = 9,
        LOCATION = 12,
        IN_ZONE = 17,
        OUT_ZONE = 18,
        IN_ZONE_CURFEW = 19,
        IN_ROUTE = 20,
        OUT_ROUTE = 21,
        ENGINE_ON = 22,
        ENGINE_OFF = 23,
        NO_INZONE_CURFEW = 25,
        NO_OUTZONE_CURFEW = 26,
        TEMPERATURE = 27,
        SMARTCARD_DATA = 28,
        ENGINE_ON_IN_PROHITBIT_TIME = 29,
        EVENT_GPS_DISCONNECTED = 33,
        EVENT_USER_DEFINED1_ON = 34,
        EVENT_USER_DEFINED1_OFF = 35,
        EVENT_USER_DEFINED2_ON = 36,
        EVENT_USER_DEFINED2_OFF = 37,
        EVENT_POWER_LINE_DISCONNECTED = 38,
        EVENT_IGNITION_LINE_ON = 40,
        EVENT_IGNITION_LINE_OFF = 41,
        EVENT_USER_DEFINED1_OUT_ON = 42,
        EVENT_USER_DEFINED1_OUT_OFF = 43,
        EVENT_USER_DEFINED2_OUT_ON = 44,
        EVENT_USER_DEFINED2_OUT_OFF = 45,
        EVENT_USER_DEFINED3_OUT_ON = 46,
        EVENT_USER_DEFINED3_OUT_OFF = 47,
        ENGINE_ON_OFF_IN_OUT_ZONE = 49,
        ENGINE_ON_IN_ZONE = 50,
        ENGINE_OFF_IN_ZONE = 51,
        ENGINE_ON_OUT_ZONE = 52,
        ENGINE_OFF_OUT_ZONE = 53,
        EVENT_GPS_CONNECTED = 54,
        EVENT_POWER_LINE_CONNECTED = 55,
        EVENT_DFM_MAIN_LIGHT_ON = 56,
        EVENT_DFM_MAIN_LIGHT_OFF = 57,
        EVENT_DFM_PTO_SWITCH_ON = 58,
        EVENT_DFM_PTO_SWITCH_OFF = 59,
        EVENT_DFM_HORN_ON = 60,
        EVENT_DFM_HORN_OFF = 61,
        EVENT_DFM_MINOR_LIGHT_ON = 62,
        EVENT_DFM_MINOR_LIGHT_OFF = 63,
        EVENT_DFM_LEFT_SIGNAL_ON = 64,
        EVENT_DFM_LEFT_SIGNAL_OFF = 65,
        EVENT_DFM_RIGHT_SIGNAL_ON = 66,
        EVENT_DFM_RIGHT_SIGNAL_OFF = 67,
        EVENT_DFM_WIPER_ON = 68,
        EVENT_DFM_WIPER_OFF = 69,
        EVENT_DFM_REVERSE_LIGHT_ON = 70,
        EVENT_DFM_REVERSE_LIGHT_OFF = 71,
        NO_ZONE_CURFEW_WITH_SPEED = 73,
        NO_INZONE_CURFEW_WITH_SPEED = 74,
        NO_OUTZONE_CURFEW_WITH_SPEED = 75,
        FUEL_OIL = 76,
        FUEL_OIL_ON = 77,
        FUEL_OIL_OFF = 78,
        FUEL_GAS = 79,
        FUEL_GAS_ON = 80,
        FUEL_GAS_OFF = 81,
        EXTERNAL_INPUT01_ON = 82,
        EXTERNAL_INPUT01_OFF = 83,
        EXTERNAL_INPUT02_ON = 84,
        EXTERNAL_INPUT02_OFF = 85,
        EXTERNAL_INPUT03_ON = 86,
        EXTERNAL_INPUT03_OFF = 87,
        EXTERNAL_INPUT04_ON = 88,
        EXTERNAL_INPUT04_OFF = 89,
        EXTERNAL_INPUT05_ON = 90,
        EXTERNAL_INPUT05_OFF = 91,
        EXTERNAL_INPUT06_ON = 92,
        EXTERNAL_INPUT06_OFF = 93,
        SOS = 94,
        BUZZER = 95,
        BUZZER_ON = 96,
        BUZZER_OFF = 97,
        LED = 98,
        LED_ON = 99,
        LED_OFF = 100,
        ENGINE_CONTROL = 101,
        ENGINE_CONTROL_ON = 102,
        ENGINE_CONTROL_OFF = 103,
        START_ROUTE = 104,
        PARKING = 105,
        OFF_LOAD = 106,
        END_ROUTE = 107,
        LOADING = 108,
        UNLOADING = 109,
        FULL_LOAD = 110,
        RESUME = 111,
        DOOR_OPEN = 112,
        DOOR_CLOSE = 113,
        VALID_CARD = 114,
        INVALID_CARD = 115,
        NO_CARD = 116,
        CARD_DATA_ERROR = 117,
        EVENT_USER_DEFINED3_ON = 118,
        EVENT_USER_DEFINED3_OFF = 119,
        EVENT_USER_DEFINED4_ON = 120,
        EVENT_USER_DEFINED4_OFF = 121,
        POWER_UNPLUGED_6HR = 122,
        PTO_ON = 123,
        PTO_OFF = 124,
        OUT_ZONE_CURFEW = 125,
        EVENT_USER_DEFINED5_ON = 126,
        EVENT_USER_DEFINED5_OFF = 127,
        EVENT_USER_DEFINED6_ON = 128,
        EVENT_USER_DEFINED6_OFF = 129,
        EVENT_USER_DEFINED7_ON = 130,
        EVENT_USER_DEFINED7_OFF = 131,
        EVENT_USER_DEFINED8_ON = 132,
        EVENT_USER_DEFINED8_OFF = 133,
        USER_DEFINED_4_OUTPUT_ON = 134,
        USER_DEFINED_4_OUTPUT_OFF = 135,
        USER_DEFINED_5_OUTPUT_ON = 136,
        USER_DEFINED_5_OUTPUT_OFF = 137,
        USER_DEFINED_6_OUTPUT_ON = 138,
        USER_DEFINED_6_OUTPUT_OFF = 139,
        USER_DEFINED_7_OUTPUT_ON = 140,
        USER_DEFINED_7_OUTPUT_OFF = 141,
        USER_DEFINED_8_OUTPUT_ON = 142,
        USER_DEFINED_8_OUTPUT_OFF = 143,
        TURN_LEFT_LIGHT_ON = 144,
        TURN_LEFT_LIGHT_OFF = 145,
        TURN_RIGHT_LIGHT_ON = 146,
        TURN_RIGHT_LIGHT_OFF = 147,
        BREAK = 148,
        UNBREAK = 149,
        SAFETY_BELT = 150,
        UNSAFETY_BELT = 151,
        JAMMER = 152,
        UNLOCK_ACCELERATOR = 153,
        LOCK_ACCLERATOR = 154,
        DISTANCE_OVER = 155,
        OTA = 997,
        CALLCENTER = 998,
        WATCHDOG = 999,
    }
    [Serializable]

    //public struct ZEBRASMSDISTANCE
    //{
    //    public int type;
    //    public int idx;
    //    public int veh_id;
    //    public string msg;
    //    public string timestamp;
    //    public double lat;
    //    public double lon;
    //}
    public struct ZEBRABROADCASTMQINFO
    {
        public int type;
        public int idx;
        public int veh_id;
        public int fleet_id;
        public string msg;
        public string timestamp;
        public string location_t;
        public string location_e;
        public double lat;
        public double lon;
        public int speed;
    }

    [Serializable]
    public struct ConnectionInfo
    {
        public String source_ip;
        public String source_port;
        public String destination_ip;
        public String destination_port;
        public String type_of_box;
    }

    [Serializable]
    public struct Max2FmtMsg
    {
        public String header;
        public String vid;
        public String gps_date_time;
        public String utc;
        public String lat;
        public String lon;
        public String speed;
        public String course;
        public String type_of_fix;
        public String no_of_satellite;
        public String gpio_status;
        public String analog_level;
        public String type_of_message;
        public String tag;
        public String odometor;
        public String internal_power;
        public String external_power;
        public ConnectionInfo conInfo;
    }

   [Serializable]
    public struct LogInfo
    {
        public String data_type;
        public String data;
        public String time;
        public String source;
        public String destination;
    }
    public interface ReceiverInterface
    {
        bool IsAlive();
        void PushMax2Q(Max2FmtMsg msg);
        void PushBroadCastQ(ZEBRABROADCASTMQINFO msg);
        void PushLogInfo(LogInfo msg);
        interface_debug[] debug(Max2FmtMsg msg);
        //void PushDistanceOverQ(ZEBRASMSDISTANCE msg);
    }

    public class interface_debug 
    {
        public interface_debug() {
            timestamp = DateTime.Now;
        }
        public string name { set; get; }
        public DateTime timestamp { private set; get; }
    }

}
