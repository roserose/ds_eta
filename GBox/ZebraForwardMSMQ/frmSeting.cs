using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraForwardMSMQ
{
    public partial class Seting : Form
    {
        public DataTable dtXml;
        public Seting()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            dtXml.Rows[0]["CbIPSecond"] = this.cbEnableIP2.Checked.ToString();
            dtXml.Rows[0]["IPFirst"] = this.ipTextBox1.Text;
            dtXml.Rows[0]["IPSecond"] = this.ipTextBox2.Text;
            dtXml.Rows[0]["Port"] = this.mtbPort.Text;
            dtXml.Rows[0]["SystemQueue"] = this.tbSystemQ.Text;
            dtXml.Rows[0]["Max2Format"] = this.rbMax2.Checked.ToString();
            dtXml.Rows[0]["BroadCastFormat"] = this.rbBroadCast.Checked.ToString();
            dtXml.Rows[0]["LogFormat"] = this.rbLogData.Checked.ToString();
            dtXml.Rows[0]["TmInterval"] = this.mtbTimer.Text;
            dtXml.Rows[0]["TextHead"] = this.tbHeader.Text;
        }

        private void Seting_Load(object sender, EventArgs e)
        {
            foreach (DataRow dr in dtXml.Rows)
            {
                bool is_Enable = Convert.ToBoolean((string)dr["CbIPSecond"]);
                if (this.cbEnableIP2.Checked == is_Enable)
                {
                    this.cbEnableIP2.Checked = !this.cbEnableIP2.Checked;
                    this.cbEnableIP2.Checked = is_Enable;
                }
                else
                {
                    this.cbEnableIP2.Checked = is_Enable;
                }

                this.ipTextBox1.Text = (string)dr["IPFirst"];
                this.ipTextBox2.Text = (string)dr["IPSecond"];
                this.mtbPort.Text = (string)dr["Port"];
                this.tbSystemQ.Text = (string)dr["SystemQueue"];
                this.mtbTimer.Text = (string)dr["TmInterval"];
                this.tbHeader.Text = (string)dr["TextHead"];
                this.rbMax2.Checked = Convert.ToBoolean((string)dr["Max2Format"]);
                this.rbBroadCast.Checked = Convert.ToBoolean((string)dr["BroadCastFormat"]);
                this.rbLogData.Checked = Convert.ToBoolean((string)dr["LogFormat"]);
            }
        }
        private void mtb_TextChanged(object sender, System.EventArgs e)
        {
            MaskedTextBox mtb = (MaskedTextBox)sender;
            int number = 0;
            if (Int32.TryParse(mtb.Text.Trim(), out number))
            {
                mtb.Text = number.ToString();
                return;
            }
            else
            {
                string stNumber = mtb.Text;
                if (Int32.TryParse(stNumber.Replace(" ", ""), out number))
                {
                    mtb.Text = number.ToString();
                }
            }
        }
        private void MaskedTextBox_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            int number = 0;
            MaskedTextBox mtb = (MaskedTextBox)sender;
            if (Int32.TryParse(mtb.Text, out number)) { return; }
        }
        private void cbEnableIP2_CheckedChanged(object sender, System.EventArgs e)
        {
            if (this.cbEnableIP2.Checked)
            {
                this.ipTextBox2.Enabled = true;
            }
            else
            {
                this.ipTextBox2.Enabled = false;
            }
        }
    }
}