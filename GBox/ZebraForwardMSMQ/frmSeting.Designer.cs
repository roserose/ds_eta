namespace ZebraForwardMSMQ
{
    partial class Seting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Seting));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbLogData = new System.Windows.Forms.RadioButton();
            this.rbBroadCast = new System.Windows.Forms.RadioButton();
            this.rbMax2 = new System.Windows.Forms.RadioButton();
            this.btSave = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.mtbTimer = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbHeader = new System.Windows.Forms.TextBox();
            this.mtbPort = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbSystemQ = new System.Windows.Forms.TextBox();
            this.cbEnableIP2 = new System.Windows.Forms.CheckBox();
            this.ipTextBox2 = new iptb.IPTextBox();
            this.ipTextBox1 = new iptb.IPTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbLogData);
            this.groupBox1.Controls.Add(this.rbBroadCast);
            this.groupBox1.Controls.Add(this.rbMax2);
            this.groupBox1.Controls.Add(this.btSave);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.mtbTimer);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbHeader);
            this.groupBox1.Controls.Add(this.mtbPort);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tbSystemQ);
            this.groupBox1.Controls.Add(this.cbEnableIP2);
            this.groupBox1.Controls.Add(this.ipTextBox2);
            this.groupBox1.Controls.Add(this.ipTextBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 287);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seting";
            // 
            // rbLogData
            // 
            this.rbLogData.AutoSize = true;
            this.rbLogData.Location = new System.Drawing.Point(10, 173);
            this.rbLogData.Name = "rbLogData";
            this.rbLogData.Size = new System.Drawing.Size(75, 17);
            this.rbLogData.TabIndex = 16;
            this.rbLogData.TabStop = true;
            this.rbLogData.Text = "LogFormat";
            this.rbLogData.UseVisualStyleBackColor = true;
            // 
            // rbBroadCast
            // 
            this.rbBroadCast.AutoSize = true;
            this.rbBroadCast.Location = new System.Drawing.Point(98, 150);
            this.rbBroadCast.Name = "rbBroadCast";
            this.rbBroadCast.Size = new System.Drawing.Size(106, 17);
            this.rbBroadCast.TabIndex = 15;
            this.rbBroadCast.TabStop = true;
            this.rbBroadCast.Text = "BroadCastFormat";
            this.rbBroadCast.UseVisualStyleBackColor = true;
            // 
            // rbMax2
            // 
            this.rbMax2.AutoSize = true;
            this.rbMax2.Location = new System.Drawing.Point(10, 150);
            this.rbMax2.Name = "rbMax2";
            this.rbMax2.Size = new System.Drawing.Size(83, 17);
            this.rbMax2.TabIndex = 14;
            this.rbMax2.TabStop = true;
            this.rbMax2.Text = "Max2Format";
            this.rbMax2.UseVisualStyleBackColor = true;
            // 
            // btSave
            // 
            this.btSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btSave.Location = new System.Drawing.Point(152, 251);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 13;
            this.btSave.Text = "&OK";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 202);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Timer Interval :";
            // 
            // mtbTimer
            // 
            this.mtbTimer.Location = new System.Drawing.Point(99, 199);
            this.mtbTimer.Mask = "000";
            this.mtbTimer.Name = "mtbTimer";
            this.mtbTimer.PromptChar = ' ';
            this.mtbTimer.Size = new System.Drawing.Size(128, 20);
            this.mtbTimer.TabIndex = 11;
            this.mtbTimer.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.MaskedTextBox_MaskInputRejected);
            this.mtbTimer.TextChanged += new System.EventHandler(this.mtb_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "HeaderProgram : ";
            // 
            // tbHeader
            // 
            this.tbHeader.Location = new System.Drawing.Point(99, 225);
            this.tbHeader.Name = "tbHeader";
            this.tbHeader.Size = new System.Drawing.Size(128, 20);
            this.tbHeader.TabIndex = 9;
            // 
            // mtbPort
            // 
            this.mtbPort.Location = new System.Drawing.Point(98, 95);
            this.mtbPort.Mask = "00000";
            this.mtbPort.Name = "mtbPort";
            this.mtbPort.PromptChar = ' ';
            this.mtbPort.Size = new System.Drawing.Size(128, 20);
            this.mtbPort.TabIndex = 8;
            this.mtbPort.ValidatingType = typeof(int);
            this.mtbPort.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.MaskedTextBox_MaskInputRejected);
            this.mtbPort.TextChanged += new System.EventHandler(this.mtb_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "System Queue :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Port :";
            // 
            // tbSystemQ
            // 
            this.tbSystemQ.Location = new System.Drawing.Point(98, 124);
            this.tbSystemQ.Name = "tbSystemQ";
            this.tbSystemQ.Size = new System.Drawing.Size(128, 20);
            this.tbSystemQ.TabIndex = 5;
            // 
            // cbEnableIP2
            // 
            this.cbEnableIP2.AutoSize = true;
            this.cbEnableIP2.Location = new System.Drawing.Point(10, 19);
            this.cbEnableIP2.Name = "cbEnableIP2";
            this.cbEnableIP2.Size = new System.Drawing.Size(125, 17);
            this.cbEnableIP2.TabIndex = 4;
            this.cbEnableIP2.Text = "Enable IP Second IP";
            this.cbEnableIP2.UseVisualStyleBackColor = true;
            this.cbEnableIP2.CheckedChanged += new System.EventHandler(this.cbEnableIP2_CheckedChanged);
            // 
            // ipTextBox2
            // 
            this.ipTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ipTextBox2.Location = new System.Drawing.Point(98, 71);
            this.ipTextBox2.Name = "ipTextBox2";
            this.ipTextBox2.Size = new System.Drawing.Size(128, 18);
            this.ipTextBox2.TabIndex = 3;
            this.ipTextBox2.ToolTipText = "";
            // 
            // ipTextBox1
            // 
            this.ipTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ipTextBox1.Location = new System.Drawing.Point(98, 43);
            this.ipTextBox1.Name = "ipTextBox1";
            this.ipTextBox1.Size = new System.Drawing.Size(128, 18);
            this.ipTextBox1.TabIndex = 2;
            this.ipTextBox1.ToolTipText = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "IP Second :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP First :";
            // 
            // Seting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 294);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(264, 332);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(256, 311);
            this.Name = "Seting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Seting";
            this.Load += new System.EventHandler(this.Seting_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbSystemQ;
        private System.Windows.Forms.CheckBox cbEnableIP2;
        private iptb.IPTextBox ipTextBox2;
        private iptb.IPTextBox ipTextBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox mtbPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbHeader;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox mtbTimer;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.RadioButton rbBroadCast;
        private System.Windows.Forms.RadioButton rbMax2;
        private System.Windows.Forms.RadioButton rbLogData;
    }
}