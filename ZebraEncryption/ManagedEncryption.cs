using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ZebraEncryption
{
    public enum EncryptionAlgorithm
    {
        None = 0,
        Des = 1,
        Rc2 = 2,
        RC4 = 3,
        Rijndael = 4,
        TripleDes = 5
    }

    public enum EncryptionDataType
    {
        None = 0,
        String = 1
    }

    public class ManagedEncryption
    {
        private EncryptionAlgorithm algorithmID;
        private byte[] initKey;
        private byte[] initVec;

        public ManagedEncryption(EncryptionAlgorithm deCryptId, byte[] deKey, byte[] deIV)
        {
            algorithmID = deCryptId;
            initKey = deKey;
            initVec = deIV;
        }

        public object InitializeSerializer(byte data)
        {
            return null;
        }

        public object InitializeDeserializer(byte[] byteData, int leng, EncryptionDataType DataType)
        {
            switch (DataType)
            {
                case EncryptionDataType.String:
                    {
                        DecryptTransformer decry = new DecryptTransformer(algorithmID, initKey, initVec);

                        if (algorithmID != EncryptionAlgorithm.None)
                        {
                            return decry.GetCryptoServiceProvider(CalByteEncryption(byteData, leng),leng);
                        }
                        else
                        {
                            return decry.GetCryptoServiceProvider(byteData,leng);
                        }
                    }
                default:
                    {
                        return null;
                    }
            }
        }
  
        private Byte[] CalByteEncryption(Byte[] data, int leng)
        {
            Byte[] buffer = new Byte[leng];
            //buffer = data;
            try
            {
                for (int i = 0; i < leng; i++)
                {
                    buffer[i] = data[i];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return buffer;
        }
    }
}
