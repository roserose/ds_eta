using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Collections;

namespace ZebraPresentationLayer
{
    #region MESSAGE
    enum WLOCATOR_BUFFER {REAL_TIME = 0 , BUFFER = 1}
    enum WLOCATOR_MSG_LOCATION
    {
        GPS_DATE_TIME = 0,
        TYPE_OF_FIX = 1,
        LAT = 2,
        LON = 3,
        ALTITUDE = 4,
        SPEED = 5,
        COURSE = 6,
        NO_OF_SATELLITE = 7,
        EXTERNAL_INPUT = 8,
        ANALOG_LEVEL = 9,
        VID = 10
    }
    //Kbox 3G
    enum WLOCATOR_MSG_LOCATION2
    {
        GPS_DATE_TIME = 0,
        TYPE_OF_FIX = 1,
        LAT = 2,
        LON = 3,
        ALTITUDE = 4,
        SPEED = 5,
        COURSE = 6,
        NO_OF_SATELLITE = 7,
        EXTERNAL_INPUT = 8,
        ANALOG_LEVEL = 9,
        IO_STATUS = 10,
        MILEAGE = 11,
        VID = 12
    }

    enum WLOCATOR_MSG_STATUS
    {
        GPS_DATE_TIME = 0,
        TYPE_OF_FIX = 1,
        LAT = 2,
        LON = 3,
        ALTITUDE = 4,
        SPEED = 5,
        COURSE = 6,
        NO_OF_SATELLITE = 7,
        EXTERNAL_INPUT = 8,
        ANALOG_LEVEL = 9,
        EVENT_TYPE = 10,
        EVENT_VALUE1 = 11,
        EVENT_VALUE2 = 12,
        EVENT_VALUE3 = 13,
        EVENT_VALUE4 = 14,
        EVENT_VALUE5 = 15,
        EVENT_VALUE6 = 16,
        EVENT_VALUE7 = 17
       // VID = 13
    }
    enum WLOCATOR_STATUSREPORT 
    {
        ALERT_IO = 1, 
        TEMPERATURE = 2, 
        BATTERY_STATUS = 3, 
        SERVICE_CALL = 4, 
        DEBUG_MESSAGE = 5, 
        SPEEDING = 6, 
        MILEAGE_REPORT = 7, 
        LICENSE_INFO = 8 
    }

    enum IOTYPE
    {
        ENGINEOFF = 0x00,
        ENGINEON = 0x10,

        USERINPUT1 = 0x20,
        USERINPUT2 = 0x40,
        POWER = 0x80,

        IGNITION = 0x01,     ///out1
        USEROUTPUT1 = 0x02,     ///out2 =   user1  
        USEROUTPUT2 = 0x04,     ///out3 =   user2
        USEROUTPUT3 = 0x08,      ///out4 =   user3          //not use

        EXTERNAL_INPUT01 = 0x01,
        EXTERNAL_INPUT02 = 0x02,
        EXTERNAL_INPUT03 = 0x04,
        EXTERNAL_INPUT04 = 0x08,
        EXTERNAL_INPUT05 = 0x10,
        EXTERNAL_INPUT06 = 0x20,
        EXTERNAL_INPUT07 = 0x40,
        EXTERNAL_INPUT08 = 0x80
    }
    #endregion

    public class WLocator
    {
        const char WLocatorL_HEADER = '$';
        const char WLocatorS_HEADER = '#';
        const char WLocatorBF_HEADER = '?';
  
        public WLocator()
        {
        }

        public object Translate(byte[] data)
         {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length > 0)
            {
                /// location
                if ((data[0] == WLocatorL_HEADER) || (data[0] == WLocatorBF_HEADER && data[1] == WLocatorL_HEADER))
                {
                    msg = TranslateMax2MsgLocation(data);

                    if (msg.vid == null) 
                        return null;
                }
                ///status
                else if ((data[0] == WLocatorS_HEADER) || ((data[0] == WLocatorBF_HEADER && data[1] == WLocatorS_HEADER)))   
                {
                    msg = TranslateMax2MsgStatus(data);

                    if (msg.vid == null)
                        return null;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            return msg;
        }

        private Max2FmtMsg TranslateMax2MsgLocation(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
           // if (data.Length <= 50) { return (WLocatorMsg)(new object()); }
            if (data.Length <= 43) { return msg; }   //check ex $020202000000,0,0.0,0.0,0.0,0.0,0.0,99.99,1
            try
            {
                String msgStr = Encoding.ASCII.GetString(data).Trim();
                String[] seqMsg = msgStr.Split(new char[] { ',', '\n' });

                if (seqMsg[0].Length < 2)
                {
                    return msg;
                }
                //�����͹� ������ 11 
                if (seqMsg.Length == 11)
                {
                    string gps_date_time = seqMsg[(int)WLOCATOR_MSG_LOCATION.GPS_DATE_TIME];
                    if (gps_date_time[0] == WLocatorBF_HEADER)
                    {
                        gps_date_time = seqMsg[(int)WLOCATOR_MSG_LOCATION.GPS_DATE_TIME].Substring(2, 6);
                        msg.utc = seqMsg[(int)WLOCATOR_MSG_LOCATION.GPS_DATE_TIME].Substring(8, 6);
                        msg.type_of_message = "*L";
                    }
                    else
                    {
                        //?121220025258,0,13.727285,100.540565,12.6,0.0,0.0,99.99,0,2841,1,ANTDETECT,1,756 
                        gps_date_time = seqMsg[(int)WLOCATOR_MSG_LOCATION.GPS_DATE_TIME].Substring(1, 6);
                        msg.utc = seqMsg[(int)WLOCATOR_MSG_LOCATION.GPS_DATE_TIME].Substring(7, 6);
                        msg.type_of_message = "L";
                    }

                    msg.header = "$";
                    msg.vid = seqMsg[(int)WLOCATOR_MSG_LOCATION.VID];
                    string yy = gps_date_time.Substring(0, 2);
                    string MM = gps_date_time.Substring(2, 2);
                    string dd = gps_date_time.Substring(4, 2);
                    msg.gps_date_time = dd + MM + yy;
                    msg.lat = seqMsg[(int)WLOCATOR_MSG_LOCATION.LAT] + "N";
                    msg.lon = seqMsg[(int)WLOCATOR_MSG_LOCATION.LON] + "E";
                    msg.speed = seqMsg[(int)WLOCATOR_MSG_LOCATION.SPEED];
                    msg.course = seqMsg[(int)WLOCATOR_MSG_LOCATION.COURSE];
                    msg.type_of_fix = seqMsg[(int)WLOCATOR_MSG_LOCATION.TYPE_OF_FIX];
                    msg.no_of_satellite = seqMsg[(int)WLOCATOR_MSG_LOCATION.NO_OF_SATELLITE];
                    msg.tag = "";
                    msg.odometor = "0";
                    msg.internal_power = "0";
                    msg.gpio_status = "00";
                    /// analog_level
                    string[] analog_level = (seqMsg[(int)WLOCATOR_MSG_STATUS.ANALOG_LEVEL]).Split('.');
                    if (analog_level.Length > 1)
                    {
                        msg.analog_level = analog_level[0] + analog_level[1];
                        if (analog_level[1].Length == 1) { msg.analog_level += "0"; }
                    }
                    else
                    {
                        msg.analog_level = "0";
                    }
                    /// external_power
                    string[] external_power = (seqMsg[(int)WLOCATOR_MSG_STATUS.EXTERNAL_INPUT]).Split('.');
                    if (external_power.Length > 1)
                    {
                        msg.external_power = external_power[0] + external_power[1];
                        if (external_power[1].Length == 1) { msg.external_power += "0"; }
                    }
                    else
                    {
                        msg.external_power = "0";
                    }
                }


                //������ 13
                if (seqMsg.Length == 13)
                {
                    string gps_date_time = seqMsg[(int)WLOCATOR_MSG_LOCATION2.GPS_DATE_TIME];
                    if (gps_date_time[0] == WLocatorBF_HEADER)
                    {
                        gps_date_time = seqMsg[(int)WLOCATOR_MSG_LOCATION2.GPS_DATE_TIME].Substring(2, 6);
                        msg.utc = seqMsg[(int)WLOCATOR_MSG_LOCATION2.GPS_DATE_TIME].Substring(8, 6);
                        msg.type_of_message = "*L";
                    }
                    else
                    {
                        //?121220025258,0,13.727285,100.540565,12.6,0.0,0.0,99.99,0,2841,1,ANTDETECT,1,756 
                        gps_date_time = seqMsg[(int)WLOCATOR_MSG_LOCATION2.GPS_DATE_TIME].Substring(1, 6);
                        msg.utc = seqMsg[(int)WLOCATOR_MSG_LOCATION2.GPS_DATE_TIME].Substring(7, 6);
                        msg.type_of_message = "L";
                    }

                    msg.header = "$";
                    msg.vid = seqMsg[(int)WLOCATOR_MSG_LOCATION2.VID];
                    string yy = gps_date_time.Substring(0, 2);
                    string MM = gps_date_time.Substring(2, 2);
                    string dd = gps_date_time.Substring(4, 2);
                    msg.gps_date_time = dd + MM + yy;
                    msg.lat = seqMsg[(int)WLOCATOR_MSG_LOCATION2.LAT] + "N";
                    msg.lon = seqMsg[(int)WLOCATOR_MSG_LOCATION2.LON] + "E";
                    msg.speed = seqMsg[(int)WLOCATOR_MSG_LOCATION2.SPEED];
                    msg.course = seqMsg[(int)WLOCATOR_MSG_LOCATION2.COURSE];
                    msg.type_of_fix = seqMsg[(int)WLOCATOR_MSG_LOCATION2.TYPE_OF_FIX];
                    msg.no_of_satellite = seqMsg[(int)WLOCATOR_MSG_LOCATION2.NO_OF_SATELLITE];
                    msg.tag = "";
                    msg.odometor = seqMsg[(int)WLOCATOR_MSG_LOCATION2.MILEAGE];
                    msg.internal_power = "0";
                    msg.gpio_status = seqMsg[(int)WLOCATOR_MSG_LOCATION2.IO_STATUS];
                    /// analog_level
                    string[] analog_level = (seqMsg[(int)WLOCATOR_MSG_STATUS.ANALOG_LEVEL]).Split('.');
                    if (analog_level.Length > 1)
                    {
                        msg.analog_level = analog_level[0] + analog_level[1];
                        if (analog_level[1].Length == 1) { msg.analog_level += "0"; }
                    }
                    else
                    {
                        msg.analog_level = "0";
                    }
                    /// external_power
                    string[] external_power = (seqMsg[(int)WLOCATOR_MSG_STATUS.EXTERNAL_INPUT]).Split('.');
                    if (external_power.Length > 1)
                    {
                        msg.external_power = external_power[0] + external_power[1];
                        if (external_power[1].Length == 1) { msg.external_power += "0"; }
                    }
                    else
                    {
                        msg.external_power = "0";
                    }
                }
            }
            catch
            {
                return msg = new Max2FmtMsg();
            }

            return msg;
        }
        private Max2FmtMsg TranslateMax2MsgStatus(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length <= 40) 
            { 
                return msg; 
            }

            try
            {
                String msgStr = Encoding.ASCII.GetString(data).Trim();
                String[] seqMsg = msgStr.Split(new char[] { ',', '\n' });

                if (seqMsg[0].Length < 2)
                {
                    return msg;
                }
                if (seqMsg.Length == 4)
                {
                    #region Reset
                    msg.header = "$";
                    msg.vid = seqMsg[seqMsg.Length - 1];
                    string gps_date_time = seqMsg[(int)WLOCATOR_MSG_STATUS.GPS_DATE_TIME].Substring(1, 6);
                    string yy = gps_date_time.Substring(0, 2);
                    string MM = gps_date_time.Substring(2, 2);
                    string dd = gps_date_time.Substring(4, 2);
                    msg.gps_date_time = dd + MM + yy;
                    msg.utc = seqMsg[(int)WLOCATOR_MSG_STATUS.GPS_DATE_TIME].Substring(7, 6);
                 
                    msg.lat = "0.0N";
                    msg.lon = "0.0E";
                    msg.speed = "0";
                    msg.course = "0";
                    msg.type_of_fix = "0";
                    msg.no_of_satellite = "0";
                    msg.tag = "";
                    msg.odometor = "0";
                    msg.internal_power = "0"; 
                    msg.analog_level = "0";
                    msg.external_power = "0";
                    msg.gpio_status = "00";

                    if (seqMsg[2].StartsWith("ProgramStart"))
                    {
                        msg.type_of_message = "+";

                        string[] fw = seqMsg[2].Split(':');
                        if (fw.Length == 2)
                        {
                            msg.tag = fw[1];
                        }
                        if (fw.Length == 3)
                        {
                            msg.tag = fw[1];
                        }
                    }
                    #endregion
                }
                else if (seqMsg.Length >= 13)
                {
                    string gps_date_time = seqMsg[(int)WLOCATOR_MSG_STATUS.GPS_DATE_TIME];
                    if (gps_date_time[0] == WLocatorBF_HEADER)
                    {
                        gps_date_time = seqMsg[(int)WLOCATOR_MSG_STATUS.GPS_DATE_TIME].Substring(2, 6);
                        msg.utc = seqMsg[(int)WLOCATOR_MSG_STATUS.GPS_DATE_TIME].Substring(8, 6);
                        msg.type_of_message = "*" + seqMsg[10];
                    }
                    else
                    {
                        gps_date_time = seqMsg[(int)WLOCATOR_MSG_STATUS.GPS_DATE_TIME].Substring(1, 6);
                        msg.utc = seqMsg[(int)WLOCATOR_MSG_STATUS.GPS_DATE_TIME].Substring(7, 6);
                        msg.type_of_message = seqMsg[10];
                    }
                    msg.header = "$";
                    msg.vid = seqMsg[seqMsg.Length-1];
                    string yy = gps_date_time.Substring(0, 2);
                    string MM = gps_date_time.Substring(2, 2);
                    string dd = gps_date_time.Substring(4, 2);
                    msg.gps_date_time = dd + MM + yy;
                    msg.lat = seqMsg[(int)WLOCATOR_MSG_STATUS.LAT] + "N";
                    msg.lon = seqMsg[(int)WLOCATOR_MSG_STATUS.LON] + "E";
                    msg.speed = seqMsg[(int)WLOCATOR_MSG_STATUS.SPEED];
                    msg.course = seqMsg[(int)WLOCATOR_MSG_STATUS.COURSE];
                    msg.type_of_fix = seqMsg[(int)WLOCATOR_MSG_STATUS.TYPE_OF_FIX];
                    msg.no_of_satellite = seqMsg[(int)WLOCATOR_MSG_STATUS.NO_OF_SATELLITE];
                
                    /// analog_level
                    string[] analog_level = (seqMsg[(int)WLOCATOR_MSG_STATUS.ANALOG_LEVEL]).Split('.');
                    if (analog_level.Length > 1) 
                    {
                        msg.analog_level = analog_level[0] + analog_level[1];
                        if (analog_level[1].Length == 1) { msg.analog_level += "0"; }
                    }
                    else
                    {
                        msg.analog_level = "0"; 
                    }

                    /// external_power
                    string[] external_power = (seqMsg[(int)WLOCATOR_MSG_STATUS.EXTERNAL_INPUT]).Split('.');
                    if (external_power.Length > 1)
                    {
                        msg.external_power = external_power[0] + external_power[1]; 
                        if (external_power[1].Length == 1) { msg.external_power += "0"; }
                    }
                    else
                    {
                        msg.external_power = "0";
                    }
            
                    msg.odometor = "0";
                    msg.internal_power = "0";
                    msg.gpio_status = "00";
                    msg.tag = "";

                    for (int i = (int)WLOCATOR_MSG_STATUS.EVENT_VALUE1;i < seqMsg.Length - 1 ; i++)
                    {
                        msg.tag+= seqMsg[i] + ",";
                    }

                    if (msg.tag.Length > 0) msg.tag = msg.tag.Substring(0, msg.tag.Length - 1);
                }
            }
            catch 
            {
                return msg = new Max2FmtMsg();
            }

            return msg;
        }

        private void CalGPIO(Max2FmtMsg msg, string[] str, out string gpio, out string type_msg, out string tag, out string odometor, out string internal_power)
        {
            gpio = "80";
            type_msg = "G";
            tag = "";
            odometor = "0";
            internal_power = "0";

            try
            {
                switch (Convert.ToInt16(str[(int)WLOCATOR_MSG_STATUS.EVENT_TYPE]))
                {
                    case (int)WLOCATOR_STATUSREPORT.ALERT_IO:
                        {
                            switch (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE1])
                            {
                                case "PB": break;
                                case "TP": break;
                                case "IG":
                                    {
                                        #region Engine ON-OFF "IG"
                                        // gpio = str[12] + "0";
                                        msg.type_of_message = type_msg = "I";

                                        if (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE2] == "1")
                                        {

                                        }
                                        else if (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE2] == "0")
                                        {
                                         
                                        }
                                        break;
                                        #endregion
                                    }
                                case "IN":
                                    {
                                    }
                                    break;
                                case "OUT":
                                    { 

                                    }
                                    break;
                                case "ANTDETECT":
                                    {
                                        #region GPS ANT DETECT "ANTDETECT"
                                        if (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE2] == "0")
                                        {
                                            //type_msg = "X";
                                            //gpio = VehObjDataBuffer(MANAGETYPE.SELECT, msg);
                                        }
                                        else if (str[12] == "1")
                                        {
                                            //type_msg = "x";
                                            //gpio = VehObjDataBuffer(MANAGETYPE.SELECT, msg);
                                        }
                                        break;
                                        #endregion
                                    }
                                case "PS":
                                    {
                                        #region Power Battery "PS"
                                //        msg.type_of_message = type_msg = "I";
                                //        string tmpStIO = VehObjDataBuffer(MANAGETYPE.SELECT, msg);
                                //        int tmpIO = Convert.ToInt32(tmpStIO, 16);

                                //        if (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE2] == "0")
                                //        {
                                //            if ((tmpIO & (int)IOTYPE.POWER) == (int)IOTYPE.POWER)
                                //            {
                                //                int iTmpIO = tmpIO - (int)IOTYPE.POWER;
                                //                msg.gpio_status = gpio = Convert.ToString(iTmpIO, 16);
                                //                VehObjDataBuffer(MANAGETYPE.INSERT, msg);
                                //            }
                                //            else
                                //            {
                                //                msg.gpio_status = gpio = tmpStIO;
                                //            }
                                //        }
                                //        else if (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE2] == "1")
                                //        {
                                //            if (!((tmpIO & (int)IOTYPE.POWER) == (int)IOTYPE.POWER))
                                //            {
                                //                int iTmpIO = tmpIO + (int)IOTYPE.POWER;
                                //                msg.gpio_status = gpio = Convert.ToString(iTmpIO, 16);
                                //                VehObjDataBuffer(MANAGETYPE.INSERT, msg);
                                //            }
                                //            else
                                //            {
                                //                msg.gpio_status = gpio = tmpStIO;
                                //            }
                                        //        
                                        #endregion
                                        break;
                              
                                    }
                                case "SP":
                                    {
                                        #region speed "SP"
                                        if (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE2] == "1")
                                        {
                                            msg.type_of_message = type_msg = "K";
                                        }
                                        else if (str[(int)WLOCATOR_MSG_STATUS.EVENT_VALUE2] == "0")
                                        {
                                            msg.type_of_message = type_msg = "k";
                                        } 
                                        break;
                                        #endregion
                                    }
                            
                                default: break;
                            }
                            break;
                        }
                    case (int)WLOCATOR_STATUSREPORT.TEMPERATURE:
                        {
                            break;
                        }
                    case (int)WLOCATOR_STATUSREPORT.BATTERY_STATUS:
                        {
                            break;
                        }
                    case (int)WLOCATOR_STATUSREPORT.SERVICE_CALL:
                        {
                            break;
                        }
                    case (int)WLOCATOR_STATUSREPORT.DEBUG_MESSAGE:
                        {
                            break;
                        }
                    case (int)WLOCATOR_STATUSREPORT.MILEAGE_REPORT:
                        {
                            break;
                        }
                    case (int)WLOCATOR_STATUSREPORT.LICENSE_INFO:
                        {
                            //D,MC,24,1,0003955,30502   Max2
                            //8,SUKSAWADDEE SAITHARN MISS,6007643100500157891,150619800909,24,2,0004552,00100,999  KBox
                            /*
                             * 8,                           *�8� � License Info
                             * SUKSAWADDEE SAITHARN MISS,   *Driver�s name
                             * 6007643100500157891,         *Country (6 Digits) + ID Number(Remaining Digits)
                             * 150619800909,                *Expiry Date (YYMM) + Birthday (YYYYMMDD)
                             * 24,                          *License Type
                             * 2,                           *Sex
                             * 0004552,                     *License Number
                             * 00100,                       *Place of Issued
                             * 999                          *Device ID
                             */
                        }
                        break;
                    default:
                        break;
                }
            }
            catch
            {
            }
        }

        private bool IsDoubleValue(int value, int prv_state, int type)
        {
            value = value & type;
            prv_state = prv_state & type;

            if (value == prv_state)
            {
                return true;
            }
            return false;
        }
    }
}
