using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace ZebraPresentationLayer
{
    public enum GBOXCMD
    {
        RTFV = 005,
        PRGPIO = 100,
        RTPV = 087,
        RTPV6 = 125,
        RTPV7 = 127,
        RTPS = 030,
        RTDL = 093,
        RTPL = 091
    }

    [StructLayout(LayoutKind.Explicit | LayoutKind.Sequential)]
    public struct GBoxFmt
    {
        [FieldOffset(0)]
        public char a;
        [FieldOffset(0)]
        public char b;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RTFVmsg
    {
        public byte cmd;

        public byte product1;
        public byte product2;
        public byte product3;
        public byte product4;

        public byte major_ver1;
        public byte major_ver2;

        public byte minor_ver1;
        public byte minor_ver2;

        public byte DDMMYY1;
        public byte DDMMYY2;
        public byte DDMMYY3;
        public byte DDMMYY4;
        public byte DDMMYY5;
        public byte DDMMYY6;

        public byte pwd1;
        public byte pwd2;
        public byte pwd3;
        public byte pwd4;
        public byte pwd5;

        public byte VID1;
        public byte VID2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PRGPIOmsg
    {
        public byte cmd;

        public byte utc1;
        public byte utc2;

        public byte lat1;
        public byte lat2;
        public byte lat3;
        public byte lat4;

        public byte lon1;
        public byte lon2;
        public byte lon3;
        public byte lon4;

        public byte gps_status1;
        public byte gpio1;
        public byte gpio_status1;

        public byte VID1;
        public byte VID2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PRPSmsg
    {
        public byte cmd;

        public byte VID1;
        public byte VID2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PRDLmsg
    {
        public byte cmd;

        public byte VID1;
        public byte VID2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct PRPLmsg
    {
        public byte cmd;

        public byte VID1;
        public byte VID2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RTPVmsg
    {
        public byte cmd;

        public byte utc1;
        public byte utc2;

        public byte lat1;
        public byte lat2;
        public byte lat3;
        public byte lat4;

        public byte lon1;
        public byte lon2;
        public byte lon3;
        public byte lon4;

        public byte alt1;
        public byte spd1;
        public byte crs1;
        public byte hdop1;
        public byte num_of_satellite;
        public byte gps_status1;

        public byte odo1;
        public byte odo2;
        public byte odo3;
        public byte odo4;

        public byte gps_odo1;
        public byte gps_odo2;
        public byte gps_odo3;
        public byte gps_odo4;

        public byte cell_id1;
        public byte cell_id2;

        public byte loc_id1;
        public byte loc_id2;

        public byte rssi1;
        public byte veh_battery1;
        public byte veh_power1;

        public byte fuel_gauge;
        public byte input_stat;

        public byte gps_antenna_status1;
        public byte gps_antenna_status2;
        public byte gps_antenna_status3;
        public byte gps_antenna_status4;

        public byte gps_antenna_power1;
        public byte gps_antenna_power2;
        public byte gps_antenna_power3;
        public byte gps_antenna_power4;

        public byte VID1;
        public byte VID2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RTPV6msg
    {
        public byte cmd;

        public byte utc1;
        public byte utc2;

        public byte lat1;
        public byte lat2;
        public byte lat3;
        public byte lat4;

        public byte lon1;
        public byte lon2;
        public byte lon3;
        public byte lon4;

        public byte alt1;
        public byte alt2;
        public byte spd1;
        public byte crs1;
        public byte hdop1;
        public byte num_of_satellite;
        public byte gps_status1;

        public byte odo1;
        public byte odo2;
        public byte odo3;
        public byte odo4;

        public byte gps_odo1;
        public byte gps_odo2;
        public byte gps_odo3;
        public byte gps_odo4;

        public byte cell_id1;
        public byte cell_id2;

        public byte loc_id1;
        public byte loc_id2;

        public byte rssi1;
        public byte veh_battery1;
        public byte veh_power1;

        public byte fuel_gauge;
        public byte input_stat;

        public byte VID1;
        public byte VID2;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RTPV7msg
    {
        public byte cmd;

        public byte utc1;
        public byte utc2;

        public byte lat1;
        public byte lat2;
        public byte lat3;
        public byte lat4;

        public byte lon1;
        public byte lon2;
        public byte lon3;
        public byte lon4;

        public byte alt1;
        public byte alt2;
        public byte spd1;
        public byte crs1;
        public byte hdop1;
        public byte num_of_satellite;
        public byte gps_status1;

        public byte odo1;
        public byte odo2;
        public byte odo3;
        public byte odo4;

        public byte gps_odo1;
        public byte gps_odo2;
        public byte gps_odo3;
        public byte gps_odo4;

        public byte cell_id1;
        public byte cell_id2;

        public byte loc_id1;
        public byte loc_id2;

        public byte rssi1;
        public byte veh_battery1;
        public byte veh_power1;

        public byte fuel_gauge;
        public byte input_stat;

        public byte raw_define_gpio;
        public byte gps_antenna_status1;
        public byte gps_antenna_power1;

        public byte VID1;
        public byte VID2;
    }

    public class GBox
    {
        public GBox()
        {
        }

        public object Translate(byte[] data)
        {
            if (data.Length > 0)
            {
                GBOXCMD cmd = (GBOXCMD)data[0];
                switch (cmd)
                {
                    case GBOXCMD.RTFV:
                        RTFVmsg fvMsg = PutRTFVMsg(data);
                        return fvMsg;

                    case GBOXCMD.PRGPIO:
                        PRGPIOmsg gpioMsg = PutRTGPIOMsg(data);
                        return gpioMsg;

                    case GBOXCMD.RTPS:
                        PRPSmsg psMsg = PutRTPSMsg(data);
                        return psMsg;

                    case GBOXCMD.RTDL:
                        PRDLmsg dlMsg = PutRTDLMsg(data);
                        return dlMsg;

                    case GBOXCMD.RTPL:
                        PRPLmsg plMsg = PutRTPLMsg(data);
                        return plMsg;

                    case GBOXCMD.RTPV:
                        RTPVmsg rtpvMsg = PutRTPVMsg(data);
                        return rtpvMsg;

                    case GBOXCMD.RTPV6:
                        RTPV6msg rtpvMsg6 = PutRTPV6Msg(data);
                        return rtpvMsg6;

                    case GBOXCMD.RTPV7:
                        RTPV7msg rtpvMsg7 = PutRTPV7Msg(data);
                        return rtpvMsg7;
                }
            }

            return null;
        }

        private RTFVmsg PutRTFVMsg(byte[] data)
        {
            int i = 0;
            RTFVmsg msg = new RTFVmsg();

            msg.cmd = data[i++];

            msg.product1 = data[i++];
            msg.product2 = data[i++];
            msg.product3 = data[i++];
            msg.product4 = data[i++];

            i++; //comma

            msg.major_ver1 = data[i++];
            msg.major_ver2 = data[i++];

            i++; //comma

            msg.minor_ver1 = data[i++];
            msg.minor_ver2 = data[i++];

            i++; //comma

            msg.DDMMYY1 = data[i++];
            msg.DDMMYY2 = data[i++];
            msg.DDMMYY3 = data[i++];
            msg.DDMMYY4 = data[i++];
            msg.DDMMYY5 = data[i++];
            msg.DDMMYY6 = data[i++];

            i++; //comma

            msg.pwd1 = data[i++];
            msg.pwd2 = data[i++];
            msg.pwd3 = data[i++];
            msg.pwd4 = data[i++];
            msg.pwd5 = data[i++];

            i++; //comma

            msg.VID1 = data[i++];
            msg.VID2 = data[i++];

            return msg;
        }
        public String GetRTFVString(RTFVmsg msg)
        {
            String prnStr = "";
            byte[] tmp;

            prnStr = msg.cmd.ToString() + ",";

            tmp = new byte[] { msg.product1, msg.product2, msg.product3, msg.product4 };
            prnStr += System.Text.ASCIIEncoding.ASCII.GetString(tmp) + ",";

            tmp = new byte[] { msg.major_ver1, msg.major_ver2 };
            prnStr += System.Text.ASCIIEncoding.ASCII.GetString(tmp) + ",";

            tmp = new byte[] { msg.minor_ver1, msg.minor_ver2 };
            prnStr += System.Text.ASCIIEncoding.ASCII.GetString(tmp) + ",";

            tmp = new byte[] { msg.DDMMYY1, msg.DDMMYY2, msg.DDMMYY3, msg.DDMMYY4, msg.DDMMYY5, msg.DDMMYY6 };
            prnStr += System.Text.ASCIIEncoding.ASCII.GetString(tmp) + ",";

            tmp = new byte[] { msg.pwd1, msg.pwd2, msg.pwd3, msg.pwd4, msg.pwd5 };
            prnStr += System.Text.ASCIIEncoding.ASCII.GetString(tmp) + ",";

            tmp = new byte[] { msg.VID2, msg.VID1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString();

            return prnStr;
        }

        private PRGPIOmsg PutRTGPIOMsg(byte[] data)
        {
            int i = 0;
            PRGPIOmsg msg = new PRGPIOmsg();

            msg.cmd = data[i++];

            uint sec = GetUtcTimeInSeconds();
            byte[] utc_byte = BitConverter.GetBytes(sec);
            msg.utc1 = data[i++]; msg.utc1 = utc_byte[1];
            msg.utc2 = data[i++]; msg.utc2 = utc_byte[0];

            msg.lat1 = data[i++];
            msg.lat2 = data[i++];
            msg.lat3 = data[i++];
            msg.lat4 = data[i++];

            msg.lon1 = data[i++];
            msg.lon2 = data[i++];
            msg.lon3 = data[i++];
            msg.lon4 = data[i++];

            msg.gps_status1 = data[i++];
            msg.gpio1 = data[i++];
            msg.gpio_status1 = data[i++];

            msg.VID1 = data[i++];
            msg.VID2 = data[i++];

            return msg;
        }
        public String GetRTGPIOString(PRGPIOmsg msg)
        {
            String prnStr = "";
            byte[] tmp;

            prnStr = msg.cmd.ToString() + ",";

            tmp = new byte[] { msg.utc2, msg.utc1 };
            //prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";
            prnStr += GetUtcTimeInSeconds().ToString() + ",";

            tmp = new byte[] { msg.lat4, msg.lat3, msg.lat2, msg.lat1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.lon4, msg.lon3, msg.lon2, msg.lon1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            prnStr += msg.gps_status1.ToString() + ",";
            prnStr += msg.gpio1.ToString() + ",";
            prnStr += msg.gpio_status1.ToString() + ",";

            tmp = new byte[] { msg.VID2, msg.VID1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString();

            return prnStr;
        }

        private PRPSmsg PutRTPSMsg(byte[] data)
        {
            int i = 0;
            PRPSmsg msg = new PRPSmsg();

            msg.cmd = data[i++];

            msg.VID1 = data[i++];
            msg.VID2 = data[i++];

            return msg;
        }
        public String GetRTPSString(PRPSmsg msg)
        {
            String prnStr = "";
            byte[] tmp;

            prnStr = msg.cmd.ToString() + ",";

            tmp = new byte[] { msg.VID2, msg.VID1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString();

            return prnStr;
        }

        private PRDLmsg PutRTDLMsg(byte[] data)
        {
            int i = 0;
            PRDLmsg msg = new PRDLmsg();

            msg.cmd = data[i++];
            return msg;
        }
        public String GetRTDLString(PRDLmsg msg)
        {
            String prnStr = "";
          //  byte[] tmp;

            prnStr = msg.cmd.ToString();
            return prnStr;
        }

        private PRPLmsg PutRTPLMsg(byte[] data)
        {
            int i = 0;
            PRPLmsg msg = new PRPLmsg();

            msg.cmd = data[i++];
            return msg;
        }
        public String GetRTPLString(PRPLmsg msg)
        {
            String prnStr = "";
           // byte[] tmp;

            prnStr = msg.cmd.ToString();
            return prnStr;
        }

        private RTPVmsg PutRTPVMsg(byte[] data)
        {
            int i = 0;
            RTPVmsg msg = new RTPVmsg();

            msg.cmd = data[i++];

            uint sec = GetUtcTimeInSeconds();
            byte[] utc_byte = BitConverter.GetBytes(sec);
            msg.utc1 = data[i++]; msg.utc1 = utc_byte[1];
            msg.utc2 = data[i++]; msg.utc2 = utc_byte[0];

            msg.lat1 = data[i++];
            msg.lat2 = data[i++];
            msg.lat3 = data[i++];
            msg.lat4 = data[i++];

            msg.lon1 = data[i++];
            msg.lon2 = data[i++];
            msg.lon3 = data[i++];
            msg.lon4 = data[i++];

            msg.alt1 = data[i++];
            msg.spd1 = data[i++];
            msg.crs1 = data[i++];
            msg.hdop1 = data[i++];
            msg.num_of_satellite = data[i++];
            msg.gps_status1 = data[i++];

            msg.odo1 = data[i++];
            msg.odo2 = data[i++];
            msg.odo3 = data[i++];
            msg.odo4 = data[i++];

            msg.gps_odo1 = data[i++];
            msg.gps_odo2 = data[i++];
            msg.gps_odo3 = data[i++];
            msg.gps_odo4 = data[i++];

            msg.cell_id1 = data[i++];
            msg.cell_id2 = data[i++];

            msg.loc_id1 = data[i++];
            msg.loc_id2 = data[i++];

            msg.rssi1 = data[i++];
            msg.veh_battery1 = data[i++];
            msg.veh_power1 = data[i++];

            msg.VID1 = data[i++];
            msg.VID2 = data[i++];

            return msg;
        }
        private RTPV6msg PutRTPV6Msg(byte[] data)
        {
            int i = 0;
            RTPV6msg msg = new RTPV6msg();

            msg.cmd = data[i++];

            uint sec = GetUtcTimeInSeconds();
            byte[] utc_byte = BitConverter.GetBytes(sec);
            msg.utc1 = data[i++]; msg.utc1 = utc_byte[1];
            msg.utc2 = data[i++]; msg.utc2 = utc_byte[0];

            msg.lat1 = data[i++];
            msg.lat2 = data[i++];
            msg.lat3 = data[i++];
            msg.lat4 = data[i++];

            msg.lon1 = data[i++];
            msg.lon2 = data[i++];
            msg.lon3 = data[i++];
            msg.lon4 = data[i++];

            msg.alt1 = data[i++];
            msg.alt2 = data[i++];
            msg.spd1 = data[i++];
            msg.crs1 = data[i++];
            msg.hdop1 = data[i++];
            msg.num_of_satellite = data[i++];
            msg.gps_status1 = data[i++];

            msg.odo1 = data[i++];
            msg.odo2 = data[i++];
            msg.odo3 = data[i++];
            msg.odo4 = data[i++];

            msg.gps_odo1 = data[i++];
            msg.gps_odo2 = data[i++];
            msg.gps_odo3 = data[i++];
            msg.gps_odo4 = data[i++];

            msg.cell_id1 = data[i++];
            msg.cell_id2 = data[i++];

            msg.loc_id1 = data[i++];
            msg.loc_id2 = data[i++];

            msg.rssi1 = data[i++];
            msg.veh_battery1 = data[i++];
            msg.veh_power1 = data[i++];

            msg.fuel_gauge = data[i++];
            msg.input_stat = data[i++];

            msg.VID1 = data[i++];
            msg.VID2 = data[i++];

            return msg;
        }
        private RTPV7msg PutRTPV7Msg(byte[] data)
        {
            int i = 0;
            RTPV7msg msg = new RTPV7msg();

            msg.cmd = data[i++];

            uint sec = GetUtcTimeInSeconds();
            byte[] utc_byte = BitConverter.GetBytes(sec);
            msg.utc1 = data[i++]; msg.utc1 = utc_byte[1];
            msg.utc2 = data[i++]; msg.utc2 = utc_byte[0];

            msg.lat1 = data[i++];
            msg.lat2 = data[i++];
            msg.lat3 = data[i++];
            msg.lat4 = data[i++];

            msg.lon1 = data[i++];
            msg.lon2 = data[i++];
            msg.lon3 = data[i++];
            msg.lon4 = data[i++];

            msg.alt1 = data[i++];
            msg.alt2 = data[i++];
            msg.spd1 = data[i++];
            msg.crs1 = data[i++];
            msg.hdop1 = data[i++];
            msg.num_of_satellite = data[i++];
            msg.gps_status1 = data[i++];

            msg.odo1 = data[i++];
            msg.odo2 = data[i++];
            msg.odo3 = data[i++];
            msg.odo4 = data[i++];

            msg.gps_odo1 = data[i++];
            msg.gps_odo2 = data[i++];
            msg.gps_odo3 = data[i++];
            msg.gps_odo4 = data[i++];

            msg.cell_id1 = data[i++];
            msg.cell_id2 = data[i++];

            msg.loc_id1 = data[i++];
            msg.loc_id2 = data[i++];

            msg.rssi1 = data[i++];

            msg.veh_battery1 = data[i++];
            msg.veh_power1 = data[i++];

            msg.fuel_gauge = data[i++];
            msg.raw_define_gpio = data[i++];

            //msg.input_stat      = data[i++] ;
            byte tmp = data[i++];
            msg.gps_antenna_status1 = (byte)((tmp & 0xF0) >> 4);
            msg.gps_antenna_power1 = (byte)(tmp & 0x0F);

            msg.VID1 = data[i++];
            msg.VID2 = data[i++];

            return msg;
        }
        public String GetRTPVString(RTPVmsg msg)
        {
            String prnStr = "";
            byte[] tmp;

            prnStr = msg.cmd.ToString() + ",";

            tmp = new byte[] { msg.utc2, msg.utc1 };
            //prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";
            prnStr += GetUtcTimeInSeconds().ToString() + ",";

            tmp = new byte[] { msg.lat4, msg.lat3, msg.lat2, msg.lat1 };
            prnStr += ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString() + ",";

            tmp = new byte[] { msg.lon4, msg.lon3, msg.lon2, msg.lon1 };
            prnStr += ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString() + ",";

            prnStr += msg.alt1.ToString() + ",";
            prnStr += msg.spd1.ToString() + ",";
            prnStr += msg.crs1.ToString() + ",";
            prnStr += msg.hdop1.ToString() + ",";
            prnStr += msg.num_of_satellite.ToString() + ",";
            prnStr += msg.gps_status1.ToString() + ",";

            tmp = new byte[] { msg.odo4, msg.odo3, msg.odo2, msg.odo1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.gps_odo4, msg.gps_odo3, msg.gps_odo2, msg.gps_odo1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.cell_id2, msg.cell_id1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.loc_id2, msg.loc_id1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            prnStr += msg.rssi1.ToString() + ",";
            prnStr += msg.veh_battery1.ToString() + ",";
            prnStr += msg.veh_power1.ToString() + ",";

            tmp = new byte[] { msg.VID2, msg.VID1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString();

            return prnStr;
        }
        public String GetRTPV6String(RTPV6msg msg)
        {
            String prnStr = "";
            byte[] tmp;

            prnStr = msg.cmd.ToString() + ",";

            tmp = new byte[] { msg.utc2, msg.utc1 };
            //prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";
            prnStr += GetUtcTimeInSeconds().ToString() + ",";

            tmp = new byte[] { msg.lat4, msg.lat3, msg.lat2, msg.lat1 };
            prnStr += ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString() + ",";

            tmp = new byte[] { msg.lon4, msg.lon3, msg.lon2, msg.lon1 };
            prnStr += ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString() + ",";

            prnStr += msg.alt2.ToString() + ",";
            prnStr += msg.spd1.ToString() + ",";
            prnStr += msg.crs1.ToString() + ",";
            prnStr += msg.hdop1.ToString() + ",";
            prnStr += msg.num_of_satellite.ToString() + ",";
            prnStr += msg.gps_status1.ToString() + ",";

            tmp = new byte[] { msg.odo4, msg.odo3, msg.odo2, msg.odo1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.gps_odo4, msg.gps_odo3, msg.gps_odo2, msg.gps_odo1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.cell_id2, msg.cell_id1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.loc_id2, msg.loc_id1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            prnStr += msg.rssi1.ToString() + ",";
            prnStr += msg.veh_battery1.ToString() + ",";
            prnStr += msg.veh_power1.ToString() + ",";

            prnStr += msg.fuel_gauge.ToString() + ",";

            tmp = new byte[] { msg.VID2, msg.VID1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString();

            return prnStr;
        }
        public String GetRTPV7String(RTPV7msg msg)
        {
            String prnStr = "";
            byte[] tmp;

            prnStr = msg.cmd.ToString() + ",";

            tmp = new byte[] { msg.utc2, msg.utc1 };
            //prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";
            prnStr += GetUtcTimeInSeconds().ToString() + ",";

            tmp = new byte[] { msg.lat4, msg.lat3, msg.lat2, msg.lat1 };
            prnStr += ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString() + ",";

            tmp = new byte[] { msg.lon4, msg.lon3, msg.lon2, msg.lon1 };
            prnStr += ((double)BitConverter.ToUInt32(tmp, 0) / 1000000.0).ToString() + ",";

            tmp = new byte[] { msg.alt2, msg.alt1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";
            prnStr += msg.spd1.ToString() + ",";
            prnStr += msg.crs1.ToString() + ",";
            prnStr += msg.hdop1.ToString() + ",";
            prnStr += msg.num_of_satellite.ToString() + ",";
            prnStr += msg.gps_status1.ToString() + ",";

            tmp = new byte[] { msg.odo4, msg.odo3, msg.odo2, msg.odo1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.gps_odo4, msg.gps_odo3, msg.gps_odo2, msg.gps_odo1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.cell_id2, msg.cell_id1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            tmp = new byte[] { msg.loc_id2, msg.loc_id1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";

            prnStr += msg.rssi1.ToString() + ",";
            prnStr += msg.veh_battery1.ToString() + ",";
            prnStr += msg.veh_power1.ToString() + ",";

            prnStr += msg.fuel_gauge.ToString() + ",";
            prnStr += msg.input_stat.ToString() + ",";

            //tmp = new byte[] { msg.gps_antenna_status4, msg.gps_antenna_status3, msg.gps_antenna_status2, msg.gps_antenna_status1 };
            //prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";
            byte bTmp = msg.gps_antenna_status1;
            prnStr += bTmp.ToString() + ",";

            //tmp = new byte[] { msg.gps_antenna_power4, msg.gps_antenna_power3, msg.gps_antenna_power2, msg.gps_antenna_power1 };
            //prnStr += BitConverter.ToUInt16(tmp, 0).ToString() + ",";
            bTmp = msg.gps_antenna_power1;
            prnStr += bTmp.ToString() + ",";

            tmp = new byte[] { msg.VID2, msg.VID1 };
            prnStr += BitConverter.ToUInt16(tmp, 0).ToString();

            return prnStr;
        }

        private uint GetUtcTimeInSeconds()
        {
            uint seconds = ((uint)(DateTime.UtcNow.Hour * 60 * 60)) + ((uint)(DateTime.UtcNow.Minute * 60)) + ((uint)(DateTime.UtcNow.Second));
            return seconds;
        }
    }
}
