using System;
using System.Collections.Generic;
using System.Text;

namespace ZebraDataInterface
{
    public interface DataInterface
    {
        string GetLocation(float lat, float lon);
        string GetLocation(float lat, float lon, int lang) ;
    }
}
