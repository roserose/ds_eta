using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraManagementGUI
{
    public partial class frmMachindMenu : BaseFrm
    {
        public frmMachindMenu()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            frmMachineAddEdit frm = new frmMachineAddEdit();
            frm.ShowDialog();
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            frmMachineAddEdit frm = new frmMachineAddEdit();
            frm.ShowDialog();
        }

        private void btRemove_Click(object sender, EventArgs e)
        {

        }
    }
}