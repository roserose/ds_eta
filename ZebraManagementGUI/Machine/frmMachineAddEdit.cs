using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraManagementGUI
{
    public partial class frmMachineAddEdit : Form
    {
        private string ip = "";
        private string user = "";
        private string pass = "";
        private Image img = null;
        #region Properties
        public string IP
        {
            set
            { 
                ip = value; 
            }
            get 
            {
                return ip;
            }
        }
        public string USERNAME
        {
            set
            {
                user = value;
            }
            get
            {
                return user;
            }
        }
        public string PASSWORD
        {
            set
            {
                pass = value;
            }
            get
            {
                return pass;
            }
        }
        public Image IMG
        {
            set
            {
                img = value;
            }
        }
        #endregion

        public frmMachineAddEdit()
        {
            InitializeComponent();
        }
    }
}