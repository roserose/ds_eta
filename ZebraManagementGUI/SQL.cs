using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace ZebraManagementGUI
{
    class SQL
    {
        private string dbConn = "Data Source = 10.0.10.70; Initial Catalog = ZebraDB; uid = sa; pwd = aab-gps-gtt";
        public SQL()
        { }

        public DataTable GetMachine()
        {
            DataTable dtRet = new DataTable();
            try
            {
                SqlConnection cn = new SqlConnection(dbConn);
                cn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT *, host_ip AS display FROM SystemMonitor.dbo.ZbMachines WHERE active = 1";
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 30;
                cmd.Connection = cn;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtRet);

                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return dtRet;
        }

        public DataTable GetApplication()
        {
            DataTable dtRet = new DataTable();
            try
            {
                SqlConnection cn = new SqlConnection(dbConn);
                cn.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT *,app_name AS display FROM SystemMonitor.dbo.ZbApplication WHERE active = 1";
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 30;
                cmd.Connection = cn;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dtRet);

                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return dtRet;
        }
    }
}
