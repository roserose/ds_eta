using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZebraEncryption;
using ZebraDataCentre;

namespace ZebraGateWay
{
    public partial class ConfigForm : Form
    {
        private DataTable cmdEncryptionTable = new DataTable();
        private DataTable cmdBoxTable = new DataTable();
        private DataTable cmdOutputType = new DataTable();
        public DataTable dtCMDList;

        public ConfigForm()
        {
            InitializeComponent();
        }

        private void InitializeCombobox()
        {
            InitCmdTable();

            cbEncryption.DataSource = cmdEncryptionTable;
            cbEncryption.DisplayMember = "cmd_display";
            cbEncryption.ValueMember = "cmd_algorithm";
            cbEncryption.SelectedIndex = 0;

            cbBoxType.DataSource = cmdBoxTable;
            cbBoxType.DisplayMember = "cmd_display";
            cbBoxType.ValueMember = "cmd_version";
            cbBoxType.SelectedIndex = 0;

            cbOutputType.DataSource = cmdOutputType;
            cbOutputType.DisplayMember = "cmd_display";
            cbOutputType.ValueMember = "cmd_output";
            cbOutputType.SelectedIndex = 0;
        }

        private void InitCmdTable()
        {
            cmdEncryptionTable.Columns.Add("cmd_display", typeof(String));
            cmdEncryptionTable.Columns.Add("cmd_algorithm", typeof(EncryptionAlgorithm));
            foreach (EncryptionAlgorithm suit in Enum.GetValues(typeof(EncryptionAlgorithm)))
            {
                cmdEncryptionTable.Rows.Add(new object[] { suit.ToString(), suit });
            }

            cmdBoxTable.Columns.Add("cmd_display", typeof(String));
            cmdBoxTable.Columns.Add("cmd_version", typeof(BoxVersions));
            foreach (BoxVersions suit in Enum.GetValues(typeof(BoxVersions)))
            {
                cmdBoxTable.Rows.Add(new object[] { suit.ToString(), suit });
            }
           
            cmdOutputType.Columns.Add("cmd_display", typeof(String));
            cmdOutputType.Columns.Add("cmd_output", typeof(EncryptionDataType));
            foreach (EncryptionDataType suit in Enum.GetValues(typeof(EncryptionDataType)))
            {
                cmdOutputType.Rows.Add(new object[] { suit.ToString(), suit });
            }
        }

        private void InitializeText()
        {
            try
            {
                tbConDatabase.Text = (string)dtCMDList.Rows[0]["Database"];
                tbConLocalIP.Text = (string)dtCMDList.Rows[0]["LocalIP"];
                tbConLocalPort.Text = (string)dtCMDList.Rows[0]["LocalPort"];
                tbConForwadPort.Text = (string)dtCMDList.Rows[0]["ForwardPort"];
                tbConRemotePort.Text = (string)dtCMDList.Rows[0]["RemotePort"];
                tbConReceiveTime.Text = (string)dtCMDList.Rows[0]["ReceiveTime"];
                tbConSendTime.Text = (string)dtCMDList.Rows[0]["SendTime"];

                cbEncryption.SelectedIndex = (int)(EncryptionAlgorithm)Enum.Parse(typeof(EncryptionAlgorithm), (string)dtCMDList.Rows[0]["EncryptionType"]);
                cbBoxType.SelectedIndex = (int)(BoxVersions)Enum.Parse(typeof(BoxVersions), (string)dtCMDList.Rows[0]["BoxVersion"]);
                cbOutputType.SelectedIndex = (int)(EncryptionDataType)Enum.Parse(typeof(EncryptionDataType), (string)dtCMDList.Rows[0]["OutputType"]);

                tbConIV.Text = (string)dtCMDList.Rows[0]["EncryptionKey"];
                tbConKey.Text = (string)dtCMDList.Rows[0]["EncryptionIV"];

                rtConSystemQueue.Text = ((string)dtCMDList.Rows[0]["SystemQueue"]).Replace(',', '\n');

                ckConRecDB.Checked = Convert.ToBoolean((string)dtCMDList.Rows[0]["RecordConnection"]);

                tbConRam.Text = (string)dtCMDList.Rows[0]["RamRestart"];


                cbEnnableKeep.Text = (string)dtCMDList.Rows[0]["EnnableKeep"];

                tbKeepInterval.Text = (string)dtCMDList.Rows[0]["SyncIntervalSec"];
                tbKeepString.Text = (string)dtCMDList.Rows[0]["SyncKeep"];

                tbQueueLog.Text = (string)dtCMDList.Rows[0]["LogQueue"];
                ckEnableRemoteObj.Checked = Convert.ToBoolean((string)dtCMDList.Rows[0]["EnableRemoteObj"]);
                ckEnableLogQueue.Checked = Convert.ToBoolean((string)dtCMDList.Rows[0]["EnableLogQueue"]);
            }
            catch
            { }
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {
            InitializeCombobox();
            InitializeText();
        }

        private void btConfig_Click(object sender, EventArgs e)
        {
            if (!((tbConKey.Text.Length >= 16 && (tbConKey.Text.Length % 8) == 0) && (tbConIV.Text.Length >= 16 && (tbConIV.Text.Length % 8) == 0)))
            {
                MessageBox.Show("Check Config.", "Message");
                return;
            }
            else
            {
                if (MessageBox.Show("Do You Want to Save.", "Message", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {

                    dtCMDList.Rows[0]["Database"] = tbConDatabase.Text;
                    dtCMDList.Rows[0]["LocalIP"] = tbConLocalIP.Text;
                    dtCMDList.Rows[0]["LocalPort"] = tbConLocalPort.Text;
                    dtCMDList.Rows[0]["ForwardPort"] = tbConForwadPort.Text;
                    dtCMDList.Rows[0]["RemotePort"] = tbConRemotePort.Text;
                    dtCMDList.Rows[0]["ReceiveTime"] = tbConReceiveTime.Text;
                    dtCMDList.Rows[0]["SendTime"] = tbConSendTime.Text;

                    dtCMDList.Rows[0]["EncryptionType"] = cbEncryption.Text;
                    dtCMDList.Rows[0]["BoxVersion"] = cbBoxType.Text;
                    dtCMDList.Rows[0]["OutputType"] = cbOutputType.Text;

                    dtCMDList.Rows[0]["EncryptionKey"] = tbConIV.Text;
                    dtCMDList.Rows[0]["EncryptionIV"] = tbConKey.Text;

                    dtCMDList.Rows[0]["SystemQueue"] = rtConSystemQueue.Text.Replace('\n', ',');

                    dtCMDList.Rows[0]["RecordConnection"] = ckConRecDB.Checked.ToString();

                    dtCMDList.Rows[0]["RamRestart"] = tbConRam.Text;

                    dtCMDList.Rows[0]["EnnableKeep"] = cbEnnableKeep.Text;
                    dtCMDList.Rows[0]["SyncIntervalSec"] = tbKeepInterval.Text;
                    dtCMDList.Rows[0]["SyncKeep"] = tbKeepString.Text;

                    dtCMDList.Rows[0]["LogQueue"] = tbQueueLog.Text;
                    dtCMDList.Rows[0]["EnableRemoteObj"] = ckEnableRemoteObj.Checked.ToString();
                    dtCMDList.Rows[0]["EnableLogQueue"] = ckEnableLogQueue.Checked.ToString();

                    btConfig.DialogResult = DialogResult.OK;
                    this.Dispose();
                }
            }
        }

        private void tbText_TextChanged(object sender, System.EventArgs e)
        {
            if ((tbConKey.Text.Length >= 16 && (tbConKey.Text.Length % 8) == 0) && (tbConIV.Text.Length >= 16 && (tbConIV.Text.Length % 8) == 0))
            {
                lbLength.Text = "Key : " + tbConKey.Text.Length + "  IV : " + tbConIV.Text.Length + " : OK";
            }
            else
            {
                lbLength.Text = "Key : " + tbConKey.Text.Length + "  IV : " + tbConIV.Text.Length + " : Error";
            }

        }


    }
}