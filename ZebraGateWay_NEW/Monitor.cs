using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Messaging;
using System.Collections;
using System.IO;
using System.Data;
using System.Net.Sockets;
using System.Threading;
using ZebraPresentationLayer;
using ZebraEncryption;
using ZebraDataCentre;
using ZebraLogTools;

namespace ZebraGateWay
{
    enum MEITRACK_MSG_LOCATION
    {
        VID = 1,
        GPS_DATE_TIME = 6,
        LAT = 4,
        LON = 5,
        SPEED = 10,
        COURSE, //�ѧ������
        TYPE_OF_FIX = 12, 
        NO_OF_SATELLITE = 8,
        ANALOG_LEVEL, //�ѧ������
        TYPE_OF_MSG //�ѧ������
    };
    public class Monitor
    {
        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);

        public event MonitorsMsgHandler OnMonitorsReceived;
        public event MonitorsMsgHandler OnMonitorsReceivedMBox;
        public event MonitorsMsgHandler2 OnMonitorsDisconnected;
       
        #region GlobleValue
        private string _dbHost = "10.0.10.70";
        public string Database 
        {
            get
            {
                return _dbHost;
            }
            set
            {
                _dbHost = value;
            }
        }
        private string _localIP = "127.0.0.1";
        public string LocalIP
        {
            get
            {
                return _localIP;
            }
            set
            {
                _localIP = value;
            }
        }
        private int _localPort = 6000;
        public int LocalPort 
        {
            get
            {
                return _localPort;
            }
            set
            {
                _localPort = value;
            }
        }
        private int _forwardPort = 8000;
        public int ForwardPort 
        {
            get
            {
                return _forwardPort;
            }
            set
            {
                _forwardPort = value;
            }
        }
        private int _remotePort = 9000;
        public int RemotePort
        {
            get
            {
                return _remotePort;
            }
            set
            {
                _remotePort = value;
            }
        }
        private int _receiveTime = 300;
        public int ReceiveTime
        {
            get
            {
                return _receiveTime;
            }
            set
            {
                _receiveTime = value;
            }
        }
        private int _sendTime = 30;
        public int SendTime
        {
            get
            {
                return _sendTime;
            }
            set
            {
                _sendTime = value;
            }
        }
        private EncryptionAlgorithm _encryptionAl = EncryptionAlgorithm.None;
        public EncryptionAlgorithm EncryptionAl
        {
            get
            {
                return _encryptionAl;
            }
            set
            {
                _encryptionAl = value;
            }
        }
        private BoxVersions _boxVersion = BoxVersions.None;
        public BoxVersions BoxVersion
        {
            get
            {
                return _boxVersion;
            }
            set
            {
                _boxVersion = value;
            }
        }
        private EncryptionDataType _outputType = EncryptionDataType.None;
        public EncryptionDataType OutputType
        {
            get
            {
                return _outputType;
            }
            set
            {
                _outputType = value;
            }
        }
        private byte[] _encryptionKey;
        public byte[] EncryptionKey
        {
            get
            {
                return _encryptionKey;
            }
            set
            {
                _encryptionKey = value;
            }
        }
        private byte[] _encryptionIV;
        public byte[] EncryptionIV
        {
            get
            {
                return _encryptionIV;
            }
            set
            {
                _encryptionIV = value;
            }
        }
        private string _systemQueue = "zbgatewayq";
        public string SystemQueue
        {
            get
            {
                return _systemQueue;
            }
            set
            {
                _systemQueue = value;
            }
        }
        private bool _RecordConnection = false;
        public bool RecordConnection
        {
            get
            {
                return _RecordConnection;
            }
            set
            {
                _RecordConnection = value;
            }
        }
        private int _ramRestart = 100;
        public int RamRestart
        {
            get
            {
                return _ramRestart;
            }
            set
            {
                _ramRestart = value;
            }
        }
        private DataTable dtXmlConfig;
        public DataTable DtXmlConfig
        {
            get
            {
                return dtXmlConfig;
            }
            set
            {
                dtXmlConfig = value;
            }
        }
        private bool _ennableKeep = false;
        public bool EnnableKeep
        {
            get
            {
                return _ennableKeep;
            }
            set
            {
                _ennableKeep = value;
            }
        }
        private int _syncInterval = 300;
        public int SyncInterval
        {
            get
            {
                return _syncInterval;
            }
            set
            {
                _syncInterval = value;
            }
        }
        private string _syncKeep = "";
        public string SyncKeep
        {
            get
            {
                return _syncKeep;
            }
            set
            {
                _syncKeep = value;
            }
        }
        private string _logQueue = "rawdata";
        public string LogQueue
        {
            get
            {
                return _logQueue;
            }
            set
            {
                _logQueue = value;
            }
        }

        private bool _enableRemoteObj = false;
        public bool EnableRemoteObj
        {
            get
            {
                return _enableRemoteObj;
            }
            set
            {
                _enableRemoteObj = value;
            }
        }

        private bool _enableLogQueue = false;
        public bool EnableLogQueue
        {
            get
            {
                return _enableLogQueue;
            }
            set
            {
                _enableLogQueue = value;
            }
        }


        private CommonQeue MQ;
        private CommonQeue MQLog;
        private List<CommonQeue> ListQueue = new List<CommonQeue>();
        public TcpSocket objTcp;
        private SQL gSql;
    
        private string HeadVID = ",VID:";
        private Queue gProcessQ = new Queue();

        #endregion

        public Monitor()
        {
            InitCfg();
            gSql = new SQL(_dbHost);
            objTcp = new TcpSocket(_localPort, _receiveTime, _sendTime, _ennableKeep, _syncKeep, _syncInterval);
            objTcp.OnReceivedMsg += new ReceivedMsgHandler(tcp_OnReceivedMsg);
            objTcp.OnNetDisconnected += new NetDisconnetedHandler(tcp_OnNetDisconnected);
        }

        private void InitCfg()
        {
            try
            {
                ListQueue.Clear();
                dtXmlConfig = createConfigTable("Config");
              
                dtXmlConfig.ReadXml("Config.xml");
                foreach (DataRow dr in dtXmlConfig.Rows)
                {
                    _dbHost = (string)dr["Database"];
                    _localIP = (string)dr["LocalIP"];
                    _localPort = Convert.ToInt16((string)dr["LocalPort"]);
                    _forwardPort = Convert.ToInt16((string)dr["ForwardPort"]);
                    _remotePort = Convert.ToInt16((string)dr["RemotePort"]);
                    _receiveTime = Convert.ToInt16((string)dr["ReceiveTime"]);
                    _sendTime = Convert.ToInt16((string)dr["SendTime"]);
                    _encryptionAl = (EncryptionAlgorithm)Enum.Parse(typeof(EncryptionAlgorithm), (string)dr["EncryptionType"]);
                    _boxVersion = (BoxVersions)Enum.Parse(typeof(BoxVersions), (string)dr["BoxVersion"]);
                    _outputType = (EncryptionDataType)Enum.Parse(typeof(EncryptionDataType), (string)dr["OutputType"]);
                    _encryptionKey = Encoding.ASCII.GetBytes(((string)dr["EncryptionKey"]).ToCharArray());
                    _encryptionIV = Encoding.ASCII.GetBytes(((string)dr["EncryptionIV"]).ToCharArray());
                    _systemQueue = (string)dr["SystemQueue"];
                    _RecordConnection = Convert.ToBoolean((string)dr["RecordConnection"]);
                    _ramRestart = Convert.ToInt32((string)dr["RamRestart"]);
                    _ennableKeep = Convert.ToBoolean((string)dr["EnnableKeep"]);
                    _syncInterval = Convert.ToInt32((string)dr["SyncIntervalSec"]);
                    _syncKeep = (string)dr["SyncKeep"];
                    _logQueue = (string)dr["LogQueue"];
                    _enableLogQueue = Convert.ToBoolean((string)dr["EnableLogQueue"]);
                    _enableRemoteObj = Convert.ToBoolean((string)dr["EnableRemoteObj"]);
                }

                foreach (String st in _systemQueue.Split(new char[] { ',' }))
                {
                    MQ = new CommonQeue(st.Trim());
                    ListQueue.Add(MQ);
                }

                MQLog = new CommonQeue(_logQueue);
            }
            catch
            {
            }
        }
        private void tcp_OnNetDisconnected(object Sender, NetDisConnectedEvent e)
        {
            try
            {
                switch (e.Event)
                {
                    case ConnectionEvent.Register:
                        {
                            int key = (int)e.tcpClient.Client.Handle;
                            CONNECTINFO ConInfo = new CONNECTINFO();
                            ConInfo.tcpclient = e.tcpClient;
                            ConInfo.ex = e.ex;
                            ConInfo.onConnect = e.onDisconnect;
                            ConInfo.Event = e.Event;
                            ConInfo.Ep = e.tcpClient.Client.RemoteEndPoint.ToString();
                         
                            OnMonitorsDisconnected(e);
                            if (!objTcp.HsClientList.ContainsKey(key))
                            {
                                objTcp.HsClientList.Add(key, ConInfo);
                            }

                            break;
                        }
                    default:
                        {
                            OnMonitorsDisconnected(e);

                            int key = (int)e.tcpClient.Client.Handle;
                            if (objTcp.HsClientList.ContainsKey(key))
                            {
                                objTcp.HsClientList.Remove(key);
                            }
                        }
                        break;
                }

                LogToFile(e.ex);
            }
            catch 
            { }
        }
        private void tcp_OnReceivedMsg(object Sender, ReceivedMsgEvent e)
        {
            try
            {
                DoCallBack(e);
            }
            catch { }
        }

        private void DoCallBack(ReceivedMsgEvent e)
        {
            NetDispatchData proc = new NetDispatchData(CallBackProc);
            proc.BeginInvoke(e, CalBackCompletedProc, null);
        }
        private void CalBackCompletedProc(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            NetDispatchData proc = (NetDispatchData)result.AsyncDelegate;
            String retStr = proc.EndInvoke(ar);

            PostMsg(retStr);
        }

        private String CallBackProc(ReceivedMsgEvent e)
        {
            String retPrnStr = "";
            String retPrnMBox = "";
            String vid = "";
            string EP = string.Empty;
            String[] stringSeparators = new string[] { "\r", "\r\n" };
            int key = (int)e.tcpClient.Client.Handle;
            string timestamp = DateTime.Now.ToString("HH:mm:ss"); 
            ReceivedMsgEvent eMsg = e;

            try
            {
                if (eMsg == null && eMsg.data.Length <= 0) return "";
              
                //Encryption
                ManagedEncryption maEn = new ManagedEncryption(_encryptionAl, _encryptionKey, _encryptionIV);
                String dataSeq = (string)maEn.InitializeDeserializer(eMsg.data, eMsg.byteread, EncryptionDataType.String);
                String[] result = dataSeq.Trim().Split(stringSeparators, StringSplitOptions.None);
                                                                                                                               // e.tcpClient.Client.LocalEndPoint.ToString()          
                if (_enableLogQueue && dataSeq.Length > 0) { PostRawData(dataSeq, e.tcpClient.Client.RemoteEndPoint.ToString(), _localIP.ToString()+':'+ _localPort.ToString()); }
                
                foreach (string str in result)
                {
                    if (str.Length < 1) continue;
                    EP = ",EP:" + e.tcpClient.Client.RemoteEndPoint.ToString();
                    if (_boxVersion != BoxVersions.None)
                    {
                        byte[] data = Encoding.ASCII.GetBytes(str.Trim().ToCharArray());
                        object obj = null;

                        switch (_boxVersion)
                        {
                            case BoxVersions.MBox:
                                {
                                    MAX2 max2 = new MAX2();
                                    obj = max2.Translate(data);
                                }
                                break;

                            case BoxVersions.KBox:
                                {
                                    WLocator wlocator = new WLocator();
                                    obj = wlocator.Translate(data);
                                }
                                break;

                            case BoxVersions.Meitrack:
                                {
                                    Meitrack meitrack = new Meitrack();
                                    obj = meitrack.Translate(data);
                                }
                                break;

                            case BoxVersions.GPSTracker:
                                {
                                    GPSTracker gpsTrack = new GPSTracker();
                                    obj = gpsTrack.Translate(data);

                                    Max2FmtMsg max2 = (Max2FmtMsg)obj;
                                    Int64 vid1 = -1;
                                    if (Int64.TryParse(max2.vid, out vid1) && vid1 > -1)
                                    {
                                        max2.vid = gSql.GetVehIDFromSerialNumber(max2.vid);
                                        obj = max2;
                                    }
                                    else
                                        obj = null;
                                }
                                break;
                        }

                        if (obj != null)
                        {
                            Max2FmtMsg max2 = (Max2FmtMsg)obj;

                            retPrnMBox += timestamp + "  Convert_," +
                                max2.header +
                                max2.vid + ',' +
                                max2.gps_date_time + ',' +
                                max2.utc + ',' +
                                max2.lat + ',' +
                                max2.lon + ',' +
                                max2.speed + ',' +
                                max2.course + ',' +
                                max2.type_of_fix + ',' +
                                max2.no_of_satellite + ',' +
                                max2.gpio_status + ',' +
                                max2.analog_level + ',' +
                                max2.type_of_message + ',' +
                                max2.tag + ',' +
                                max2.odometor + ',' +
                                max2.internal_power + ',' +
                                max2.external_power + "\n";

                            max2.conInfo = new ConnectionInfo();
                            max2.conInfo.source_ip = e.tcpClient.Client.RemoteEndPoint.ToString().Split(':')[0];
                            max2.conInfo.source_port = e.tcpClient.Client.RemoteEndPoint.ToString().Split(':')[1]; 
                            max2.conInfo.destination_ip = _localIP;
                            max2.conInfo.destination_port = _localPort.ToString();
                            max2.conInfo.type_of_box = "M";

                            vid = max2.vid;
                            PushSystemQueue(max2);
                            if (result.Length > 1) { Thread.Sleep(200); }
                        }
                    }

                    retPrnStr += timestamp + "  " + str.Trim() + "\n";

                    if (objTcp.HsClientList.ContainsKey(key))
                    {
                        CONNECTINFO info = (CONNECTINFO)objTcp.HsClientList[key];
                        info.VehID = vid;
                        objTcp.HsClientList[key] = info; 
                    }
                }
    
                #region
                if (retPrnStr.Length > 0)
                {
                    //Write log
                    string statusMsg = "";
                    if (e.tcpClient.Connected)
                    {
                        statusMsg = "  ,hd:" + e.tcpClient.Client.Handle + ", EP:" + e.tcpClient.Client.RemoteEndPoint.ToString();
                    }
                    else
                    {
                        statusMsg = "  ,hd:" + e.tcpClient.Client.Handle;
                    }
                    retPrnStr = retPrnStr.Substring(0, retPrnStr.Length - 1);

                    LogToFile(retPrnStr.Replace("\n", "\r\n") + statusMsg);

                    //if (retPrnMBox.Length > 0)
                    //{
                    //    LogToFile((retPrnMBox.Substring(0, retPrnMBox.Length - 1)).Replace("\n", "\r\n"));
                    //}
                }
                else
                {
                    return retPrnStr;
                }
                #endregion

                PostMsgMBox(retPrnMBox);
                retPrnStr += EP + HeadVID + vid;
            }
            catch 
            {
            }
            return retPrnStr;
        }
        private void PostMsg(String msg)
        {
            MonitorEvtArgs e = new MonitorEvtArgs();
            e.msg = msg;
            OnMonitorsReceived(e);
        }
        private void PostMsgMBox(String msg)
        {
            MonitorEvtArgs e = new MonitorEvtArgs();
            e.msg = msg;
            OnMonitorsReceivedMBox(e);
        }
        private void PostRawData(string msg, string source, string destination)
        {
            ZrebraFilterInterface.LogInfo e = new ZrebraFilterInterface.LogInfo();
            e.data = msg;
            e.time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            e.source = source;
            e.destination = destination;
            e.data_type = ((int)_boxVersion).ToString();
            MQLog.PushQ(e);
        }
        #region
        private void LogToFile(String msg)
        {
            WriteFileProc fn = new WriteFileProc(WriteNetFile);
            fn.BeginInvoke(msg, WriteNetFileCompleted, null);
        }
        private void WriteNetFile(String msg)
        {
            semaphor.WaitOne();
            try
            {
                TextCls text = new TextCls();
                text.WriteTextFileLiftTime(Directory.GetCurrentDirectory(), msg, 30);
            }
            catch
            { }
            semaphor.Release();
        }
        private void WriteNetFileCompleted(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            WriteFileProc proc = (WriteFileProc)result.AsyncDelegate;
            proc.EndInvoke(ar);
        }
        #endregion

        private DataTable createConfigTable(string tableName)
        {
            DataTable dtRet = new DataTable(tableName);
            dtRet.Columns.Add("Database", typeof(string));
            dtRet.Columns.Add("LocalIP", typeof(string));
            dtRet.Columns.Add("LocalPort", typeof(string));
            dtRet.Columns.Add("ForwardPort", typeof(string));
            dtRet.Columns.Add("RemotePort", typeof(string));
            dtRet.Columns.Add("ReceiveTime", typeof(string));
            dtRet.Columns.Add("SendTime", typeof(string));
            dtRet.Columns.Add("EncryptionType", typeof(string));
            dtRet.Columns.Add("BoxVersion", typeof(string));
            dtRet.Columns.Add("OutputType", typeof(string));
            dtRet.Columns.Add("EncryptionKey", typeof(string));
            dtRet.Columns.Add("EncryptionIV", typeof(string));
            dtRet.Columns.Add("SystemQueue", typeof(string));
            dtRet.Columns.Add("RecordConnection", typeof(string));
            dtRet.Columns.Add("RamRestart", typeof(string));
            dtRet.Columns.Add("EnnableKeep", typeof(string));
            dtRet.Columns.Add("SyncIntervalSec", typeof(string));
            dtRet.Columns.Add("SyncKeep", typeof(string));
            dtRet.Columns.Add("LogQueue", typeof(string));
            dtRet.Columns.Add("EnableRemoteObj", typeof(string));
            dtRet.Columns.Add("EnableLogQueue", typeof(string));
            return dtRet;
        }

        System.Threading.Semaphore SystemQueuesemaphor = new System.Threading.Semaphore(1, 1);
        private void PushSystemQueue(Max2FmtMsg obj)
        {
            SystemQueuesemaphor.WaitOne();
            if (obj.GetType() != null)
            {
                if (obj.GetType() == typeof(Max2FmtMsg))
                {
                    for (int i = 0; i < ListQueue.Count; i++)
                    {
                        ListQueue[i].PushQ(obj);
                    }
                }
            }
            SystemQueuesemaphor.Release();
        }
        public void closeSecsion()
        {
            string key = "";
            foreach (DictionaryEntry section in objTcp.HsClientList)
            {
                key += section.Key + ",";
            }
            if (key.Length > 0)
            {
                key = key.Substring(0, key.Length - 1);

                foreach (string tmp_key in key.Split(new char[] { ',' }))
                {
                    int intkey = Convert.ToInt32(tmp_key);

                    if (objTcp.HsClientList.ContainsKey(intkey))
                    {
                        ZebraGateWay.CONNECTINFO tcpClient = (ZebraGateWay.CONNECTINFO)objTcp.HsClientList[intkey];

                        try
                        {
                            if (tcpClient.tcpclient.Connected)
                            {
                                tcpClient.tcpclient.Close();
                            }
                            else
                            {
                                objTcp.HsClientList.Remove(intkey);
                            }
                        }
                        catch
                        {
                            objTcp.HsClientList.Remove(intkey);
                        }
                    }
                }
            }
        }
    }

    public class Meitrack
    {
        const char Meitrack_HEADER = '$';
        public Meitrack()
        {
        }

        public object Translate(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            if (data.Length > 0)
            {
                if (data[0] == Meitrack_HEADER && data[1] == Meitrack_HEADER)
                {
                    msg = TranslateMax2Msg(data);
                    if (msg.vid == null) return null;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            return msg;
        }

        private Max2FmtMsg TranslateMax2Msg(byte[] data)
        {
            Max2FmtMsg msg = new Max2FmtMsg();
            try
            {
                String msgStr = Encoding.ASCII.GetString(data).Trim();
                String[] seqMsg = msgStr.Split(new char[] { ',', '\n' });

                if (seqMsg[0].Length < 2)
                {
                    return msg;
                }

                if (seqMsg.Length == 23)
                {
                    msg.vid = "1000000";
                    //msg.vid = seqMsg[(int)MEITRACK_MSG_LOCATION.VID];

                    string dt = seqMsg[(int)MEITRACK_MSG_LOCATION.GPS_DATE_TIME];
                    string dt2 = "20" + dt.Substring(0, 2) + "-" + dt.Substring(2, 2) + "-" + dt.Substring(4, 2) + " " + dt.Substring(6, 2) + ":" + dt.Substring(8, 2) + ":" + dt.Substring(10, 2);
                    DateTime date_time = Convert.ToDateTime(dt2);
                    date_time = date_time.AddHours(-7);
                    string day = date_time.ToString("dd");
                    string month = date_time.ToString("MM");
                    string years = date_time.ToString("yyyy");
                    string hh = date_time.ToString("HH");
                    string mm = date_time.ToString("mm");
                    string ss = date_time.ToString("ss");
                    msg.gps_date_time = day + "" + month + "" + years.Substring(2,2);
                    msg.utc =  hh + "" + mm + "" + ss;
                    msg.lat = seqMsg[(int)MEITRACK_MSG_LOCATION.LAT];
                    msg.lon = seqMsg[(int)MEITRACK_MSG_LOCATION.LON];
                    msg.speed = seqMsg[(int)MEITRACK_MSG_LOCATION.SPEED];
                    msg.course = seqMsg[(int)MEITRACK_MSG_LOCATION.COURSE]; //�ѧ������
                    msg.analog_level = seqMsg[(int)MEITRACK_MSG_LOCATION.ANALOG_LEVEL]; //�ѧ������
                    msg.no_of_satellite = seqMsg[(int)MEITRACK_MSG_LOCATION.NO_OF_SATELLITE];
                    msg.type_of_message = "G"; //�ó� engion on/off �� I 
                    msg.tag = "";
                    msg.odometor = "";
                    msg.gpio_status = "00"; //���� engine on(10)/off(00)  
                    msg.external_power = ""; 
                    msg.internal_power = "";

                    msg.type_of_fix = "1"; //�ѧ������
                    Decimal type_of_fix_D = Convert.ToDecimal(seqMsg[(int)MEITRACK_MSG_LOCATION.TYPE_OF_FIX]);
                    int type_of_fix = Convert.ToInt32(type_of_fix_D);
                    msg.type_of_fix = Convert.ToString(type_of_fix);
                }
            }
            catch (Exception ex)
            {
                string msgex = ex.Message;
                return msg = new Max2FmtMsg();
            }

            return msg;
        }
    }
}
