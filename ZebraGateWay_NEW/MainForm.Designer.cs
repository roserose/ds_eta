namespace ZebraGateWay
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmSetCommand = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.writeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readCommandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshCommandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectedSessionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmDisplay = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsTimeStamp = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmSystem = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.rtSystemQueue = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbVehicle = new System.Windows.Forms.Label();
            this.tvConnection = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.rtMessageLog = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tbLocalIP = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.ipDataBase = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.tbSection = new System.Windows.Forms.TextBox();
            this.tbMemory = new System.Windows.Forms.TextBox();
            this.tbTheard = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.tbRemotePort = new System.Windows.Forms.TextBox();
            this.tbForwardPort = new System.Windows.Forms.TextBox();
            this.tbLocalPort = new System.Windows.Forms.TextBox();
            this.tvEngineOn = new System.Windows.Forms.TreeView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbEngineOn = new System.Windows.Forms.Label();
            this.tvOffline = new System.Windows.Forms.TreeView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbOffline = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lbEngineOff = new System.Windows.Forms.Label();
            this.tvEngineOff = new System.Windows.Forms.TreeView();
            this.cmSetCommand.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmSetCommand
            // 
            this.cmSetCommand.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.writeToolStripMenuItem,
            this.readCommandToolStripMenuItem,
            this.refreshCommandToolStripMenuItem});
            this.cmSetCommand.Name = "cmSetCommand";
            this.cmSetCommand.Size = new System.Drawing.Size(174, 70);
            // 
            // writeToolStripMenuItem
            // 
            this.writeToolStripMenuItem.Name = "writeToolStripMenuItem";
            this.writeToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.writeToolStripMenuItem.Text = "Write Command";
            this.writeToolStripMenuItem.Click += new System.EventHandler(this.writeToolStripMenuItem_Click);
            // 
            // readCommandToolStripMenuItem
            // 
            this.readCommandToolStripMenuItem.Name = "readCommandToolStripMenuItem";
            this.readCommandToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.readCommandToolStripMenuItem.Text = "Read Command";
            this.readCommandToolStripMenuItem.Click += new System.EventHandler(this.readCommandToolStripMenuItem_Click);
            // 
            // refreshCommandToolStripMenuItem
            // 
            this.refreshCommandToolStripMenuItem.Name = "refreshCommandToolStripMenuItem";
            this.refreshCommandToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.refreshCommandToolStripMenuItem.Text = "Refresh Command";
            this.refreshCommandToolStripMenuItem.Click += new System.EventHandler(this.refreshCommandToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1084, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.disconnectedSessionToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // disconnectedSessionToolStripMenuItem
            // 
            this.disconnectedSessionToolStripMenuItem.Name = "disconnectedSessionToolStripMenuItem";
            this.disconnectedSessionToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.disconnectedSessionToolStripMenuItem.Text = "Disconnected Session";
            this.disconnectedSessionToolStripMenuItem.Click += new System.EventHandler(this.disconnectedSessionToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configToolStripMenuItem,
            this.directoryToolStripMenuItem,
            this.stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.configToolStripMenuItem.Text = "Configuration";
            this.configToolStripMenuItem.Click += new System.EventHandler(this.configToolStripMenuItem_Click);
            // 
            // directoryToolStripMenuItem
            // 
            this.directoryToolStripMenuItem.Name = "directoryToolStripMenuItem";
            this.directoryToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.directoryToolStripMenuItem.Text = "Directory";
            this.directoryToolStripMenuItem.Click += new System.EventHandler(this.directoryToolStripMenuItem_Click);
            // 
            // stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem
            // 
            this.stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem.Name = "stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtT" +
    "oolStripMenuItem";
            this.stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem.Text = "Open Log Today";
            this.stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem.Click += new System.EventHandler(this.stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // tmDisplay
            // 
            this.tmDisplay.Interval = 50;
            this.tmDisplay.Tick += new System.EventHandler(this.tmDisplay_Tick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTimeStamp});
            this.statusStrip1.Location = new System.Drawing.Point(0, 491);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1084, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsTimeStamp
            // 
            this.tsTimeStamp.Name = "tsTimeStamp";
            this.tsTimeStamp.Size = new System.Drawing.Size(110, 17);
            this.tsTimeStamp.Text = "00-00-0000 00:00:00";
            // 
            // tmSystem
            // 
            this.tmSystem.Interval = 1000;
            this.tmSystem.Tick += new System.EventHandler(this.tmSystem_Tick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.37178F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.10388F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.37178F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.67566F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.37178F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.10512F));
            this.tableLayoutPanel5.Controls.Add(this.textBox17, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox1, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox2, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox3, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox4, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBox5, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox6, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox7, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox8, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox9, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox10, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBox11, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox12, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox15, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox18, 4, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox16, 4, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox19, 4, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBox20, 2, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBox21, 3, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox22, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.textBox23, 5, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox24, 5, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBox25, 5, 3);
            this.tableLayoutPanel5.Controls.Add(this.textBox26, 3, 3);
            this.tableLayoutPanel5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(108, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(507, 94);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // textBox17
            // 
            this.textBox17.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox17.Location = new System.Drawing.Point(334, 3);
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.Size = new System.Drawing.Size(87, 20);
            this.textBox17.TabIndex = 14;
            this.textBox17.Text = "Theard";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(87, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Database";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox2.Location = new System.Drawing.Point(3, 26);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(87, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "Systen Queue";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox3.Location = new System.Drawing.Point(3, 49);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(87, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.Text = "Local Port";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox4.Location = new System.Drawing.Point(3, 72);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(87, 20);
            this.textBox4.TabIndex = 3;
            this.textBox4.Text = "Forward Port";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox5.Location = new System.Drawing.Point(167, 3);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(87, 20);
            this.textBox5.TabIndex = 4;
            this.textBox5.Text = "Remote Obj";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox6.Location = new System.Drawing.Point(167, 26);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(87, 20);
            this.textBox6.TabIndex = 5;
            this.textBox6.Text = "Section";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Window;
            this.textBox7.Location = new System.Drawing.Point(96, 3);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(65, 20);
            this.textBox7.TabIndex = 6;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Window;
            this.textBox8.Location = new System.Drawing.Point(96, 26);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(65, 20);
            this.textBox8.TabIndex = 7;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Window;
            this.textBox9.Location = new System.Drawing.Point(96, 49);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(65, 20);
            this.textBox9.TabIndex = 8;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Window;
            this.textBox10.Location = new System.Drawing.Point(96, 72);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(65, 20);
            this.textBox10.TabIndex = 9;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.Window;
            this.textBox11.Location = new System.Drawing.Point(260, 3);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(68, 20);
            this.textBox11.TabIndex = 10;
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.SystemColors.Window;
            this.textBox12.Location = new System.Drawing.Point(260, 26);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(68, 20);
            this.textBox12.TabIndex = 11;
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox15.Location = new System.Drawing.Point(167, 49);
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(87, 20);
            this.textBox15.TabIndex = 12;
            this.textBox15.Text = "Online";
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox18.Location = new System.Drawing.Point(334, 26);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(87, 20);
            this.textBox18.TabIndex = 15;
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox16.Location = new System.Drawing.Point(334, 49);
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(87, 20);
            this.textBox16.TabIndex = 13;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox19.Location = new System.Drawing.Point(334, 72);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(87, 20);
            this.textBox19.TabIndex = 16;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox20.Location = new System.Drawing.Point(167, 72);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(87, 20);
            this.textBox20.TabIndex = 17;
            this.textBox20.Text = "Memory";
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Window;
            this.textBox21.Location = new System.Drawing.Point(260, 49);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(68, 20);
            this.textBox21.TabIndex = 18;
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.SystemColors.Window;
            this.textBox22.Location = new System.Drawing.Point(427, 3);
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(77, 20);
            this.textBox22.TabIndex = 19;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.SystemColors.Window;
            this.textBox23.Location = new System.Drawing.Point(427, 26);
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(77, 20);
            this.textBox23.TabIndex = 20;
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Window;
            this.textBox24.Location = new System.Drawing.Point(427, 49);
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(77, 20);
            this.textBox24.TabIndex = 21;
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.SystemColors.Window;
            this.textBox25.Location = new System.Drawing.Point(427, 72);
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.Size = new System.Drawing.Size(77, 20);
            this.textBox25.TabIndex = 22;
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.SystemColors.Window;
            this.textBox26.Location = new System.Drawing.Point(260, 72);
            this.textBox26.Name = "textBox26";
            this.textBox26.ReadOnly = true;
            this.textBox26.Size = new System.Drawing.Size(68, 20);
            this.textBox26.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(99, 97);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.27957F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.72043F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 27);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(584, 466);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.textBox40, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.rtSystemQueue, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel1, 0, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(401, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(180, 463);
            this.tableLayoutPanel4.TabIndex = 12;
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox40.Location = new System.Drawing.Point(4, 4);
            this.textBox40.Multiline = true;
            this.textBox40.Name = "textBox40";
            this.textBox40.ReadOnly = true;
            this.textBox40.Size = new System.Drawing.Size(172, 21);
            this.textBox40.TabIndex = 17;
            this.textBox40.Text = "System Queue ";
            // 
            // rtSystemQueue
            // 
            this.rtSystemQueue.Location = new System.Drawing.Point(4, 32);
            this.rtSystemQueue.Name = "rtSystemQueue";
            this.rtSystemQueue.Size = new System.Drawing.Size(172, 76);
            this.rtSystemQueue.TabIndex = 18;
            this.rtSystemQueue.Text = "";
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbVehicle);
            this.panel1.Controls.Add(this.tvConnection);
            this.panel1.Location = new System.Drawing.Point(4, 115);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(172, 340);
            this.panel1.TabIndex = 20;
            // 
            // lbVehicle
            // 
            this.lbVehicle.AutoSize = true;
            this.lbVehicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbVehicle.Location = new System.Drawing.Point(4, 4);
            this.lbVehicle.Name = "lbVehicle";
            this.lbVehicle.Size = new System.Drawing.Size(64, 13);
            this.lbVehicle.TabIndex = 2;
            this.lbVehicle.Text = "0 Vehicle ";
            // 
            // tvConnection
            // 
            this.tvConnection.BackColor = System.Drawing.Color.Gainsboro;
            this.tvConnection.ContextMenuStrip = this.cmSetCommand;
            this.tvConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tvConnection.Location = new System.Drawing.Point(4, 20);
            this.tvConnection.Name = "tvConnection";
            this.tvConnection.Size = new System.Drawing.Size(163, 315);
            this.tvConnection.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.tableLayoutPanel6);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(392, 463);
            this.panel2.TabIndex = 16;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(4, 114);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(386, 349);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rtMessageLog);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(378, 323);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Message";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // rtMessageLog
            // 
            this.rtMessageLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtMessageLog.BackColor = System.Drawing.Color.White;
            this.rtMessageLog.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.rtMessageLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.rtMessageLog.Location = new System.Drawing.Point(3, 3);
            this.rtMessageLog.Name = "rtMessageLog";
            this.rtMessageLog.ReadOnly = true;
            this.rtMessageLog.Size = new System.Drawing.Size(373, 317);
            this.rtMessageLog.TabIndex = 17;
            this.rtMessageLog.Text = "";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.59184F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.16327F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.9596F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.76768F));
            this.tableLayoutPanel6.Controls.Add(this.tbLocalIP, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.textBox32, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.ipDataBase, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.textBox28, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tbSection, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.tbMemory, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbTheard, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.textBox33, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.textBox44, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.textBox27, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.textBox31, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.textBox30, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.textBox41, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbRemotePort, 3, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbForwardPort, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbLocalPort, 1, 2);
            this.tableLayoutPanel6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(390, 109);
            this.tableLayoutPanel6.TabIndex = 11;
            // 
            // tbLocalIP
            // 
            this.tbLocalIP.BackColor = System.Drawing.SystemColors.Window;
            this.tbLocalIP.Location = new System.Drawing.Point(80, 31);
            this.tbLocalIP.Name = "tbLocalIP";
            this.tbLocalIP.ReadOnly = true;
            this.tbLocalIP.Size = new System.Drawing.Size(101, 20);
            this.tbLocalIP.TabIndex = 23;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Control;
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox32.Location = new System.Drawing.Point(188, 85);
            this.textBox32.Multiline = true;
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(55, 19);
            this.textBox32.TabIndex = 4;
            this.textBox32.Text = "Remote Obj";
            // 
            // ipDataBase
            // 
            this.ipDataBase.Location = new System.Drawing.Point(80, 4);
            this.ipDataBase.Name = "ipDataBase";
            this.ipDataBase.Size = new System.Drawing.Size(101, 20);
            this.ipDataBase.TabIndex = 25;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.Control;
            this.textBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox28.Location = new System.Drawing.Point(4, 4);
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(69, 20);
            this.textBox28.TabIndex = 0;
            this.textBox28.Text = "Database";
            // 
            // tbSection
            // 
            this.tbSection.BackColor = System.Drawing.SystemColors.Window;
            this.tbSection.Location = new System.Drawing.Point(250, 4);
            this.tbSection.Name = "tbSection";
            this.tbSection.ReadOnly = true;
            this.tbSection.Size = new System.Drawing.Size(136, 20);
            this.tbSection.TabIndex = 10;
            // 
            // tbMemory
            // 
            this.tbMemory.BackColor = System.Drawing.SystemColors.Window;
            this.tbMemory.Location = new System.Drawing.Point(250, 31);
            this.tbMemory.Name = "tbMemory";
            this.tbMemory.ReadOnly = true;
            this.tbMemory.Size = new System.Drawing.Size(136, 20);
            this.tbMemory.TabIndex = 11;
            // 
            // tbTheard
            // 
            this.tbTheard.BackColor = System.Drawing.SystemColors.Window;
            this.tbTheard.Location = new System.Drawing.Point(250, 58);
            this.tbTheard.Name = "tbTheard";
            this.tbTheard.ReadOnly = true;
            this.tbTheard.Size = new System.Drawing.Size(136, 20);
            this.tbTheard.TabIndex = 18;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Control;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox33.Location = new System.Drawing.Point(188, 4);
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(55, 20);
            this.textBox33.TabIndex = 5;
            this.textBox33.Text = "Section";
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.Control;
            this.textBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox44.Location = new System.Drawing.Point(188, 31);
            this.textBox44.Name = "textBox44";
            this.textBox44.ReadOnly = true;
            this.textBox44.Size = new System.Drawing.Size(55, 20);
            this.textBox44.TabIndex = 17;
            this.textBox44.Text = "Memory";
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.SystemColors.Control;
            this.textBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox27.Location = new System.Drawing.Point(188, 58);
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(55, 20);
            this.textBox27.TabIndex = 14;
            this.textBox27.Text = "Theard";
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Control;
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox31.Location = new System.Drawing.Point(4, 85);
            this.textBox31.Multiline = true;
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(69, 19);
            this.textBox31.TabIndex = 3;
            this.textBox31.Text = "Forward Port";
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.Control;
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox30.Location = new System.Drawing.Point(4, 58);
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.Size = new System.Drawing.Size(69, 20);
            this.textBox30.TabIndex = 2;
            this.textBox30.Text = "Local Port";
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.Control;
            this.textBox41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox41.Location = new System.Drawing.Point(4, 31);
            this.textBox41.Name = "textBox41";
            this.textBox41.ReadOnly = true;
            this.textBox41.Size = new System.Drawing.Size(69, 20);
            this.textBox41.TabIndex = 15;
            this.textBox41.Text = "Local IP";
            // 
            // tbRemotePort
            // 
            this.tbRemotePort.BackColor = System.Drawing.SystemColors.Window;
            this.tbRemotePort.Location = new System.Drawing.Point(250, 85);
            this.tbRemotePort.Name = "tbRemotePort";
            this.tbRemotePort.ReadOnly = true;
            this.tbRemotePort.Size = new System.Drawing.Size(136, 20);
            this.tbRemotePort.TabIndex = 9;
            // 
            // tbForwardPort
            // 
            this.tbForwardPort.BackColor = System.Drawing.SystemColors.Window;
            this.tbForwardPort.Location = new System.Drawing.Point(80, 85);
            this.tbForwardPort.Name = "tbForwardPort";
            this.tbForwardPort.ReadOnly = true;
            this.tbForwardPort.Size = new System.Drawing.Size(101, 20);
            this.tbForwardPort.TabIndex = 8;
            // 
            // tbLocalPort
            // 
            this.tbLocalPort.BackColor = System.Drawing.SystemColors.Window;
            this.tbLocalPort.Location = new System.Drawing.Point(80, 58);
            this.tbLocalPort.Name = "tbLocalPort";
            this.tbLocalPort.ReadOnly = true;
            this.tbLocalPort.Size = new System.Drawing.Size(101, 20);
            this.tbLocalPort.TabIndex = 7;
            // 
            // tvEngineOn
            // 
            this.tvEngineOn.BackColor = System.Drawing.Color.Gainsboro;
            this.tvEngineOn.ContextMenuStrip = this.cmSetCommand;
            this.tvEngineOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tvEngineOn.Location = new System.Drawing.Point(3, 20);
            this.tvEngineOn.Name = "tvEngineOn";
            this.tvEngineOn.Size = new System.Drawing.Size(150, 434);
            this.tvEngineOn.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel5.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.lbEngineOn);
            this.panel5.Controls.Add(this.tvEngineOn);
            this.panel5.Location = new System.Drawing.Point(590, 27);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(159, 459);
            this.panel5.TabIndex = 22;
            // 
            // lbEngineOn
            // 
            this.lbEngineOn.AutoSize = true;
            this.lbEngineOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbEngineOn.Location = new System.Drawing.Point(4, 4);
            this.lbEngineOn.Name = "lbEngineOn";
            this.lbEngineOn.Size = new System.Drawing.Size(123, 13);
            this.lbEngineOn.TabIndex = 2;
            this.lbEngineOn.Text = "0 Vehicle Engine On";
            // 
            // tvOffline
            // 
            this.tvOffline.BackColor = System.Drawing.Color.Gainsboro;
            this.tvOffline.ContextMenuStrip = this.cmSetCommand;
            this.tvOffline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tvOffline.Location = new System.Drawing.Point(3, 20);
            this.tvOffline.Name = "tvOffline";
            this.tvOffline.Size = new System.Drawing.Size(150, 434);
            this.tvOffline.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lbOffline);
            this.panel4.Controls.Add(this.tvOffline);
            this.panel4.Location = new System.Drawing.Point(920, 27);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(159, 459);
            this.panel4.TabIndex = 23;
            // 
            // lbOffline
            // 
            this.lbOffline.AutoSize = true;
            this.lbOffline.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbOffline.Location = new System.Drawing.Point(4, 4);
            this.lbOffline.Name = "lbOffline";
            this.lbOffline.Size = new System.Drawing.Size(101, 13);
            this.lbOffline.TabIndex = 2;
            this.lbOffline.Text = "0 Vehicle Offline";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(755, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 24;
            // 
            // panel3
            // 
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.lbEngineOff);
            this.panel3.Controls.Add(this.tvEngineOff);
            this.panel3.Location = new System.Drawing.Point(755, 27);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(159, 459);
            this.panel3.TabIndex = 23;
            // 
            // lbEngineOff
            // 
            this.lbEngineOff.AutoSize = true;
            this.lbEngineOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbEngineOff.Location = new System.Drawing.Point(4, 4);
            this.lbEngineOff.Name = "lbEngineOff";
            this.lbEngineOff.Size = new System.Drawing.Size(130, 13);
            this.lbEngineOff.TabIndex = 2;
            this.lbEngineOff.Text = "0 Vehicle Engine OFF";
            // 
            // tvEngineOff
            // 
            this.tvEngineOff.BackColor = System.Drawing.Color.Gainsboro;
            this.tvEngineOff.ContextMenuStrip = this.cmSetCommand;
            this.tvEngineOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tvEngineOff.Location = new System.Drawing.Point(3, 20);
            this.tvEngineOff.Name = "tvEngineOff";
            this.tvEngineOff.Size = new System.Drawing.Size(150, 434);
            this.tvEngineOff.TabIndex = 1;
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1084, 513);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(1100, 552);
            this.MinimumSize = new System.Drawing.Size(1100, 552);
            this.Name = "MainFrm";
            this.Text = "Zebra GateWay";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.cmSetCommand.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ContextMenuStrip cmSetCommand;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem writeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readCommandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disconnectedSessionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.Timer tmDisplay;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Timer tmSystem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripStatusLabel tsTimeStamp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox tbLocalPort;
        private System.Windows.Forms.TextBox tbForwardPort;
        private System.Windows.Forms.TextBox tbRemotePort;
        private System.Windows.Forms.TextBox tbSection;
        private System.Windows.Forms.TextBox tbMemory;
        private System.Windows.Forms.TextBox tbTheard;
        private System.Windows.Forms.TextBox tbLocalIP;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.RichTextBox rtMessageLog;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.RichTextBox rtSystemQueue;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ToolStripMenuItem directoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stringLogPathDirectoryGetCurrentDirectoryLogTcplogDateTimeNowToStringyyyyMMddtxtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshCommandToolStripMenuItem;
        private System.Windows.Forms.TextBox ipDataBase;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbVehicle;
        private System.Windows.Forms.TreeView tvConnection;
        private System.Windows.Forms.TreeView tvEngineOn;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbEngineOn;
        private System.Windows.Forms.TreeView tvOffline;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbOffline;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbEngineOff;
        private System.Windows.Forms.TreeView tvEngineOff;
    }
}

