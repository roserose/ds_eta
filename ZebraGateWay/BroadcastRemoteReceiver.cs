﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ZebraCommonInterface;

namespace ZebraGateWay
{
    
    public class BroadcastRemoteReceiver : MarshalByRefObject, BroadcastInterface
    {
        private MessageQueue MQ = null;
        public bool IsAlive()
        {
            return true;
        }

        public void SendMsgBulkSMS(string telno, string msg)
        {
            throw new NotImplementedException();
        }

        public void SendMsgSMS(string telno, string msg)
        {
            throw new NotImplementedException();
        }

        public void SendMsgSMSx(string telno, string msg)
        {
            throw new NotImplementedException();
        }

    

        public void InitQ(string QueueName)
        {
            string GlobalQPath = String.Format(".\\Private$\\{0}", QueueName);
            if (MessageQueue.Exists(GlobalQPath))
            {
                MQ = new MessageQueue(GlobalQPath);
            }
            else
            {
                MQ = MessageQueue.Create(GlobalQPath);
            }
        }

        public void PushQ(object txtMsg)
        {
            try
            {
                XmlWriterSettings wrSettings = new XmlWriterSettings();
                wrSettings.Indent = true;

                XmlSerializer s = new XmlSerializer(txtMsg.GetType());
                TextWriter txtWr = new StringWriter();

                XmlWriter xmlWriter = XmlWriter.Create(txtWr, wrSettings);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\"");

                s.Serialize(xmlWriter, txtMsg);
                txtWr.Close();

                byte[] buff = Encoding.UTF8.GetBytes(txtWr.ToString());
                Stream stm = new MemoryStream(buff);

                System.Messaging.Message qMsg = new System.Messaging.Message();
                qMsg.BodyStream = stm;
                MQ.Send(qMsg);
            }
            catch (Exception ex)
            {
                //WriteLogEvent("ZebraBroadcast::SMS Error #1 :" + ex.Message);
            }
        }
    }
}
