using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using System.Collections;
using System.Timers;

namespace ZebraGateWay
{
    class ClientManager
    {
        System.Threading.Semaphore processsemaphor = new System.Threading.Semaphore(1, 1);

        /*
        public string gException;
        public DateTime gOnConnect;
        public DateTime gOnDisconnect;
        public ConnectionEvent gEvent;
        public string gVehID;
        public string gStatus;
        */ 
        public TcpClient tcpClient;

        public bool isRunThread = false;

        private string syncKeep = "";
        private bool ennableKeep = false;

        private  System.Timers.Timer tmTimer;

        public ClientManager(TcpClient tcp, bool eKeep, string syncString,int syncInterval)
        {
            tcpClient = tcp;
            Thread receiveTh = new Thread(new ThreadStart(BeginReceived));
            receiveTh.Start();

            ennableKeep = eKeep;
            syncKeep = syncString;

            tmTimer = new System.Timers.Timer(5000); // 5 seconds

            if (ennableKeep)
            {
                tmTimer.Elapsed += new ElapsedEventHandler(tmTimer_Elapsed);

                tmTimer.Interval = 1000 * syncInterval;
                tmTimer.Enabled = true;
            }
        }

        void tmTimer_Elapsed(object sender, ElapsedEventArgs e)
       {
            tmTimer.Enabled = false;
            try
            {
                if (tcpClient.Client.Connected)
                {
                    SendData(syncKeep);
                    tmTimer.Enabled = true;
                }
            }
            catch { }
        }

        private void BeginReceived()
        {
            int cnt = 0;
            while (!isRunThread)
            {
                Thread.Sleep(200);
                if (cnt > 100) { break; }
                cnt++;
            }

            if (tcpClient != null && !tcpClient.Client.Connected)
            {
                tcpClient.Close();
                return;
            }
            else
            {
                 OnDisConnected(tcpClient, "New Client Accept", ConnectionEvent.Register);
            }
               
            #region
            NetworkStream clientStream = tcpClient.GetStream();
            byte[] message;

            while (true)
            {
                message = new byte[4096];
                StringBuilder myCompleteMessage = new StringBuilder();
                int bytesRead = 0;

                try
                {
                    bytesRead = clientStream.Read(message, 0, 4096);
                }
                catch(Exception ex)
                {
                    Thread.Sleep(1000 * 5);
                    OnDisConnected(tcpClient, "Socket Error : " + ex.Message, ConnectionEvent.SockerError);
                    break;
                }

                if (bytesRead == 0)
                {
                    Thread.Sleep(1000 * 5);
                    OnDisConnected(tcpClient, "Disconnect", ConnectionEvent.Disconnect);
                    break;
                }


                //"#130905024752,5,Command Error,1301100006\r"
                //"#130905033202,7,Command Error\r,1301100004\r"
               // string msg = Encoding.ASCII.GetString(message, 0, bytesRead);
                this.OnDataReceived(tcpClient, message, bytesRead);

            }
             
            #endregion
            tmTimer.Enabled = false;
            clientStream.Close();
            tcpClient.Close();
        }

        private void OnDataReceived(TcpClient tcp, byte[] data, int leng)
        {
            try
            {
                ReceivedMsgEvent evt = new ReceivedMsgEvent();
                evt.data = data;
                evt.tcpClient = tcp;
                evt.byteread = leng;
                evt.onConnect = DateTime.Now;
                FileProcess(evt);
            }
            catch (Exception e)
            {
                OnDisConnected(tcp, e.Message, ConnectionEvent.Exception);
            }
        }

        private void FileProcess(ReceivedMsgEvent info)
        {
            FileProcess fn = new FileProcess(OnReceiveMsg);
            fn.BeginInvoke(info, ProcessFileCompleted, null);
        }
    
        private void ProcessFileCompleted(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;
            FileProcess proc = (FileProcess)result.AsyncDelegate;
            proc.EndInvoke(ar);
        }

        private void OnDisConnected(TcpClient tcp, string ex,ConnectionEvent ConEvent)
        {
            try {
                NetDisConnectedEvent evt = new NetDisConnectedEvent();
                evt.tcpClient = tcp;
                evt.ex = DateTime.Now.ToString("HH:mm:ss") + "  " + ex + " ,hd:" + tcpClient.Client.Handle;
                evt.onDisconnect = DateTime.Now;
                evt.Event = ConEvent;
                this.OnDisconnectMsg(evt);
            }
            catch 
            {
            }
        }

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        private bool SendData(string msg)
        {
            semaphor.WaitOne();
            try
            {
                NetworkStream clientStream = tcpClient.GetStream();
                ASCIIEncoding encoder = new ASCIIEncoding();

                byte[] buffer = null;
                buffer = encoder.GetBytes(msg + '\r');
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch
            {
                semaphor.Release();
                return false;
            }
            semaphor.Release();
            return true;
        }

        #region Events
        public event NetMsgReceivedEventHandler ReceiveMsg;
        protected virtual void OnReceiveMsg(ReceivedMsgEvent e)
        {
            processsemaphor.WaitOne();
            try
            {
                if (ReceiveMsg != null)
                    ReceiveMsg(this, e);
            }
            catch(Exception ex)
            {
                OnDisConnected(e.tcpClient, ex.Message, ConnectionEvent.Exception);
            }
            processsemaphor.Release();
        }

        public event NetDisconnetedHandler OnDisconnect;
        protected virtual void OnDisconnectMsg(NetDisConnectedEvent e)
        {
            try
            {
                if (OnDisconnect != null)
                    OnDisconnect(this, e);
            }
            catch { }
        }
        #endregion


    }
}
