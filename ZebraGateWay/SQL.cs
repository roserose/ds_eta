using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ZebraGateWay
{
    class SQL
    {
        //private string dbConn = "Data Source = 10.0.10.70; Initial Catalog = ZebraDB; uid = sa; pwd = aab-gps-gtt";
        private string dbConn = string.Empty;
        public SQL(string db)
        {
           dbConn = string.Format(@"Data Source = {0}; Initial Catalog = ZebraDB; uid = sa; pwd = aab-gps-gtt",db);
        }

        public void InsertNewBoxStatus(int zVehID, string zIP, int zPort, bool isConnected, string zLocalIP, int zLocalPort)
        {
            try
            {
                SqlConnection cn = new SqlConnection(dbConn);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "spBox_Mbox_InsertNewStatus_3";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", zVehID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@ip", zIP);
                cm.Parameters.Add(p);

                p = new SqlParameter("@port", zPort);
                cm.Parameters.Add(p);

                p = new SqlParameter("@isConnected", isConnected);
                cm.Parameters.Add(p);

                p = new SqlParameter("@local_port", zLocalPort);
                cm.Parameters.Add(p);

                p = new SqlParameter("@local_ip", zLocalIP);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
            }
        }
        public void GetLastIOStatus(int veh_id, out int state, out int ref_idx)
        {
            state = 0;
            ref_idx = 0;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetLastIOStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (int)cm.Parameters["@ref_idx"].Value;
                    }
                    catch
                    {
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

        }

        public string GetVehIDFromSerialNumber(string emi)
        {
            string veh_id = "100";
            try
            {
                DataTable fzTable = new DataTable();
                using (SqlConnection cn = new SqlConnection(dbConn))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spGet_VehIDFromSerialNumber";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@emi", emi);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        veh_id = cm.Parameters["@veh_id"].Value.ToString();
                    }
                    catch
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return veh_id;
        }
    }
}
