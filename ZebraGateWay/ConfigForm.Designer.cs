namespace ZebraGateWay
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.rtConSystemQueue = new System.Windows.Forms.RichTextBox();
            this.btConfig = new System.Windows.Forms.Button();
            this.tbConReceiveTime = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.tbConRemotePort = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.tbConForwadPort = new System.Windows.Forms.TextBox();
            this.tbConLocalPort = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tbConRam = new System.Windows.Forms.TextBox();
            this.tbKeepString = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.tbQueueLog = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbConLocalIP = new iptb.IPTextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tbKeepInterval = new System.Windows.Forms.TextBox();
            this.cbEnnableKeep = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.cbOutputType = new System.Windows.Forms.ComboBox();
            this.cbBoxType = new System.Windows.Forms.ComboBox();
            this.cbEncryption = new System.Windows.Forms.ComboBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tbConSendTime = new System.Windows.Forms.TextBox();
            this.tbConDatabase = new System.Windows.Forms.TextBox();
            this.ckConRecDB = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbConKey = new System.Windows.Forms.TextBox();
            this.tbConIV = new System.Windows.Forms.TextBox();
            this.lbLength = new System.Windows.Forms.Label();
            this.ckEnableLogQueue = new System.Windows.Forms.CheckBox();
            this.ckEnableRemoteObj = new System.Windows.Forms.CheckBox();
            this.ckEnableTrigger = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.textBox40, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.rtConSystemQueue, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 265);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.59575F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 73.40426F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(530, 107);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox40.Location = new System.Drawing.Point(4, 4);
            this.textBox40.Name = "textBox40";
            this.textBox40.ReadOnly = true;
            this.textBox40.Size = new System.Drawing.Size(522, 20);
            this.textBox40.TabIndex = 16;
            this.textBox40.Text = "System Queue : Queue per Line";
            // 
            // rtConSystemQueue
            // 
            this.rtConSystemQueue.Location = new System.Drawing.Point(4, 32);
            this.rtConSystemQueue.Name = "rtConSystemQueue";
            this.rtConSystemQueue.Size = new System.Drawing.Size(522, 71);
            this.rtConSystemQueue.TabIndex = 12;
            this.rtConSystemQueue.Text = "";
            // 
            // btConfig
            // 
            this.btConfig.Location = new System.Drawing.Point(459, 431);
            this.btConfig.Name = "btConfig";
            this.btConfig.Size = new System.Drawing.Size(75, 23);
            this.btConfig.TabIndex = 14;
            this.btConfig.Text = "Config";
            this.btConfig.UseVisualStyleBackColor = true;
            this.btConfig.Click += new System.EventHandler(this.btConfig_Click);
            // 
            // tbConReceiveTime
            // 
            this.tbConReceiveTime.BackColor = System.Drawing.SystemColors.Window;
            this.tbConReceiveTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConReceiveTime.Location = new System.Drawing.Point(130, 134);
            this.tbConReceiveTime.Name = "tbConReceiveTime";
            this.tbConReceiveTime.Size = new System.Drawing.Size(131, 20);
            this.tbConReceiveTime.TabIndex = 5;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox33.Location = new System.Drawing.Point(4, 134);
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(119, 20);
            this.textBox33.TabIndex = 5;
            this.textBox33.Text = "Receive TimeOut";
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox13.Location = new System.Drawing.Point(268, 56);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(119, 20);
            this.textBox13.TabIndex = 32;
            this.textBox13.Text = "Box Version : Cmd";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox5.Location = new System.Drawing.Point(268, 82);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(119, 20);
            this.textBox5.TabIndex = 23;
            this.textBox5.Text = "Output Type";
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox32.Location = new System.Drawing.Point(4, 108);
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(119, 20);
            this.textBox32.TabIndex = 4;
            this.textBox32.Text = "Remote Obj";
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox28.Location = new System.Drawing.Point(4, 4);
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(119, 20);
            this.textBox28.TabIndex = 0;
            this.textBox28.Text = "Database";
            // 
            // tbConRemotePort
            // 
            this.tbConRemotePort.BackColor = System.Drawing.SystemColors.Window;
            this.tbConRemotePort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConRemotePort.Location = new System.Drawing.Point(130, 108);
            this.tbConRemotePort.Name = "tbConRemotePort";
            this.tbConRemotePort.Size = new System.Drawing.Size(131, 20);
            this.tbConRemotePort.TabIndex = 4;
            // 
            // textBox41
            // 
            this.textBox41.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox41.Location = new System.Drawing.Point(268, 30);
            this.textBox41.Name = "textBox41";
            this.textBox41.ReadOnly = true;
            this.textBox41.Size = new System.Drawing.Size(119, 20);
            this.textBox41.TabIndex = 15;
            this.textBox41.Text = "Encryption Type";
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox31.Location = new System.Drawing.Point(4, 82);
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(119, 20);
            this.textBox31.TabIndex = 3;
            this.textBox31.Text = "Forward Port";
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox30.Location = new System.Drawing.Point(4, 56);
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.Size = new System.Drawing.Size(119, 20);
            this.textBox30.TabIndex = 2;
            this.textBox30.Text = "Local Port";
            // 
            // tbConForwadPort
            // 
            this.tbConForwadPort.BackColor = System.Drawing.SystemColors.Window;
            this.tbConForwadPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConForwadPort.Location = new System.Drawing.Point(130, 82);
            this.tbConForwadPort.Name = "tbConForwadPort";
            this.tbConForwadPort.Size = new System.Drawing.Size(131, 20);
            this.tbConForwadPort.TabIndex = 3;
            // 
            // tbConLocalPort
            // 
            this.tbConLocalPort.BackColor = System.Drawing.SystemColors.Window;
            this.tbConLocalPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConLocalPort.Location = new System.Drawing.Point(130, 56);
            this.tbConLocalPort.Name = "tbConLocalPort";
            this.tbConLocalPort.Size = new System.Drawing.Size(131, 20);
            this.tbConLocalPort.TabIndex = 2;
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox4.Location = new System.Drawing.Point(4, 160);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(119, 20);
            this.textBox4.TabIndex = 16;
            this.textBox4.Text = "Send TimeOut";
            // 
            // tbConRam
            // 
            this.tbConRam.BackColor = System.Drawing.SystemColors.Window;
            this.tbConRam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConRam.Location = new System.Drawing.Point(394, 4);
            this.tbConRam.Name = "tbConRam";
            this.tbConRam.Size = new System.Drawing.Size(131, 20);
            this.tbConRam.TabIndex = 27;
            // 
            // tbKeepString
            // 
            this.tbKeepString.BackColor = System.Drawing.SystemColors.Window;
            this.tbKeepString.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbKeepString.Location = new System.Drawing.Point(394, 160);
            this.tbKeepString.Name = "tbKeepString";
            this.tbKeepString.Size = new System.Drawing.Size(132, 20);
            this.tbKeepString.TabIndex = 28;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel6.Controls.Add(this.textBox9, 3, 7);
            this.tableLayoutPanel6.Controls.Add(this.textBox7, 2, 7);
            this.tableLayoutPanel6.Controls.Add(this.tbQueueLog, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.textBox2, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.tbConLocalIP, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.textBox28, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.textBox8, 2, 6);
            this.tableLayoutPanel6.Controls.Add(this.tbKeepString, 3, 6);
            this.tableLayoutPanel6.Controls.Add(this.textBox3, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.tbKeepInterval, 3, 5);
            this.tableLayoutPanel6.Controls.Add(this.cbEnnableKeep, 3, 4);
            this.tableLayoutPanel6.Controls.Add(this.textBox1, 2, 4);
            this.tableLayoutPanel6.Controls.Add(this.textBox5, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.textBox11, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.cbOutputType, 3, 3);
            this.tableLayoutPanel6.Controls.Add(this.textBox13, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.cbBoxType, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.textBox41, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.cbEncryption, 3, 1);
            this.tableLayoutPanel6.Controls.Add(this.tbConRam, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.textBox6, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.textBox4, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.tbConSendTime, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.tbConReceiveTime, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.tbConRemotePort, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.textBox33, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.textBox32, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.textBox31, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.tbConForwadPort, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.textBox30, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.tbConLocalPort, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.tbConDatabase, 1, 0);
            this.tableLayoutPanel6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(4, 2);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 8;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(530, 209);
            this.tableLayoutPanel6.TabIndex = 9;
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Location = new System.Drawing.Point(394, 186);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(131, 20);
            this.textBox9.TabIndex = 48;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox7.Location = new System.Drawing.Point(268, 186);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(119, 20);
            this.textBox7.TabIndex = 48;
            // 
            // tbQueueLog
            // 
            this.tbQueueLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbQueueLog.Location = new System.Drawing.Point(130, 186);
            this.tbQueueLog.Name = "tbQueueLog";
            this.tbQueueLog.Size = new System.Drawing.Size(131, 20);
            this.tbQueueLog.TabIndex = 47;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox2.Location = new System.Drawing.Point(4, 186);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(119, 20);
            this.textBox2.TabIndex = 47;
            this.textBox2.Text = "Log Queue";
            // 
            // tbConLocalIP
            // 
            this.tbConLocalIP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConLocalIP.Location = new System.Drawing.Point(130, 30);
            this.tbConLocalIP.Name = "tbConLocalIP";
            this.tbConLocalIP.Size = new System.Drawing.Size(131, 19);
            this.tbConLocalIP.TabIndex = 45;
            this.tbConLocalIP.ToolTipText = "";
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox8.Location = new System.Drawing.Point(268, 160);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(119, 20);
            this.textBox8.TabIndex = 37;
            this.textBox8.Text = "KeepString";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox3.Location = new System.Drawing.Point(268, 134);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(119, 20);
            this.textBox3.TabIndex = 39;
            this.textBox3.Text = "KeepInterval";
            // 
            // tbKeepInterval
            // 
            this.tbKeepInterval.BackColor = System.Drawing.SystemColors.Window;
            this.tbKeepInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbKeepInterval.Location = new System.Drawing.Point(394, 134);
            this.tbKeepInterval.Name = "tbKeepInterval";
            this.tbKeepInterval.Size = new System.Drawing.Size(131, 20);
            this.tbKeepInterval.TabIndex = 42;
            // 
            // cbEnnableKeep
            // 
            this.cbEnnableKeep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEnnableKeep.FormattingEnabled = true;
            this.cbEnnableKeep.Items.AddRange(new object[] {
            "TRUE",
            "FALSE"});
            this.cbEnnableKeep.Location = new System.Drawing.Point(394, 108);
            this.cbEnnableKeep.Name = "cbEnnableKeep";
            this.cbEnnableKeep.Size = new System.Drawing.Size(131, 21);
            this.cbEnnableKeep.TabIndex = 44;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox1.Location = new System.Drawing.Point(268, 108);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(119, 20);
            this.textBox1.TabIndex = 40;
            this.textBox1.Text = "EnnableKeepAlive";
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox11.Location = new System.Drawing.Point(4, 30);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(119, 20);
            this.textBox11.TabIndex = 43;
            this.textBox11.Text = "Local IP";
            // 
            // cbOutputType
            // 
            this.cbOutputType.FormattingEnabled = true;
            this.cbOutputType.Location = new System.Drawing.Point(394, 82);
            this.cbOutputType.Name = "cbOutputType";
            this.cbOutputType.Size = new System.Drawing.Size(132, 21);
            this.cbOutputType.TabIndex = 9;
            // 
            // cbBoxType
            // 
            this.cbBoxType.FormattingEnabled = true;
            this.cbBoxType.Location = new System.Drawing.Point(394, 56);
            this.cbBoxType.Name = "cbBoxType";
            this.cbBoxType.Size = new System.Drawing.Size(132, 21);
            this.cbBoxType.TabIndex = 8;
            // 
            // cbEncryption
            // 
            this.cbEncryption.FormattingEnabled = true;
            this.cbEncryption.Location = new System.Drawing.Point(394, 30);
            this.cbEncryption.Name = "cbEncryption";
            this.cbEncryption.Size = new System.Drawing.Size(132, 21);
            this.cbEncryption.TabIndex = 7;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.GrayText;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox6.Location = new System.Drawing.Point(268, 4);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(119, 20);
            this.textBox6.TabIndex = 36;
            this.textBox6.Text = "Restart : Mb";
            // 
            // tbConSendTime
            // 
            this.tbConSendTime.BackColor = System.Drawing.SystemColors.Window;
            this.tbConSendTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConSendTime.Location = new System.Drawing.Point(130, 160);
            this.tbConSendTime.Name = "tbConSendTime";
            this.tbConSendTime.Size = new System.Drawing.Size(131, 20);
            this.tbConSendTime.TabIndex = 6;
            // 
            // tbConDatabase
            // 
            this.tbConDatabase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConDatabase.Location = new System.Drawing.Point(130, 4);
            this.tbConDatabase.Name = "tbConDatabase";
            this.tbConDatabase.Size = new System.Drawing.Size(131, 20);
            this.tbConDatabase.TabIndex = 46;
            // 
            // ckConRecDB
            // 
            this.ckConRecDB.AutoSize = true;
            this.ckConRecDB.Location = new System.Drawing.Point(8, 378);
            this.ckConRecDB.Name = "ckConRecDB";
            this.ckConRecDB.Size = new System.Drawing.Size(182, 17);
            this.ckConRecDB.TabIndex = 13;
            this.ckConRecDB.Text = "Record Last Connect on DBHost";
            this.ckConRecDB.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 220);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "KEY :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 244);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "IV :";
            // 
            // tbConKey
            // 
            this.tbConKey.Location = new System.Drawing.Point(41, 217);
            this.tbConKey.Name = "tbConKey";
            this.tbConKey.Size = new System.Drawing.Size(488, 20);
            this.tbConKey.TabIndex = 10;
            this.tbConKey.TextChanged += new System.EventHandler(this.tbText_TextChanged);
            // 
            // tbConIV
            // 
            this.tbConIV.Location = new System.Drawing.Point(41, 241);
            this.tbConIV.Name = "tbConIV";
            this.tbConIV.Size = new System.Drawing.Size(488, 20);
            this.tbConIV.TabIndex = 11;
            this.tbConIV.TextChanged += new System.EventHandler(this.tbText_TextChanged);
            // 
            // lbLength
            // 
            this.lbLength.AutoSize = true;
            this.lbLength.Location = new System.Drawing.Point(1, 454);
            this.lbLength.Name = "lbLength";
            this.lbLength.Size = new System.Drawing.Size(10, 13);
            this.lbLength.TabIndex = 16;
            this.lbLength.Text = "-";
            // 
            // ckEnableLogQueue
            // 
            this.ckEnableLogQueue.AutoSize = true;
            this.ckEnableLogQueue.Location = new System.Drawing.Point(195, 401);
            this.ckEnableLogQueue.Name = "ckEnableLogQueue";
            this.ckEnableLogQueue.Size = new System.Drawing.Size(115, 17);
            this.ckEnableLogQueue.TabIndex = 20;
            this.ckEnableLogQueue.Text = "Enable Log Queue";
            this.ckEnableLogQueue.UseVisualStyleBackColor = true;
            // 
            // ckEnableRemoteObj
            // 
            this.ckEnableRemoteObj.AutoSize = true;
            this.ckEnableRemoteObj.Location = new System.Drawing.Point(196, 378);
            this.ckEnableRemoteObj.Name = "ckEnableRemoteObj";
            this.ckEnableRemoteObj.Size = new System.Drawing.Size(118, 17);
            this.ckEnableRemoteObj.TabIndex = 19;
            this.ckEnableRemoteObj.Text = "Enable Remote Obj";
            this.ckEnableRemoteObj.UseVisualStyleBackColor = true;
            // 
            // ckEnableTrigger
            // 
            this.ckEnableTrigger.AutoSize = true;
            this.ckEnableTrigger.Enabled = false;
            this.ckEnableTrigger.Location = new System.Drawing.Point(8, 401);
            this.ckEnableTrigger.Name = "ckEnableTrigger";
            this.ckEnableTrigger.Size = new System.Drawing.Size(95, 17);
            this.ckEnableTrigger.TabIndex = 18;
            this.ckEnableTrigger.Text = "Enable Trigger";
            this.ckEnableTrigger.UseVisualStyleBackColor = true;
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 466);
            this.Controls.Add(this.ckEnableLogQueue);
            this.Controls.Add(this.ckEnableRemoteObj);
            this.Controls.Add(this.ckEnableTrigger);
            this.Controls.Add(this.lbLength);
            this.Controls.Add(this.tbConIV);
            this.Controls.Add(this.tbConKey);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ckConRecDB);
            this.Controls.Add(this.btConfig);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfigForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Setting";
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.RichTextBox rtConSystemQueue;
        private System.Windows.Forms.TextBox tbConReceiveTime;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox tbConRemotePort;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox tbConForwadPort;
        private System.Windows.Forms.TextBox tbConLocalPort;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox tbConRam;
        private System.Windows.Forms.TextBox tbKeepString;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.ComboBox cbEncryption;
        private System.Windows.Forms.CheckBox ckConRecDB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbConKey;
        private System.Windows.Forms.TextBox tbConIV;
        private System.Windows.Forms.ComboBox cbBoxType;
        public System.Windows.Forms.Button btConfig;
        private System.Windows.Forms.ComboBox cbOutputType;
        private System.Windows.Forms.TextBox tbConSendTime;
        private System.Windows.Forms.Label lbLength;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox tbKeepInterval;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.ComboBox cbEnnableKeep;
        private iptb.IPTextBox tbConLocalIP;
        private System.Windows.Forms.TextBox tbConDatabase;
        private System.Windows.Forms.TextBox tbQueueLog;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.CheckBox ckEnableLogQueue;
        private System.Windows.Forms.CheckBox ckEnableRemoteObj;
        private System.Windows.Forms.CheckBox ckEnableTrigger;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox7;
    }
}