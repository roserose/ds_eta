using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;

namespace ZebraGateWay
{
    public delegate void NetMsgReceivedEventHandler(object sender, ReceivedMsgEvent e);
    public delegate void NetMsgSentEventHandler(object sender, EventArgs e);
    public delegate void NetMsgSendingFailedEventHandler(object sender, EventArgs e);
    public delegate void NetDisconnetedHandler(object sender, NetDisConnectedEvent e);
    public delegate void ThreadExpiredHandler(object obj);
    public delegate String NetDispatchData(ReceivedMsgEvent e);
    public delegate void WriteFileProc(String msg);
    public delegate void SetTextCallback(String t);
    public delegate void MonitorsMsgHandler(MonitorEvtArgs e);
    public delegate void MonitorsMsgHandler2(NetDisConnectedEvent e);
    public delegate void FileProcess(ReceivedMsgEvent info);

    [Serializable]
    public class MonitorEvtArgs : EventArgs
    {
        public String msg;
    }

    public class EventWrapper : MarshalByRefObject
    {
        public event MonitorsMsgHandler OnMonitorsMsg;
        public void MonitorsEventHandler(MonitorEvtArgs o)
        {
            OnMonitorsMsg(o);
        }
    }

    public class ClientEventArgs : EventArgs
    {
        private Socket socket;
        public IPAddress IP
        {
            get { return ((IPEndPoint)this.socket.RemoteEndPoint).Address; }
        }
        public int Port
        {
            get { return ((IPEndPoint)this.socket.RemoteEndPoint).Port; }
        }
        public ClientEventArgs(Socket clientManagerSocket)
        {
            this.socket = clientManagerSocket;
        }
    }

    public struct CONNECTINFO
    {
        public TcpClient tcpclient;
        public string ex;
        public DateTime onConnect;
        public DateTime onDisconnect;
        public ConnectionEvent Event;
        public string VehID;
        public string Status;
        public string Ep;
    }
    public struct BufferInfo
    {
        public Queue qBufferLog;
        public bool isProcess;
    }
    public struct VehInfo
    {
        public int zVehID;
        public string zSourceIP;
        public int zSourcePort;
        public bool zConnected;
        public string zLocalIP;
        public int zLocalPort;

    }
    public enum ConnectionEvent { Register = 0, SockerError = 1, Disconnect = 2, ReceivedData = 3, Exception = 4 }
   
    public class NetDisConnectedEvent : EventArgs
    {
        public TcpClient tcpClient;
        public string ex;
        public byte[] data;
        public DateTime onDisconnect;
        public ConnectionEvent Event;
    }

    public class ReceivedMsgEvent : EventArgs
    {
        public TcpClient tcpClient;
        public byte[] data;
        public int byteread;
        public DateTime onConnect;
    }
   
}
