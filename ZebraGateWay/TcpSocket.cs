using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Collections;

namespace ZebraGateWay
{
    public delegate void ReceivedMsgHandler(Object Sender, ReceivedMsgEvent e);

    public class TcpSocket
    {
        private Hashtable _hsClientList = new Hashtable();
        public Hashtable HsClientList
        {
            get
            {
                return _hsClientList;
            }
            set
            {
                _hsClientList = value;
            }
        }

        private bool _isClose = false;
        public bool IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                _isClose = value;
            }
        }

        public event ReceivedMsgHandler OnReceivedMsg;
        public event NetDisconnetedHandler OnNetDisconnected;
        System.Threading.Semaphore clientListBusy = new System.Threading.Semaphore(1, 1);
  
        private TcpListener tcpListener;
        private Thread TcplistenThread;
        private int CONNECTION_TIMER_RECEIVE;
        private int CONNECTION_TIMER_SEND;
        private int TcpPort;
        private bool EnnableKeep = false;
        private string SyncKeep = "";
        private int SyncInterval = 300;

        public TcpSocket(int port, int RECEIVE, int SEND,bool keep, string sync,int syncInterval)
        {
            TcpPort = port;
            CONNECTION_TIMER_RECEIVE = 1000 * RECEIVE;
            CONNECTION_TIMER_SEND = 1000 * SEND;
            tcpListener = new TcpListener(IPAddress.Any, TcpPort);
            TcplistenThread = new Thread(new ThreadStart(ListenForClients));
            EnnableKeep = keep;
            SyncKeep = sync;
            SyncInterval = syncInterval;

            if (TcplistenThread != null)
            {
                TcplistenThread.Start();
            }
        }

        private void ListenForClients()
        {
            try
            {
                tcpListener.Start();

                while (!_isClose)
                {
                    try
                    {
                        TcpClient client = tcpListener.AcceptTcpClient();
                        client.ReceiveTimeout = CONNECTION_TIMER_RECEIVE;
                        client.SendTimeout = CONNECTION_TIMER_SEND;

                        Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                        clientThread.Start(client);
                    }
                    catch 
                    {
                    
                    }
                }
            }
            catch (Exception ex)
            {
                NetDisConnectedEvent evt = new NetDisConnectedEvent();
                evt.Event = ConnectionEvent.Exception;
                evt.ex = ex.Message;
                OnDisconnectMsg(evt);
            }
        }
        private void HandleClientComm(object obj)
        {
            clientListBusy.WaitOne();
            try
            {
                TcpClient tcpClient = (TcpClient)obj;
                ClientManager cmg = new ClientManager(tcpClient, EnnableKeep, SyncKeep, SyncInterval);
                cmg.ReceiveMsg += new NetMsgReceivedEventHandler(cmg_ReceiveMsg);
                cmg.OnDisconnect += new NetDisconnetedHandler(cmg_OnDisconnect);
                cmg.isRunThread = true;

                if (_isClose)
                {
                    tcpClient.Close();
                }
            }
            catch
            {
            }
            clientListBusy.Release();
        }
        private void cmg_ReceiveMsg(object sender, ReceivedMsgEvent e)
        {
            ReceiveMsg(e);
        }
        private void cmg_OnDisconnect(object sender, NetDisConnectedEvent e)
        {
            OnDisconnectMsg(e);
        }
        protected virtual void ReceiveMsg(ReceivedMsgEvent e)
        {
            try
            {
                if (OnReceivedMsg != null)
                    OnReceivedMsg(this, e);
            }
            catch { }
        }
        protected virtual void OnDisconnectMsg(NetDisConnectedEvent e)
        {
            try
            {
                if (OnNetDisconnected != null)
                    OnNetDisconnected(this, e);
            }
            catch { 
            }
        }
    } // END TcpSocket
 
}
