using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using ZebraGatewayInterface;

namespace ZebraRemoteInterface
{    
    public class RemoteSeverInterface 
    {
        public  event RemoteMsgHandler OnRemoteMsg;

        #region properties
        private Socket      gNetObj ;

        private const int   MAXCONN = 10 ;
        private int         gPort ;

        private Boolean     bIsRun = true ;
        public ArrayList   connList = new ArrayList();
        #endregion

        public RemoteSeverInterface(int port)
        {
            gPort = port;            
        }
        ~RemoteSeverInterface()
        {
            bIsRun = false;
        }
        public void Dispose()
        {
            bIsRun = false;

            if (connList != null && connList.Count > 0)
            {
                foreach (ClientList c in connList)
                {
                    try {
                        c.Dispose();
                    } catch {
                    }
                }
                connList = null;
            }

            try {
                if (gNetObj != null)
                {
                    if (gNetObj.Connected)
                    {
                        gNetObj.Disconnect(false);
                    }
                    gNetObj.Close();
                }
            } catch {
            }

            connList = null;
        }

        public void Init()
        {
            try
            {
                gNetObj = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint ep = new IPEndPoint(IPAddress.Any, gPort);
                gNetObj.Bind((EndPoint)ep);
                gNetObj.Listen(10);

                Thread remThread = new Thread(new ParameterizedThreadStart(WaitForRemote));
                remThread.Start(gNetObj);
            }
            catch
            {
            }
        }
        private void WaitForRemote(object obj)
        {
            if (gNetObj == null) return;
            while (bIsRun)
            {
                try
                {
                    Socket sck = gNetObj.Accept();                    
                    RECEIVESOCKETINFO rcvInfo = new RECEIVESOCKETINFO();
                    rcvInfo.buf = new byte[1024];
                    rcvInfo.socket = sck;
                    ClientList clnList = new ClientList(rcvInfo);
                    clnList.OnServerReceiveMsg += new ServerReceiveMsgHandler(clnList_OnServerReceiveMsg);
                    connList.Add(clnList);
                }
                catch
                {
                }
            }
        }

        private void clnList_OnServerReceiveMsg(Socket socket, int veh_id, string cmd, string data)
        {
            if (OnRemoteMsg != null) {
                OnRemoteMsg(socket, veh_id, cmd, data);
            }
        }

        public void SendCmd(Socket socket, int veh_id, string cmd, string data)
        {
            try
            {
                RECEIVESOCKETINFO info = new RECEIVESOCKETINFO();
                info.veh_id = veh_id;
                info.cmd = cmd;
                info.data = data;

                string msg = "VID"+info.veh_id.ToString() + "," + info.cmd + "," + info.data;
                byte[] buf = Encoding.ASCII.GetBytes(msg.ToCharArray());

                if (socket != null && socket.Connected)
                {
                    NetworkStream stm = new NetworkStream(socket);
                    stm.Write(buf, 0, buf.Length);
                }
            }
            catch{
            }
        }
        public void ReceiveDataAsyncCallback(IAsyncResult ar)
        {
            try
            {
                RECEIVESOCKETINFO rcvInfo = (RECEIVESOCKETINFO)ar.AsyncState;
                rcvInfo.socket.EndReceive(ar);

                if (OnRemoteMsg != null)
                {
                    string msg = Encoding.ASCII.GetString(rcvInfo.buf);
                    string[] str = msg.Split(',');
                    str[2] = str[2].Replace('@', ',');
                    OnRemoteMsg(rcvInfo.socket, Convert.ToInt32(str[0]), str[1], str[2]);
                }
            }
            catch
            {
              
            }
        }
        public void ReceiveCmdDataAsyncCallback(IAsyncResult ar)
        {
            try
            {
                RECEIVESOCKETINFO rcvInfo = (RECEIVESOCKETINFO)ar.AsyncState;
                rcvInfo.socket.EndReceive(ar);

                if (OnRemoteMsg != null)
                {
                    string msg = Encoding.ASCII.GetString(rcvInfo.buf);
                    string[] str = msg.Split(',');
                    OnRemoteMsg(rcvInfo.socket, Convert.ToInt32(str[0]), str[1], str[2]);
                }
            }
            catch 
            {
            }
        }
        public void SendCmdAsyncCallback(IAsyncResult ar)
        {
            try
            {
                Socket sck = (Socket)ar.AsyncState;
                sck.EndSend(ar);
            }
            catch
            {
            }
        }
    }

    public class ClientList
    {
        public event ServerReceiveMsgHandler OnServerReceiveMsg;
        bool   bIsRun = true;
        Socket gSocket = null;

        public ClientList(RECEIVESOCKETINFO info)
        {
            StartProcess(info);
        }
        public void Dispose()
        {
            try
            {
                bIsRun = false;
                if (gSocket != null)
                {
                    if (gSocket.Connected)
                    {
                        gSocket.Disconnect(false);
                    }
                    gSocket.Close();
                }
            }
            catch
            {
            }
        }

        private void StartProcess(RECEIVESOCKETINFO info)
        {
            try
            {
                gSocket = info.socket;
                info.socket.BeginReceive(info.buf, 0, info.buf.Length, SocketFlags.None, new AsyncCallback(ReceiveDataAsyncCallback), info);
            }
            catch
            { }
        }
        public void ReceiveDataAsyncCallback(IAsyncResult ar)
        {
            Socket socket = null;
            try
            {
                RECEIVESOCKETINFO rcvInfo = (RECEIVESOCKETINFO)ar.AsyncState;
                socket = rcvInfo.socket;
                rcvInfo.socket.EndReceive(ar);

                if (OnServerReceiveMsg != null)
                {
                    string msg = Encoding.ASCII.GetString(rcvInfo.buf);
                    string[] str = msg.Split(',');
                    OnServerReceiveMsg(rcvInfo.socket, Convert.ToInt32(str[0]), str[1], str[2].Replace('@',','));
                }
            }
            catch 
            {
            }

            if (bIsRun)
            {
                RECEIVESOCKETINFO info = new RECEIVESOCKETINFO();
                info.buf = new byte[1024];
                info.socket = socket;
                StartProcess(info);
            }
            else
            {
            }
        }
    }
}
