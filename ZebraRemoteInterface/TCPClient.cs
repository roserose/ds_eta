using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using ZebraGatewayInterface;
using System.IO;
using System.Windows.Forms;

namespace ZebraRemoteInterface
{
    public class TCPClient
    {
        public event TcpReceiveMsg OnRemoteMsg;
        public event TcpReceiveMsg2 OnRemoteMsg2;
        public event TcpConnectedMsg OnConnected;
        public event TcpDisconnectedMsg OnDisConnected;

        private Hashtable _gHsServerList = new Hashtable();
        public Hashtable gHsServerList
        {
            get
            {
                return _gHsServerList;
            }
            set
            {
                _gHsServerList = value;
            }
        }
        private enumReciveMsgType reciveType = enumReciveMsgType.Normal;

        public TCPClient()
        {
        }

        public void Connect(string IP, int Port, enumReciveMsgType reType)
        {
            try
            {
                reciveType = reType;
                ManagerClient mClient = new ManagerClient(IP, Port);
                mClient.OnClientReceiveMsg += new TcpReceiveMsg(mClient_OnClientReceiveMsg);
                mClient.OnConnected += new TcpConnectedMsg(mClient_OnConnected);
                mClient.OnDisConnected += new TcpConnectedMsg(mClient_OnDisConnected);
                mClient.Run();

                string key = IP + ':' + Port.ToString();
                if (!gHsServerList.ContainsKey(key))
                {
                    gHsServerList.Add(key, mClient);
                }
                else
                {
                    gHsServerList[key] = mClient;
                }
            }
            catch
            { }
        }
        public void Connect(string IP, int Port, bool KeepAlive, string stringKeep, int sycSec, enumReciveMsgType reType)
        {
            try
            {
                reciveType = reType;
                ManagerClient mClient = new ManagerClient(IP, Port, KeepAlive, stringKeep, sycSec);
                mClient.OnClientReceiveMsg += new TcpReceiveMsg(mClient_OnClientReceiveMsg);
                mClient.OnConnected += new TcpConnectedMsg(mClient_OnConnected);
                mClient.OnDisConnected += new TcpConnectedMsg(mClient_OnDisConnected);
                mClient.Run();

                string key = IP + ':' + Port.ToString();
                if (!gHsServerList.ContainsKey(key))
                {
                    gHsServerList.Add(key, mClient);
                }
                else
                {
                    gHsServerList[key] = mClient;
                }
            }
            catch
            { }
        }
        public void Connect(string IP, int Port, string GateWayIP, int GatewayPort, bool KeepAlive, string stringKeep, int sycSec, enumReciveMsgType reType)
        {
            try
            {
                reciveType = reType;
                ManagerClient mClient = new ManagerClient(IP, Port, GateWayIP, GatewayPort, KeepAlive, stringKeep, sycSec);
                mClient.OnClientReceiveMsg += new TcpReceiveMsg(mClient_OnClientReceiveMsg);
                mClient.OnConnected += new TcpConnectedMsg(mClient_OnConnected);
                mClient.OnDisConnected += new TcpConnectedMsg(mClient_OnDisConnected);
                mClient.Run();

                string key = IP + ':' + Port.ToString();
                if (!gHsServerList.ContainsKey(key))
                {
                    gHsServerList.Add(key, mClient);
                }
                else
                {
                    gHsServerList[key] = mClient;
                }
            }
            catch
            { }
        }
        private void mClient_OnDisConnected(TcpClient tcp ,string data)
        {
            try
            {
                ArrayList arRemove = new ArrayList();
                foreach (DictionaryEntry entry in gHsServerList)
                {
                    try
                    {
                        if (gHsServerList.ContainsKey(entry.Key))
                        {
                            ManagerClient mClient = (ManagerClient)gHsServerList[entry.Key];
                            mClient.OnClientReceiveMsg -= new TcpReceiveMsg(mClient_OnClientReceiveMsg);
                            mClient.OnConnected -= new TcpConnectedMsg(mClient_OnConnected);
                            mClient.OnDisConnected -= new TcpConnectedMsg(mClient_OnDisConnected);
                          
                            if (mClient == null || mClient.isRunning == false || mClient.client.Client.Connected == false)
                            {
                                arRemove.Add(entry.Key);
                                mClient = null;
                            }
                        }
                    }
                    catch
                    { }
                }
                foreach (string index in arRemove)
                {
                    if (gHsServerList.ContainsKey(index))
                    {
                        gHsServerList.Remove(index);
                    }

                    if (OnDisConnected != null)
                    {
                        OnDisConnected(new TcpClient() ,"");
                    }
                }
            }
            catch { }
        }
        private void mClient_OnConnected(TcpClient tcp, string data)
        {
            try
            {
                if (OnConnected != null)
                {
                    OnConnected(tcp,"");
                }
            }
            catch
            { }
        }
        private void mClient_OnClientReceiveMsg(TcpClient tcp, int veh_id, string cmd, string data)
        {
            try
            {

                switch (reciveType)
                {
                    case enumReciveMsgType.Normal:
                        {
                            if (OnRemoteMsg2 != null)
                            {
                                OnRemoteMsg2(tcp, data);
                            }
                        }
                        break;
                    case enumReciveMsgType.Zebra:
                        {
                            //string msg = "VID" + veh_id.ToString() + "," + cmd + "," + data;
                            if (OnRemoteMsg != null)
                            {
                                int _vid = -1;
                                string _cmd = "";
                                string _param = "";

                                string[] vidStr = data.Split(new string[] { "VID" }, StringSplitOptions.None);
                                foreach (string vidSunStr in vidStr)
                                {
                                    if (vidSunStr.Trim().Length < 1)
                                    {
                                        continue;
                                    }

                                    string[] str = vidSunStr.Split(',');

                                    if (str.Length == 3)
                                    {
                                        _vid = Convert.ToInt32(str[0]);
                                        _cmd = str[1];
                                        _param = str[2];
                                        string[] lnStr = _param.Split(new char[] { '\n', '\r' });
                                        foreach (string sl in lnStr)
                                        {
                                            if (sl.Trim().Length < 1) continue;
                                            OnRemoteMsg(tcp, _vid, _cmd, _param);
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        { }
                        break;
                }
            }
            catch
            {
            }
        }
        public void Disconnect()
        {
            try
            {
                if (gHsServerList.Count == 0) { }
                ArrayList arDis = new ArrayList();
                foreach (DictionaryEntry entry in gHsServerList)
                {
                    try
                    {
                        if (gHsServerList.ContainsKey(entry.Key))
                        {
                            arDis.Add(entry.Key.ToString());
                        }
                    }
                    catch
                    { }
                }
                foreach (string key in arDis)
                {
                    if (gHsServerList.ContainsKey(key))
                    {
                        ManagerClient msClient = (ManagerClient)gHsServerList[key];
                        msClient.isRunning = false;
                        msClient.client.Close();
                    }
                }
            }
            catch
            { }
        }
        public bool SendData(string key, string msg)
        {
            bool isSuccess = false;
            try
            {
                if (gHsServerList.ContainsKey(key))
                {
                    ManagerClient mServer = (ManagerClient)gHsServerList[key];
                    if (mServer.client.Client.Connected)
                    {
                        isSuccess = mServer.SendMsg(msg);
                    }
                }
            }
            catch
            { }
            return isSuccess;
        }
        public void SendDataBroadCast(string msg)
        {
            try
            {
                if (gHsServerList.Count < 1) { return; }
                foreach (DictionaryEntry entry in gHsServerList)
                {
                    try
                    {
                        if (gHsServerList.ContainsKey(entry.Key))
                        {
                            ((ManagerClient)gHsServerList[entry.Key]).SendMsg(msg);
                        }
                    }
                    catch
                    { }
                }
            }
            catch
            { }
        }
    }
    class ManagerClient
    {
        #region Init
        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        private System.Threading.Timer thTimer;

        public event TcpReceiveMsg OnClientReceiveMsg;
        public event TcpConnectedMsg OnConnected;
        public event TcpConnectedMsg OnDisConnected;
        public Boolean isRunning;
        private DateTime gNexSend = DateTime.Now.AddSeconds(30);
        public TcpClient client;
        private IPEndPoint serverEndPoint;
        private string gIpEndPoint = "";
        private string gSync = "#T";
        private int gPort = -1;
        private int gGatewayPort = -1;
        private string gGatewayIP = "0.0.0.0";
        private bool gKeepAlive = false;
        private int gKeepSec = 60;
      
        #endregion

        ~ManagerClient()
        {
        }
        public ManagerClient(string zIP, int zPort)
        {
            gIpEndPoint = zIP;
            gPort = zPort;
        }

        public ManagerClient(string zIP, int zPort, bool keepAlive, string stringKeep, int sycSec)
        {
            gIpEndPoint = zIP;
            gPort = zPort;
            gKeepAlive = keepAlive;

            if (stringKeep.Length > 0)
            {
                gSync = stringKeep;
            }
            if (sycSec > 0)
            {
                gKeepSec = sycSec;
            }
        }

        public ManagerClient(string zIP, int zPort,string zGatewayIP, int zGatewayPort, bool keepAlive, string stringKeep, int sycSec)
        {
            gIpEndPoint = zIP;
            gPort = zPort;
            gGatewayPort = zGatewayPort;
            gGatewayIP = zGatewayIP;

            gKeepAlive = keepAlive;
            if (stringKeep.Length > 0)
            {
                gSync = stringKeep;
            }
            if (sycSec > 0)
            {
                gKeepSec = sycSec;
            }
        }

        public void Run()
        {
            try
            {
                Connect(gIpEndPoint, gPort);
                if (client == null)
                {
                    return;
                }

                isRunning = true;
                if (gKeepAlive)
                {
                    thTimer = new System.Threading.Timer(new TimerCallback(DoTimerEvent), null, (1000 * gKeepSec), (1000 * gKeepSec));
                }
            }
            catch
            {
            }
        }
        private void DoTimerEvent(object obj)
        {
            try
            {
                if (isRunning)
                {
                    if (!client.Connected)
                    {
                        Connect(gIpEndPoint, gPort);
                    }
                    else
                    {
                        SendMsg(gSync);
                    }
                }
                else
                {
                    if (client.Connected)
                    {
                        client.Client.Close();
                    }
                    EventDisConnect();
                }
            }
            catch
            {
            }
        }
     
        private bool Connect(string zEP, int zPort)
        {
            try
            {
                serverEndPoint = new IPEndPoint(IPAddress.Parse(zEP), zPort);
                client = new TcpClient();

                client.Client.ReceiveTimeout = 1000 * 60 * 5;
                client.Client.SendTimeout = 1000 * 60 * 5;

                if (client == null || client.Client == null || !client.Connected)
                {
                    client.Connect(serverEndPoint);

                    Thread th = new Thread(ReciveMsg);
                    th.IsBackground = true;
                    th.Start();

                    EventConnect();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        private void ReciveMsg()
        {
            try
            {
                if (gGatewayIP != "0.0.0.0" && gGatewayPort != -1)
                {
                    IPEndPoint svPort = new IPEndPoint(IPAddress.Parse(gGatewayIP), gGatewayPort);
                    Thread.Sleep(500);
                    SendMsg("SVIP:" + svPort.ToString());
                }

                Thread.Sleep(1000);

                NetworkStream clientStream = client.GetStream();
                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] message = new byte[4096];
                int bytesRead = 0;

                while (isRunning && client.Connected)
                {
                    clientStream = client.GetStream();

                    try
                    {
                        bytesRead = clientStream.Read(message, 0, 4096);
                    }
                    catch
                    {
                        break;
                    }

                    if (bytesRead == 0)
                    {
                        break;
                    }
                    EventReceiveMsg(encoder.GetString(message, 0, bytesRead));
                    Thread.Sleep(1);
                }
                clientStream.Close();
                client.Close();
            }
            catch
            {
                client.Close();
            }
        }
        public bool SendMsg(string zMsg)
        {
            semaphor.WaitOne();
            try
            {
                if (!client.Client.Connected) 
                {
                    semaphor.Release();
                    return false;
                }
               
                NetworkStream clientStream = client.GetStream();
                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] buffer = encoder.GetBytes(zMsg + '\r');

                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
            }
            catch
            {
                semaphor.Release();
                return false;
            }
            semaphor.Release();
            return true;
        }
        public bool IsExist(string ep)
        {
            try
            {
                if (client == null) return false;
                if (client.Client.RemoteEndPoint == null) return false;

                if (((IPEndPoint)client.Client.RemoteEndPoint).ToString() == ep) return true;
            }
            catch
            {
            }
            return false;
        }
        private string GetIp()
        {
            string _local_ip = "127.0.0.1";
            try
            {
                IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (IPAddress ip in localIPs)
                {
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        _local_ip = ip.ToString();
                    }
                }
            }
            catch { }
            return _local_ip;
        }
        #region OnEvent
        private void EventConnect()
        {
            try
            {
                if (OnConnected != null)
                {
                    OnConnected(client,"");
                }
            }
            catch { }
        }
        private void EventDisConnect()
        {
            try
            {
                if (OnDisConnected != null)
                {
                    OnDisConnected(client,"");
                }
            }
            catch { }
        }
        private void EventReceiveMsg(string zMsg)
        {
            if (OnClientReceiveMsg != null)
            {
                if (!(zMsg == "#T" || zMsg == "#T\r"))
                {
                    OnClientReceiveMsg(client, -1, "", zMsg);
                }
            }
        }
        #endregion
    }
}
