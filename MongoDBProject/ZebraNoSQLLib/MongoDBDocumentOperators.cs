﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace ZebraNoSQLLib
{
    public class MongoDBDocumentOperators
    {
        private string ConnectionString = string.Empty;
        private string ServerString = "Server=";
        private string userDB = "sa";
        private string passDB = "aab-gps-gtt";

        public MongoDBDocumentOperators(string serverNamePort)
        {
            ConnectionString = ServerString += serverNamePort;
        }

        public ObjectId Insert_KTLogMsg(KTLogMsg msg)
        {
            ObjectId _id = ObjectId.Empty;
            try
            {
                string DBName = "ZebraDB";
                string CollectionName = "log_msg";
                MongoServer server = MongoServer.Create(ServerString);
                try
                {
                    MongoCredentials credentials = new MongoCredentials(userDB, passDB);
                    MongoDatabase myDatabase = server.GetDatabase(DBName, credentials);
                    MongoCollection<KTLogMsg> ZebraDBLog = myDatabase.GetCollection<KTLogMsg>(CollectionName);
                    ZebraDBLog.Insert(msg);
                    _id = msg._id;
                }
                catch
                { }
                server.Disconnect();
            }
            catch
            { }
            return _id;
        }

        public List<KTLogMsg> GetLogMsg(int veh_id, Int64 date_id_start, Int64 date_id_end)
        {
            List<KTLogMsg> lst = new List<KTLogMsg>();
            try
            {
                string DBName = "ZebraDB";
                string CollectionName = "log_msg";
                bool noError = false;
                MongoServer server = MongoServer.Create(ServerString + ";slaveOk=true");

                try
                {
                    MongoCredentials credentials = new MongoCredentials(userDB, passDB);
                    MongoDatabase myDatabase = server.GetDatabase(DBName, credentials);
                    MongoCollection<KTLogMsg> ZebraDBLog = myDatabase.GetCollection<KTLogMsg>(CollectionName);

                    var query = Query.And(
                     Query.EQ("vid", veh_id),
                     Query.GTE("date_id", date_id_start),
                     Query.LTE("date_id", date_id_end)
                     );

                     var temp = ZebraDBLog.Find(query).SetSortOrder(SortBy.Ascending("date_id"));

                     server.Disconnect();
                     noError = true;

                     foreach (KTLogMsg item in temp)
                     {
                         item.timestamp = item.timestamp.AddHours(7);
                         lst.Add(item);
                     }
                }
                catch
                { }

                if (!noError)
                {
                    server.Disconnect();
                }
              
            }
            catch (Exception) { } return lst;
        }


    }
}
