﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace ZebraNoSQLLib
{
    /** LOG DATA ****************************/
    public class Gateway_Log
    {
        public ObjectId _id { get; set; }
        public Double number { get; set; }
        public Int64 date_id { get; set; }
        public int type { get; set; }
        public int vid { get; set; }
        public int s_ip { get; set; }
        public int s_port { get; set; }
        public int d_ip { get; set; }
        public int d_port { get; set; }
        public String time { get; set; }
        public String data { get; set; }
       
    }

}
