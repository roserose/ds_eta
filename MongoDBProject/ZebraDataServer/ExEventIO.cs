﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZebraNoSQLLib;

namespace ZebraDataServer
{
    class ExEventIO
    {
        #region --- local variables ---------------------------------------------------------------
        private KTLogMsg _objMsg = new KTLogMsg();
        public KTLogMsg objMsg
        {
            set
            {
                _objMsg = value;
            }
            get
            {
                return _objMsg;
            }
        }

        CommonQeue SMSQ;
        CommonQeue EMAILQ;

        #endregion
        bool IsEnableBroadcastQ = true;
        IOStatusCls oIOStatus;
        public ExEventIO()
        {
            Properties.Settings pf = new ZebraDataServer.Properties.Settings();
            SMSQ = new CommonQeue(pf.Broadcast_SMS_Q);
            EMAILQ = new CommonQeue(pf.Broadcast_EMAIL_Q);
            IsEnableBroadcastQ = pf.EnableBroadcastQ;
            oIOStatus = new IOStatusCls(pf.DBSever);
        }

        public void DetectEventIO(int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg, string tag_msg,string type_of_box)
        {
            oIOStatus.DetectStatus(veh_id, in_out_status, type_of_msg, IsEnableBroadcastQ, speed, dtTimestampe, lat, lon, SMSQ, EMAILQ, tag_msg, _objMsg, type_of_box);
            _objMsg = oIOStatus.objMSG;
        }
    }
}
