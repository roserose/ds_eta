using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Drawing;
using ZebraCommonInterface;
namespace ZebraDataServer
{
    class TrueZoneAlertCls
    {
        SQL gSql;
        public TrueZoneAlertCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public bool IsVehicleExisted(int veh_id)
        {
            return gSql.GetTrueVehicle(veh_id);
        }

        public DataTable GetZone()
        {
            return gSql.GetTrueZone();
        }

        public void GetLastZoneStatus(int veh_id, int zone_id, out int evt_id)
        {
            gSql.GetLastTrueZoneStatus(veh_id, zone_id, out evt_id);
        }

        public void InsertNewRecord(ZebraDataServer.FencingCls.SIDE_TYPE side, int veh_id, int zone_id)
        {
            if (side == ZebraDataServer.FencingCls.SIDE_TYPE.IN)
            {
                gSql.InsertPrvTrueZoneState(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE);
            }
            else if (side == ZebraDataServer.FencingCls.SIDE_TYPE.OUT)
            {
                gSql.InsertPrvTrueZoneState(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE);
            }
        }

        ////
        // 1.check veh in group 
        public DataTable True2IsVehicleTrueCorp(int veh_id)
        {
            return gSql.True2GetTrueCorpVehicle(veh_id);
        }
        ////
        // 2.Zone in group 
        public DataTable True2GetVehicleGroupZone(int veh_id)
        {
            return gSql.True2GetVehicleZone(veh_id);
        }

        // 3.Zone Type
        //True2GetVehicleZoneGroupType
        public DataTable True2GetVehicleGroupZoneType(int veh_id)
        {
            return gSql.True2GetVehicleZoneGroupType(veh_id);
        }

        // 4.Zone All
        public DataTable True2GetAllTrueZone()
        {
            return gSql.True2GetAllTrueZone();
        }
    }
}
