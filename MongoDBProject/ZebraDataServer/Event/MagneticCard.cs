using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;

namespace ZebraDataServer
{
    class MagneticCard
    {
        SQL gSql;
        public MagneticCard(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public void InsertNewRecord(int ref_idx,EVT_TYPE evt_type)
        {
            gSql.InsertMsgEvt(ref_idx, evt_type);
        }
        public bool GetVehicleMagneticType(int veh_id)
        {
            return gSql.GetVehicleMagneticType(veh_id);
        }

        public DataTable GetVehicleMagneticParam(int veh_id)
        {
            return gSql.GetVehicleMagneticParam(veh_id);
        }
        public bool CheckLastNocard(int veh_id, out DateTime last_rec)
        {
            last_rec = new DateTime();
            bool isFind = false;
            try
            {
                DataTable dtRet = gSql.GetVehicleMagneticLastNoCard(veh_id);
                if (dtRet.Rows.Count == 0){ return true; }

                foreach (DataRow dr in dtRet.Rows)
                {
                    int evt_id = (int)dr["evt_id"];

                    if (evt_id == (int)EVT_TYPE.ENGINE_ON)
                    {
                        last_rec = (DateTime)dr["local_timestamp"];
                    }
                    else if (evt_id == (int)EVT_TYPE.VALID_CARD || evt_id ==  (int)EVT_TYPE.NO_CARD || evt_id == (int)EVT_TYPE.ENGINE_OFF)  
                    {
                        isFind = true;
                    }
                }
            }
            catch { }
            return isFind;
        }

    }
}
