using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ZebraDataServer
{
    class SmartCard
    { 
        SQL gSql;
        public SmartCard(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public void GetSmartCardEvent(int veh_id, DateTime timestamp, int iAddress1, string sAddress2, string sData, string sTag, out int _evt_id, out string _event_desc)
        {
            _evt_id = -1;
            _event_desc = string.Empty;
            gSql.GetSmartCardEvent(veh_id, timestamp, iAddress1, sAddress2, sData, sTag, out _evt_id,out _event_desc);
        }
    }
}
