using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using System.Drawing;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    public struct ETAINFO
    {
        public int      eta_id ;
        public float    eta_time_left ;
        public int      destination_zone_id ;
    }

    class ETACls
    {
        enum ZONEMODE { IN_ZONE=17, OUTZONE=18 };            
        SQL gSql;

        public ETACls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public ArrayList CalculateETA1(int VehID, double lat, double lon, int speed, out float dtTimeLeft)
        {
            dtTimeLeft = 100.0F;
            ArrayList retList = new ArrayList();

            /*
            int      dst     = 0 ;            
            int      lz_id   = -1 ;
            int      levt_id = -1 ;
            float    dstVeh  = 0 ;
            float    dstOff  = 0 ;
            DateTime lz_time = new DateTime() ;

            DataTable lzTable  = gSql.GetLastZoneByVeh(VehID);
            if (lzTable == null || lzTable.Rows.Count < 1)
            {
                return retList;
            }

            foreach (DataRow r in lzTable.Rows)
            {
                lz_id   = (int)r["zone_id"] ;
                levt_id = (int)r["evt_id"] ;
                lz_time = (DateTime)r["local_timestamp"] ;
                break; 
            }

            if (lz_id < 0 || levt_id == (int)ZONEMODE.IN_ZONE)
            {
                return retList;
            }
            
            

            DataTable etaTable = gSql.GetETAListByVeh1(VehID);
            if (etaTable == null || etaTable.Rows.Count < 1)
            {
                return retList;
            }
            
            int nearestRouteID = GetNearestRoute(etaTable, lat, lon);
            if (nearestRouteID >= 0)
            {
                DataRow r = etaTable.Rows[nearestRouteID];
                int eta_id   = (int)r["eta_id"];
                int start_id = (int)r["start_zone_id"];
                int stop_id  = (int)r["stop_zone_id"];
                int route_id = (int)r["route_id"];

                int destination_id = -1;

                bool dir = true;        // forward 
                if (lz_id == start_id) {
                    dir = true;
                } else if (lz_id == stop_id) {
                    dir = false;
                } else {
                    return retList;
                }

                double stLat = 0.0;  double stLon = 0.0;                
                gSql.GetZoneFirstPoint(start_id, out stLat, out stLon);

                double enLat = 0.0; double enLon = 0.0;
                gSql.GetZoneFirstPoint(stop_id, out enLat, out enLon);

                DataTable wpTable = gSql.GetRouteWayPoints(route_id);
                
                int lp = GetNearestPoint(lat,lon, wpTable);
                if (lp < 0)
                {
                    return retList;
                }

                double rtLatP = (double)wpTable.Rows[lp]["lat"];
                double rtLonP = (double)wpTable.Rows[lp]["lon"];

                dstOff = (float)GetDistanceFromProjection(lp, wpTable, lat, lon);

                double rtLat1 = (double)wpTable.Rows[0]["lat"];
                double rtLon1 = (double)wpTable.Rows[0]["lon"];

                double odo1 = GlobalClass.GetFromTheGreatCircle(stLat, stLon, rtLat1, rtLon1);
                double odo2 = GlobalClass.GetFromTheGreatCircle(enLat, enLon, rtLat1, rtLon1);

                if (odo1 < odo2) {                    
                    if (dir) {
                        dstVeh = (float)GetODOForward(wpTable, lp);
                        destination_id = stop_id;
                    } else {
                        dstVeh = (float)GetODOBackward(wpTable, lp);
                        destination_id = start_id;
                    }
                } else {
                    if (dir) {
                        dstVeh = (float)GetODOBackward(wpTable, lp);
                        destination_id = start_id;
                    } else {
                        dstVeh = (float)GetODOForward(wpTable, lp);
                        destination_id = stop_id;
                    }
                }

                dstVeh += dstOff ;
                if (route_id > 0)
                {
                    dst = (int)GetODO(wpTable);
                }

                if (speed <= 0) {
                    dtTimeLeft = 999.99F ;
                } else {
                    dtTimeLeft = (dst - dstVeh) / (float)speed;
                }
                dtTimeLeft = (float)Math.Round(dtTimeLeft,2);

                ETAINFO info = new ETAINFO();
                info.eta_id = eta_id;
                info.eta_time_left = dtTimeLeft;
                info.destination_zone_id = destination_id;

                retList.Add(info);
            }
            */
            return retList;
        }
        public ArrayList CalculateETA11(int VehID, double lat, double lon, int speed, out float dtTimeLeft)
        {
            dtTimeLeft = 100.0F;
            ArrayList retList = new ArrayList();

            /*
            int dst = 0;
            int lz_id = -1;
            int levt_id = -1;
            float dstVeh = 0;
            float dstOff = 0;
            DateTime lz_time = new DateTime();

            DataTable lzTable = gSql.GetLastZoneByVeh(VehID);
            if (lzTable == null || lzTable.Rows.Count < 1)
            {
                return retList;
            }

            foreach (DataRow r in lzTable.Rows)
            {
                lz_id = (int)r["zone_id"];
                levt_id = (int)r["evt_id"];
                lz_time = (DateTime)r["local_timestamp"];
                break;
            }

            if (lz_id < 0 || levt_id == (int)ZONEMODE.IN_ZONE)
            {
                return retList;
            }


            DataTable etaTable = gSql.GetETAListByVeh1(VehID);
            if (etaTable == null || etaTable.Rows.Count < 1)
            {
                return retList;
            }

            int nearestRouteID = GetNearestRoute(etaTable, lat, lon);
            if (nearestRouteID >= 0)
            {
                if( etaTable.Rows[nearestRouteID]["route_id"] == null ){
                    return retList;
                }
                int r_id = (int)etaTable.Rows[nearestRouteID]["route_id"];
                ArrayList dpZnList = GetDropZone(VehID, r_id);
                if (dpZnList.Count < 2)
                {
                    return retList;
                }

                DataRow r = etaTable.Rows[nearestRouteID];
                int eta_id = (int)r["eta_id"];
                int start_id = (int)r["start_zone_id"];
                int stop_id = (int)r["stop_zone_id"];
                int route_id = (int)r["route_id"];

                int destination_id = -1;

                bool dir = true;        // forward 
                if (lz_id == (int)dpZnList[0])
                {
                    dir = true;
                    start_id = (int)dpZnList[0] ;
                    stop_id  = (int)dpZnList[1] ;
                }
                else if (lz_id == (int)dpZnList[dpZnList.Count-1])
                {
                    dir = false;
                    start_id = (int)dpZnList[dpZnList.Count - 1] ;
                    stop_id  = (int)dpZnList[dpZnList.Count - 2] ;
                }
                else
                {
                    int i = 0 ;
                    bool fnd = false;
                    foreach (int zn_id in dpZnList)
                    {
                        if (lz_id == zn_id)
                        {
                            fnd = true ;
                            if( (i-1) < 0 || (i+1) == dpZnList.Count ) {
                                return retList ;
                            }

                            int zn0 = (int)dpZnList[i];
                            int zn1 = (int)dpZnList[i-1];
                            int zn2 = (int)dpZnList[i+1];

                            double zn0Lat = 0.0; double zn0Lon = 0.0;
                            gSql.GetZoneFirstPoint(zn0, out zn0Lat, out zn0Lon);

                            double zn1Lat = 0.0; double zn1Lon = 0.0;
                            gSql.GetZoneFirstPoint(zn1, out zn1Lat, out zn1Lon);

                            double zn2Lat = 0.0; double zn2Lon = 0.0;
                            gSql.GetZoneFirstPoint(zn2, out zn2Lat, out zn2Lon);

                            ZebraDataServer.FencingCls.iVector v0 = new FencingCls.iVector();
                            v0.p1.X = (float)zn0Lon ;
                            v0.p1.Y = (float)zn0Lat ;
                            v0.p2.X = (float)lon;
                            v0.p2.Y = (float)lat;

                            ZebraDataServer.FencingCls.iVector v1 = new FencingCls.iVector();
                            v1.p1.X = (float)zn0Lon;
                            v1.p1.Y = (float)zn0Lat;
                            v1.p2.X = (float)zn1Lon;
                            v1.p2.Y = (float)zn1Lat;

                            ZebraDataServer.FencingCls.iVector v2 = new FencingCls.iVector();
                            v2.p1.X = (float)zn0Lon;
                            v2.p1.Y = (float)zn0Lat;
                            v2.p2.X = (float)zn2Lon;
                            v2.p2.Y = (float)zn2Lat;
                            
                            double z1 = Math.Abs( GetTanZeta(v0, v1) );                            
                            double z2 = Math.Abs( GetTanZeta(v0, v2) );                            

                            if( z2 < z1 ) {
                                dir = true ;
                                start_id = (int)dpZnList[i];
                                stop_id = (int)dpZnList[i+1];
                            } else {
                                dir = false ;
                                start_id = (int)dpZnList[i];
                                stop_id = (int)dpZnList[i-1];
                            }
                            break;
                        }
                        i++;
                    }

                    if( fnd == false )
                    {
                        return retList;
                    }
                }

                double stLat = 0.0; double stLon = 0.0;
                gSql.GetZoneFirstPoint(start_id, out stLat, out stLon);

                double enLat = 0.0; double enLon = 0.0;
                gSql.GetZoneFirstPoint(stop_id, out enLat, out enLon);

                DataTable wpTable = gSql.GetRouteWayPoints(route_id);

                int lpZn1 = GetNearestPoint(stLat, stLon, wpTable);
                int lpZn2 = GetNearestPoint(enLat, enLon, wpTable);

                int lp = GetNearestPoint(lat, lon, wpTable);
                if (lp < 0)
                {
                    return retList;
                }

                double rtLatP = (double)wpTable.Rows[lp]["lat"];
                double rtLonP = (double)wpTable.Rows[lp]["lon"];

                dstOff = (float)GetDistanceFromProjection(lp, wpTable, lat, lon);

                double rtLat1 = (double)wpTable.Rows[0]["lat"];
                double rtLon1 = (double)wpTable.Rows[0]["lon"];

                double odo1 = GlobalClass.GetFromTheGreatCircle(stLat, stLon, rtLat1, rtLon1);
                double odo2 = GlobalClass.GetFromTheGreatCircle(enLat, enLon, rtLat1, rtLon1);

                if (odo1 < odo2)
                {
                    if (dir)
                    {
                        dstVeh = (float)GetODOForward(wpTable, lp, lpZn1, lpZn2);
                        destination_id = stop_id;
                    }
                    else
                    {
                        dstVeh = (float)GetODOBackward(wpTable, lp, lpZn1, lpZn2);
                        destination_id = start_id;
                    }
                }
                else
                {
                    if (!dir)
                    {
                        dstVeh = (float)GetODOBackward(wpTable, lp, lpZn1, lpZn2);
                        destination_id = stop_id ;
                    }
                    else
                    {
                        dstVeh = (float)GetODOForward(wpTable, lp, lpZn1, lpZn2);
                        destination_id = start_id ;
                    }
                }

                dstVeh += dstOff;
                if (route_id > 0)
                {
                    if (dir) {
                        dst = (int)GetODO(wpTable, lpZn1, lpZn2);
                    } else {
                        dst = (int)GetODO(wpTable, lpZn2, lpZn1);
                    }
                }

                if (speed <= 0)
                {
                    dtTimeLeft = 999.99F;
                }
                else
                {
                    dtTimeLeft = (dst - dstVeh) / (float)speed;
                }
                dtTimeLeft = (float)Math.Round(dtTimeLeft, 2);

                ETAINFO info = new ETAINFO();
                info.eta_id = eta_id;
                info.eta_time_left = dtTimeLeft;
                info.destination_zone_id = destination_id;

                retList.Add(info);
            }
            */
            return retList;
        }
        public ArrayList CalculateETA2(int VehID, double lat, double lon, int speed, out float dtTimeLeft)
        {
            dtTimeLeft = 100.0F;
            ArrayList retList = new ArrayList();

            double minOdo = 99999.99;
            int destination_id = -1;
            ETAINFO info = new ETAINFO();

            DataTable etaTable = gSql.GetETAListByVeh2(VehID);
            if (etaTable == null || etaTable.Rows.Count < 1)
            {
                return retList;
            }

            foreach(DataRow r in etaTable.Rows)
            {
                int eta_id = (int)r["eta_id"];
                int start_id = (int)r["start_zone_id"];
                int stop_id = (int)r["stop_zone_id"];
                int route_id = (int)r["route_id"];

                if (start_id < 0 || stop_id >= 0 || route_id >= 0 || lat <= 0.0 || lon <= 0.0) {
                    continue;
                }
                destination_id = start_id;

                double stLat = 0.0; double stLon = 0.0;
                gSql.GetZoneFirstPoint(start_id, out stLat, out stLon);
                double odo1 = Math.Abs(GlobalClass.GetFromTheGreatCircle(stLat, stLon, lat, lon));

                double lat2 = 0.0;
                double lon2 = 0.0;
                gSql.GetVehLatWP(VehID, out lat, out lon);
                double odo2 = Math.Abs(GlobalClass.GetFromTheGreatCircle(stLat, stLon, lat2, lon2));

                if (odo1 >= odo2)
                {
                    continue;
                }

                dtTimeLeft =  ((float)odo1) / (float)speed;                
                minOdo = Math.Min(minOdo, odo1) ;

                if (minOdo == odo1)
                {
                    info.eta_id = eta_id;
                    info.eta_time_left = dtTimeLeft;
                    info.destination_zone_id = destination_id;
                }
            }

            dtTimeLeft = info.eta_time_left;
            retList.Add(info);
            return retList;
        }

        private int GetNearestRoute(DataTable etaTable, double lat, double lon)
        {
            int ret_route_id = -1;
            int old_route_id = -2;
            int cnt = 0;
            double dmin = 99999.99;
            foreach (DataRow r in etaTable.Rows)
            {
                int route_id = (int)r["route_id"];
                if (route_id < 0)
                {
                    continue;
                }
                DataTable wpTable = gSql.GetRouteWayPoints(route_id);
                foreach(DataRow wp in wpTable.Rows)
                {
                    double wplat = (double)wp["lat"];
                    double wplon = (double)wp["lon"];
                    if (lat == 0.0 || lon == 0.0)
                    {
                        continue;
                    }

                    double d = GlobalClass.GetFromTheGreatCircle(lat, lon, wplat, wplon);
                    dmin = Math.Min(dmin, d);
                    if (dmin == d)
                    {
                        if (old_route_id != route_id)
                        {
                            ret_route_id = cnt;
                            old_route_id = route_id;
                        }
                    }                    
                }
                cnt++;                
            }
            return ret_route_id ;
        }
        private double GetDistanceFromProjection(int lp, DataTable wpTable, double lat, double lon)
        {
            ZebraDataServer.RoutingCls.vector vt1 = new RoutingCls.vector();            

            double rtLatP1 = (double)wpTable.Rows[lp]["lat"];
            double rtLonP1 = (double)wpTable.Rows[lp]["lon"];

            vt1.p1 = new System.Drawing.PointF((float)rtLonP1, (float)rtLatP1);
            vt1.p2 = new System.Drawing.PointF((float)lon, (float)lat);

            double dmin = GlobalClass.GetFromTheGreatCircle(lat, lon, rtLatP1, rtLonP1);
            if (lp + 1 < wpTable.Rows.Count)
            {
                ZebraDataServer.RoutingCls.vector vt2 = new RoutingCls.vector();
                ZebraDataServer.RoutingCls.vector vto = new RoutingCls.vector();

                double rtLatP2 = (double)wpTable.Rows[lp+1]["lat"];
                double rtLonP2 = (double)wpTable.Rows[lp+1]["lon"];
                vt2.p1 = vt1.p1;
                vt2.p2 = new System.Drawing.PointF((float)rtLonP2, (float)rtLatP2);

                ProjectVector(vt1, vt2, out vto);
                double d = GlobalClass.GetFromTheGreatCircle(lat, lon, vto.p2.Y, vto.p2.X);
                dmin = Math.Min(dmin, d);
            }

            if (lp - 1 >= 0)
            {
                ZebraDataServer.RoutingCls.vector vt3 = new RoutingCls.vector();
                ZebraDataServer.RoutingCls.vector vto = new RoutingCls.vector();

                double rtLatP3 = (double)wpTable.Rows[lp - 1]["lat"];
                double rtLonP3 = (double)wpTable.Rows[lp - 1]["lon"];
                vt3.p1 = vt1.p1;
                vt3.p2 = new System.Drawing.PointF((float)rtLonP3, (float)rtLatP3);

                ProjectVector(vt1, vt3, out vto);
                double d = GlobalClass.GetFromTheGreatCircle(lat, lon, vto.p2.Y, vto.p2.X);
                dmin = Math.Min(dmin, d);
            }

            return dmin;
        }
        public void ProjectVector(ZebraDataServer.RoutingCls.vector v1, ZebraDataServer.RoutingCls.vector v2, out ZebraDataServer.RoutingCls.vector pv1)
        {
            pv1 = new ZebraDataServer.RoutingCls.vector();

            float x1 = v1.p2.X - v1.p1.X;
            float y1 = v1.p2.Y - v1.p1.Y;

            float x2 = v2.p2.X - v2.p1.X;
            float y2 = v2.p2.Y - v2.p1.Y;
            float r2 = (x2 * x2) + (y2 * y2);
            if (r2 == 0)
            {
                pv1 = v1;
            }

            float c = ((x1 * x2) + (y1 * y2)) / r2;

            ZebraDataServer.RoutingCls.vector u = new ZebraDataServer.RoutingCls.vector();
            u.p1.X = 0;
            u.p1.Y = 0;
            u.p2.X = x2 * c;
            u.p2.Y = y2 * c;

            float px = u.p2.X;
            float py = u.p2.Y;

            pv1.p1 = v1.p1;
            pv1.p2.X = v1.p1.X + px;
            pv1.p2.Y = v1.p1.Y + py;
        }
        private int GetNearestPoint(double lat, double lon, DataTable wpTable)
        {
            int     p = -1;
            double  d = 0;
            double  min = 99999.999;

            int i = 0;            
            foreach (DataRow r in wpTable.Rows)
            {
                double wpLat = (double)r["lat"];
                double wpLon = (double)r["lon"];

                if (wpLat != 0.0 || wpLon != 0.0)
                {
                    d = GlobalClass.GetFromTheGreatCircle(wpLat, wpLon, lat, lon);
                    min = Math.Min(min, d);
                    if (min == d)
                    {
                        p = i;
                    }
                }
                i++;
            }

            return p;
        }
        private double GetODO(DataTable wpTable)
        {
            double distance = 0.0;
            double old_lat = 0.0;
            double old_lon = 0.0;

            foreach (DataRow r in wpTable.Rows)
            {
                double lat = (double)r["lat"];
                double lon = (double)r["lon"];

                if (old_lat != 0.0 || old_lon != 0.0)
                {
                    distance += GlobalClass.GetFromTheGreatCircle(old_lat, old_lon, lat, lon);
                }
                old_lat = lat;
                old_lon = lon;
            }

            return distance;
        }
        private double GetODO(DataTable wpTable, int beginP, int endP)
        {
            double distance = 0.0;
            double old_lat = 0.0;
            double old_lon = 0.0;

            for (int i = beginP; i<=endP && endP<wpTable.Rows.Count; i++)
            {
                double lat = (double)wpTable.Rows[i]["lat"];
                double lon = (double)wpTable.Rows[i]["lon"];

                if (old_lat != 0.0 || old_lon != 0.0)
                {
                    distance += GlobalClass.GetFromTheGreatCircle(old_lat, old_lon, lat, lon);
                }
                old_lat = lat;
                old_lon = lon;
            }

            return distance;
        }
        private double GetODOForward(DataTable wpTable, int start)
        {
            double distance = 0.0;
            double old_lat = 0.0;
            double old_lon = 0.0;

            for (int i = 0; i <= start; i++)
            {
                double lat = (double)wpTable.Rows[i]["lat"];
                double lon = (double)wpTable.Rows[i]["lon"];

                if (old_lat != 0.0 || old_lon != 0.0)
                {
                    distance += GlobalClass.GetFromTheGreatCircle(old_lat, old_lon, lat, lon);
                }
                old_lat = lat;
                old_lon = lon;
            }

            return distance;
        }
        private double GetODOForward(DataTable wpTable, int start, int beginP, int endP)
        {
            double distance = 0.0;
            double old_lat = 0.0;
            double old_lon = 0.0;

            for (int i = beginP; i <= start && start<=endP; i++)
            {
                double lat = (double)wpTable.Rows[i]["lat"];
                double lon = (double)wpTable.Rows[i]["lon"];

                if (old_lat != 0.0 || old_lon != 0.0)
                {
                    distance += GlobalClass.GetFromTheGreatCircle(old_lat, old_lon, lat, lon);
                }
                old_lat = lat;
                old_lon = lon;
            }

            return distance;
        }
        private double GetODOBackward(DataTable wpTable, int end)
        {
            double distance = 0.0;
            double old_lat = 0.0;
            double old_lon = 0.0;

            for (int i = wpTable.Rows.Count-1; i >= end; i--)
            {
                double lat = (double)wpTable.Rows[i]["lat"];
                double lon = (double)wpTable.Rows[i]["lon"];

                if (old_lat != 0.0 || old_lon != 0.0)
                {
                    distance += GlobalClass.GetFromTheGreatCircle(old_lat, old_lon, lat, lon);
                }
                old_lat = lat;
                old_lon = lon;
            }

            return distance;
        }
        private double GetODOBackward(DataTable wpTable, int end, int beginP, int endP)
        {
            double distance = 0.0;
            double old_lat = 0.0;
            double old_lon = 0.0;

            for (int i = beginP; i >= end && i >= endP; i--)
            {
                double lat = (double)wpTable.Rows[i]["lat"];
                double lon = (double)wpTable.Rows[i]["lon"];

                if (old_lat != 0.0 || old_lon != 0.0)
                {
                    distance += GlobalClass.GetFromTheGreatCircle(old_lat, old_lon, lat, lon);
                }
                old_lat = lat;
                old_lon = lon;
            }

            return distance;
        }

        public void InsertNewRecord(int ref_idx)
        {
           // gSql.InsertMsgEvt(ref_idx, EVT_TYPE. frmDataServer.EVT_TYPE.ETA);
        }
        public void InsertMsgETA(int ref_idx, int eta_id, float time_left, int destination_id)
        {
            gSql.InsertMsgETA(ref_idx, eta_id, time_left, destination_id);
        }

        private ArrayList GetDropZone(int veh_id, int route_id)
        {
            bool fst = true;
            ArrayList dpZnList = new ArrayList();

            DataTable dpZnTable = gSql.GetETADropZone(veh_id, route_id);
            foreach (DataRow r in dpZnTable.Rows)
            {
                int start_zone_id = (int)r["start_zone_id"] ;
                int stop_zone_id  = (int)r["stop_zone_id"] ;

                if (fst)
                {
                    fst = false;
                    dpZnList.Add(start_zone_id);
                    dpZnList.Add(stop_zone_id);
                }
                else
                {
                    dpZnList.Add(stop_zone_id);
                }
            }

            return dpZnList;
        }
        private double GetTanZeta(ZebraDataServer.FencingCls.iVector v1, ZebraDataServer.FencingCls.iVector v2)
        {
            PointF p1 = new PointF();
            p1.X = (v1.p2.X - v1.p1.X);
            p1.Y = (v1.p2.Y - v1.p1.Y);

            PointF p2 = new PointF();
            p2.X = (v2.p2.X - v2.p1.X);
            p2.Y = (v2.p2.Y - v2.p1.Y);

            double dd1 = Math.Atan2(p1.Y, p1.X);
            dd1 = (180 / Math.PI) * dd1;

            double dd2 = Math.Atan2(p2.Y, p2.X);
            dd2 = (180 / Math.PI) * dd2;

            double dd = dd1 - dd2;

            if (dd <= -180 && dd > -360)
            {
                dd = 360 + dd;
            }
            else if (dd > 180 && dd < 360)
            {
                dd = -(360 - dd);
            }
            return dd;
        }
    }
}
