﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ZebraDataServer
{
    class GetDataMongo
    {
        private SQL gSql;
        public GetDataMongo(string dbServer)
        {
            gSql = new SQL(dbServer);
        }
        public DataTable GetEventDesc(int evt_id)
        { 
            return gSql.GetEventDesc(evt_id); 
        }
        public DataTable GetZoneDesc(int zone_id)
        {
            return gSql.GetZoneDesc(zone_id);
        }
        public DataTable GetRouteDesc(int route_id)
        {
            return gSql.GetRouteDesc(route_id);
        }
    }
}
