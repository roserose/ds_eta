using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;

namespace ZebraDataServer
{
    class IdlingCls
    {
        SQL gSql;

        public IdlingCls(String dbServer)
        {
            gSql = new SQL(dbServer);
        }

        public DataTable DetectIdleVeh(int veh_id)
        {
            return gSql.DetectIdleVeh(veh_id);
        }

        public DataTable GetVehIdleTime(int veh_id)
        {
            return gSql.GetVehIdleTimeLimit(veh_id);
        }

        public Boolean DetectEngineOn(int veh_id)
        {
            Boolean bRet = gSql.GetVehEngineOn(veh_id);
            return bRet;
        }

        public void InsertNewRecord(int ref_idx)
        {
            gSql.InsertMsgEvt(ref_idx, EVT_TYPE.IDLE);
        }
    }
}
