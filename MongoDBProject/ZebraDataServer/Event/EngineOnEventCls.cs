using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class EngineOnEventCls 
    {
        SQL gSql;
        public EngineOnEventCls(String dbServer)            
        {
            gSql = new SQL(dbServer);
        }

        //1
        public DataTable GetEngineOnEventByVeh(int veh_id)
        {
            return gSql.GetEngineOnEventByVeh(veh_id);
        }

        //2 last engin on off
        public void GetEngineOnCurfewEvt(int veh_id, out int last_evt_id, out DateTime last_local_timestamp)
        {
            gSql.GetLastEngineEventStatus(veh_id, out last_evt_id, out last_local_timestamp);
        }

        //3 last event
        public void GetLastEngineOnCurfewEventStatus(int veh_id, int evt_id, out int o_evt_id, out DateTime o_local_timestamp)
        {
            gSql.GetLasEventStatus(veh_id, evt_id, out o_evt_id, out o_local_timestamp);
        }
      
        public void GetLastEngineOnEventStatus(int veh_id, out int evt_id, out DateTime local_timestamp)
        {//3
            gSql.GetLastEngineOnEventStatus(veh_id, out evt_id, out local_timestamp);
        }

    }
}
