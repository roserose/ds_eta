using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Threading;
using System.Globalization;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Diagnostics;
using System.Collections;
using ZebraDataCentre;
using ZebraDataInterface;
using ZebraCommonInterface;
using ZebraPresentationLayer;
using System.ServiceProcess;
using ZebraNoSQLLib;

namespace ZebraDataServer
{
    #region ENUMUNIC
    public enum CONTROLID
    {
        MAXSPEEDTIMEID, MAXIDLETIMEID, MAXZNTIMEID,
        MAXZNINCURFEWTIMEID, MAXLOCTIMEID, MAXROUTINGTIMEID,
        MAXETA1TIMEID, MAXETA2TIMEID, MAXIOSTATUSTIMEID,
        MAXENGINEONEVENTID,
        MAXTEMPERATUREID, MAXENGINEONOFFINOUTZONE, MAXSPEEDHAZRD, MAXANGLEHAZARD, MAXTRUEFENCING, MAXTRUEIO,
        MAXDFMIO, MAXZNINCURFEWWITHSPEEDTIMEID,
        TOTALOFDATAID, TOTALCURTHDID
    };
    public enum PROFILEMENUID
    {
        FLEET,
        FLEET_MENU,
        FLEET_LIST,
        VEHICLE,
        VEHICLE_MENU,
        VEHICLE_LIST,
        FLEETTOOL,
        FLEETTOOL_FENCING,
        FLEETTOOL_FENCING_MENU,
        FLEETTOOL_FENCING_LIST,
        FLEETTOOL_ROUTING,
        FLEETTOOL_ROUTING_MENU,
        FLEETTOOL_ROUTING_LIST,
        FLEETTOOL_MISCTOOL,
        FLEETTOOL_MISCTOOL_ETA,
        FLEETTOOL_MISCTOOL_MARKLOCATION,
        FLEETTOOL_MISCTOOL_EDITFLEET,
        FLEETTOOL_MISCTOOL_EDITVEHICLE,
        FLEETTOOL_MISCTOOL_MODEL,
        FLEETTOOL_MISCTOOL_USER,
        FLEETTOOL_MISCTOOL_DRIVER,
        FLEETTOOL_MISCTOOL_PROFILE,
        FLEETTOOL_MISCTOOL_SELECT_EVENT,
        PREFERRENCE,
        PREFERRENCE_SETEVENTCOLOR,
        PREFERRENCE_SETSOUND,
        PREFERRENCE_SHOWDETAILONMAP,
        REPORT,
        REPORT_ACTIVITIES,
        REPORT_SPEED,
        REPORT_SPEEDING,
        REPORT_IDLE,
        REPORT_INOUTZONE,
        REPORT_SPEEDGRAPH,
        REPORT_FUELLEVEL,
        REPORT_NOGPS,
        REPORT_ALLREPORT,
        REPORT_PLAYBACK,

        FLEETTOOL_MISCTOOL_EDITFLEET_ADDNEW,
        FLEETTOOL_MISCTOOL_EDITFLEET_EDIT,
        FLEETTOOL_MISCTOOL_EDITFLEET_DELETE,

        FLEETTOOL_MISCTOOL_EDITVEHICLE_ADDNEW,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_EDIT,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_DELETE,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_MOVE,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_COPY,

        FLEETTOOL_MISCTOOL_MODEL_ADDNEW,
        FLEETTOOL_MISCTOOL_MODEL_EDIT,
        FLEETTOOL_MISCTOOL_MODEL_DELETE,

        FLEETTOOL_MISCTOOL_USER_ADDNEW,
        FLEETTOOL_MISCTOOL_USER_EDIT,
        FLEETTOOL_MISCTOOL_USER_DELETE,

        FLEETTOOL_MISCTOOL_DRIVER_ADDNEW,
        FLEETTOOL_MISCTOOL_DRIVER_EDIT,
        FLEETTOOL_MISCTOOL_DRIVER_DELETE,
        FLEETTOOL_MISCTOOL_DRIVER_MOVE,

        REPORT_INOUTROUTE,
        REPORT_ENGINEONOFF,

        EVENT,
        EVENT_RESET,
        EVENT_SPEEDING,
        EVENT_IDLE,
        EVENT_HAZARD_SPEED,
        EVENT_HAZARD_ANGLE,
        EVENT_DIGITAL_SENSOR,
        EVENT_ANALOG_SENSOR,
        EVENT_LOCATION,
        EVENT_IN_ZONE,
        EVENT_OUT_ZONE,
        EVENT_NO_ZONE_CURFEW,
        EVENT_IN_ROUTE,
        EVENT_OUT_ROUTE,
        EVENT_ENGINE_ON,
        EVENT_ENGINE_OFF,
        EVENT_NO_INZONE_CURFEW,
        EVENT_NO_OUTZONE_CURFEW,

        EVENT_DFM_MAIN_LIGHT_ON,
        EVENT_DFM_MAIN_LIGHT_OFF,
        EVENT_DFM_PTO_SWITCH_ON,
        EVENT_DFM_PTO_SWITCH_OFF,
        EVENT_DFM_HORN_ON,
        EVENT_DFM_HORN_OFF,
        EVENT_DFM_MINOR_LIGHT_ON,
        EVENT_DFM_MINOR_LIGHT_OFF,
        EVENT_DFM_LEFT_SIGNAL_ON,
        EVENT_DFM_LEFT_SIGNAL_OFF,
        EVENT_DFM_RIGHT_SIGNAL_ON,
        EVENT_DFM_RIGHT_SIGNAL_OFF,
        EVENT_DFM_WIPER_ON,
        EVENT_DFM_WIPER_OFF,
        EVENT_DFM_REVERSE_LIGHT_ON,
        EVENT_DFM_REVERSE_LIGHT_OFF,

        EVENT_NO_ZONE_CURFEW_WITH_SPEED,
        EVENT_NO_INZONE_CURFEW_WITH_SPEED,
        EVENT_NO_OUTZONE_CURFEW_WITH_SPEED,

        EVENT_FUEL_OIL,
        EVENT_FUEL_OIL_ON,
        EVENT_FUEL_OIL_OFF,
        EVENT_FUEL_GAS,
        EVENT_FUEL_GAS_ON,
        EVENT_FUEL_GAS_OFF,

        EXTERNAL_INPUT01_ON,
        EXTERNAL_INPUT01_OFF,
        EXTERNAL_INPUT02_ON,
        EXTERNAL_INPUT02_OFF,
        EXTERNAL_INPUT03_ON,
        EXTERNAL_INPUT03_OFF,
        EXTERNAL_INPUT04_ON,
        EXTERNAL_INPUT04_OFF,
        EXTERNAL_INPUT05_ON,
        EXTERNAL_INPUT05_OFF,
        EXTERNAL_INPUT06_ON,
        EXTERNAL_INPUT06_OFF,

        REPORT_SPEEDINGTIME,
        REPORT_INOUTZONETIME,

        FLEETTOOL_MISCTOOL_ISEDITVEHICLEALL,
        ENABLE_SMS,

        GV_CURLOC,
        GV_CURLOC_VID,
        GV_CURLOC_REG,
        GV_CURLOC_MODEL,
        GV_CURLOC_DRIVER,
        GV_CURLOC_TIMESTAMP,
        GV_CURLOC_SPEED,
        GV_CURLOC_OIL_LEVEL,
        GV_CURLOC_LOCATION,
        GV_CURLOC_ETA,
        GV_CURLOC_CONTACT_NO,
        GV_CURLOC_TEMPERATURE,
        GV_CURLOC_SMARTCARD_DATA,

        EVENT_TEMPERATURE,
        EVENT_SMARTCARD_DATA,
        EVENT_ENGINEON_IN_PROHITBIT_TIME,
        FLEETTOOL_MISCTOOL_MARKLOCATION_SELECTION,
        FLEETTOOL_MISCTOOL_EDITFLEET_REFRESH,

        REPORT_IDLETIME,
        FLEETTOOL_MISCTOOL_POINT_SYSTEM,
        FLEETTOOL_MISCTOOL_POINT_SYSTEM_HOLIDAY,

        REPORT_ZONE_MODEL,

        PREFERRENCE_SEND_SMS,
        EVENT_GPS_DISCONNECTED,
        EVENT_POWER_LINE_DISCONNECTED,
        EVENT_USER_DEFINED1_ON,
        EVENT_USER_DEFINED1_OFF,
        EVENT_USER_DEFINED2_ON,
        EVENT_USER_DEFINED2_OFF,

        EVENT_IGNITION_LINE_ON,
        EVENT_IGNITION_LINE_OFF,
        EVENT_USER_DEFINED1_OUT_ON,
        EVENT_USER_DEFINED1_OUT_OFF,
        EVENT_USER_DEFINED2_OUT_ON,
        EVENT_USER_DEFINED2_OUT_OFF,
        EVENT_USER_DEFINED3_OUT_ON,
        EVENT_USER_DEFINED3_OUT_OFF,

        EVENT_ENGINE_ON_OFF_IN_OUTZONE,

        EVENT_POWER_LINE_CONNECTED,
        EVENT_GPS_CONNECTED
    }

    public struct PrcObj
    {
        public Max2FmtMsg msg;
        public KTLogMsg log;
        public int lasted_index;
    }
    #endregion

    public partial class frmDataServer : Form
    {
        public delegate void SetTextCallback(Control ctrl, String msg);
        private System.Threading.Semaphore semaphorLock = new System.Threading.Semaphore(1, 1);

        private bool IsClosing = false;
        private bool IsRunThread = true;
        private bool IsCheckThread = false;
        private bool IsEnableForwardIP = false;
        
        private int ResetGCTimer = 0;

        private String szDatabaseServer = "";
        private String szHeaderProgram = "";
        private String szSystemQ = "";
        private CommonQeue MQ;

        private String szForwardQ = "";
        private String szForwardIP = "";
        private CommonQeue MQ_FWD;

        private String fileNameOld;
        private String fileName;

        private int TotalData = 0;
        private int TotalData_Old = 0;
        private int TotalCurThread = 0;
        private int TotalStartThread = 0;
        private int TotalDataCommit = 0;

        private DateTime nextUpdateTrueZone = new DateTime();

        private Queue LogUpdateQueue = new Queue();
        private Queue ThreadQueue = new Queue();
        private Queue QLogElapsedTime = new Queue();

        private ExEvent oExEvent = new ExEvent();
        private GlobalClass gbClass = new GlobalClass();

        #region Status String
        private string lbMaxSpeedTimeStr = "";
        private string lbMaxIdleTimeStr = "";
        private string lbMaxZnTimeStr = "";
        private string lbMaxZnCurfewTimeStr = "";
        private string lbMaxLocTimeStr = "";
        private string lbMaxRoutingTimeStr = "";
        private string lbMaxETA1TimeStr = "";
        private string lbMaxETA2TimeStr = "";
        private string lbMaxIOSTATUSTimeStr = "";

        private string lbMaxEngineOnEventStr = "";
        private string lbMaxTemperatureStr = "";
        private string lbEngineOnOffInOutZoneStr = "";
        private string lbMaxSpeeadHazardStr = "";
        private string lbMaxAngleHazardStr = "";
        private string lbMaxTrueFencingStr = "";
        private string lbMaxTrueIOStr = "";
        private string lbMaxDFMIOStr = "";
        private string lbMaxZnCurfewWithSpeedStr = "";
        #endregion
        public frmDataServer()
        {
            try
            {
                InitializeComponent();

                InitCfg();

                Properties.Settings pf = new ZebraDataServer.Properties.Settings();
                int interfacePortNo = Convert.ToInt32(pf.InterfacePort);
                TcpChannel chanel = new TcpChannel(interfacePortNo);
                RemotingConfiguration.RegisterWellKnownServiceType(Type.GetType("ZebraDataServer.ExEvent,ZebraDataServer"),
                    "ZebraDataServer", WellKnownObjectMode.Singleton);
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #1" + ex.Message);
            }
        }
        private void InitCfg()
        {
            try
            {
                //---- Read config. from setting file ------------------------------
                Properties.Settings pf = new ZebraDataServer.Properties.Settings();
                szHeaderProgram = pf.HeaderProgram;
                IsEnableForwardIP = pf.EnableForwardIP;
                tsSystemQ.Text = String.Format("   SystemQ : {0}", pf.SystemQ);
                tsDBHostName.Text = " DB Host : " + pf.DBSever;
                this.Text = String.Format("Zebra Data Server V{0} {1}", "2.87", szHeaderProgram);//DateTime.Now.ToString("yyyyMMddHHmm", new CultureInfo("en-US")));

                szDatabaseServer = pf.DBSever;
                szSystemQ = pf.SystemQ;

                ServiceController controller = null;
                controller = this.FindService("MSMQ");

                if (controller != null)
                {
                    if (controller.Status == ServiceControllerStatus.Stopped)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nPlease Start MSMQ services.", "MSMQ Services not start.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        Environment.Exit(0);
                    }
                    while (controller.Status != ServiceControllerStatus.Running)
                    {
                        controller = this.FindService("MSMQ");
                        Thread.Sleep(0x2710);
                    }
                }
                else
                {
                    MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    Environment.Exit(0);
                }
        

                //---- Init Forward Q ----------------------------------------------
                szForwardIP = pf.ForwardIP;
                szForwardQ = pf.ForwardQ;
                MQ_FWD = new CommonQeue(szForwardIP, szForwardQ);

                //---- Init System Q -----------------------------------------------
                MQ = new CommonQeue(szSystemQ);
                MQ.OnReceiveMQ += new ReceiveMQ(mq_OnReceiveMQ);
                MQ.InitReceiveQMsg();

                //Init Zone True
                InitTrueAllZone();
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #2" + ex.Message);
            }
        }
        public ServiceController FindService(string ServiceName)
        {
            try
            {
                ServiceController[] services = ServiceController.GetServices();
                for (int i = 0; i < services.Length; i++)
                {
                    if (services[i].ServiceName.Trim().ToUpper() == ServiceName.Trim().ToUpper())
                    {
                        return services[i];
                    }
                }
                return null;
            }
            catch //(Exception exception)
            {
               // Global.ErrorHandler(Thread.CurrentThread.Name, "FindService: " + ServiceName, exception);
                return null;
            }
        }

        private void InitTrueAllZone()
        {
            try
            {
                oExEvent.GetTrueZone();
                nextUpdateTrueZone = DateTime.Now.AddHours(1);
            }
            catch(Exception ex)
            {
                gbClass.WriteLogEvent(ex.StackTrace, ex.ToString());
            }
        }
        private void InitProcessList()
        {
            ListViewItem itemMaxSp = this.lstPerformance.Items.Add("lbMaxSpeedTime", "1", null); itemMaxSp.SubItems.Add("Speeding"); itemMaxSp.SubItems.Add("?");
            ListViewItem itemMaxIdle = this.lstPerformance.Items.Add("lbMaxIdleTime", "2", null); itemMaxIdle.SubItems.Add("Idle"); itemMaxIdle.SubItems.Add("?");
            ListViewItem itemMaxZn = this.lstPerformance.Items.Add("lbMaxZnTime", "3", null); itemMaxZn.SubItems.Add("Zoning"); itemMaxZn.SubItems.Add("?");
            ListViewItem itemMaxZnCurfew = this.lstPerformance.Items.Add("lbMaxZnCurfewTime", "3", null); itemMaxZnCurfew.SubItems.Add("Zoning in Curfew"); itemMaxZnCurfew.SubItems.Add("?");
            ListViewItem itemMaxLoc = this.lstPerformance.Items.Add("lbMaxLocTime", "4", null); itemMaxLoc.SubItems.Add("Location"); itemMaxLoc.SubItems.Add("?");
            ListViewItem itemMaxRoute = this.lstPerformance.Items.Add("lbMaxRoutingTime", "5", null); itemMaxRoute.SubItems.Add("Routing"); itemMaxRoute.SubItems.Add("?");
            ListViewItem itemMaxETA1 = this.lstPerformance.Items.Add("lbMaxETA1Time", "6", null); itemMaxETA1.SubItems.Add("ETA1"); itemMaxETA1.SubItems.Add("?");
            ListViewItem itemMaxETA2 = this.lstPerformance.Items.Add("lbMaxETA2Time", "7", null); itemMaxETA2.SubItems.Add("ETA2"); itemMaxETA2.SubItems.Add("?");
            ListViewItem itemMaxIOStatus = this.lstPerformance.Items.Add("lbMaxIOSTATUSTime", "8", null); itemMaxIOStatus.SubItems.Add("IO Status"); itemMaxIOStatus.SubItems.Add("?");
            ListViewItem itemMaxEngineOnEventStatus = this.lstPerformance.Items.Add("lbMaxEngineOnEventStr", "9", null); itemMaxEngineOnEventStatus.SubItems.Add("Engine On in Curfew  "); itemMaxEngineOnEventStatus.SubItems.Add("?");
            ListViewItem itemMaxTemperatireStatus = this.lstPerformance.Items.Add("lbMaxTemperatureStr", "10", null); itemMaxTemperatireStatus.SubItems.Add("Temperature Status"); itemMaxTemperatireStatus.SubItems.Add("?");
            ListViewItem itemEngineOnOffInOutZoneStatus = this.lstPerformance.Items.Add("lbEngineOnOffInOutZoneStr", "11", null); itemEngineOnOffInOutZoneStatus.SubItems.Add("Engine On/Off In/Out Zone Status"); itemEngineOnOffInOutZoneStatus.SubItems.Add("?");
            ListViewItem itemMaxSpeedHazardStatus = this.lstPerformance.Items.Add("lbMaxSpeeadHazardStr", "12", null); itemMaxSpeedHazardStatus.SubItems.Add("Speed Hazard"); itemMaxSpeedHazardStatus.SubItems.Add("?");
            ListViewItem itemMaxAngleHazardStatus = this.lstPerformance.Items.Add("lbMaxAngleHazardStr", "13", null); itemMaxAngleHazardStatus.SubItems.Add("Angle Hazard"); itemMaxAngleHazardStatus.SubItems.Add("?");
            ListViewItem itemMaxTrueFencingStatus = this.lstPerformance.Items.Add("lbMaxTrueFencingStr", "14", null); itemMaxTrueFencingStatus.SubItems.Add("True Fencing"); itemMaxTrueFencingStatus.SubItems.Add("?");
            ListViewItem itemMaxTrueIOStatus = this.lstPerformance.Items.Add("lbMaxTrueIOStr", "15", null); itemMaxTrueIOStatus.SubItems.Add("True IO Status"); itemMaxTrueIOStatus.SubItems.Add("?");
            ListViewItem itemMaxDFMIO = this.lstPerformance.Items.Add("lbMaxDFMIOStr", "16", null); itemMaxDFMIO.SubItems.Add("DFM IO"); itemMaxDFMIO.SubItems.Add("?");
            ListViewItem itemMaxZnCurfewWithSpeed = this.lstPerformance.Items.Add("lbMaxZnCurfewWithSpeedTime", "17", null); itemMaxZnCurfewWithSpeed.SubItems.Add("Zoning in Curfew With Speed"); itemMaxZnCurfewWithSpeed.SubItems.Add("?");

            ListViewItem itemsp1 = this.lstPerformance.Items.Add("sp1", "", null); itemsp1.SubItems.Add(""); itemsp1.SubItems.Add("");
            ListViewItem itemsp2 = this.lstPerformance.Items.Add("sp2", "", null); itemsp2.SubItems.Add(""); itemsp2.SubItems.Add("");

            ListViewItem itemTotalThrd = this.lstPerformance.Items.Add("lbTotalCurThd", "", null); itemTotalThrd.SubItems.Add("Total Current Thread : Total LogElapsedTime"); itemTotalThrd.SubItems.Add("?");
            ListViewItem itemTotalData = this.lstPerformance.Items.Add("lbTotalOfData", "", null); itemTotalData.SubItems.Add("Total of Data"); itemTotalData.SubItems.Add("?");
            ListViewItem itemTotalDataCommit = this.lstPerformance.Items.Add("lbTotalOfDataCommit", "", null); itemTotalDataCommit.SubItems.Add("Data Commit/Sec"); itemTotalDataCommit.SubItems.Add("?");
        }
        private void frmDataServer_Load(object sender, EventArgs e)
        {
            InitProcessList();
        }
        private void frmDataAgency_Shown(object sender, EventArgs e)
        {

            if (MQ != null)
            {
                TotalStartThread = Process.GetCurrentProcess().Threads.Count;

                MQ.BeginReceiveQMsg();
                dataServerTimer.Start();
                tmLogElapsedTime.Start();
            }
            else
            {
                MessageBox.Show("Have no Queue " + szSystemQ + " in system", "Warning!!!");
            }
        }
        private void frmDataServer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsClosing)
            {
                Process p = Process.GetCurrentProcess();
                p.Kill();
            }

            e.Cancel = !IsClosing;
            IsClosing = true;
        }
        private void mq_OnReceiveMQ(object owner, XmlReader data, Stream stmData)
        {
            try
            {
                //--- Forward Q
                if (IsEnableForwardIP) { MQ_FWD.ForwardQ(stmData); }
                //-------------

                XmlSerializer ser = new XmlSerializer(typeof(Max2FmtMsg));
                Max2FmtMsg msg = (Max2FmtMsg)ser.Deserialize(data);
                msg = MsgCheckFormat(msg);
                //--- Do IO Status Event Before Throw to Threading

                KTLogMsg objLog = ProcessDataLog(msg);
                ProcessThreadClass thrd = new ProcessThreadClass(msg, objLog ,szDatabaseServer);
                thrd.OnCompleted += new CompletedHandler(thrd_OnCompleted);
                thrd.OnReportStatus += new ReportStatusHandler(thrd_OnReportStatus);
                thrd.OnProcessTimeCompleted += new ElapedTimeProcess(thrd_OnProcessTimeCompleted);
              
                TotalData++;
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #3" + ex.Message);
            }

            if (!IsClosing && MQ != null && IsRunThread && !IsCheckThread)
            {
                MQ.BeginReceiveQMsg();
            }
        }
        private KTLogMsg ProcessDataLog(object obj)
        {
            int lasted_index = -1;
            KTLogMsg objMsg = new KTLogMsg();

            try
            {
                if (obj.GetType() == typeof(Max2FmtMsg))
                {
                    Max2FmtMsg msg = (Max2FmtMsg)obj;
                    if (msg.gps_date_time == null || msg.utc == null) { return null; }

                    SQL sql = new SQL(szDatabaseServer);

                    String timestamp = GetTimeStamp(msg.gps_date_time, msg.utc);

                    DateTime testDate = DateTime.Parse(timestamp);
                    DateTime validDate1 = DateTime.Now.Add(new TimeSpan(-2, 0, 0, 0));
                    DateTime validDate2 = DateTime.Now.Add(new TimeSpan(2, 0, 0, 0));

                    String szValidDate1 = validDate1.ToString("yyyy-MM-dd", new CultureInfo("en-US"));
                    String szValidDate2 = validDate2.ToString("yyyy-MM-dd", new CultureInfo("en-US"));

                    validDate1 = DateTime.Parse(szValidDate1);
                    validDate2 = DateTime.Parse(szValidDate2);

                    if (!(validDate1 <= testDate && testDate <= validDate2))
                    {
                        DateTime d = DateTime.Now;
                        timestamp = d.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        testDate = DateTime.Parse(timestamp);

                        d = d.AddHours(-7);
                        msg.gps_date_time = d.ToString("ddMMyy");
                        msg.utc = d.ToString("HHmmss");
                        msg.lon = "0.0";
                        msg.lat = "0.0";
                    }
                    msg.vid = msg.vid.Trim();
                    objMsg.vid = int.Parse(msg.vid);
                    objMsg.reg = sql.GetRegistration(objMsg.vid);
                    objMsg.timestamp = Convert.ToDateTime(timestamp);//2013-12-25 14:57:13 //20131225145713
                    objMsg.date_id = Convert.ToInt64(Convert.ToDateTime(timestamp).ToString("yyyyMMddHHmmss"));
                    objMsg.lat = double.Parse(msg.lat.Substring(0, msg.lat.Length - 1));
                    objMsg.lon = double.Parse(msg.lon.Substring(0, msg.lon.Length - 1));
                    objMsg.speed = (int)float.Parse(msg.speed);
                    objMsg.course = (int)float.Parse(msg.course);
                    objMsg.fix = int.Parse(msg.type_of_fix);
                    objMsg.sat = int.Parse(msg.no_of_satellite);
                    objMsg.io_status = Convert.ToInt32(msg.gpio_status, 16);
                    objMsg.analog = int.Parse(msg.analog_level);
                    objMsg.type_msg = msg.type_of_message;
                    objMsg.tag = msg.tag;
                    objMsg.loc_t = string.Empty;
                    objMsg.loc_e = string.Empty;
                    objMsg.list_event = new List<Events>();
                    objMsg.log_mode = (int)LOGGINGMOGE.UNKNOWN;
                    msg.type_of_message = msg.type_of_message.Replace("*", "");

                    objMsg.number = CreateShardKey(objMsg.reg, objMsg.vid.ToString());

                    #region DEVICE/LogMsg
                    if (validDate1 <= testDate && testDate <= validDate2)
                    {
                        try
                        {
                            System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                            if (msg.type_of_message == "D" || msg.type_of_message == "8")
                            {
                                #region  Device
                                ///temperature 
                                ///D,T0+2420,080,040,025
                                ///D,T1+2420,080,040,050
                                ///D,T2+2420,080,040,050
                                if (msg.tag[0] == 'T' || msg.tag[0] == 't')
                                {
                                    string[] f = msg.tag.Split(new char[] { ',' });
                                    if (f.Length == 4)
                                    {
                                        int temp_id = Convert.ToInt32(f[0][1].ToString());
                                        float temp_value = (float)Convert.ToDouble(f[0].Substring(2)) / 100.0F;
                                        float max_temp = (float)Convert.ToDouble(f[1]) - 55.0F;
                                        float min_temp = (float)Convert.ToDouble(f[2]) - 55.0F;

                                        if (objMsg.option == null) objMsg.option = new Option();

                                        objMsg.option.optTemp = new Option_Temperature();
                                        objMsg.option.optTemp.temp_id = temp_id;
                                        objMsg.option.optTemp.temperature = temp_value;
                                        objMsg.option.optTemp.max_temp = max_temp;
                                        objMsg.option.optTemp.min_temp = min_temp;
                                    }
                                }
                                ///smart card
                                else if (msg.tag[0] == 'C')
                                {
                                    /*
                                     * CP**********,AA,3460200267001   insert  Enanble,Valid
                                     * CE                              insert  Invalid
                                     * Cp**********,AA,3460200267001   insert  Disable,Valid
                                     * CD                              remove
                                    */
                                    SMARTCARDEVENT evt = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT;
                                    switch (msg.tag[1])
                                    {
                                        case 'P': evt = SMARTCARDEVENT.VALID_SMARTCARD_INSERT; break;
                                        case 'E': evt = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT; break;
                                        case 'p': evt = SMARTCARDEVENT.VALID_BUT_DISABLE_SMART_CARD; break;
                                        case 'D': evt = SMARTCARDEVENT.SMART_CATD_REMOVAL; break;
                                    }

                                    string data = msg.tag.Substring(2);

                                    if (objMsg.option == null) objMsg.option = new Option();

                                    objMsg.option.optSMC = new Option_SmartCard();
                                    objMsg.option.optSMC.smc_evt_id = (int)evt;
                                    objMsg.option.optSMC.data = data;
                                }
                                ///analog level
                                else if (msg.tag[0] == 'V')
                                {
                                    string[] f = msg.tag.Split(new char[] { ',' });
                                    if (f.Length == 4)
                                    {
                                        int vo_id = Convert.ToInt32(f[0][1].ToString());
                                        float analog_2 = (float)Convert.ToDouble(f[1]);
                                        float analog_3 = (float)Convert.ToDouble(f[2]);
                                        float interval = (float)Convert.ToDouble(f[3]);

                                        if (objMsg.option == null) objMsg.option = new Option();

                                        objMsg.option.optAnalog = new Option_Analog_level();
                                        objMsg.option.optAnalog.analog2 = (int)analog_2;
                                        objMsg.option.optAnalog.analog3 = (int)analog_3;
                                    }
                                }
                                else if (msg.tag[0] == 'I')
                                {
                                    //External Input 6 port
                                    string[] inputStr = msg.tag.Split(new char[] { ',' });
                                    if (inputStr.Length == 6)
                                    {

                                        int input_id = Convert.ToInt32(inputStr[0][1].ToString());
                                        int msgType = sql.GetOptionalMsgTypeIDByMsgType(inputStr[1]);
                                        int status_byte = Convert.ToInt32(GetHexToDex2(inputStr[2]));
                                        int input_mark = Convert.ToInt32(GetHexToDex2(inputStr[3]));
                                        int report_time = Convert.ToInt32(inputStr[4]);
                                        int input_status = Convert.ToInt32(GetHexToDex2(inputStr[5]));

                                        if (objMsg.option == null) objMsg.option = new Option();

                                        objMsg.option.optExIn = new Option_External_Input();
                                        objMsg.option.optExIn.input_id = input_id;
                                        objMsg.option.optExIn.message_type = msgType;
                                        objMsg.option.optExIn.status_byte = status_byte;
                                        objMsg.option.optExIn.input_mark = input_mark;
                                        objMsg.option.optExIn.report_time = report_time;
                                        objMsg.option.optExIn.input_status = input_status;
                                    }
                                }
                                else if ((msg.tag[0] == 'M' && msg.tag[1] == 'C') || (msg.type_of_message == "8"))
                                {
                                    //MC,24,2,0004552,00100
                                    //MC,24,
                                    //2,
                                    //0004552,
                                    //00100
                                    //* SUKSAWADDEE SAITHARN MISS,6007643100500157891,150619800909,24,2,0004552,00100

                                    string[] st = msg.tag.Split(new char[] { ',' });
                                    if (st.Length > 4)
                                    {
                                        string driver_type = string.Empty;
                                        string driver_sex = string.Empty;
                                        string driver_number = string.Empty;
                                        string country_code = string.Empty;
                                        string distic_code = string.Empty;
                                        string driver_name = string.Empty;
                                        string province_code = string.Empty;
                                        string card_id = string.Empty;
                                        string expiry_date = string.Empty;
                                        string birthday = string.Empty;

                                        if ((msg.tag[0] == 'M' && msg.tag[1] == 'C'))
                                        {
                                            string code = st[4];
                                            driver_type = st[1];
                                            driver_sex = st[2];
                                            driver_number = st[3];
                                            province_code = code.Substring(0, 3);
                                            distic_code = code.Substring(3, 2);
                                        }
                                        else
                                        {
                                            driver_type = st[3];
                                            driver_sex = st[4];
                                            driver_number = st[5];
                                            country_code = st[6].Substring(0, 3);
                                            distic_code = st[6].Substring(3, 2);
                                            driver_name = st[0];
                                            province_code = st[1].Substring(0, 6);
                                            card_id = st[1].Substring(6, 13);
                                            expiry_date = st[2].Substring(0, 4);
                                            birthday = st[2].Substring(4, 8);
                                        }

                                        if (objMsg.option == null) objMsg.option = new Option();

                                        objMsg.option.optMNT = new Option_Magnetic();
                                        objMsg.option.optMNT.drive_lic = new Driver_License();
                                        objMsg.option.optMNT.drive_lic.driver_type = driver_type;
                                        objMsg.option.optMNT.drive_lic.driver_sex = driver_sex;
                                        objMsg.option.optMNT.drive_lic.driver_number = driver_number;
                                        objMsg.option.optMNT.drive_lic.country_code = country_code;
                                        objMsg.option.optMNT.drive_lic.distic_code = distic_code;
                                        objMsg.option.optMNT.drive_lic.driver_name = driver_name;
                                        objMsg.option.optMNT.drive_lic.province_code = province_code;
                                        objMsg.option.optMNT.drive_lic.card_id = card_id;
                                        objMsg.option.optMNT.drive_lic.expiry_date = expiry_date;
                                        objMsg.option.optMNT.drive_lic.birthday = birthday;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region GSM Signal
                                //--- For MBOX
                                if (msg.type_of_message == "Q")
                                {
                                    string[] f = msg.tag.Split(new char[] { ',' });
                                    if (f.Length >= 7)
                                    {
                                        string mcc = f[0];
                                        string mnc = f[1];
                                        string lac = GetHexToDex(f[2]);
                                        string cell = GetHexToDex(f[3]);
                                        string bsic = f[4];
                                        int arfcn = Convert.ToInt32(f[5]);
                                        int rxlev = Convert.ToInt32(f[6]);

                                        objMsg.gsm = new GSM();
                                        objMsg.gsm.mcc = mcc;
                                        objMsg.gsm.mnc = mnc;
                                        objMsg.gsm.lac = lac;
                                        objMsg.gsm.cell = cell;
                                        objMsg.gsm.bsic = bsic;
                                        objMsg.gsm.arfcn = arfcn;
                                        objMsg.gsm.rxlev = rxlev;
                                    }
                                }
                                //--- For GBOX
                                else if (lasted_index > 0 && msg.tag.Length > 0) //Tag[0] = Q : Is Addition format for GBox GSM. EX: Tag = Q,520,01,36BA,0233,13,116,57
                                {
                                    if (msg.tag[0] == 'Q')
                                    {
                                        string[] f = msg.tag.Split(new char[] { ',' });
                                        if (f.Length >= 7)
                                        {
                                            string mcc = f[1];
                                            string mnc = f[2];
                                            string lac = f[3];
                                            string cell = f[4];
                                            string bsic = f[5];
                                            int arfcn = Convert.ToInt32(f[6]);
                                            int rxlev = Convert.ToInt32(f[7]);

                                            objMsg.gsm = new GSM();
                                            objMsg.gsm.mcc = mcc;
                                            objMsg.gsm.mnc = mnc;
                                            objMsg.gsm.lac = lac;
                                            objMsg.gsm.cell = cell;
                                            objMsg.gsm.bsic = bsic;
                                            objMsg.gsm.arfcn = arfcn;
                                            objMsg.gsm.rxlev = rxlev;
                                        }
                                    }
                                }
                                #endregion
                            }
                            System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                        }
                        catch
                        {
                        }
                    }
                    #endregion
                    #region  IO/Odometer/Distance/Power/CurLoc
                    if (Validate(msg))
                    {
                        bool bDone = true;

                        if (bDone)
                        {
                            #region Reset
                            if (msg.type_of_message == "+")
                            {
                                objMsg.fw_vs = new Firmware_version();
                                if (msg.tag.Trim().Length > 0 && msg.conInfo.type_of_box != null && msg.conInfo.type_of_box == "K")
                                {
                                    objMsg.fw_vs.fw_version = msg.tag.Trim();
                                }
                                if (msg.conInfo.type_of_box != null)
                                {
                                    objMsg.fw_vs.source_ip = msg.conInfo.source_ip;
                                    objMsg.fw_vs.local_ip = msg.conInfo.destination_ip;
                                    objMsg.fw_vs.local_port = int.Parse(msg.conInfo.destination_port);
                                    objMsg.fw_vs.type_of_box = msg.conInfo.type_of_box;
                                }
                                //sql ok
                                sql.UpdateBoxFwVs_Current(int.Parse(msg.vid), msg.tag);
                            }
                            #endregion


                            objMsg.distance = new Distance();
                            #region process Tag msg
                            if (msg.tag.Length > 0)
                            {
                                // OD,0000008612,ER,507;
                                // ER,507 
                                string[] tmp = msg.tag.Split(',');
                                for (int i = 0; i < tmp.Length; i++)
                                {
                                    if (tmp[i] == "OD")
                                    {
                                        //odometer
                                        double odometer = -1.0;
                                        if (double.TryParse(tmp[(i + 1)], out odometer) && odometer > 0.0)
                                        {
                                            objMsg.distance.odometor = odometer;
                                        }
                                    }
                                    else if (tmp[i] == "ER")
                                    {
                                        objMsg.eng_rpm = new RPM();
                                        int tmp_rpm = -1;
                                        if (int.TryParse(tmp[(i + 1)], out tmp_rpm))
                                        {
                                            objMsg.eng_rpm.rawdata = tmp_rpm;
                                        }
                                    }
                                }
                            }
                            #endregion process Tag msg

                            #region current(reset)and distance
                            double distance = 0.00;
                            if (int.Parse(msg.no_of_satellite) >= 3 && int.Parse(msg.type_of_fix) > 0 && int.Parse(msg.speed) < 175)
                            {
                                //Call Distance
                                DistanceCls distCls = new DistanceCls(szDatabaseServer);
                                //sql ok
                                if (distCls.CalDistance(int.Parse(msg.vid), float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)), out distance))
                                {
                                    //sql ok
                                    sql.UpdateBoxLatLonDis_Current(int.Parse(msg.vid), float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)), objMsg.sat, distance);
                                }
                                else if (objMsg.lat > 0.0 && objMsg.lon > 0.0) // update lat lon
                                {
                                    sql.UpdateBoxLatLonDis_Current(objMsg.vid, float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)), objMsg.sat, distance);
                                }
                            }
                            //sql ok
                            sql.UpSertBox_Current(int.Parse(msg.vid), msg.conInfo.type_of_box == null ? "?" : msg.conInfo.type_of_box,
                                true,
                                msg.conInfo.source_ip == null ? "?" : msg.conInfo.type_of_box,
                                int.Parse(msg.conInfo.source_port == null ? "0" : msg.conInfo.source_port),
                                msg.conInfo.destination_ip == null ? "?" : msg.conInfo.destination_ip,
                                int.Parse(msg.conInfo.destination_port == null ? "0" : msg.conInfo.destination_port));
                            objMsg.distance.distance = distance;
                            #endregion

                            #region Loging Mode
                            if (objMsg.type_msg.StartsWith("*"))
                            {
                                objMsg.log_mode = (int)LOGGINGMOGE.BUFFER;
                            }
                            else if (msg.conInfo.type_of_box != null)
                            {
                                switch (msg.conInfo.type_of_box)
                                {
                                    case "K":
                                        objMsg.log_mode = (int)LOGGINGMOGE.REALTIME;
                                        break;
                                    case "G":
                                        objMsg.log_mode = (int)LOGGINGMOGE.REALTIME;
                                        break;
                                    case "M":
                                        objMsg.log_mode = (int)LOGGINGMOGE.UNKNOWN;
                                        break;
                                }
                            }
                            #endregion Loging Mode

                            #region Power
                            double internal_power = 0.0;
                            double external_power = 0.0;
                            if ((double.TryParse(msg.internal_power, out internal_power) && double.TryParse(msg.external_power, out external_power)) && (internal_power > 0 || external_power > 0))
                            {
                                objMsg.power = new Power();
                                objMsg.power.inernal = internal_power;
                                objMsg.power.external = external_power;
                            }
                            #endregion

                            #region Event IO
                            ExEventIO oExIO = new ExEventIO();
                            oExIO.objMsg = objMsg;

                            oExIO.DetectEventIO(
                                int.Parse(msg.vid),
                                timestamp,
                                float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                                (int)float.Parse(msg.speed), (int)float.Parse(msg.course), int.Parse(msg.type_of_fix),
                                int.Parse(msg.no_of_satellite), Convert.ToInt32(msg.gpio_status, 16), int.Parse(msg.analog_level),
                                msg.type_of_message, msg.tag, (msg.conInfo.type_of_box != null ? msg.conInfo.type_of_box : "")
                                );
                            #endregion

                            objMsg = oExIO.objMsg;
                        }
                    }
                    #endregion
                }
            }
            catch
            {
            }
            return objMsg;
        }
        private Double CreateShardKey(string reg, string vid)
        {
            Double key = -1.0;
            try
            {
                string heard_vid = vid[vid.Length - 1].ToString();
                string sub2 = "0000";
                if (heard_vid == "0")
                {
                    string temp_head_vid1 = ELFHash(reg).ToString();
                    if (temp_head_vid1[temp_head_vid1.Length - 1] != '0')
                    {
                        heard_vid = temp_head_vid1[temp_head_vid1.Length - 1].ToString();
                    }
                    else if (vid.Length > 1 && vid[vid.Length - 2] != '0')
                    {
                        heard_vid = vid[vid.Length - 2].ToString();
                    }
                    else
                    {
                        Random ran = new Random();
                        heard_vid = ran.Next(1, 9).ToString();
                    }
                }

                reg += DateTime.Now.ToString("yyyyMMdd");
                string temp = ELFHash(reg).ToString();                          //54998631
                string temp2 = heard_vid + temp.Substring(temp.Length - 5, 5);  //198631

                if (vid.Length < 4)
                    sub2 = vid.PadLeft(4, '0');
                else
                    sub2 = vid.Substring(vid.Length - 4, 4);

                temp2 += sub2;
                key = Convert.ToDouble(temp2);
                key += Convert.ToDouble("0." + DateTime.Now.ToString("yyyyMMdd"));
            }
            catch { }
            return key;
        }

        private Int64 ELFHash(String str)
        {
            Int64 hash = 0;
            Int64 x = 0;

            for (Int32 i = 0; i < str.Length; i++)
            {
                hash = (hash << 4) + str[i];

                if ((x = hash & 0xF0000000L) != 0)
                {
                    hash ^= (x >> 24);
                }
                hash &= ~x;
            }

            return hash;
        }

        void thrd_OnProcessTimeCompleted(string zMsg)
        {
            semaphorLock.WaitOne();
            try { QLogElapsedTime.Enqueue(zMsg); }
            catch { }
            semaphorLock.Release();
        }
        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private Max2FmtMsg MsgCheckFormat(Max2FmtMsg msg)
        {
            try
            {
                if (msg.tag == null) { msg.tag = ""; }
                if (msg.odometor == null) { msg.odometor = ""; }
                if (msg.internal_power == null) { msg.internal_power = ""; }
                if (msg.external_power == null) { msg.external_power = ""; }
            }
            catch
            { }
            return msg;
        }
        private void thrd_OnReportStatus(object sender, CONTROLID ControlID, string data)
        {
            switch (ControlID)
            {
                case CONTROLID.MAXSPEEDTIMEID:
                    lbMaxSpeedTimeStr = data;
                    break;
                case CONTROLID.MAXIDLETIMEID:
                    lbMaxIdleTimeStr = data;
                    break;
                case CONTROLID.MAXZNTIMEID:
                    lbMaxZnTimeStr = data;
                    break;
                case CONTROLID.MAXZNINCURFEWTIMEID:
                    lbMaxZnCurfewTimeStr = data;
                    break;
                case CONTROLID.MAXLOCTIMEID:
                    lbMaxLocTimeStr = data;
                    break;
                case CONTROLID.MAXROUTINGTIMEID:
                    lbMaxRoutingTimeStr = data;
                    break;
                case CONTROLID.MAXETA1TIMEID:
                    lbMaxETA1TimeStr = data;
                    break;
                case CONTROLID.MAXETA2TIMEID:
                    lbMaxETA2TimeStr = data;
                    break;
                case CONTROLID.MAXIOSTATUSTIMEID:
                    lbMaxIOSTATUSTimeStr = data;
                    break;
                case CONTROLID.MAXENGINEONEVENTID:
                    lbMaxEngineOnEventStr = data;
                    break;
                case CONTROLID.MAXTEMPERATUREID:
                    lbMaxTemperatureStr = data;
                    break;
                case CONTROLID.MAXENGINEONOFFINOUTZONE:
                    lbEngineOnOffInOutZoneStr = data;
                    break;
                case CONTROLID.MAXSPEEDHAZRD:
                    lbMaxSpeeadHazardStr = data;
                    break;
                case CONTROLID.MAXANGLEHAZARD:
                    lbMaxAngleHazardStr = data;
                    break;
                case CONTROLID.MAXTRUEFENCING:
                    lbMaxTrueFencingStr = data;
                    break;
                case CONTROLID.MAXTRUEIO:
                    lbMaxTrueIOStr = data;
                    break;
                case CONTROLID.MAXDFMIO:
                    lbMaxDFMIOStr = data;
                    break;
                case CONTROLID.MAXZNINCURFEWWITHSPEEDTIMEID:
                    lbMaxZnCurfewWithSpeedStr = data;
                    break;
                default:
                    break;
            }
        }
        private void thrd_OnCompleted(object sender)
        {
            if (TotalCurThread < 0) TotalCurThread = 0;
        }
        private void WriteCtrlMsg(String ctrlStr, String msg)
        {
            try
            {
                if (lstPerformance.InvokeRequired)
                {
                    /*
                    SetTextCallback proc = new SetTextCallback(WriteCtrlMsg);
                    this.Invoke(proc, new object[] { ctrl, msg });
                    */
                }
                else
                {
                    lstPerformance.Items[ctrlStr].SubItems[2].Text = msg;
                }
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #4" + ex.Message);
            }
        }
        private void dataServerTimer_Tick(object sender, EventArgs e)
        {
            if (TotalCurThread > 500)
            {
                IsRunThread = false;

                Process p = Process.GetCurrentProcess();
                p.Kill();
            }

            if (!IsRunThread && TotalCurThread < 200)
            {
                IsRunThread = true;
                MQ.BeginReceiveQMsg();
            }

            TotalCurThread = Process.GetCurrentProcess().Threads.Count - TotalStartThread;
            TotalDataCommit = TotalData - TotalData_Old;

            if (TotalCurThread < 0) TotalCurThread = 0;

            WriteCtrlMsg("lbMaxSpeedTime", lbMaxSpeedTimeStr);
            WriteCtrlMsg("lbMaxIdleTime", lbMaxIdleTimeStr);
            WriteCtrlMsg("lbMaxZnTime", lbMaxZnTimeStr);
            WriteCtrlMsg("lbMaxZnCurfewTime", lbMaxZnCurfewTimeStr);
            WriteCtrlMsg("lbMaxLocTime", lbMaxLocTimeStr);
            WriteCtrlMsg("lbMaxRoutingTime", lbMaxRoutingTimeStr);
            WriteCtrlMsg("lbMaxETA1Time", lbMaxETA1TimeStr);
            WriteCtrlMsg("lbMaxETA2Time", lbMaxETA2TimeStr);
            WriteCtrlMsg("lbMaxIOSTATUSTime", lbMaxIOSTATUSTimeStr);

            WriteCtrlMsg("lbMaxEngineOnEventStr", lbMaxEngineOnEventStr);
            WriteCtrlMsg("lbMaxTemperatureStr", lbMaxTemperatureStr);

            WriteCtrlMsg("lbEngineOnOffInOutZoneStr", lbEngineOnOffInOutZoneStr);

            WriteCtrlMsg("lbMaxSpeeadHazardStr", lbMaxSpeeadHazardStr);
            WriteCtrlMsg("lbMaxAngleHazardStr", lbMaxAngleHazardStr);
            WriteCtrlMsg("lbMaxTrueFencingStr", lbMaxTrueFencingStr);
            WriteCtrlMsg("lbMaxTrueIOStr", lbMaxTrueIOStr);
            WriteCtrlMsg("lbMaxDFMIOStr", lbMaxDFMIOStr);
            WriteCtrlMsg("lbMaxZnCurfewWithSpeedTime", lbMaxZnCurfewWithSpeedStr);

            WriteCtrlMsg("lbTotalCurThd", TotalCurThread.ToString() + " : " + QLogElapsedTime.Count.ToString());
            WriteCtrlMsg("lbTotalOfData", TotalData.ToString());
            WriteCtrlMsg("lbTotalOfDataCommit", TotalDataCommit.ToString());

            if (IsClosing)
            {
                if (TotalCurThread <= 0)
                {
                    dataServerTimer.Stop();
                    Close();
                    Application.Exit();
                }
                else
                {
                    tsTimerStatus.Text = "Please wait, still threre are running Thread left !!!";
                }
            }
            else
            {
                tsTimerStatus.Text = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                TotalData_Old = TotalData;
            }

            if (++ResetGCTimer > 60)
            {
                ResetGCTimer = 0;
                GC.Collect();
            }

            //if (DateTime.Now > nextUpdateTrueZone)
            //{
            //    InitTrueAllZone();
            //}
        }

        private void stopMQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IsCheckThread = true;
        }
        private void startMQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IsCheckThread = false;
            MQ.BeginReceiveQMsg();
        }
        private bool Validate(Max2FmtMsg msg)
        {
            bool bIsOk = true; 
            //SQL sql = new SQL(szDatabaseServer);

            //int maxSpeed = 200; // Km/Hrs 
            //int totalSec = 2 * 60;
            string eventStr = "K,M,V,A,S,G,L,O,F,X,x"; // "2,3,4,5,9,10,12,15,16,17,18"; msg type id.

            //double lat = 0.0;
            //double lon = 0.0;
            DateTime timestamp = new DateTime();

            try
            {
                String szTimeStamp = GetTimeStamp(msg.gps_date_time, msg.utc);
                DateTime curTimestamp = Convert.ToDateTime(szTimeStamp);

                String d1 = curTimestamp.ToString("MM/dd/yyyy HH:mm:ss");
                String d2 = timestamp.ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));
                String d3 = DateTime.Now.AddHours(1).ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));

                if (Convert.ToDateTime(d3) < Convert.ToDateTime(d1))
                {
                    return false;
                }

                int idx = eventStr.IndexOf(msg.type_of_message);
                if (idx < 0)
                {
                    return bIsOk;
                }
            }
            catch
            {
            }

            return bIsOk;
        }
        private String GetTimeStamp(String date, String time)
        {
            try
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                d = d.AddHours(7.0);
                String szTimeStamp = d.ToString("yyyy-MM-dd HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server(Thread) #3" + ex.Message);
            }

            return "";
        }
        private string GetHexToDex(string zHexStr)
        {
            string retDec = zHexStr;
            try
            {
                int decValue = 0;
                if (!(int.TryParse(zHexStr, out decValue)))
                {
                    decValue = Convert.ToInt32(zHexStr, 16);
                    retDec = decValue.ToString("0");
                }
            }
            catch
            {

            }
            return retDec;
        }
        private string GetHexToDex2(string zHexStr)
        {
            string retDec = "0";
            try
            {
                int decValue = Convert.ToInt32(zHexStr, 16);
                retDec = decValue.ToString("0");
            }
            catch
            {
            }
            return retDec;
        }
        private void WriteLogEvent(String Msg)
        {
            try
            {
                String logPath = Directory.GetCurrentDirectory() + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }

                fileName = logPath + "\\" + "ElapsedTimelog" + DateTime.Now.ToString("_yyyy_MM_dd") + ".txt";

                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    fileNameOld = logPath + "\\" + "ElapsedTimelog" + DateTime.Now.AddDays(-1).ToString("_yyyy_MM_dd") + ".txt";

                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();

                    if (File.Exists(fileNameOld))
                    {
                        AddFileArchive();
                        if (File.Exists(fileNameOld + ".rar"))
                        {
                            File.Delete(fileNameOld);
                            Thread.Sleep(3000);
                        }
                    }

                }
            }
            catch { }
        }
        private void AddFileArchive()
        {
            string src_file_name = fileNameOld;
            string dst_file_name = fileNameOld + ".rar";

            System.IO.FileStream file_input = null;
            System.IO.FileStream file_output = null;
            System.IO.Compression.GZipStream rar = null;
            byte[] buffer;

            try
            {
                file_output = new System.IO.FileStream(dst_file_name, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);
                rar = new System.IO.Compression.GZipStream(file_output, System.IO.Compression.CompressionMode.Compress, true);

                file_input = new System.IO.FileStream(src_file_name, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                buffer = new byte[file_input.Length];
                int iBuff = file_input.Read(buffer, 0, buffer.Length);
                file_input.Close();
                file_input = null;

                rar.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (rar != null)
                {
                    rar.Close();
                    rar = null;
                }
                if (file_output != null)
                {
                    file_output.Close();
                    file_output = null;
                }
                if (file_input != null)
                {
                    file_input.Close();
                    file_input = null;
                }
            }
        }
        private void tmLogElapsedTime_Tick(object sender, EventArgs e)
        {
            tmLogElapsedTime.Stop();

            try
            {
                if (QLogElapsedTime.Count > 0)
                {
                    string Msg = (string)QLogElapsedTime.Dequeue();
                    WriteLogEvent(Msg);
                }
            }
            catch { }
            tmLogElapsedTime.Start();
        }
    }
}