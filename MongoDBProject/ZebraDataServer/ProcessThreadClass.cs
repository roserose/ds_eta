using System;
using System.Collections.Generic;
using System.Text;
using ZebraPresentationLayer;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.Collections;
using System.Diagnostics;
using System.IO;

using ZebraCommonInterface;
using ZebraNoSQLLib;

namespace ZebraDataServer
{
    public delegate void ReportStatusHandler(object sender, CONTROLID ControlID, string data);
    public delegate void CompletedHandler(object sender);

    public class ProcessThreadClass
    {
        public event ReportStatusHandler OnReportStatus;
        public event CompletedHandler OnCompleted;
        public event ElapedTimeProcess OnProcessTimeCompleted;
        private String szDatabaseServer = "";
        private ExEvent oExEvent = new ExEvent();

        private System.Threading.Semaphore runLock = new Semaphore(1, 1);

        public struct PrcObj
        {
            public Max2FmtMsg Msg;
            public KTLogMsg ktLog;
        }

        public ProcessThreadClass(Max2FmtMsg msg, KTLogMsg log, String szDBServer)
        {
            szDatabaseServer = szDBServer;
            oExEvent.OnProcessCompleted += new ProcessCompleted(oExEvent_OnProcessCompleted);
            oExEvent.OnProcessTime += new ElapedTimeProcess(oExEvent_OnProcessTime);

            try
            {
                PrcObj prcObj = new PrcObj();
                prcObj.Msg = msg;
                prcObj.ktLog = log;
              
                Thread thread = new Thread(new ParameterizedThreadStart(ReadQCallBack));
                thread.Start(prcObj);
                Thread.Sleep(10);
            }
            catch
            {
            }
        }

        private void ReadQCallBack(object obj)
        {
            try
            {
                PrcObj prcObj = (PrcObj)obj;
                Max2FmtMsg msg = prcObj.Msg;
                KTLogMsg _ktLog = prcObj.ktLog;

                if (msg.gps_date_time == null || msg.utc == null)
                {
                    return;
                }

                if (msg.GetType() == typeof(Max2FmtMsg))
                {
                    String timestamp = GetTimeStamp(msg.gps_date_time, msg.utc);

                    DateTime testDate = DateTime.Parse(timestamp);
                    DateTime validDate1 = DateTime.Now.Add(new TimeSpan(-2, 0, 0, 0));
                    DateTime validDate2 = DateTime.Now.Add(new TimeSpan(2, 0, 0, 0));

                    String szValidDate1 = validDate1.ToString("yyyy-MM-dd", new CultureInfo("en-US"));
                    String szValidDate2 = validDate2.ToString("yyyy-MM-dd", new CultureInfo("en-US"));

                    validDate1 = DateTime.Parse(szValidDate1);
                    validDate2 = DateTime.Parse(szValidDate2);

                    if (!(validDate1 <= testDate && testDate <= validDate2))
                    {
                        timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        testDate = DateTime.Parse(timestamp);
                        msg.lon = "0.0";
                        msg.lat = "0.0";
                    }

                    if (Validate(msg))
                    {
                        Thread.Sleep(10);
                        oExEvent.KtLog = _ktLog;
                      
                        oExEvent.DetectEvent
                            (
                                int.Parse(msg.vid),
                                timestamp,
                                float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)),
                                float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                                (int)float.Parse(msg.speed),
                                (int)float.Parse(msg.course),
                                int.Parse(msg.type_of_fix),
                                int.Parse(msg.no_of_satellite),
                                Convert.ToInt32(msg.gpio_status, 16),
                                int.Parse(msg.analog_level),
                                msg.type_of_message,
                                msg.tag,
                                testDate
                            );
                        
                        _ktLog = oExEvent.KtLog;
                        InsertLocationEvent(_ktLog, true);
                    }
                    else // insert to DB No Create Event
                    {
                        InsertLocationEvent(_ktLog, false);
                    }
                }
                if (OnCompleted != null) { OnCompleted(this); }
            }
            catch
            {
            }
        }
        private void InsertLocationEvent(KTLogMsg ktLog, bool valid)
        {
            try
            {
                string smard_card_data = string.Empty;

                if (ktLog.option != null && ktLog.option.optSMC != null)
                {
                    foreach (Events e in ktLog.list_event)
                    {
                        if (e.smart_card != null)
                        {
                            smard_card_data = e.smart_card.smart_desc;
                            break;
                        }
                    }
                }

                double temperature = 999.99;
                if(ktLog.option != null && ktLog.option.optTemp != null)
                {
                    if (ktLog.option.optTemp != null) temperature = ktLog.option.optTemp.temperature;
                }

                oExEvent.UpSert_VehCurLocation(
                    ktLog.vid,
                    ktLog.timestamp,
                    ktLog.speed,
                    ktLog.date_id,
                    temperature,
                    smard_card_data,
                    ktLog.energy == null ? 999.99 : ktLog.energy.oil1,
                    ktLog.energy == null ? 999.99 : ktLog.energy.oil2,
                    ktLog.energy == null ? 999.99 : ktLog.energy.oil3,
                    ktLog.loc_t,
                    ktLog.loc_e
                    );
                oExEvent.InsertDataToDataBase(ktLog);
            }
            catch { }
        }

        private void oExEvent_OnProcessCompleted(object sender, ExEventCompleteStruc msg)
        {
            WriteCtrlMsg(CONTROLID.MAXSPEEDTIMEID, msg.speeding_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXIDLETIMEID, msg.idle_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXZNTIMEID, msg.fencing_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXZNINCURFEWTIMEID, msg.zone_in_curfew_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXLOCTIMEID, msg.location_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXROUTINGTIMEID, msg.routing_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXETA1TIMEID, msg.eta1_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXETA2TIMEID, msg.eta2_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXIOSTATUSTIMEID, msg.io_status_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXENGINEONEVENTID, msg.engine_on_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXTEMPERATUREID, msg.temperature_elapsed_time_maximum.ToString());

            WriteCtrlMsg(CONTROLID.MAXENGINEONOFFINOUTZONE, msg.engine_onoff_inout_zone_elapsed_time_maximun.ToString());

            WriteCtrlMsg(CONTROLID.MAXSPEEDHAZRD, msg.speed_hazard_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXANGLEHAZARD, msg.angle_hazard_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXTRUEFENCING, msg.true_fencing_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAXTRUEIO, msg.true_io_elapsed_time_maximum_.ToString());
            WriteCtrlMsg(CONTROLID.MAXDFMIO, msg.DFM_io_status_elapsed_time_maximum.ToString());

            WriteCtrlMsg(CONTROLID.MAXZNINCURFEWWITHSPEEDTIMEID, msg.zone_in_curfew_elapsed_time_maximum.ToString());
        }

        private bool Validate(Max2FmtMsg msg)
        {
            bool bIsOk = true;
            string eventStr = "K,M,V,A,S,G,L,O,F,X,x"; // "2,3,4,5,9,10,12,15,16,17,18"; msg type id.
            DateTime timestamp = new DateTime();

            try
            {
                String szTimeStamp = GetTimeStamp(msg.gps_date_time, msg.utc);
                DateTime curTimestamp = Convert.ToDateTime(szTimeStamp);

                String d1 = curTimestamp.ToString("MM/dd/yyyy HH:mm:ss");
                String d2 = timestamp.ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));
                String d3 = DateTime.Now.AddHours(1).ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));

                if (Convert.ToDateTime(d3) < Convert.ToDateTime(d1))
                {
                    return false;
                }

                int idx = eventStr.IndexOf(msg.type_of_message);
                if (idx < 0)
                {
                    return bIsOk;
                }
            }
            catch
            {
            }

            return bIsOk;
        }
        private String GetTimeStamp(String date, String time)
        {
            try
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                d = d.AddHours(7.0);
                String szTimeStamp = d.ToString("yyyy-MM-dd HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server(Thread) #3" + ex.Message);
            }

            return "";
        }
        private void WriteCtrlMsg(CONTROLID CtrlID, String msg)
        {
            if (OnReportStatus != null)
            {
                OnReportStatus(this, CtrlID, msg);
            }
        }
        void oExEvent_OnProcessTime(string zMsg)
        {
            if (OnProcessTimeCompleted != null)
            {
                OnProcessTimeCompleted(zMsg);
            }
        }

        private double GetODOFromTheGreatCircle(double lat1, double lon1, double lat2, double lon2)
        {
            double R = 6371.0;
            double rad = 57.2958;

            lat1 = lat1 / rad;
            lat2 = lat2 / rad;
            lon1 = lon1 / rad;
            lon2 = lon2 / rad;

            double a = Math.Sin(lat1) * Math.Sin(lat2);
            double b = Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(lon2 - lon1);
            double c = Math.Acos(a + b);
            double d = R * c;

            return d;
        }
        private string GetHexToDex(string zHexStr)
        {
            string retDec = zHexStr;
            try
            {
                int decValue = 0;
                if (!(int.TryParse(zHexStr, out decValue)))
                {
                    decValue = Convert.ToInt32(zHexStr, 16);
                    retDec = decValue.ToString("0");
                }
            }
            catch
            {

            }
            return retDec;
        }
        private string GetHexToDex2(string zHexStr)
        {
            string retDec = "0";
            try
            {
                int decValue = Convert.ToInt32(zHexStr, 16);
                retDec = decValue.ToString("0");
            }
            catch
            {

            }
            return retDec;
        }
     
    }
}
