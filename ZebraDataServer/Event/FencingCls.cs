using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Drawing;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class FencingCls
    {
        public enum SIDE_TYPE { NONE, IN, OUT };
        public struct iVector
        {
            public PointF p1;
            public PointF p2;
        }

        double mX = 0;
        double mY = 0;

        double minX = 1000000;
        double maxX = -1000000;

        double minY = 1000000;
        double maxY = -1000000;

        Boolean gClkWise = false;

        SIDE_TYPE gSide = SIDE_TYPE.OUT;
        internal SIDE_TYPE GSide
        {
            get { return gSide; }
            set { gSide = value; }
        }

        List<PointF> wayPoints = new List<PointF>();

        SQL gSql;
        SQL2 gSql2;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Zone Function         

        public FencingCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public Boolean DetectFencing(int veh_id,int zone_id, float lat, float lon, float lat_min, float lat_max, float lon_min, float lon_max, Boolean clkdir, DateTime lasted_update, out SIDE_TYPE side)
        {
            GSide = SIDE_TYPE.OUT;
            if (!((lon_min <= lon && lon <= lon_max) && (lat_min <= lat && lat <= lat_max)))
            {
                GSide = SIDE_TYPE.OUT;
            }
            else 
            {
                DataTable wpTable = gSql.GetZoneWayPoints(zone_id);
                DataTable wp = new DataTable();
                wp.Columns.Add("X", typeof(float));
                wp.Columns.Add("Y", typeof(float));

                foreach (DataRow wp_row in wpTable.Rows)
                {
                    double wpX = (double)wp_row["lon"];
                    double wpY = (double)wp_row["lat"];
                    wp.Rows.Add(new object[] { (float)wpX, (float)wpY });
                }

                if (TestPointInsideArea(lon, lat, wp))
                {
                    GSide = SIDE_TYPE.IN;
                }
            }
            side = GSide;
            return true;
        }

        public Boolean DetectFencingTrueCorp(int veh_id, float lat, float lon, float lat_min, float lat_max, float lon_min, float lon_max, Boolean clkdir, DateTime lasted_update,DataTable wpTable, out SIDE_TYPE side)
        {
            GSide = SIDE_TYPE.OUT;
            if (!((lon_min <= lon && lon <= lon_max) && (lat_min <= lat && lat <= lat_max)))
            {
                GSide = SIDE_TYPE.OUT;
            }
            else if (TestPointInsideArea(lon, lat, wpTable))
            {
                GSide = SIDE_TYPE.IN;
            }
            side = GSide;
            return true;
        }

        public SIDE_TYPE DetectFencing2(DataTable wpTable, float lon, float lat)
        {
            /*
            CreateWayPoints(wpTable);
            MinMax(wayPoints, ref minX, ref maxX, ref minY, ref maxY);
            gClkWise = ClockWise();

            gSide = SIDE_TYPE.OUT;
            CalculateFencing(lon, lat);
            return gSide;
             */

            GSide = SIDE_TYPE.OUT;
            if (TestPointInsideArea(lon, lat, wpTable))
            {
                GSide = SIDE_TYPE.IN;
            }

            return GSide;
        }
        public void InsertMsgZone(int ref_idx, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.InsertMsgZone(ref_idx, zone_id, (int)EVT_TYPE.IN_ZONE);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.InsertMsgZone(ref_idx, zone_id, (int)EVT_TYPE.OUT_ZONE);
            }
        }
        public void InsertNewRecord(int ref_idx, int veh_id, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.IN_ZONE);
                gSql.InsertPrvZoneState(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE, ref_idx);
            }

            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.OUT_ZONE);
                gSql.InsertPrvZoneState(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE, ref_idx);
            }
        }
        public void InsertNewRecord(decimal ref_idx, int veh_id, DateTime timestamp, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.IN_ZONE, zone_id, -1);
                gSql2.InsertPrvZoneState(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE, ref_idx);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id,timestamp, (int)EVT_TYPE.OUT_ZONE, zone_id,-1);
                gSql2.InsertPrvZoneState(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE, ref_idx);
            }
        }
        public void InsertNewRecordZone_Curfew(decimal ref_idx, int veh_id, DateTime timestamp, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.IN_ZONE_CURFEW, zone_id, -1);
                gSql2.InsertPrvZoneState_Curfew(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE_CURFEW, ref_idx);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.OUT_ZONE_CURFEW, zone_id, -1);
                gSql2.InsertPrvZoneState_Curfew(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE_CURFEW, ref_idx);
            }
        }

        public void InsertNewRecordNoInZone_Curfew(decimal ref_idx, int veh_id, DateTime timestamp, int zone_id)
        {
            if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.NO_INZONE_CURFEW, zone_id, -1);
                gSql2.InsertPrvZoneState_Curfew(veh_id, zone_id, (int)EVT_TYPE.NO_INZONE_CURFEW, ref_idx);
            }
            else if (GSide == SIDE_TYPE.IN)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.IN_ZONE_CURFEW, zone_id, -1);
                gSql2.InsertPrvZoneState_Curfew(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE_CURFEW, ref_idx);
            }
        }

        public void InsertNewRecordNoOutZone_Curfew(decimal ref_idx, int veh_id, DateTime timestamp, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.NO_OUTZONE_CURFEW, zone_id, -1);
                gSql2.InsertPrvZoneState_Curfew(veh_id, zone_id, (int)EVT_TYPE.NO_OUTZONE_CURFEW, ref_idx);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.OUT_ZONE_CURFEW, zone_id, -1);
                gSql2.InsertPrvZoneState_Curfew(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE_CURFEW, ref_idx);
            }
        }
    
        public DataTable GetFleetZoneByVeh(int veh_id)
        {
            return gSql.GetFleetZoneByVeh(veh_id);
        }

        public Boolean GetEngineOnCurfewEvt(int veh_id)
        {///1
            return gSql.GetEngineOnByTime(veh_id);
        }

        public DataTable GetZoneProfile(int zone_id)
        {
            return gSql.GetZoneProfile(zone_id);
        }

        public DataTable GetZoneProfile_new(int zone_id)
        {
            return gSql.GetZoneProfile_new(zone_id);
        }

        public DataTable GetZoneWayPoints(int zone_id)
        {
            return gSql.GetZoneWayPoints(zone_id);
        }
        public void GetLastZoneStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            gSql.GetLastZoneStatus(veh_id, last_zone_id, out evt_id, out zone_id);
        }

        public void GetLastZoneStatus_Log(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            gSql2.GetLastZoneStatus(veh_id, last_zone_id, out evt_id, out zone_id);
        }

        public void InsertPrvZoneState(int veh_id, int zone_id, int evt_id, int ref_idx)
        {
            gSql.InsertPrvZoneState(veh_id, zone_id, evt_id, ref_idx);
        }

        public void True2GetZoneLastStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            gSql.True2GetZoneLastStatus(veh_id, last_zone_id, out evt_id, out zone_id);
        }
        public void True2GetZoneLastStatus_Log(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            gSql2.True2GetZoneLastStatus_Log(veh_id, last_zone_id, out evt_id, out zone_id);
        }

        public int True2InsertMsgZone(int ref_idx, int zone_id)
        {
            int id = -1;
            if (GSide == SIDE_TYPE.IN)
            {
                id = gSql.True2InsertMsgZone(ref_idx, zone_id, (int)EVT_TYPE.IN_ZONE);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                id = gSql.True2InsertMsgZone(ref_idx, zone_id, (int)EVT_TYPE.OUT_ZONE);
            }
            return id;
        }


        public int True2InsertMsgZone_Log(Decimal ref_idx, int zone_id)
        {
            int id = -1;
            if (GSide == SIDE_TYPE.IN)
            {
                id = gSql2.True2InsertMsgZone_Log(ref_idx, zone_id, (int)EVT_TYPE.IN_ZONE);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                id = gSql2.True2InsertMsgZone_Log(ref_idx, zone_id, (int)EVT_TYPE.OUT_ZONE);
            }
            return id;
        }
        public void True2ZoneState(int ref_idx, int veh_id, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.True2InsertPrvZoneState(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE, ref_idx);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.True2InsertPrvZoneState(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE, ref_idx);
            }
        }

        public void True2ZoneState_Log(Decimal ref_idx, int veh_id, int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.True2InsertPrvZoneState_Log(veh_id, zone_id, (int)EVT_TYPE.IN_ZONE, ref_idx);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.True2InsertPrvZoneState_Log(veh_id, zone_id, (int)EVT_TYPE.OUT_ZONE, ref_idx);
            }
        }

        public void True2InsertMsgZoneTypeGroup(int id_insert, int group_id, int type_id)
        {
            gSql.True2InsertMsgZoneTypeGroup(id_insert, group_id, type_id);
        }

        public void True2InsertMsgZoneTypeGroup_Log(int id_insert, int group_id, int type_id)
        {
            gSql2.True2InsertMsgZoneTypeGroup_Log(id_insert, group_id, type_id);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void InitObj(float lat_min, float lat_max, float lon_min, float lon_max, Boolean clkdir, DataTable wpTable)
        {
            CreateWayPoints(wpTable);
            if (lat_min <= 0 || lat_max <= 0 || lon_min <= 0 || lon_max <= 0)
            {
                MinMax(wayPoints, ref minX, ref maxX, ref minY, ref maxY);
                gClkWise = ClockWise();
            }
            else
            {
                minX = lon_min;
                maxX = lon_max;
                minY = lat_min;
                maxY = lat_max;
                //gClkWise = !clkdir;
                gClkWise = ClockWise();
            }

            gSide = SIDE_TYPE.OUT;
        }
        private void CalculateFencing(float wpX, float wpY)
        {
            mX = wpX;
            mY = wpY;

            float minX = wpX;
            float maxX = wpX;
            int iMinX = 0;
            int iMaxX = 0;

            for (int i = 0; i < wayPoints.Count - 1; i++)
            {
                minX = Math.Min(minX, wayPoints[i].X);
                if (minX == wayPoints[i].X)
                {
                    iMinX = i;
                }
                maxX = Math.Max(maxX, wayPoints[i].X);
                if (maxX == wayPoints[i].X)
                {
                    iMaxX = i;
                }
            }

            int mxOffset = 0; //gClkWise ? iMaxX : iMinX;

            if (!((minX <= mX && mX <= maxX) && (minY <= mY && mY <= maxY)))
            {
                gSide = SIDE_TYPE.OUT;
            }
            else
            {
                for (int i = 0; i < wayPoints.Count - 1; i++)
                {
                    iVector v1 = new iVector();
                    v1.p1 = wayPoints[mxOffset + i];
                    v1.p2 = wayPoints[mxOffset + i + 1];

                    if (v1.p1 == v1.p2)
                        continue;

                    iVector v2 = new iVector();
                    v2.p1 = wayPoints[mxOffset + i];
                    v2.p2.X = (float)mX;
                    v2.p2.Y = (float)mY;

                    iVector v3 = new iVector();
                    v3.p1 = wayPoints[mxOffset + i];
                    v3.p2 = wayPoints[mxOffset + i + 2 >= wayPoints.Count ? 0 : mxOffset + i + 2];

                    double z1 = GetZetaByTan(v1, v2);
                    SetInOutStatus(z1);

                    if (i == 0)
                    {
                        iVector v4 = new iVector();
                        v4.p1 = wayPoints[mxOffset + i];
                        v4.p2 = wayPoints[mxOffset + i - 1 < 0 ? wayPoints.Count - 2 : mxOffset + i - 2];

                        double z3 = GetZetaByTan(v1, v4);
                        if ((z1 * z3) > 0)
                        {
                            iVector v5 = new iVector();
                            v5.p1 = wayPoints[mxOffset + i - 1 < 0 ? wayPoints.Count - 2 : mxOffset + i - 2];
                            v5.p2 = wayPoints[mxOffset + i];

                            iVector v6 = new iVector();
                            v6.p1 = wayPoints[mxOffset + i - 1 < 0 ? wayPoints.Count - 2 : mxOffset + i - 2];
                            v6.p2.X = (float)mX;
                            v6.p2.Y = (float)mY;

                            double z4 = GetZetaByTan(v5, v6);
                            SetInOutStatus(z4);
                        }
                    }

                    double z2 = GetZetaByTan(v1, v3);
                    if ((z1 * z2) <= 0)
                    {
                        Boolean bInts = false;
                        Boolean bWrap = false;
                        int j = mxOffset + i + 1;

                        for (int c = 0; c < wayPoints.Count - 1; c++)
                        {
                            iVector vt = new iVector();
                            vt.p1 = wayPoints[j];
                            vt.p2 = wayPoints[(j + 1) < wayPoints.Count ? (j + 1) : 0];

                            bInts = Intersect(v2, vt);
                            if (bInts)
                                break;

                            j++;
                            if (j >= wayPoints.Count)
                            {
                                j = 0;
                                bWrap = true;
                            }
                        }

                        if (!bInts || (bInts && bWrap))
                            break;
                    }
                }
            }
        }
        private void CreateWayPoints(DataTable wpTable)
        {
            wayPoints.Clear();

            foreach (DataRow r in wpTable.Rows)
            {
                PointF p = new PointF();
                p.X = float.Parse(r["X"].ToString());
                p.Y = float.Parse(r["Y"].ToString());
                wayPoints.Add(p);
            }

            #region to create way points
            /*                
                Point[]  wayPoints = new Point[]
                {
                    #region 1
                    //new Point((int)oX+00,(int)oY+0)   ,
                    //new Point((int)oX+00,(int)oY+20)  ,
                    //new Point((int)oX+60,(int)oY+20)  ,  
                    //new Point((int)oX+60,(int)oY+60)  ,  

                    //new Point((int)oX+20,(int)oY+60)  ,  

                    //new Point((int)oX+10,(int)oY+100) ,       
                    //new Point((int)oX+120,(int)oY+90) ,       

                    //new Point((int)oX+100,(int)oY+60) ,       
                    //new Point((int)oX+80,(int)oY+60)  ,        
                    //new Point((int)oX+80,(int)oY+20)  ,   
                    //new Point((int)oX+120,(int)oY+20) ,   

                    //new Point((int)oX+120,(int)oY-20) ,   
                    //new Point((int)oX+100,(int)oY+0)  ,
                    //new Point((int)oX+00,(int)oY+0)   
                    #endregion

                    #region 2
                    //new Point((int)oX+00,(int)oY+0)   ,
                    //new Point((int)oX+00,(int)oY+20)  ,
                    //new Point((int)oX+10,(int)oY+100) ,       
                    //new Point((int)oX+120,(int)oY+90) ,       
                    //new Point((int)oX+100,(int)oY+60) ,       
                    //new Point((int)oX+80,(int)oY+60)  ,        
                    //new Point((int)oX+100,(int)oY+0)  ,
                    //new Point((int)oX+00,(int)oY+0)
                    #endregion

                    #region 3
                    //new Point(oX-00,oY-0)   ,
                    //new Point(oX-00,oY-20)  ,
                    //new Point(oX-60,oY-20)  ,  
                    //new Point(oX-60,oY-60)  ,  

                    //new Point(oX-20,oY-60)  ,  

                    //new Point(oX-10,oY-100) ,       
                    //new Point(oX-120,oY-90) ,       

                    //new Point(oX-100,oY-60) ,       
                    //new Point(oX-80,oY-60)  ,        
                    //new Point(oX-80,oY-20)  ,   
                    //new Point(oX-120,oY-20) ,   

                    //new Point(oX-120,oY-20) ,   
                    //new Point(oX-100,oY-0)  ,
                    //new Point(oX-00,oY-0)
                    #endregion

                    #region 4
                    new Point(oX-0,oY+0)   ,
                    new Point(oX-0,oY-100) ,
                    new Point(oX+100,oY-100) ,
                    new Point(oX+100,oY-0) ,
                    new Point(oX-0,oY+0)   
                    #endregion
                };                           

                DataTable dt = new DataTable("WayPoints");

                dt.Columns.Add("X", typeof(float));
                dt.Columns.Add("Y", typeof(float));
                foreach (PointF p in wayPoints)
                {
                    dt.Rows.Add(new object[] { p.X, p.Y });
                }
                dt.WriteXml("wayPoints4.xml");                                     
                */
            #endregion
        }
        private void SetInOutStatus(double z)
        {
            if ((!gClkWise && z >= 0) || (gClkWise && z <= 0))
            {
                gSide = SIDE_TYPE.IN;
            }
            else
            {
                gSide = SIDE_TYPE.OUT;
            }
        }
        private Boolean ClockWise()
        {
            int c = 0;
            double zeta = 0.0;

            for (int i = 0; i < wayPoints.Count - 2; i++, c++)
            {
                iVector v1 = new iVector();
                iVector v2 = new iVector();

                v1.p1 = wayPoints[i];
                v1.p2 = wayPoints[i + 1];

                if (v1.p1 == v1.p2)
                    continue;

                v2.p1 = wayPoints[i + 1];
                v2.p2 = wayPoints[i + 2 >= wayPoints.Count - 1 ? 0 : i + 2];

                double z = GetZetaByTan(v1, v2);
                zeta += z;
            }

            if (zeta < 0)
                return true;
            return false;
        }
        private void MinMax(List<PointF> v, ref double minX, ref double maxX, ref double minY, ref double maxY)
        {
            for (int i = 0; i < v.Count; i++)
            {
                minX = Math.Min(v[i].X, minX);
            }
            for (int i = 0; i < v.Count; i++)
            {
                minY = Math.Min(v[i].Y, minY);
            }
            for (int i = 0; i < v.Count; i++)
            {
                maxX = Math.Max(v[i].X, maxX);
            }
            for (int i = 0; i < v.Count; i++)
            {
                maxY = Math.Max(v[i].Y, maxY);
            }
        }
        private double Magnitude(iVector v)
        {
            float x = v.p2.X - v.p1.X;
            float y = v.p2.Y - v.p1.Y;
            double r = Math.Sqrt((x * x) + (y * y));

            return r;
        }
        private double Project(iVector v1, iVector v2)
        {
            float x1 = v1.p2.X - v1.p1.X;
            float y1 = v1.p2.Y - v1.p1.Y;
            double r1 = Math.Sqrt((x1 * x1) + (y1 * y1));

            float x2 = v2.p2.X - v2.p1.X;
            float y2 = v2.p2.Y - v2.p1.Y;
            double r2 = Math.Sqrt((x2 * x2) + (y2 * y2));

            double ux = (x1 / r1);
            double uy = (y1 / r1);

            double p1 = (x2 * ux);
            double p2 = (y2 * uy);

            double p3 = p1 + p2;
            return p3;
        }
        private Boolean Intersect(iVector v1, iVector v2)
        {
            iVector p1 = new iVector();
            iVector p2 = new iVector();

            /// Point 1st ///////////////////////////////////

            p1.p1 = v1.p1;
            p1.p2 = v2.p1;

            p2.p1 = v1.p1;
            p2.p2 = v2.p2;

            double z1 = GetZetaByTan(v1, p1);
            double z2 = GetZetaByTan(v1, p2);

            /// Point 2nd ///////////////////////////////////

            p1.p1 = v1.p2;
            p1.p2 = v2.p1;

            p2.p1 = v1.p2;
            p2.p2 = v2.p2;

            double z3 = GetZetaByTan(v1, p1);
            double z4 = GetZetaByTan(v1, p2);

            /// Point 3th ///////////////////////////////////

            p1.p1 = v2.p1;
            p1.p2 = v1.p1;

            p2.p1 = v2.p1;
            p2.p2 = v1.p2;

            double z5 = GetZetaByTan(v2, p1);
            double z6 = GetZetaByTan(v2, p2);

            /// Point 4th //////////////////////////////////

            p1.p1 = v2.p2;
            p1.p2 = v1.p1;

            p2.p1 = v2.p2;
            p2.p2 = v1.p2;

            double z7 = GetZetaByTan(v2, p1);
            double z8 = GetZetaByTan(v2, p2);

            if ((z1 * z2) < 0 && (z3 * z4) < 0 && (z5 * z6) < 0 && (z7 * z8) < 0)
            {
                return true;
            }
            return false;
        }

        private double GetZetaByTan(iVector v1, iVector v2)
        {
            double r = GetTanZeta(v1, v2);
            return r;
        }
        private double GetTanZeta(iVector v1, iVector v2)
        {
            PointF p1 = new PointF();
            p1.X = (v1.p2.X - v1.p1.X);
            p1.Y = (v1.p2.Y - v1.p1.Y);

            PointF p2 = new PointF();
            p2.X = (v2.p2.X - v2.p1.X);
            p2.Y = (v2.p2.Y - v2.p1.Y);

            double dd1 = Math.Atan2(p1.Y, p1.X);
            dd1 = (180 / Math.PI) * dd1;

            double dd2 = Math.Atan2(p2.Y, p2.X);
            dd2 = (180 / Math.PI) * dd2;

            double dd = dd1 - dd2;

            if (dd <= -180 && dd > -360)
            {
                dd = 360 + dd;
            }
            else if (dd > 180 && dd < 360)
            {
                dd = -(360 - dd);
            }
            return dd;
        }
        private double GetCosZeta(iVector v1, iVector v2)
        {
            PointF p1 = new PointF();
            p1.X = (v1.p2.X - v1.p1.X);
            p1.Y = (v1.p2.Y - v1.p1.Y);

            PointF p2 = new PointF();
            p2.X = (v2.p2.X - v2.p1.X);
            p2.Y = (v2.p2.Y - v2.p1.Y);

            double a = p1.X * p2.X;
            double b = p1.Y * p2.Y;
            double c = a + b;

            double ra = Math.Sqrt((p1.X * p1.X) + (p1.Y * p1.Y));
            double rb = Math.Sqrt((p2.X * p2.X) + (p2.Y * p2.Y));

            double cosZeta = c / (ra * rb);
            cosZeta = Math.PI - Math.Acos(cosZeta);
            if (p1.Y < 0 || p2.Y < 0)
            {
                cosZeta = -cosZeta;
            }
            return cosZeta;
        }

        private void TestInterSection()
        {
            iVector e1 = new iVector();
            iVector e2 = new iVector();

            e1.p1.X = 20;
            e1.p1.Y = 100;
            e1.p2.X = 200;
            e1.p2.Y = 100;

            int mmy = -50;
            int mmx = 49;
            e2.p1.X = mmx + 100;
            e2.p1.Y = mmy + 100;
            e2.p2.X = mmx + 200;
            e2.p2.Y = mmy + 200;

            Boolean b = Intersect(e1, e2);

            //e.Graphics.DrawLine(new Pen(Color.Black, 1), e1.p1.X, e1.p1.Y, e1.p2.X, e1.p2.Y);
            //e.Graphics.DrawLine(new Pen(Color.Black, 1), e2.p1.X, e2.p1.Y, e2.p2.X, e2.p2.Y);
        }
        public bool TestPointInsideArea(double X, double Y, DataTable ringArr)
        {
            DataTable myPts = ringArr;

            if (myPts.Rows.Count <= 0)
            {
                return false;
            }

            int sides = myPts.Rows.Count - 1;
            int j = sides - 1;
            bool status = false;

            try
            {
                for (int i = 0; i < sides; i++)
                {
                    double pntYi = (double)Convert.ToDouble(myPts.Rows[i]["Y"].ToString());
                    double pntYj = (double)Convert.ToDouble(myPts.Rows[j]["Y"].ToString());
                    double pntXi = (double)Convert.ToDouble(myPts.Rows[i]["X"].ToString());
                    double pntXj = (double)Convert.ToDouble(myPts.Rows[j]["X"].ToString());

                    if (pntYi < Y && pntYj >= Y || pntYj < Y && pntYi >= Y)
                    {
                        if (pntXi + (Y - pntYi) / (pntYj - pntYi) * (pntXj - pntXi) < X)
                        {
                            status = !status;
                        }
                    }
                    j = i;
                }
            }
            catch (Exception e)
            { }
            return status;
        }
        private bool PointInPolygon(double X, double Y, DataTable ringArr)
        {
            PointF p = new PointF((float)X, (float)Y);

            List<PointF> poly = new List<PointF>();
            foreach (DataRow r in ringArr.Rows)
            {
                PointF pt = new PointF((float)r["X"], (float)r["Y"]);
                poly.Add(pt);
            }

            PointF p1, p2;

            bool inside = false;

            if (poly.Count < 3)
            {
                return inside;
            }

            PointF oldPoint = new PointF(
                poly[poly.Count - 1].X, poly[poly.Count - 1].Y);

            for (int i = 0; i < poly.Count; i++)
            {
                PointF newPoint = new PointF((float)poly[i].X, (float)poly[i].Y);
                if (newPoint.X > oldPoint.X)
                {
                    p1 = oldPoint;
                    p2 = newPoint;
                }
                else
                {
                    p1 = newPoint;
                    p2 = oldPoint;
                }

                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                    && ((float)p.Y - (float)p1.Y) * (float)(p2.X - p1.X)
                     < ((float)p2.Y - (float)p1.Y) * (float)(p.X - p1.X))
                {
                    inside = !inside;
                }
                oldPoint = newPoint;
            }
            return inside;
        }


        public bool IsFeatureExisted(int veh_id)
        {
            if (gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_OUT_ZONE) < 1 &&
                gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_IN_ZONE) < 1)
            {
                return false;
            }
            return true;
        }

        public void KPICalculatorZoneByVeh(int idx, int veh_id, int zone_id)
        {
            DataTable dtKPI = gSql.KPIGetZoneByVeh(veh_id, zone_id);

            if (dtKPI.Rows.Count > 0)
            {
                int kpi_type = -1;
                if (gSide == SIDE_TYPE.IN)
                {
                    kpi_type = 20;
                    DataRow[] rowIn = dtKPI.Select("zone_end = " + zone_id);

                    for (int i = 0; i < rowIn.Length; i++)
                    {
                        int zone_start = (int)rowIn[i]["zone_start"];
                        DataTable dtLastZone = gSql.KPIGetLastZone(veh_id, zone_start, (int)EVT_TYPE.OUT_ZONE);
                        if (dtLastZone.Rows.Count > 0)
                        {
                            gSql.KPIInsertLogZone(idx, veh_id, "A", kpi_type, (int)dtLastZone.Rows[0]["idx"], (DateTime)dtLastZone.Rows[0]["local_timestamp"]);
                        }
                    }
                }
                else if (gSide == SIDE_TYPE.OUT)
                {
                    kpi_type = 10;
                    DataRow[] rowOut = dtKPI.Select("zone_start = " + zone_id);

                    if (rowOut.Length > 0)
                    {
                        DateTime dt = new DateTime(2000,01,01);
                        gSql.KPIInsertLogZone(idx, veh_id, "A", kpi_type, -1, dt);
                    }
                }
            }
        }

    }
}
