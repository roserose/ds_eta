using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;

namespace ZebraDataServer
{
    class MagneticCard
    {
        SQL gSql;
        SQL2 gSql2;
        public MagneticCard(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public void InsertNewRecord(int ref_idx, int evt_type)
        {
            gSql.InsertMsgEvt(ref_idx, evt_type);
        }

        public void InsertNewRecord_Log(decimal ref_idx ,int veh_id, DateTime timestamp, int evt_type,int remark, int evt_back_side)
        {
            gSql2.InsertMsgEvt(ref_idx,veh_id,timestamp, evt_type, remark, evt_back_side);
        }

        public void InsertDriverID(int veh_id, string Driver_ID, string License)
        {
            gSql2.InsertID_DLTESRI(veh_id, Driver_ID, License);
        }

        public string GetDriverID(int veh_id)
        {
            string dt_driver = gSql2.GetDriverID(veh_id);            
            return dt_driver;
        }

        public void UpdateIDMeitrack(string ID, int vid)
        {
            gSql2.updateDriverID(ID, vid);
        }
        public bool GetAccessorieType(int veh_id )
        {
            return gSql2.GetAccessorieByVid(veh_id);
        }
        public bool GetVehicleMagneticType(int veh_id)
        {
            return gSql.GetVehicleMagneticType(veh_id);
        }

        public DataTable GetVehicleMagneticParam(int veh_id)
        {
            return gSql.GetVehicleMagneticParam(veh_id);
        }
        public DataTable GetVehicleMagneticParam_new(int veh_id)
        {
            return gSql.GetVehicleMagneticParam_new(veh_id);
        }

        public bool CheckLastNocard(int veh_id, out DateTime last_rec)
        {
            last_rec = new DateTime();
            bool isFind = false;
            try
            {
                DataTable dtRet = gSql.GetVehicleMagneticLastNoCard(veh_id);
                if (dtRet.Rows.Count == 0){ return true; }

                foreach (DataRow dr in dtRet.Rows)
                {
                    int evt_id = (int)dr["evt_id"];

                    if (evt_id == (int)EVT_TYPE.ENGINE_ON)
                    {
                        last_rec = (DateTime)dr["local_timestamp"];
                    }
                    else if (evt_id == (int)EVT_TYPE.VALID_CARD || evt_id ==  (int)EVT_TYPE.NO_CARD || evt_id == (int)EVT_TYPE.ENGINE_OFF)  
                    {
                        isFind = true;
                    }
                }
            }
            catch { }
            return isFind;
        }
        public bool CheckLastNocard_Log(int veh_id, out DateTime last_rec)
        {
            last_rec = new DateTime();
            bool isFind = false;
            try
            {
                DataTable dtRet = gSql2.GetVehicleMagneticLastNoCard(veh_id);
                if (dtRet.Rows.Count == 0) { return true; }

                foreach (DataRow dr in dtRet.Rows)
                {
                    int evt_id = (int)dr["evt_id"];

                    if (evt_id == (int)EVT_TYPE.ENGINE_ON)
                    {
                        last_rec = (DateTime)dr["local_timestamp"];
                    }
                    else if (evt_id == (int)EVT_TYPE.VALID_CARD || evt_id == (int)EVT_TYPE.NO_CARD || evt_id == (int)EVT_TYPE.ENGINE_OFF)
                    {
                        isFind = true;
                    }
                }
            }
            catch { }
            return isFind;
        }
       

        public DataTable GetMagnetic(int veh_id)
        {
            return gSql2.GetMagnetic(veh_id);
        }

       
    }
}
