using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ZebraDataServer
{
    class SmartCard
    { 
        SQL gSql;
        SQL2 gSql2;
        public SmartCard(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public void InsertNewRecord(int veh_id, DateTime timestamp, int ref_idx, int iAddress1, string sAddress2, string sData, string sTag)
        {
            gSql.InsertSmartCardEvent(veh_id, timestamp, ref_idx, iAddress1, sAddress2, sData, sTag);
        }

        public void InsertNewRecord_Log(int veh_id, DateTime timestamp, decimal ref_idx, int iAddress1, string sAddress2, string sData, string sTag)
        {
            gSql2.InsertSmartCardEvent(veh_id, timestamp, ref_idx, iAddress1, sAddress2, sData, sTag);
        }
    }
}
