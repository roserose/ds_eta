using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class DistanceCls
    {
        SQL gSql;
        SQL2 gSql2;
        public DistanceCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public bool CalDistance(int veh_id, float lat, float lon, out double distance)
        {
            bool is_chang = false;
            distance = 0.0;

            try
            {
                /***********SQL1*************/
                //DataTable dtData = gSql.GetCurLoc(veh_id);

                /***********SQL2*************/
                DataTable dtData = gSql2.GetCurLoc(veh_id);

                if (dtData.Rows.Count > 0)
                {
                    double tmp_lat = (double)dtData.Rows[0]["lat"];
                    double tmp_lon = (double)dtData.Rows[0]["lon"];

                    distance = Convert.ToDouble((decimal)dtData.Rows[0]["distance"]);

                    double tmp_distance = 0.0;
                    double browsing = 0.0;
                    bool is_cal = false;

                    double lat_ = lat;
                    double lon_ = lon;

                    if (!(tmp_lon == 0.00 && tmp_lon == 0.00))
                    {
                        browsing = GetDistance(tmp_lat, tmp_lon, lat_, lon_);

                        //Browsing Fillter
                        if (browsing < 100)
                        {
                            tmp_distance += browsing;
                            is_cal = true;
                        }
                    }

                    if (is_cal)
                    {
                        distance += tmp_distance;
                        if (tmp_distance > 0.0)
                        {
                            is_chang = true;
                        }
                    }
                }
            }
            catch
            {
            }

            return is_chang;
        }

        private double GetDistance(double lat1, double lon1, double lat2, double lon2)
        {
            const double rad = 57.2958;
            const double R = 6371.0;
            double distance = 0.00;

            if (lat1 == lat2 && lon1 == lon2)
            {
                return distance;
            }

            //--- M.
            //distance = Math.Acos((Math.Sin(lat1 / rad) * Math.Sin((lat2) / rad)) +
            //           (Math.Cos(lat1 / rad) * Math.Cos((lat2) / rad) * Math.Cos(((lon2) / rad) - (lon1 / rad)))
            //           ) * R * 1000.0;

            //--- KM.
            distance = Math.Acos((Math.Sin(lat1 / rad) * Math.Sin((lat2) / rad)) +
                       (Math.Cos(lat1 / rad) * Math.Cos((lat2) / rad) * Math.Cos(((lon2) / rad) - (lon1 / rad)))
                       ) * R;

            return distance;
        }

        public double GetDistance_Over(int veh_id, DateTime timestart, DateTime timestop)
        {
            double distance_over = 0.00;
            try
            {
                distance_over = gSql2.Getdistance(veh_id, timestart, timestop);
            }
            catch
            { }
            return distance_over;
        }

        public double GetDistance_Now(int veh_id)
        {
            double distance_Now = 0.00;
            try
            {
                /***********SQL2*************/
                DataTable dtData = gSql2.GetCurLoc(veh_id);

                if (dtData.Rows.Count > 0)
                {
                    distance_Now = Convert.ToDouble((decimal)dtData.Rows[0]["distance"]);
                }
            }
            catch
            { }
            return distance_Now;
        }

        public DataTable GetLastEvent(int veh_id)
        {
            DataTable dtLastEvent = new DataTable();
            dtLastEvent = gSql2.GetLastEvent(veh_id);
            return dtLastEvent;
        }

        public DataTable GetFleetDistance_Over(int veh_id)
        {
            DataTable dtFleetDistanceOver = new DataTable();
            dtFleetDistanceOver = gSql.GetFleetDistanceOver(veh_id);
            return dtFleetDistanceOver;
        }

        private DataTable GetLogLocation(int veh_id)
        {
            DataTable dt = new DataTable();
            return dt;
        }

        public void InsertNewRecord(decimal ref_idx, int veh_id, DateTime timestamp)
        {
            gSql2.InsertMsgEvt(ref_idx, veh_id, timestamp, (int)EVT_TYPE.DISTANCE_OVER, -1, -1);
        }
    }
}
