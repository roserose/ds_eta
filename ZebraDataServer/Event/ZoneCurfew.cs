using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    class ZoneCurfewCls : FencingCls
    {
        SQL gSql;
        SQL2 gSql2;
        public ZoneCurfewCls(String dbServer): base(dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }
        
        public DataTable GetFleetZoneCurfewByVeh(int veh_id)
        {
            return gSql.GetFleetZoneCurfewByVeh(veh_id);
        }
        public DataTable GetFleetZoneCurfewByVeh_new(int veh_id)
        {
            return gSql.GetFleetZoneCurfewByVeh_new(veh_id);
        }
        //public DataTable GetFleetZoneCurfewByVeh_new(int veh_id)
        //{
        //    return gSql.GetFleetZoneCurfewByVeh_new(veh_id);
        //}
        public void GetLastZoneCurfewStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark)
        {
            gSql.GetLastZoneCurfewStatus(veh_id, last_zone_id, out evt_id, out zone_id, out local_timestamp, out mark);
        }
        public void GetLastZoneCurfewStatus_Log(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark)
        {
            gSql2.GetLastZoneCurfewStatus(veh_id, last_zone_id, out evt_id, out zone_id, out local_timestamp, out mark);
        }

        public void InsertMsgZoneStatus(int veh_id, int ref_idx, int zone_id, int mark)
        {
            if (GSide == SIDE_TYPE.IN) {
                gSql.InsertMsgZoneCurfew(veh_id, ref_idx, zone_id, (int)EVT_TYPE.IN_ZONE, mark);
            }
            else if (GSide == SIDE_TYPE.OUT) {
                gSql.InsertMsgZoneCurfew(veh_id, ref_idx, zone_id, (int)EVT_TYPE.OUT_ZONE, mark);
            }
        }
        public void InsertMsgZoneStatusLog(int veh_id, Decimal ref_idx, int zone_id, int mark)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.InsertMsgZoneCurfew(veh_id, ref_idx, zone_id, (int)EVT_TYPE.IN_ZONE, mark);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.InsertMsgZoneCurfew(veh_id, ref_idx, zone_id, (int)EVT_TYPE.OUT_ZONE, mark);
            }
        }

        public void InsertNewRecordEvtCurfew(int ref_idx)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.NO_INZONE_CURFEW);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.NO_OUTZONE_CURFEW);
            }
        }

        public void InsertNewRecordCurfewLog(decimal ref_idx, int veh_id, DateTime timestamp,int zone_id)
        {
            if (GSide == SIDE_TYPE.IN)
            {
                gSql2.InsertMsgEvt(ref_idx,veh_id, timestamp, (int)EVT_TYPE.NO_OUTZONE_CURFEW, zone_id, -1);
            }
            else if (GSide == SIDE_TYPE.OUT)
            {
                gSql2.InsertMsgEvt(ref_idx,veh_id, timestamp, (int)EVT_TYPE.NO_INZONE_CURFEW, zone_id, -1);
            }
        }

        public bool IsFeatureExisted(int veh_id)
        {
            if (gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_NO_INZONE_CURFEW)  < 1 &&
                gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_NO_OUTZONE_CURFEW) < 1)
            {
                return false;
            }
            return true;
        }
    }
}
