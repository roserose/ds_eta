using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;
using System.Data;

namespace ZebraDataServer
{
    class UpdateLog
    {
        SQL gSql;
        SQL2 gSql2;

        public UpdateLog(String dbServer) 
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public DataTable GetLastLatLon(int idx,int veh_id)
        {
            return gSql.GetMsgLatLon( idx, veh_id);
        }
        public DataTable GetLastLatLonLog(decimal idx, int veh_id)
        {
            return gSql2.GetMsgLatLon(idx, veh_id);
        }

        public void UpdateMsg_Error(int idx, int last_idx, double last_lat, double last_lon, int no_of_Satellite)
        {
            gSql.UpdateMsg_Error(idx, last_idx, last_lat,  last_lon,  no_of_Satellite);
        }

        public void UpdateMsg_ErrorLog(decimal idx, decimal last_idx, double last_lat, double last_lon, int no_of_Satellite)
        {
            gSql2.UpdateMsg_ErrorLog(idx, last_idx, last_lat, last_lon, no_of_Satellite);
        }
    }
}
