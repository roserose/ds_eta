using System;
using System.Collections.Generic;
using System.Text;
using ZebraCommonInterface;

namespace ZebraDataServer
{    
    class SpeedingCls
    {
        SQL gSql;
        SQL2 gSql2;
        public SpeedingCls(String dbServer)
        {
            gSql = new SQL(dbServer);
            gSql2 = new SQL2(dbServer);
        }

        public Boolean DetectSpeedViolate(int veh_id, int speed)
        {
            if (gSql.GetFeatureExistence(veh_id, (int)PROFILEMENUID.EVENT_SPEEDING) < 1) { return false ; }
            if (gSql.GetVehSpeedLimit(veh_id) < speed)
            {
                return true;
            }
            return false;
        }
        public Boolean DetectSpeedViolate_Log(int veh_id, int speed)
        {
            if (gSql2.GetVehSpeedLimit(veh_id) < speed)
            {
                return true;
            }
            return false;
        }
        public void InsertNewRecord(int ref_idx)
        {
            gSql.InsertMsgEvt(ref_idx, (int)EVT_TYPE.SPEEDING);
        }
        public void InsertNewRecord(decimal ref_idx ,int veh_id, DateTime timestamp)
        {
            gSql2.InsertMsgEvt(ref_idx,veh_id,timestamp ,(int)EVT_TYPE.SPEEDING, -1,-1);
        }
    }
}
