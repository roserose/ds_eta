using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Data;
using ZebraDataInterface;
using System.Collections;
using ZebraCommonInterface;
using System.Threading;
using System.IO;

namespace ZebraDataServer
{
    public struct ExEventCompleteStruc
    {
        public double speeding_elapsed_time_maximum;
        public double idle_elapsed_time_maximum;
        public double fencing_elapsed_time_maximum;
        public double fencing_elapsed_time_maximum_curfew;
        public double zone_in_curfew_elapsed_time_maximum;
        public double routing_elapsed_time_maximum;
        public double location_elapsed_time_maximum;
        public double eta1_elapsed_time_maximum;
        public double eta2_elapsed_time_maximum;
        public double io_status_elapsed_time_maximum;
        public double engine_on_elapsed_time_maximum;
        public double speed_hazard_elapsed_time_maximum;
        public double angle_hazard_elapsed_time_maximum;
        public double temperature_elapsed_time_maximum;
        public double engine_onoff_inout_zone_elapsed_time_maximun;
        public double true_fencing_elapsed_time_maximum;
        public double true_io_elapsed_time_maximum_;
        public double DFM_io_status_elapsed_time_maximum;
        public double zone_in_curfew_with_speed_elapsed_time_maximum;
        public double magnetic_elapsed_time_maximum;

        public double genaral_elapsed_time_maximum;
        public double logerror_elapsed_time_maximum;
        public double rpm_elapsed_time_maximum;
        public double smart_card_elapsed_time_maximum;
        public double card_nocard_elapsed_time_maximum;
        public double Commit_event_elapsed_time_maximum;

        //public double Distance_Over_elapsed_time_maximum;
    }

    public delegate void ProcessCompleted(Object sender, ExEventCompleteStruc msg);
    //public delegate void ProcessCompleted(Object sender, ExEventCompleteStruc msg, String vid);
    public delegate void ElapedTimeProcess(string zMsg);

    public class ExEvent : MarshalByRefObject, DataInterface
    {
        #region --- local variables ---------------------------------------------------------------
        decimal g_log_index;
        int g_veh_id;
      
        String g_dtTimestampe;
        float g_lat;
        float g_lon;
        int g_speed;
        int g_course;
        int g_type_of_fix;
        int g_no_of_satellite;
        int g_in_out_status;
        int g_analog_level;
        string g_type_of_msg;
        String g_tag_msg;
        bool g_df_vehicle;

        DateTime g_TimeStamp;
        double g_internal_power;
        double g_external_power;
        #endregion

        bool IsEnableBroadcastQ = true;
        bool IsEnableBroadcastQTRUE = true;
        CommonQeue SMSQ;
        CommonQeue EMAILQ;
        Hashtable hzTrueZone;

        int a = 0;

        private int ElapedTime = 100;

        #region --- declare objects ---------------------------------------------------------------
        SpeedingCls oSpeed;
        IdlingCls oIdling;
        FencingCls oFencing;
        ZoneCurfewCls oZoneCurfew;
        RoutingCls oRouting;
        LocationCls oLoc;
        IOStatusCls oIOStatus;
        ETACls oETACls1;
        ETACls oETACls2;
        EngineOnEventCls oEngineOnEvt;
        TemperatureEvtCls oTemperatureEvt;
        SpeedHazardCls oSpeedHazard;
        AngleHazardCls oAngleHazard;
        TrueZoneAlertCls oTrueZone;
        TrueIOAlertCls oTrueIO;
        TrueZoneAlertCls2 oTrueZone2;
        DFMIOStatusCls oDMFIO;
        ZoneCurfewWithSpeedCls oZoneCurfewWithSpeed;
        EngineOnOffInOutZoneCls oEngineOnOffInOutZoneObj;
        UpdateLog oUpdateLog;
        MagneticCard oMagneTic;
        SmartCard oSmartCard;
        GeneralCls oGeneral;
        RPMCls oRPM;
        DistanceCls oDistance_Over;
        #endregion

        public event ProcessCompleted OnProcessCompleted;
        public event ElapedTimeProcess OnProcessTime;

        public ExEvent()
        {
            Properties.Settings pf = new ZebraDataServer.Properties.Settings();
            IsEnableBroadcastQ = pf.EnableBroadcastQ;
            IsEnableBroadcastQTRUE = pf.EnableBroadcastQ_TRUE;
            SMSQ = new CommonQeue(pf.Broadcast_SMS_Q);
            EMAILQ = new CommonQeue(pf.Broadcast_EMAIL_Q);

            oSpeed = new SpeedingCls(pf.DbSever);
            oIdling = new IdlingCls(pf.DbSever);
            oFencing = new FencingCls(pf.DbSever);
            oZoneCurfew = new ZoneCurfewCls(pf.DbSever);
            oRouting = new RoutingCls(pf.DbSever);
            oLoc = new LocationCls(pf.DbSever);
            oIOStatus = new IOStatusCls(pf.DbSever);
            oETACls1 = new ETACls(pf.DbSever);
            oETACls2 = new ETACls(pf.DbSever);
            oEngineOnEvt = new EngineOnEventCls(pf.DbSever);
            oTemperatureEvt = new TemperatureEvtCls(pf.DbSever);
            oSpeedHazard = new SpeedHazardCls(pf.DbSever);
            oAngleHazard = new AngleHazardCls(pf.DbSever);

            oTrueZone = new TrueZoneAlertCls(pf.DbSever);
            oTrueZone2 = new TrueZoneAlertCls2(pf.DbSever);
            oTrueIO = new TrueIOAlertCls(pf.DbSever);
            oDMFIO = new DFMIOStatusCls(pf.DbSever);
           
            oEngineOnOffInOutZoneObj = new EngineOnOffInOutZoneCls(pf.DbSever);

            oZoneCurfewWithSpeed = new ZoneCurfewWithSpeedCls(pf.DbSever);
            oUpdateLog = new UpdateLog(pf.DbSever);
            oMagneTic = new MagneticCard(pf.DbSever);

            oSmartCard = new SmartCard(pf.DbSever);
            oGeneral = new GeneralCls(pf.DbSever);
            oRPM = new RPMCls(pf.DbSever);
            //Cloen Hs
            hzTrueZone = (Hashtable)GlobalClass.hsZoneTrueList.Clone();

            //New Event DUMEX
            oDistance_Over = new DistanceCls(pf.DbSever);
        }

        public void DetectEvent(decimal log_index, int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg, string tag_msg, DateTime time_stamp ,double internal_power, double external_power)
        {
            #region To initialize variables
            g_log_index = log_index;
            g_veh_id = veh_id;
            g_dtTimestampe = dtTimestampe;
            g_lat = lat;
            g_lon = lon;
            g_speed = speed;
            g_course = course;
            g_type_of_fix = type_of_fix;
            g_no_of_satellite = no_of_satellite;
            g_in_out_status = in_out_status;
            g_analog_level = analog_level;
            g_type_of_msg = type_of_msg;
            g_tag_msg = tag_msg;
            g_TimeStamp = time_stamp;

            DateTime dateTime1;
            DateTime dateTime2;
            TimeSpan totalElapedTime;

            DateTime dateTimeStart = DateTime.Now;
            DateTime dateTimeEnd = DateTime.Now;
            ExEventCompleteStruc retMsg = new ExEventCompleteStruc();
            #endregion

            #region --- Event

            try
            {  
                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                ProcessGenaralMsg();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.genaral_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                if (g_type_of_fix == 0 || g_no_of_satellite < 3)
                {
                    dateTime1 = DateTime.Now;
                    
                    //UpdateLogError(); 

                    //SQL2 OK OK*
                    UpdateLogErrorLog();
                    dateTime2 = DateTime.Now;
                    totalElapedTime = dateTime2 - dateTime1;
                    retMsg.logerror_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                }

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectIdling();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.idle_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectSpeeding();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.speeding_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectFencing();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.fencing_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectZoneCurfew();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.zone_in_curfew_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
             
                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectRouting();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.routing_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                GetNPutLocation();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.location_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;


               



                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectEngineOnEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.engine_on_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;


                //rose  ===== TEST ETA/// 
                /********NOT USE********/
                dateTime1 = DateTime.Now;
                DetectETA1();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.eta1_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                /*******NOT USE********/
                dateTime1 = DateTime.Now;
             //   DetectETA2();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.eta2_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
                //end rose =============


                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectSpeedHazard();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.speed_hazard_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectAngleHazard();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.angle_hazard_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK
                dateTime1 = DateTime.Now;
                DetectTemperatureEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.temperature_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK
                dateTime1 = DateTime.Now;
                DetectEngineOnOffInOutZone();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.engine_onoff_inout_zone_elapsed_time_maximun = totalElapedTime.TotalMilliseconds;

                dateTime1 = DateTime.Now;
                /*DetectFencingForTrueCorp();*/
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                //retMsg.true_fencing_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                dateTime1 = DateTime.Now;
                /*DetectTrueIOStatus();*/
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.true_io_elapsed_time_maximum_ = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK
                dateTime1 = DateTime.Now;
                DetectZoneCurfewWithSpeed();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.zone_in_curfew_with_speed_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectMagneticEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.magnetic_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //SQL2 OK OK*
                dateTime1 = DateTime.Now;
                DetectSmartCardEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.smart_card_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //NOT USE NO EVENT
                dateTime1 = DateTime.Now;
                DetectRPMEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.rpm_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                //Distance_Over
                //dateTime1 = DateTime.Now;
                //DetectDistance_Over();
                //dateTime2 = DateTime.Now;
                //totalElapedTime = dateTime2 - dateTime1;
                //retMsg.speeding_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                dateTime1 = DateTime.Now;
                DetectNoCardEvent();
                dateTime2 = DateTime.Now;
                totalElapedTime = dateTime2 - dateTime1;
                retMsg.card_nocard_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;

                dateTimeEnd = DateTime.Now;
                totalElapedTime = dateTimeEnd - dateTimeStart;
                retMsg.Commit_event_elapsed_time_maximum = totalElapedTime.TotalMilliseconds;
            }
            catch 
            {
            }
            #endregion
            OnProcessCompleted(this, retMsg);
         //   OnProcessCompleted(this, retMsg, g_veh_id.ToString());
        }
        public void updateDataInvalid(decimal log_index, int veh_id, String dtTimestampe, float lat, float lon)
        {
            #region To initialize variables
            g_log_index = log_index;
            g_veh_id = veh_id;
            g_dtTimestampe = dtTimestampe;
            g_lat = lat;
            g_lon = lon;
            #endregion

            GetNPutLocation();
        }
        private void ProcessGenaralMsg()
        {
            if (g_type_of_msg == "+" && g_tag_msg.Length > 0)
            {
                //KBox 
                if (g_tag_msg.IndexOf("FW") > -1) 
                {
                    oGeneral.InsertFwVersion(g_veh_id, g_log_index, g_tag_msg);
                }
            }

            //process Tag msg
            if (g_tag_msg.Length > 0)
            {
                // OD,0000008612,ER,507;
                // ER,507 
                if (g_tag_msg.IndexOf("OD") > -1 || g_tag_msg.IndexOf("ER") > -1)
                {
                    string[] tmp = g_tag_msg.Split(',');
                    for (int i = 0; i < tmp.Length; i++)
                    {
                        if (tmp[i] == "OD")
                        {
                            //odometer
                            double odometer = -1.0;
                            if (double.TryParse(tmp[(i + 1)], out odometer) && odometer > 0.0)
                            {
                                oGeneral.InsertOdometer(g_log_index, odometer);
                            }
                        }
                        else if (tmp[i] == "ER")
                        {
                            double rpm_vlaue = double.Parse(tmp[(i + 1)]);
                            DataTable dtConfig = oRPM.GetRPMCalibration(g_veh_id, (int)rpm_vlaue);
                            if (dtConfig.Rows.Count > 0)
                            {
                                double result = 0.0;
                                double min_rpm_car = (double)((decimal)dtConfig.Rows[0]["min_rpm_car"]);
                                double min_rpm_avg = (double)((decimal)dtConfig.Rows[0]["min_rpm_avg"]);
                                double max_rpm_car = (double)((decimal)dtConfig.Rows[0]["max_rpm_car"]);
                                double max_rpm_avg = (double)((decimal)dtConfig.Rows[0]["max_rpm_avg"]);

                                if (min_rpm_car > 0.0 && max_rpm_car > 0.0 && min_rpm_car != rpm_vlaue && max_rpm_car != rpm_vlaue)
                                {
                                    double different_rpm = max_rpm_car - min_rpm_car;
                                    double different_rpm_avg = max_rpm_avg - min_rpm_avg;
                                    double division_rpm_avg = 0.0;
                                    division_rpm_avg = different_rpm_avg / different_rpm;
                                    double temp_rpm = rpm_vlaue - min_rpm_car;
                                    double temp_rpm1 = temp_rpm * division_rpm_avg;
                                    double temp_rpm2 = temp_rpm1 + min_rpm_avg;
                                    result = rpm_vlaue * temp_rpm2;
                                }
                                else
                                {
                                    if (min_rpm_avg > 0)
                                    {
                                        result = rpm_vlaue * min_rpm_avg;
                                    }
                                    else
                                    {
                                        result = rpm_vlaue * max_rpm_avg;
                                    }
                                }
                                oGeneral.InsertRPM(g_log_index, (int)rpm_vlaue, (int)result);
                            }
                            else
                            {
                                oGeneral.InsertRPM(g_log_index, (int)rpm_vlaue, 0);
                            }
                        }
                    }
                }
                oGeneral.InsertTagMsg(g_log_index, g_veh_id, g_tag_msg);
                
            }
        }
        private void UpdateLogError()
        {
            //int last_idx = -1;
            //double last_lat = 0.00;
            //double last_lon = 0.00;
            //bool lastRecord = false;
            //int no_of_Satellite = 0;
            //DataTable LastData = new DataTable();

            //try
            //{

            //    LastData = oUpdateLog.GetLastLatLon(g_ref_idx, g_veh_id);

            //    foreach (DataRow r in LastData.Rows)
            //    {
            //        last_idx = (int)r["idx"];
            //        last_lat = (double)r["lat"];
            //        last_lon = (double)r["lon"];
            //        no_of_Satellite = (int)r["no_of_satellite"];

            //        string lat_new = String.Format("{0:F5}", last_lat);
            //        string lon_new = String.Format("{0:F5}", last_lon);
            //        g_lat = float.Parse(lat_new);
            //        g_lon = float.Parse(lon_new);

            //        lastRecord = true;
            //    }

            //    if (lastRecord)
            //    {
            //        oUpdateLog.UpdateMsg_Error(g_ref_idx, last_idx, last_lat, last_lon, no_of_Satellite);
            //    }
            // }
            //catch 
            //{ }
        }
        private void UpdateLogErrorLog()
        {
            decimal last_idx = -1.0m;
            double last_lat = 0.00;
            double last_lon = 0.00;
            bool lastRecord = false;
            int no_of_Satellite = 0;
            DataTable LastData = new DataTable();

            try
            {

                LastData = oUpdateLog.GetLastLatLonLog(g_log_index, g_veh_id);

                foreach (DataRow r in LastData.Rows)
                {
                    last_idx = (decimal)r["idx"];
                    last_lat = (double)r["lat"];
                    last_lon = (double)r["lon"];
                    no_of_Satellite = (int)r["no_of_satellite"];

                    string lat_new = String.Format("{0:F5}", last_lat);
                    string lon_new = String.Format("{0:F5}", last_lon);
                    g_lat = float.Parse(lat_new);
                    g_lon = float.Parse(lon_new);

                    lastRecord = true;
                }

                if (lastRecord)
                {
                    oUpdateLog.UpdateMsg_ErrorLog(g_log_index, last_idx, last_lat, last_lon, no_of_Satellite);
                }
            }
            catch
            { }
        }

        private void DetectETA1()
        {
            double dtTimeLeft = 100.0F;
         //   ArrayList etaList = oETACls1.CalculateETA11(g_veh_id, g_lat, g_lon, g_speed, out dtTimeLeft);

            //rose calculate ETA
            ArrayList etaList = oETACls1.CalculateETA12(g_veh_id, g_lat, g_lon, g_speed, out dtTimeLeft);

            //rose ============//
            if (etaList == null || etaList.Count < 1)
            {
                return;
            }

            foreach (ETAINFO info in etaList)
            {
            //    oETACls1.InsertMsgETA_M(g_log_index, info.eta_id, info.eta_time_left, info.destination_zone_id, info.eta_route_no,info.distance);
               // oETACls1.InsertNewRecord(g_log_index);
                //oETACls1.InsertCurrentETA(g_log_index, g_veh_id);
                
             //   oETACls1.
                /*******SQL2********/
                oETACls1.InsertMsgETA_Log_M(g_log_index, info.eta_id, info.eta_time_left, info.destination_zone_id,info.eta_route_no,info.distance,info.diffirst);
                oETACls1.InsertNewRecord(g_log_index);
                oETACls1.InsertCurrentETA(g_log_index, g_veh_id);
            }
        }
        private void DetectETA2()
        {
            //double dtTimeLeft = 100.0F;
            //ArrayList etaList = oETACls2.CalculateETA2(g_veh_id, g_lat, g_lon, g_speed, out dtTimeLeft);
            //if (etaList == null || etaList.Count < 1)
            //{
            //    return;
            //}

            //foreach (ETAINFO info in etaList)
            //{
            //    oETACls2.InsertMsgETA(g_log_index, info.eta_id, info.eta_time_left, info.destination_zone_id);
            //    oETACls2.InsertNewRecord(g_log_index);

            //    /*******SQL2********/
            //    oETACls1.InsertMsgETA_Log(g_log_index, info.eta_id, info.eta_time_left, info.destination_zone_id);
            //    oETACls1.InsertNewRecord(g_log_index);
            //}
        }
        private void DetectSpeeding()
        {
            /*****SQL1****/
            //if (oSpeed.DetectSpeedViolate(g_veh_id, g_speed))
            //{
            //    oSpeed.InsertNewRecord(g_ref_idx);

            //    /*****SQL2******/
            //    oSpeed.InsertNewRecord(g_log_index,g_veh_id,g_TimeStamp);

            //    if (IsEnableBroadcastQ)
            //    {
            //        ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
            //        info.idx = g_ref_idx;
            //        info.type = (int)EVT_TYPE.SPEEDING;
            //        info.veh_id = g_veh_id;
            //        info.msg = g_speed.ToString();
            //        info.timestamp = g_dtTimestampe;
            //        info.location_t = GetLocation(g_lon, g_lat);
            //        info.location_e = string.Empty;
            //        info.lat = g_lat;
            //        info.lon = g_lon;
            //        info.speed = g_speed;
            //        SMSQ.PushQ(info);
            //        EMAILQ.PushQ(info);
            //    }
            //}

            /*****SQL2****/
            if (oSpeed.DetectSpeedViolate_Log(g_veh_id, g_speed))
            {
                /*****SQL2******/
                oSpeed.InsertNewRecord(g_log_index, g_veh_id, g_TimeStamp);

                if (IsEnableBroadcastQ)
                {
                    ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                    info.idx = -1;
                    info.type = (int)EVT_TYPE.SPEEDING;
                    info.veh_id = g_veh_id;
                    info.msg = "Speed over : " + g_speed.ToString() + " km/hr";
                    info.timestamp = g_dtTimestampe;
                    info.location_t = GetLocation(g_lon, g_lat);
                    info.location_e = string.Empty;
                    info.lat = g_lat;
                    info.lon = g_lon;
                    info.speed = g_speed;
                    SMSQ.PushQ(info);
                    EMAILQ.PushQ(info);
                }
            }



        }
        private void DetectIdling()
        {
            /*--- Old Process
            if (oIdling.DetectIdle(g_veh_id))
            {
                oIdling.InsertNewRecord(g_ref_idx);
            }
            */

            //--- Check Speed < 6
            if (g_speed < 6)
            {  
                /*****SQL1******/
                //if (oIdling.DetectIdle(g_veh_id))
                //{
                //    //oIdling.InsertNewRecord(g_ref_idx);
                //    /*****SQL2******/
                //    oIdling.InsertNewRecord(g_log_index, g_veh_id, g_TimeStamp);
                //}

                /*****SQL2******/
                if (oIdling.DetectIdle_Log(g_veh_id))
                {
                    oIdling.InsertNewRecord(g_log_index, g_veh_id, g_TimeStamp);
                }
            }
        }
        private void DetectFencing()
        {
            try
            {
                if (oFencing.IsFeatureExisted(g_veh_id) == false) { return; }
                DataTable fzTable2 = oFencing.GetFleetZoneByVeh(g_veh_id);
                foreach (DataRow fz_r in fzTable2.Rows)
                {
                    DataTable newweb = oFencing.GetZoneProfile_new((int)fz_r["zone_id"]);
                    //DataTable newweb = oFencing.GetZoneProfile_new((int)fz_r["zone_id"], g_veh_id);
                    DataTable pfTable2 = new DataTable();
                    if (newweb.Rows.Count > 0)
                    {
                        pfTable2 = oFencing.GetZoneProfile_new((int)fz_r["zone_id"]);
                    }
                    else
                    {
                        pfTable2 = oFencing.GetZoneProfile((int)fz_r["zone_id"]);
                    }

                    foreach (DataRow pf_row in pfTable2.Rows)
                    {
                        int zone_id = (int)fz_r["zone_id"];
                        double lat_min = (double)pf_row["lat_min"];
                        double lat_max = (double)pf_row["lat_max"];
                        double lon_min = (double)pf_row["lon_min"];
                        double lon_max = (double)pf_row["lon_max"];
                        Boolean clkdir = (Boolean)(pf_row["clockwise"]);
                        DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                        FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;
                        if (oFencing.DetectFencing(g_veh_id, zone_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, clkdir, lastedUpdate, out side))
                        {
                            int last_zone_id = -1;
                            int last_evt_id = -1;

                            /*****SQL1 USE ******/
                            // oFencing.GetLastZoneStatus(g_veh_id, zone_id, out last_evt_id, out last_zone_id);

                            /*****SQL2 OK ******/
                            oFencing.GetLastZoneStatus_Log(g_veh_id, zone_id, out last_evt_id, out last_zone_id);

                            if (side == FencingCls.SIDE_TYPE.IN && last_evt_id == (int)EVT_TYPE.IN_ZONE && last_zone_id == zone_id)
                            {
                            }
                            else if (side == FencingCls.SIDE_TYPE.OUT && last_evt_id == (int)EVT_TYPE.OUT_ZONE && last_zone_id == zone_id)
                            {
                            }
                            else
                            {
                                bool bIsRecord = false;
                                if (last_evt_id != -1)
                                {
                                    bIsRecord = true;
                                }
                                else if ((last_evt_id == -1) && (side == FencingCls.SIDE_TYPE.IN))
                                {
                                    bIsRecord = true;
                                }

                                if (bIsRecord)
                                {
                                    //Detect Fencing KPI
                                    //oFencing.KPICalculatorZoneByVeh(g_ref_idx, g_veh_id, zone_id);

                                    //Calculator Zone Insert
                                    //oFencing.InsertMsgZone(g_ref_idx, zone_id);
                                    //oFencing.InsertNewRecord(g_ref_idx, g_veh_id, zone_id);

                                    /******SQL2******/
                                    oFencing.InsertNewRecord(g_log_index, g_veh_id, g_TimeStamp, zone_id);

                                    if (IsEnableBroadcastQ)
                                    {
                                        ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                                        info.idx = -1;
                                        info.type = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.IN_ZONE : (int)EVT_TYPE.OUT_ZONE;
                                        info.veh_id = g_veh_id;
                                        info.msg = zone_id.ToString();
                                        info.timestamp = g_dtTimestampe;
                                        info.location_t = GetLocation(g_lon, g_lat);
                                        info.lat = g_lat;
                                        info.lon = g_lon;
                                        info.speed = g_speed;
                                        SMSQ.PushQ(info);
                                        EMAILQ.PushQ(info);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }
        }

        private void DetectZoneCurfew()
        {
            bool IsRecord = false;
            int old_zone_id = -1;
            FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;

            try
            {
                DataTable newweb = oZoneCurfew.GetFleetZoneCurfewByVeh_new(g_veh_id);
                DataTable fzTable = new DataTable();
                if (newweb.Rows.Count > 0)
                {
                    fzTable = oZoneCurfew.GetFleetZoneCurfewByVeh_new(g_veh_id);
                }
                else
                {
                    fzTable = oZoneCurfew.GetFleetZoneCurfewByVeh(g_veh_id);
                }

                foreach (DataRow fz_r in fzTable.Rows)
                {
                    IsRecord = false;

                    int r_zone_id = Convert.ToInt32((String)fz_r["zone_id"]);
                    int r_zone_type = Convert.ToInt32((String)fz_r["zone_type_id"]);
                    int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                    String r_start_time = (String)fz_r["start_time"];
                    String r_end_time = (String)fz_r["end_time"];
                    int delay_time = Convert.ToInt32((String)fz_r["delay_time"]);

                    String now = DateTime.Now.ToLongTimeString();

                    switch (r_day)
                    {
                        case 1: r_day = 7; break;
                        case 2: r_day = 1; break;
                        case 3: r_day = 2; break;
                        case 4: r_day = 3; break;
                        case 5: r_day = 4; break;
                        case 6: r_day = 5; break;
                        case 7: r_day = 6; break;
                    }

                    if (!(r_day == (int)DateTime.Now.DayOfWeek && DateTime.Parse(r_start_time) <= DateTime.Parse(now) && DateTime.Parse(now) <= DateTime.Parse(r_end_time)))
                    {
                        continue;
                    }

                    if (old_zone_id != r_zone_id)
                    {
                        DataTable newweb2 = oZoneCurfew.GetZoneProfile_new(r_zone_id);
                        DataTable pfTable = new DataTable();
                        if (newweb2.Rows.Count > 0)
                        {
                            pfTable = oZoneCurfew.GetZoneProfile_new(r_zone_id);
                        }
                        else
                        {
                            pfTable = oZoneCurfew.GetZoneProfile(r_zone_id);
                        }

                        foreach (DataRow pf_row in pfTable.Rows)
                        {
                            int zone_id = r_zone_id;
                            double lat_min = (double)pf_row["lat_min"];
                            double lat_max = (double)pf_row["lat_max"];
                            double lon_min = (double)pf_row["lon_min"];
                            double lon_max = (double)pf_row["lon_max"];
                            Boolean clkdir = (Boolean)pf_row["clockwise"];
                            DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                            side = FencingCls.SIDE_TYPE.NONE;
                            if (oZoneCurfew.DetectFencing(g_veh_id,zone_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, clkdir, lastedUpdate, out side))
                            {
                                int last_zone_id = -1;
                                int last_evt_id = -1;
                                int mark = -1;
                                DateTime last_local_timestamp = new DateTime();
                                
                                /*******SQL1 OK*******/
                                //oZoneCurfew.GetLastZoneCurfewStatus(g_veh_id, zone_id, out last_evt_id, out last_zone_id, out last_local_timestamp, out mark);
                               
                                /*******SQL2 OK*******/
                                oZoneCurfew.GetLastZoneCurfewStatus_Log(g_veh_id, zone_id, out last_evt_id, out last_zone_id, out last_local_timestamp, out mark);
                               
                                if ((side == FencingCls.SIDE_TYPE.IN) && (last_evt_id == (int)EVT_TYPE.IN_ZONE_CURFEW) && (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;
                                        if ((int)sp.TotalMinutes > delay_time && a != 1)
                                        {
                                            IsRecord = true;
                                            a = 0;
                                        }
                                        else
                                            a = 0;
                                    }
                                }

                                else if ((side == FencingCls.SIDE_TYPE.OUT) && (last_evt_id == (int)EVT_TYPE.OUT_ZONE_CURFEW) && (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;

                                        if (sp.TotalMinutes > (double)delay_time && a != 1 )
                                        {
                                            IsRecord = true;
                                            a = 0;
                                        }
                                        else
                                            a = 0;
                                    }
                                }
                                else
                                {
                                    /******SQL1*******/
                                    //oZoneCurfew.InsertMsgZoneStatus(g_veh_id, g_ref_idx, r_zone_id, 0);
                                    
                                    /******SQL2*******/
                                    oZoneCurfew.InsertMsgZoneStatusLog(g_veh_id, g_log_index, r_zone_id, 0);
                                }
                            }
                        }
                    }
                    else
                    {
                    }

                    if (IsRecord)
                    {
                        //oZoneCurfew.InsertMsgZoneStatus(g_veh_id, g_ref_idx, r_zone_id, 1);
                        //oZoneCurfew.InsertNewRecordEvtCurfew(g_ref_idx);

                        /******SQL2*******/
                        oZoneCurfew.InsertMsgZoneStatusLog(g_veh_id, g_log_index, r_zone_id, 1);
                        oZoneCurfew.InsertNewRecordCurfewLog(g_log_index,g_veh_id, g_TimeStamp , r_zone_id);

                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = -1;
                            info.type = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.NO_OUTZONE_CURFEW : (int)EVT_TYPE.NO_INZONE_CURFEW;
                            info.veh_id = g_veh_id;
                            info.msg = r_zone_id.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t = GetLocation(g_lon, g_lat);
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }

                    IsRecord = false;
                    old_zone_id = r_zone_id;
                }
            }
            catch 
            { }
        }
        private void DetectZoneCurfewWithSpeed()
        {
            if (oZoneCurfewWithSpeed.IsFeatureExisted(g_veh_id) == false) { return; }

            bool IsRecord = false;
            int old_zone_id = -1;
            FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;

            DataTable newweb = oZoneCurfewWithSpeed.GetFleetZoneCurfewWithSpeedByVeh_new(g_veh_id);
            DataTable fzTable = new DataTable();
            if (newweb.Rows.Count > 0)
            {
                fzTable = oZoneCurfewWithSpeed.GetFleetZoneCurfewWithSpeedByVeh_new(g_veh_id);
            }
            else
            {
                fzTable = oZoneCurfewWithSpeed.GetFleetZoneCurfewWithSpeedByVeh(g_veh_id);
            }
            try
            {
                foreach (DataRow fz_r in fzTable.Rows)
                {
                    IsRecord = false;

                    int r_zone_id = Convert.ToInt32((String)fz_r["zone_id"]);
                    int r_zone_type = Convert.ToInt32((String)fz_r["zone_type_id"]);
                    int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                    String r_start_time = (String)fz_r["start_time"];
                    String r_end_time = (String)fz_r["end_time"];
                    int r_delay_time = Convert.ToInt32((String)fz_r["delay_time"]);
                    int r_speed = Convert.ToInt32((String)fz_r["speed"]);

                    String now = DateTime.Now.ToLongTimeString();

                    switch(r_day)
                    {
                        case 1: r_day = 7; break;
                        case 2: r_day = 1; break;
                        case 3: r_day = 2; break;
                        case 4: r_day = 3; break;
                        case 5: r_day = 4; break;
                        case 6: r_day = 5; break;
                        case 7: r_day = 6; break;
                    }
                    
                    if (!(r_day == (int)DateTime.Now.DayOfWeek && DateTime.Parse(r_start_time) <= DateTime.Parse(now) && DateTime.Parse(now) <= DateTime.Parse(r_end_time)))
                    {
                        continue;
                    }

                    if (old_zone_id != r_zone_id)
                    {
                        DataTable newweb2 = oZoneCurfewWithSpeed.GetZoneProfile_new(r_zone_id);
                        DataTable pfTable = new DataTable();
                        if (newweb2.Rows.Count > 0)
                        {
                            pfTable = oZoneCurfewWithSpeed.GetZoneProfile_new(r_zone_id);
                        }
                        else
                        {
                            pfTable = oZoneCurfewWithSpeed.GetZoneProfile(r_zone_id);
                        }

                        foreach (DataRow pf_row in pfTable.Rows)
                        {
                            int zone_id = r_zone_id;
                            double lat_min = (double)pf_row["lat_min"];
                            double lat_max = (double)pf_row["lat_max"];
                            double lon_min = (double)pf_row["lon_min"];
                            double lon_max = (double)pf_row["lon_max"];
                            Boolean clkdir = (Boolean)pf_row["clockwise"];
                            DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                            side = FencingCls.SIDE_TYPE.NONE;

                            if (oZoneCurfewWithSpeed.DetectFencing(g_veh_id,zone_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, clkdir, lastedUpdate, out side))
                            {
                                int last_zone_id = -1;
                                int last_evt_id = -1;
                                int mark = -1;
                                int last_speed = -1;
                                DateTime last_local_timestamp = new DateTime();


                                /***********SQL1***********/
                                //oZoneCurfewWithSpeed.GetLastZoneCurfewWithSpeedStatus(g_veh_id, zone_id, out last_evt_id, out last_zone_id, out last_local_timestamp, out mark, out last_speed);

                                /***********SQL2***********/
                                oZoneCurfewWithSpeed.GetLastZoneCurfewWithSpeedStatus_Log(g_veh_id, zone_id, out last_evt_id, out last_zone_id, out last_local_timestamp, out mark, out last_speed);

                                if ((side == FencingCls.SIDE_TYPE.IN) && (last_evt_id == (int)EVT_TYPE.IN_ZONE) && (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;
                                        if (((int)sp.TotalMinutes > r_delay_time && mark == 0) && (g_speed >= r_speed))
                                        {
                                            IsRecord = true;
                                        }
                                    }
                                }
                                else if ((side == FencingCls.SIDE_TYPE.OUT) && (last_evt_id == (int)EVT_TYPE.OUT_ZONE) && (last_zone_id == zone_id))
                                {
                                    if ((oZoneCurfew.GSide == FencingCls.SIDE_TYPE.IN && r_zone_type == 0) ||
                                        (oZoneCurfew.GSide == FencingCls.SIDE_TYPE.OUT && r_zone_type == 1))
                                    {
                                        DateTime dtNow = DateTime.Now;
                                        TimeSpan sp = dtNow - last_local_timestamp;

                                        if ((sp.TotalMinutes > (double)r_delay_time && mark == 0) && (g_speed >= r_speed))
                                        {
                                            IsRecord = true;
                                        }
                                    }
                                }
                                else
                                {
                                    /*********SQL1*********/
                                    //oZoneCurfewWithSpeed.InsertMsgZone(g_veh_id, g_ref_idx, r_zone_id, 0, g_speed);
                                  
                                    /*********SQL2*********/
                                    oZoneCurfewWithSpeed.InsertMsgZoneStatus_Log(g_veh_id, g_log_index, r_zone_id, 0, g_speed);
                                }
                            }
                        }
                    }
                    else
                    {
                    }

                    if (IsRecord)
                    {
                        /*********SQL1*********/
                        //oZoneCurfewWithSpeed.InsertMsgZone(g_veh_id, g_ref_idx, r_zone_id, 1, g_speed);
                        //oZoneCurfewWithSpeed.InsertNewRecordCurfewWithSpeed(g_ref_idx);

                        /*********SQL2*********/
                        oZoneCurfewWithSpeed.InsertMsgZoneStatus_Log(g_veh_id, g_log_index, r_zone_id, 0, g_speed);
                        oZoneCurfewWithSpeed.InsertNewRecordCurfewWithSpeed_Log(g_log_index, g_veh_id,g_TimeStamp ,r_zone_id);
                        
                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = -1;
                            info.type = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED : (int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED;
                            info.veh_id = g_veh_id;
                            info.msg = r_zone_id.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t = GetLocation(g_lon, g_lat);
                            info.location_e = string.Empty;
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }

                    IsRecord = false;
                    old_zone_id = r_zone_id;
                }
            }
            catch 
            { }
        }
        private void DetectRouting()
        {
            DataTable fzTable = oRouting.GetRouteByVeh(g_veh_id);
            try
            {
                float d_min = 99999.99F;
                int rec_route_id = -1;
                bool IsOutRoute = false;

                foreach (DataRow fz_r in fzTable.Rows)
                {
                    DataTable pfTable = oRouting.GetRouteProfile((int)fz_r["route_id"]);

                    // Console.WriteLine(fz_r["route_desc"].ToString());

                    foreach (DataRow pf_row in pfTable.Rows)
                    {
                        DataTable wpTable = oRouting.GetRouteWayPoints((int)fz_r["route_id"]);

                        int route_id = (int)fz_r["route_id"];
                        double lat_min = (double)pf_row["lat_min"];
                        double lat_max = (double)pf_row["lat_max"];
                        double lon_min = (double)pf_row["lon_min"];
                        double lon_max = (double)pf_row["lon_max"];
                        int dist = (int)pf_row["distance"];
                        DateTime lastedUpdate = (DateTime)pf_row["lasted_update"];

                        DataTable wp = new DataTable();
                        wp.Columns.Add("X", typeof(float));
                        wp.Columns.Add("Y", typeof(float));
                        foreach (DataRow wp_row in wpTable.Rows)
                        {
                            double wpX = (double)wp_row["lon"];
                            double wpY = (double)wp_row["lat"];
                            wp.Rows.Add(new object[] { wpX, wpY });
                        }

                        float d = 99999.0F;
                        oRouting.DetectOffRoad(g_veh_id, g_lat, g_lon, (float)lat_min, (float)lat_max, (float)lon_min, (float)lon_max, lastedUpdate, wp, dist, out d);
                        d_min = Math.Min(d_min, d);
                        if (d_min == d)
                        {
                            rec_route_id = route_id;
                            d *= 1000;
                            if (d <= dist)
                            {
                                IsOutRoute = false;
                            }
                            else
                            {
                                IsOutRoute = true;
                            }
                        }
                    }

                    EVT_TYPE evtToBroadcast = EVT_TYPE.IN_ROUTE;
                    if (rec_route_id > -1)
                    {
                        int last_route_id = -1;
                        int last_evt_id = -1;

                        /*********SQL1 OK***************/
                        //oRouting.GetLastRouteStatus(g_veh_id, rec_route_id, out last_evt_id, out last_route_id);

                        /*********SQL2 OK***************/
                        oRouting.GetLastRouteStatus_Log(g_veh_id, rec_route_id, out last_evt_id, out last_route_id);

                        if (last_evt_id == -1 && !IsOutRoute)
                        {
                            //oRouting.PreviousRouteState(g_veh_id, g_ref_idx, rec_route_id, IsOutRoute ? EVT_TYPE.OUT_ROUTE : EVT_TYPE.IN_ROUTE);
                            //oRouting.InsertMsgRoute(g_veh_id, g_ref_idx, rec_route_id, IsOutRoute ? EVT_TYPE.OUT_ROUTE : EVT_TYPE.IN_ROUTE);
                            //oRouting.InsertNewRecord(g_ref_idx, IsOutRoute ? (int)EVT_TYPE.OUT_ROUTE : (int)EVT_TYPE.IN_ROUTE);

                            evtToBroadcast = IsOutRoute ? EVT_TYPE.OUT_ROUTE : EVT_TYPE.IN_ROUTE;

                            /*********SQL2*********/
                            oRouting.PreviousRouteState_Log(g_veh_id, g_log_index, rec_route_id, IsOutRoute ? EVT_TYPE.OUT_ROUTE : EVT_TYPE.IN_ROUTE);
                            oRouting.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, IsOutRoute ? (int)EVT_TYPE.OUT_ROUTE : (int)EVT_TYPE.IN_ROUTE, rec_route_id, -1);
                        }
                        if (IsOutRoute && last_evt_id == (int)EVT_TYPE.IN_ROUTE)
                        {
                            //oRouting.PreviousRouteState(g_veh_id, g_ref_idx, rec_route_id, EVT_TYPE.OUT_ROUTE);
                            //oRouting.InsertMsgRoute(g_veh_id, g_ref_idx, rec_route_id, EVT_TYPE.OUT_ROUTE);
                            //oRouting.InsertNewRecord(g_ref_idx, (int)EVT_TYPE.OUT_ROUTE);

                            evtToBroadcast = EVT_TYPE.OUT_ROUTE;

                            /*********SQL2*********/
                            oRouting.PreviousRouteState_Log(g_veh_id, g_log_index, rec_route_id, EVT_TYPE.OUT_ROUTE);
                            oRouting.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)EVT_TYPE.OUT_ROUTE, rec_route_id, -1);
                        }
                        else if (!IsOutRoute && last_evt_id == (int)EVT_TYPE.OUT_ROUTE)
                        {
                            //oRouting.PreviousRouteState(g_veh_id, g_ref_idx, rec_route_id, EVT_TYPE.IN_ROUTE);
                            //oRouting.InsertMsgRoute(g_veh_id, g_ref_idx, rec_route_id, EVT_TYPE.IN_ROUTE);
                            //oRouting.InsertNewRecord(g_ref_idx, (int)EVT_TYPE.IN_ROUTE);

                            evtToBroadcast = EVT_TYPE.IN_ROUTE;

                            /*********SQL2*********/
                            oRouting.PreviousRouteState_Log(g_veh_id, g_log_index, rec_route_id, EVT_TYPE.IN_ROUTE);
                            oRouting.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)EVT_TYPE.IN_ROUTE, rec_route_id, -1);
                        }

                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = -1;
                            info.type = (int)evtToBroadcast;
                            info.veh_id = g_veh_id;
                            info.msg = rec_route_id.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t = GetLocation(g_lon, g_lat);
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }
                }
            }
            catch
            { }
        }

        private void GetNPutLocation()
        {
            string location_e = "NO GPS";
            string location_t = "ไม่มีสัญญาน GPS";
            try
            {
                if (g_lon > 0.0 && g_lat > 0.0)
                {
                    int id = oLoc.DetectCustomLandmark(g_lon, g_lat);
                    if (id > -1)
                    {
                        location_e = oLoc.GetCustomLandmarkName(id, 0);
                        location_t = oLoc.GetCustomLandmarkName(id, 1);
                    }
                    else
                    {
                        id = oLoc.DetectLandmark(g_lon, g_lat);
                        location_e = oLoc.GetLandmarkName(id, 0);
                        location_t = oLoc.GetLandmarkName(id, 1);
                    }

                    id = oLoc.DetectAdminPoly(g_lon, g_lat);
                    location_e += " " + oLoc.GetPolyName(id, 0);
                    location_t += " " + oLoc.GetPolyName(id, 1);

                    location_e = location_e.TrimEnd(new char[] { ' ' });
                    location_t = location_t.TrimEnd(new char[] { ' ' });

                    location_e = location_e.TrimStart(new char[] { ' ' });
                    location_t = location_t.TrimStart(new char[] { ' ' });
                }
            }
            catch
            {
            }
            try
            {
                //int loc_id = oLoc.InsertMsgLoc2(location_t, location_e);
                //if (loc_id != -1)
                //{
                //    oLoc.InsertMsgLoc(g_ref_idx, LOCATION_TYPE.ESRI_MAP, loc_id);
                //}

                /******SQL2******/
                if (location_t.Length > 0 || location_e.Length > 0)
                {
                    oLoc.UpdateLogLocation(g_log_index, location_t, location_e);
                }
                else
                {
                    oLoc.UpdateLogLocation(g_log_index, "ไม่มีสัญญาน GPS", "NO GPS");
                }
            }
            catch
            { }
        }
        private void DetectEngineOnEvent()
        {
            bool IsRecord = false;
            DataTable newweb = oEngineOnEvt.GetEngineOnEventByVeh_new(g_veh_id);
            DataTable feTable = new DataTable();
            if (newweb.Rows.Count > 0)
            {
                feTable = oEngineOnEvt.GetEngineOnEventByVeh_new(g_veh_id);
            }
            else
            {
                feTable = oEngineOnEvt.GetEngineOnEventByVeh(g_veh_id);
            }
                try
                {
                    foreach (DataRow fz_r in feTable.Rows)
                    {
                        IsRecord = false;
                        int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                        String r_start_time = (String)fz_r["start_time"];
                        String r_end_time = (String)fz_r["end_time"];
                        int delay_time = Convert.ToInt32((String)fz_r["delay_time"]);
                        String now = DateTime.Now.ToLongTimeString();
                        switch (r_day)
                        {
                            case 1: r_day = 7; break;
                            case 2: r_day = 1; break;
                            case 3: r_day = 2; break;
                            case 4: r_day = 3; break;
                            case 5: r_day = 4; break;
                            case 6: r_day = 5; break;
                            case 7: r_day = 6; break;
                        }

                        // int test = (int)DateTime.Now.DayOfWeek;
                        if (!(r_day == (int)DateTime.Now.DayOfWeek && DateTime.Parse(r_start_time) <= DateTime.Parse(now) && DateTime.Parse(now) <= DateTime.Parse(r_end_time)))
                        {
                            continue;
                        }
                        else
                        {
                            /*******SQL2*********/
                            //if (oEngineOnEvt.GetEngineOnByTime_Log(g_veh_id)) { }

                            /// check is engine on
                            //if (oEngineOnEvt.GetEngineOnOnByTime(g_veh_id))
                            //if (oEngineOnEvt.GetEngineOnByTime_Log(g_veh_id))

                            if (oEngineOnEvt.GetEngineOnByTime_Log(g_veh_id))
                            {
                                int last_evt_id;
                                DateTime last_local_timestamp;

                                //last time status event
                                /*******SQL1*********/
                                //oEngineOnEvt.GetLastEngineOnCurfewEventStatus(g_veh_id, out last_evt_id, out last_local_timestamp);

                                /*******SQL2*********/
                                oEngineOnEvt.GetLastStatusEventEngineOn_Log(g_veh_id, out last_evt_id, out last_local_timestamp);

                                if (last_evt_id == (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME || last_evt_id == -1)
                                {
                                    int last_evt_id2 = -1;
                                    DateTime last_local_timestamp2 = new DateTime();

                                    //get last engine on
                                    /*******SQL1*********/
                                    //oEngineOnEvt.GetLastEngineOnEventStatus(g_veh_id, out last_evt_id2, out last_local_timestamp2);

                                    /*******SQL2*********/
                                    oEngineOnEvt.GetLastEngineOnStatus_Log(g_veh_id, out last_evt_id2, out last_local_timestamp2);

                                    if (last_evt_id2 == (int)EVT_TYPE.ENGINE_ON)
                                    {
                                        // DateTime dtStamp = ge
                                        if (g_type_of_msg == "I" && last_local_timestamp2 == g_TimeStamp)
                                        {
                                            IsRecord = true;
                                        }
                                        else if (last_evt_id == -1)
                                        {
                                            DateTime dt = DateTime.Now;
                                            TimeSpan sp = dt - last_local_timestamp2;
                                            if (sp.TotalMinutes > delay_time)
                                            {
                                                IsRecord = true;
                                            }
                                        }
                                        else
                                        {
                                            DateTime dt = DateTime.Now;
                                            TimeSpan sp = dt - last_local_timestamp;
                                            if (sp.TotalMinutes > delay_time)
                                            {
                                                IsRecord = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        if (IsRecord)
                        {
                            /*****SQL1******/
                            //oEngineOnEvt.InsertMsgEngineOnEvent(g_ref_idx);

                            /*****SQL2******/
                            oEngineOnEvt.InsertMsgEngineOnEvent_Log(g_log_index, g_veh_id, g_TimeStamp);

                            if (IsEnableBroadcastQ)
                            {
                                ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                                info.idx = -1;
                                info.type = (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME;
                                info.veh_id = g_veh_id;
                                info.msg = "";
                                info.timestamp = g_dtTimestampe;
                                info.location_t = GetLocation(g_lon, g_lat);
                                info.lat = g_lat;
                                info.lon = g_lon;
                                info.speed = g_speed;
                                SMSQ.PushQ(info);
                                EMAILQ.PushQ(info);
                            }
                        }
                        IsRecord = false;
                    }
                }
                catch
                { }
        }
        private void DetectSpeedHazard()
        {
            if (g_type_of_msg == "V")
            {
                /******SQL1*********/
                //oSpeedHazard.InsertNewRecord(g_ref_idx);
                /******SQL2*********/
                oSpeedHazard.InsertNewRecord_Log(g_log_index,g_veh_id,g_TimeStamp);
            }
        } 
        private void DetectAngleHazard()
        {
            if (g_type_of_msg == "A")
            { 
                /******SQL1*********/
                //oAngleHazard.InsertNewRecord(g_ref_idx);
                /******SQL2*********/
                oAngleHazard.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp);
            }
        }
        private void DetectTemperatureEvent()
        {
            bool IsRecord = false;

            DataTable newweb = oTemperatureEvt.GetTemperatureByVeh_new(g_veh_id);
            DataTable feTable = new DataTable();
            if (newweb.Rows.Count > 0)
            {
                feTable = oTemperatureEvt.GetTemperatureByVeh_new(g_veh_id);
            }
            else
            {
                feTable = oTemperatureEvt.GetTemperatureByVeh(g_veh_id);
            }
            try
            {
                foreach (DataRow fz_r in feTable.Rows)
                {
                    IsRecord = false;
                    int r_day = Convert.ToInt32((String)fz_r["day_id"]);
                    int min_temp = Convert.ToInt32(fz_r["min_temp"]);
                    int max_temp = Convert.ToInt32(fz_r["max_temp"]);
                    DateTime start_time = Convert.ToDateTime(fz_r["start_time"]);
                    DateTime end_time = Convert.ToDateTime(fz_r["end_time"]);
                    int delay_time = Convert.ToInt32(fz_r["delay_time"]);

                    int last_temperature = 0;
                    int cur_temperature = 0;
                    
                    TimeSpan sp1 = new TimeSpan(start_time.Hour, start_time.Minute, start_time.Second);
                    TimeSpan sp2 = new TimeSpan(end_time.Hour, end_time.Minute, end_time.Second);
                    TimeSpan spNow = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

                    String now = DateTime.Now.ToLongTimeString();
                    switch (r_day)
                    {
                        case 1: r_day = 7; break;
                        case 2: r_day = 1; break;
                        case 3: r_day = 2; break;
                        case 4: r_day = 3; break;
                        case 5: r_day = 4; break;
                        case 6: r_day = 5; break;
                        case 7: r_day = 6; break;
                    }

                    int dd = (int)DateTime.Now.DayOfWeek;
                    if (!(r_day == (int)DateTime.Now.DayOfWeek))
                    {
                        continue;
                    }
                    else if (spNow.TotalMinutes < sp1.TotalMinutes || spNow.TotalMinutes > sp2.TotalMinutes)
                    {
                        continue;
                    }
                    else
                    {
                        int last_evt_id;
                        DateTime last_local_timestamp;

                        /******SQL1******/
                        //oTemperatureEvt.GetLastTemperatureEventStatus(g_veh_id, out last_evt_id, out last_temperature, out cur_temperature, out last_local_timestamp);

                        /******SQL2******/
                        oTemperatureEvt.GetLastTemperatureEventStatus_Log(g_veh_id, out last_evt_id, out last_temperature, out cur_temperature, out last_local_timestamp);

                        if (last_evt_id == -1 && last_temperature == 0)
                        {
                            continue;
                        }

                        if (last_evt_id == (int)EVT_TYPE.TEMPERATURE || last_evt_id == -1)
                        {
                            if (cur_temperature < min_temp || cur_temperature > max_temp)
                            {
                                if (last_evt_id == -1)
                                {
                                    IsRecord = true;
                                }
                                else
                                {
                                    DateTime dt = DateTime.Now;
                                    TimeSpan sp = dt - last_local_timestamp;
                                    if (sp.TotalMinutes > delay_time)
                                    {
                                        IsRecord = true;
                                    }
                                }
                            }
                        }
                    }

                    if (IsRecord)
                    {
                        /******SQL1******/
                        oTemperatureEvt.InsertMsgTemperatureEvent(Convert.ToDecimal(g_log_index));
                        /******SQL2******/
                        oTemperatureEvt.InsertMsgEvt_Log(g_log_index,g_veh_id,g_TimeStamp);
                        

                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = -1;
                            info.type = (int)EVT_TYPE.TEMPERATURE;
                            info.veh_id = g_veh_id;
                            info.msg = cur_temperature.ToString();
                            info.timestamp = g_dtTimestampe;
                            info.location_t = GetLocation(g_lon, g_lat);
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }
                    IsRecord = false;
                }
            }
            catch 
            { }
        }
        private void DetectEngineOnOffInOutZone()
        {
            try
            {
                DataTable newweb = oEngineOnOffInOutZoneObj.GetFeatureEvtParam_new(g_veh_id);
                DataTable dt = new DataTable();
                if (newweb.Rows.Count > 0)
                {
                    dt = oEngineOnOffInOutZoneObj.GetFeatureEvtParam_new(g_veh_id);
                }
                else
                {
                    dt = oEngineOnOffInOutZoneObj.GetFeatureEvtParam(g_veh_id);
                }

                foreach (DataRow r in dt.Rows)
                {
                    string zone_name = r["zone_desc"] as string;
                    string zone_id = r["zone_id"] as string;
                    string zone_type = r["zone_type"] as string;
                    int day = Convert.ToInt32(r["day_name"] as string);
                    string start_time = r["start_time"] as string;
                    string end_time = r["end_time"] as string;
                    string engine_status = r["engine_status"] as string;
                    string delay = r["delay_time"] as string;

                    /*********SQL1*********/
                    //bool IsEngineOn = oEngineOnOffInOutZoneObj.IsEngineOn(g_veh_id);

                    /*********SQL2*********/
                    bool IsEngineOn = oEngineOnOffInOutZoneObj.IsEngineOn_Log(g_veh_id);

                    int evt_id = -1;
                    int last_zone_id = -1;

                    String now = DateTime.Now.ToLongTimeString();

                    switch (day)
                    {
                        case 1: day = 7; break;
                        case 2: day = 1; break;
                        case 3: day = 2; break;
                        case 4: day = 3; break;
                        case 5: day = 4; break;
                        case 6: day = 5; break;
                        case 7: day = 6; break;
                    }

                    //int iDay = Convert.ToInt32(day) - 1;
                    //if (iDay < 1) iDay = 7;
                    if ((int)DateTime.Now.DayOfWeek != day) continue;

                    DateTime dn = Convert.ToDateTime(DateTime.Now);
                    DateTime st = Convert.ToDateTime(start_time);
                    DateTime en = Convert.ToDateTime(end_time);

                    TimeSpan spStart = new TimeSpan(st.Hour, st.Minute, st.Second);
                    TimeSpan spEnd = new TimeSpan(en.Hour, en.Minute, en.Second);
                    TimeSpan spNow = new TimeSpan(dn.Hour, dn.Minute, dn.Second);

                    if (spStart.TotalMinutes > spNow.TotalMinutes) continue;
                    if (spNow.TotalMinutes > spEnd.TotalMinutes) continue;

                    /*********SQL1*********/
                    //oEngineOnOffInOutZoneObj.GetLastZoneStatus(g_veh_id, Convert.ToInt32(zone_id), out evt_id, out last_zone_id);
                    
                    /*********SQL2*********/
                    oEngineOnOffInOutZoneObj.GetLastZoneStatus_Log(g_veh_id, Convert.ToInt32(zone_id), out evt_id, out last_zone_id);                    

                    int zone_evt_id = Convert.ToInt32(zone_type) == 0 ? (int)EngineOnOffInOutZoneCls.ZONEEVT.IN : (int)EngineOnOffInOutZoneCls.ZONEEVT.OUT;
                    int io_port = Convert.ToInt32(engine_status) == 0 ? (int)EngineOnOffInOutZoneCls.ZONETYPE.IN : (int)EngineOnOffInOutZoneCls.ZONETYPE.OUT;
                  
                    bool is_rec = false;
                    EVT_TYPE evt_tmp = EVT_TYPE.NONE;

                    /*********SQL2*********/
                    //if (oEngineOnOffInOutZoneObj.IsZonePortOutOfPolicy_Log(g_veh_id, Convert.ToInt32(zone_id), zone_evt_id, io_port, Convert.ToInt32(delay))) { }
                    
                    /*********SQL1*********/
                    //if (oEngineOnOffInOutZoneObj.IsZonePortOutOfPolicy(g_veh_id, Convert.ToInt32(zone_id), zone_evt_id, io_port, Convert.ToInt32(delay)))
                    
                    if (oEngineOnOffInOutZoneObj.IsZonePortOutOfPolicy_Log(g_veh_id, Convert.ToInt32(zone_id), zone_evt_id, io_port, Convert.ToInt32(delay))) 
                    {
                        if (io_port == 0x10 && zone_evt_id == (int)EngineOnOffInOutZoneCls.ZONEEVT.IN)
                        {
                            evt_tmp = EVT_TYPE.ENGINE_ON_IN_ZONE;
                        }
                        else if (io_port == 0x00 && zone_evt_id == (int)EngineOnOffInOutZoneCls.ZONEEVT.IN)
                        {
                            evt_tmp = EVT_TYPE.ENGINE_OFF_IN_ZONE;
                        }
                        else if (io_port == 0x10 && zone_evt_id == (int)EngineOnOffInOutZoneCls.ZONEEVT.OUT)
                        {
                            evt_tmp = EVT_TYPE.ENGINE_ON_OUT_ZONE;
                        }
                        else if (io_port == 0x00 && zone_evt_id == (int)EngineOnOffInOutZoneCls.ZONEEVT.OUT)
                        {
                            evt_tmp = EVT_TYPE.ENGINE_OFF_OUT_ZONE;
                        }
                    }

                    if (is_rec)
                    {
                        /*********SQL1*********/
                        //oEngineOnOffInOutZoneObj.InsertNewRecord(g_ref_idx, (int)evt_tmp);
                        /*********SQL2*********/
                        oEngineOnOffInOutZoneObj.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)evt_tmp, int.Parse(zone_id), -1);

                        if (IsEnableBroadcastQ)
                        {
                            ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                            info.idx = -1;
                            info.type = (int)evt_tmp;
                            info.veh_id = g_veh_id;
                            info.msg = zone_name;
                            info.timestamp = g_dtTimestampe;
                            info.location_t = GetLocation(g_lon, g_lat);
                            info.lat = g_lat;
                            info.lon = g_lon;
                            info.speed = g_speed;
                            SMSQ.PushQ(info);
                            EMAILQ.PushQ(info);
                        }
                    }
                }
            }
            catch
            {
            }
        }
      
        private void DetectMagneticEvent()
         {
            try
            {
                EVT_TYPE event_type = EVT_TYPE.NONE;
               
                if (g_tag_msg.Length > 2)
                {
                    int tag_magnetic = g_tag_msg.Length - 1;
                    string check_magnetic = Convert.ToString(g_tag_msg[tag_magnetic]);
                    #region Old Version
                    if ((g_tag_msg[0] == 'M' && g_tag_msg[1] == 'C') || (((g_type_of_msg == "8") && (g_tag_msg[tag_magnetic] != 'I' && g_tag_msg[tag_magnetic] != 'O' && g_tag_msg[tag_magnetic] != 'W'))))
                    {
                        //MC,24,2,0004552,00100  มีการรูดบัตร
                        if ((g_type_of_msg == "D" && g_tag_msg.Length > 0 && g_tag_msg[0] == 'M' && g_tag_msg[1] == 'C') || (g_type_of_msg == "8" && g_tag_msg.Length > 0))
                        {
                            #region split string
                            int driver_type = -1;
                            int driver_sex = -1;
                            int driver_number = -1;
                            int province_code = -1;

                            if (g_type_of_msg == "D")
                            {
                                string[] spit = g_tag_msg.Split(',');
                                if (spit.Length != 5) { return; }
                                driver_type = Convert.ToInt32(spit[1]);
                                driver_sex = Convert.ToInt32(spit[2]);
                                driver_number = Convert.ToInt32(spit[3]);
                                province_code = Convert.ToInt32(spit[4]);
                            }
                            else if (g_type_of_msg == "8")
                            {
                                string[] spit = g_tag_msg.Split(',');
                                if (spit.Length < 6) { return; }

                                driver_type = Convert.ToInt32(spit[3]);
                                driver_sex = Convert.ToInt32(spit[4]);
                                driver_number = Convert.ToInt32(spit[5]);
                                province_code = Convert.ToInt32(spit[6]);
                            }
                            #endregion
                            //get magnetic_id
                            if (driver_type == 14 || driver_type == 24)  //รูดบัตรแล้ว บัตรถูกประเภท
                            {
                                event_type = EVT_TYPE.VALID_CARD;
                                string Sdriver_type = Convert.ToString(driver_type);
                                string Sdriver_sex = Convert.ToString(driver_sex);
                                string Sdriver_number = Convert.ToString(driver_number);
                                string Sprovince_code = Convert.ToString(province_code);
                                string driverid = Sdriver_type.PadLeft(2, '0') + Sdriver_sex.PadLeft(1, '0') + Sdriver_number.PadLeft(7, '0') + Sprovince_code.PadLeft(5, '0');
                                string licenseid = "";
                                oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                            }
                            else if (driver_type > 0)
                            {
                                event_type = EVT_TYPE.INVALID_CARD;  //รูดบัตรแล้ว บัตรผิดประเภท
                                //oMagneTic.InsertNewRecord(g_ref_idx, (int)event_type);
                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                            }
                            else
                            {
                                event_type = EVT_TYPE.CARD_DATA_ERROR;  //รูดบัตรแล้ว ข้อมูลไม่ครบ
                                //oMagneTic.InsertNewRecord(g_ref_idx, (int)event_type);
                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                            }
                        }
                        else //ไม่ได้รูดบัตร
                        {
                            if (oMagneTic.GetVehicleMagneticType(g_veh_id))
                            {
                                DateTime dtLastRec = new DateTime();
                                /**********SQL2**********/
                                //if (!oMagneTic.CheckLastNocard_Log(g_veh_id, out dtLastRec)) { }

                                /**********SQL1**********/
                                //if (!oMagneTic.CheckLastNocard(g_veh_id, out dtLastRec))

                                if (!oMagneTic.CheckLastNocard_Log(g_veh_id, out dtLastRec))
                                {
                                    var start = DateTime.Now;

                                 TimeSpan duration = start - dtLastRec;

                                 if (duration.TotalMinutes > 10)
                                 {
                                     //rose comment
                                     //int deley = 1000;

                                     //DataTable newweb = oMagneTic.GetVehicleMagneticParam_new(g_veh_id);
                                     //DataTable dtRet = new DataTable();
                                     //if (newweb.Rows.Count > 0)
                                     //{
                                     //    dtRet = oMagneTic.GetVehicleMagneticParam_new(g_veh_id);
                                     //}
                                     //else
                                     //{
                                     //    dtRet = oMagneTic.GetVehicleMagneticParam(g_veh_id);
                                     //}
                                     //if (dtRet.Rows.Count > 0)
                                     //{
                                     //    foreach (DataRow dr in dtRet.Rows)
                                     //    {
                                     //        deley = Math.Min(deley, Convert.ToInt32(dr["delay"].ToString()));
                                     //    }
                                     //}

                                     //if (deley > 0)
                                     //{
                                     //    TimeSpan sp = Convert.ToDateTime(g_dtTimestampe) - dtLastRec;
                                     //    if (sp.TotalMinutes >= deley && sp.TotalDays < 99)
                                     //    {
                                     //        event_type = EVT_TYPE.NO_CARD;

                                     //        /********SQL1**********/
                                     //        //oMagneTic.InsertNewRecord(g_ref_idx, (int)event_type);

                                     //        /********SQL2**********/
                                     //        oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                     //    }
                                     //}
                                     //else
                                     //{
                                     event_type = EVT_TYPE.NO_CARD;
                                     /********SQL1**********/
                                     //oMagneTic.InsertNewRecord(g_ref_idx, (int)event_type);

                                     /********SQL2**********/
                                     oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                     //}

                                 }
                                }
                            }
                        }
                    }
                    #endregion
                    #region New Version
                    bool check_format = false;

                    if ((g_tag_msg[0] == 'M' && g_tag_msg[1] == 'G') || (g_tag_msg[0] == 'R' && g_tag_msg[1] == 'F' && g_tag_msg[2] == 'I' && g_tag_msg[3] == 'D') || ((g_type_of_msg == "8") && (g_tag_msg[tag_magnetic] == 'I' || g_tag_msg[tag_magnetic] == 'O' || g_tag_msg[tag_magnetic] == 'W')))
                    {
                        string type = "00";
                        string sex = "0";
                        string number = "0000000";
                        string province_code = "00000";
                        if (g_type_of_msg == "D")
                        {
                            if (g_tag_msg[0] == 'M' && g_tag_msg[1] == 'G')
                            {
                                #region MBox
                                string[] tag_msg = g_tag_msg.Split(',');
                                if (tag_msg[10] == "0") //9 ollogout
                                {
                                    type = tag_msg[1];
                                    sex = tag_msg[2];
                                    number = tag_msg[3];
                                    province_code = tag_msg[4];
                                    event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                }
                                else
                                {
                                    if (tag_msg[10] == "I") //valid card login
                                    {
                                        type = tag_msg[6];
                                        sex = tag_msg[7];
                                        number = tag_msg[8];
                                        province_code = tag_msg[9];
                                        check_format = true;
                                    }
                                    else if (tag_msg[10] == "W") //invalid card
                                    {
                                        type = tag_msg[6];
                                        sex = tag_msg[7];
                                        number = tag_msg[8];
                                        province_code = tag_msg[9];
                                    }
                                    if (check_format)
                                    {
                                        int Magnetic_Type = 0;
                                        string driverid = "";
                                        string licenseid = "";
                                        DataTable dtMagnetic = oMagneTic.GetMagnetic(g_veh_id);
                                        if (dtMagnetic.Rows.Count > 0)
                                        {
                                            foreach (DataRow drMagnetic in dtMagnetic.Rows)
                                            {
                                                Magnetic_Type = (int)drMagnetic["Magnetic_Card"];
                                            }
                                        }
                                        string input_type = type.PadLeft(2, '0');
                                        string MG_Type = Convert.ToString(Magnetic_Type).PadLeft(2, '0');
                                        if (Convert.ToInt32(input_type.Substring(0, 1)) >= Convert.ToInt32(MG_Type.Substring(0, 1)))
                                        {
                                            if (Convert.ToInt32(input_type.Substring(1, 1)) >= Convert.ToInt32(MG_Type.Substring(1, 1))) //รูดบัตรแล้ว บัตรถูกประเภท
                                            {
                                                event_type = EVT_TYPE.VALID_CARD;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                                licenseid = "";
                                                oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                                if (tag_msg[10] == "I")
                                                {
                                                    if (tag_msg[1] != "0")
                                                    {
                                                        event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                                        oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                    }
                                                    event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                }
                                                else if (tag_msg[10] == "0")
                                                {
                                                    driverid = "";
                                                    oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                                    event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                }
                                            }
                                            else
                                            {
                                                event_type = EVT_TYPE.INVALID_CARD;  //รูดบัตรแล้ว บัตรผิดประเภท
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                                licenseid = "";
                                                oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);

                                                if ((tag_msg[1] == "0") && (tag_msg[2] == "0") && (tag_msg[3] == "00000000") && (tag_msg[4] == "00000")) //ใช้บัตรผิดใบอื่นที่ไม่ใช่ใบเดิม
                                                {
                                                    event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                }
                                                else
                                                {
                                                    event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                    event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        event_type = EVT_TYPE.INVALID_CARD;  //รูดบัตรแล้ว บัตรผิดประเภท
                                        oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        string driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                        string licenseid = "";
                                        oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                        if (tag_msg[10] == "W")
                                        {
                                            if ((tag_msg[1] == "0") && (tag_msg[2] == "0") && (tag_msg[3] == "00000000") && (tag_msg[4] == "00000")) //ใช้บัตรผิดใบอื่นที่ไม่ใช่ใบเดิม
                                            {
                                                event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            }
                                            else
                                            {
                                                event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            }

                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (g_tag_msg[0] == 'R' && g_tag_msg[1] == 'F' && g_tag_msg[2] == 'I' && g_tag_msg[3] == 'D')
                            {
                                #region Meitrack
                                type = g_tag_msg.Substring(4, 2);
                                sex = g_tag_msg.Substring(18, 1);
                                number = g_tag_msg.Substring(31, 7);// 7 or 8
                                province_code = g_tag_msg.Substring(40, 5);
                                
                                int Magnetic_Type = 0;
                                string driverid = "";
                                string licenseid = "";
                                DataTable dtMagnetic = oMagneTic.GetMagnetic(g_veh_id);
                                if (dtMagnetic.Rows.Count > 0)
                                {
                                    foreach (DataRow drMagnetic in dtMagnetic.Rows)
                                    {
                                        Magnetic_Type = (int)drMagnetic["Magnetic_Card"];
                                    }
                                }
                                string input_type = type.PadLeft(2, '0');
                                string MG_Type = Convert.ToString(Magnetic_Type).PadLeft(2, '0');
                                if (Convert.ToInt32(input_type.Substring(0, 1)) >= Convert.ToInt32(MG_Type.Substring(0, 1)))
                                {
                                    if (Convert.ToInt32(input_type.Substring(1, 1)) >= Convert.ToInt32(MG_Type.Substring(1, 1))) //รูดบัตรแล้ว บัตรถูกประเภท
                                    {
                                        event_type = EVT_TYPE.VALID_CARD;
                                    }
                                    else
                                    {
                                        event_type = EVT_TYPE.INVALID_CARD;  //รูดบัตรแล้ว บัตรผิดประเภท
                                    }
                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                    driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                    licenseid = "";
                                    oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                    string dtdriverid = oMagneTic.GetDriverID(g_veh_id);
                                    if (driverid != dtdriverid)
                                    {
                                        if (dtdriverid != "0000000000000000")
                                        {
                                            event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                            oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        }
                                        event_type = EVT_TYPE.DRIVER_LOG_IN;
                                        oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        oMagneTic.UpdateIDMeitrack(driverid, g_veh_id);
                                    }
                                    else
                                    {
                                        driverid = "";
                                        oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                        event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                        oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        oMagneTic.UpdateIDMeitrack("0000000000000000", g_veh_id);
                                    }
                                    //update ID to table Meitrack
                                }
                                else
                                {
                                    event_type = EVT_TYPE.INVALID_CARD;  //รูดบัตรแล้ว บัตรผิดประเภท
                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                    driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                    licenseid = "";
                                    oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                    string dtdriverid = oMagneTic.GetDriverID(g_veh_id);
                                    if (driverid != dtdriverid)
                                    {
                                        if (dtdriverid != "0000000000000000")
                                        {
                                            event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                            oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        }
                                        event_type = EVT_TYPE.DRIVER_LOG_IN;
                                        oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        oMagneTic.UpdateIDMeitrack(driverid, g_veh_id);
                                    }
                                    else
                                    {
                                        event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                        oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        oMagneTic.UpdateIDMeitrack("0000000000000000", g_veh_id);
                                    }
                                }
                                #endregion
                            }
                        }
                        else if (g_type_of_msg == "8")
                        {
                            #region KBox
                            string[] tag_msg = g_tag_msg.Split(',');
                            if (tag_msg[9] == "O") //louout
                            {
                                type = tag_msg[0];
                                sex = tag_msg[1];
                                number = tag_msg[2];
                                province_code = tag_msg[3];
                                event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                            }
                            else
                            {
                                if (tag_msg[9] == "I") //login
                                {
                                    type = tag_msg[5];
                                    sex = tag_msg[6];
                                    number = tag_msg[7];
                                    province_code = tag_msg[8];
                                    check_format = true;
                                }
                                else if (tag_msg[9] == "W") //invalid card
                                {
                                    type = tag_msg[5];
                                    sex = tag_msg[6];
                                    number = tag_msg[7];
                                    province_code = tag_msg[8];
                                }
                                if (check_format)
                                {
                                    int Magnetic_Type = 0;
                                    string driverid = "";
                                    string licenseid = "";
                                    DataTable dtMagnetic = oMagneTic.GetMagnetic(g_veh_id);
                                    if (dtMagnetic.Rows.Count > 0)
                                    {
                                        foreach (DataRow drMagnetic in dtMagnetic.Rows)
                                        {
                                            Magnetic_Type = (int)drMagnetic["Magnetic_Card"];
                                        }
                                    }
                                    string input_type = type.PadLeft(2, '0');
                                    string MG_Type = Convert.ToString(Magnetic_Type).PadLeft(2, '0');
                                    if (Convert.ToInt32(input_type.Substring(0, 1)) >= Convert.ToInt32(MG_Type.Substring(0, 1)))
                                    {
                                        if (Convert.ToInt32(input_type.Substring(1, 1)) >= Convert.ToInt32(MG_Type.Substring(1, 1))) //รูดบัตรแล้ว บัตรถูกประเภท
                                        {
                                            event_type = EVT_TYPE.VALID_CARD;
                                            oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                            licenseid = "";
                                            oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                            if (tag_msg[9] == "I")
                                            {
                                                if (tag_msg[0] != "0")
                                                {
                                                    event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                }
                                                event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            }
                                            else if (tag_msg[9] == "O")
                                            {
                                                event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            }
                                        }
                                        else
                                        {
                                            event_type = EVT_TYPE.INVALID_CARD;  //รูดบัตรแล้ว บัตรผิดประเภท
                                            oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                            licenseid = "";
                                            oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);

                                            if ((tag_msg[0] == "0") && (tag_msg[2] == "0") && (tag_msg[3] == "00000000") && (tag_msg[4] == "00000")) //ใช้บัตรผิดใบอื่นที่ไม่ใช่ใบเดิม
                                            {
                                                event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            }
                                            else
                                            {
                                                event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                                event_type = EVT_TYPE.DRIVER_LOG_IN;
                                                oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    event_type = EVT_TYPE.INVALID_CARD;  //รูดบัตรแล้ว บัตรผิดประเภท
                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                    string driverid = type.PadLeft(2, '0') + sex.PadLeft(1, '0') + number.PadLeft(7, '0') + province_code.PadLeft(5, '0');
                                    string licenseid = "";
                                    oMagneTic.InsertDriverID(g_veh_id, driverid, licenseid);
                                    if (tag_msg[9] == "W")
                                    {
                                        if ((tag_msg[1] == "0") && (tag_msg[2] == "0") && (tag_msg[3] == "00000000") && (tag_msg[4] == "00000")) //ใช้บัตรผิดใบอื่นที่ไม่ใช่ใบเดิม
                                        {
                                            event_type = EVT_TYPE.DRIVER_LOG_IN;
                                            oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        }
                                        else
                                        {
                                            event_type = EVT_TYPE.DRIVER_LOG_OUT;
                                            oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                            event_type = EVT_TYPE.DRIVER_LOG_IN;
                                            oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                        }

                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
                else //rose test gen event nocard //ไม่รูดบัตร
                {
                    if(g_tag_msg == "")
                    {
                        if (oMagneTic.GetVehicleMagneticType(g_veh_id))
                        {
                            DateTime dtLastRec = new DateTime();


                            if (!oMagneTic.CheckLastNocard_Log(g_veh_id, out dtLastRec))
                            {
                                var start = DateTime.Now;

                                 TimeSpan duration = start - dtLastRec;

                                if (duration.TotalMinutes > 10)
                                {
                                    event_type = EVT_TYPE.NO_CARD;

                                    oMagneTic.InsertNewRecord_Log(g_log_index, g_veh_id, g_TimeStamp, (int)event_type, -1, -1);
                                }
                            }
                        }

                    }
                   


                }
                if (IsEnableBroadcastQ && event_type != EVT_TYPE.NONE)
                {
                    ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                    info.idx = -1;
                    info.type = (int)event_type;
                    info.veh_id = g_veh_id;
                    info.msg = g_speed.ToString();
                    info.timestamp = g_dtTimestampe;
                    info.location_t = GetLocation(g_lon, g_lat);
                    info.location_e = string.Empty;
                    info.lat = g_lat;
                    info.lon = g_lon;
                    info.speed = g_speed;
                    SMSQ.PushQ(info);
                    EMAILQ.PushQ(info);
                }

            }
            catch (Exception ex)
            {
            }
        }
        private void DetectNoCardEvent()
        {
            try
            {
                if(true)
                {
                   // EVT_TYPE.NO_CARD
                }
            }
            catch
            {

            }
        }
        private void DetectSmartCardEvent()
        {
            try
            {
                if (g_type_of_msg == "D" && g_tag_msg.Length > 0 && g_tag_msg[0] == 'C')
                {
                    SMARTCARDEVENT iAddress1 = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT;
                    string[] spit = g_tag_msg.Split(',');
                    string sAddress2 = "";
                    string sData = "";
                    switch (g_tag_msg[1])
                    {
                        case 'P':/// CP**********,AA,3460200267001   inser   Enanble,Valid
                            {
                                if (spit.Length < 2)  return; 
                                iAddress1 = SMARTCARDEVENT.VALID_SMARTCARD_INSERT;
                                sAddress2 = spit[1];    // AA
                                sData = spit[2];        //3460200267001
                               
                                /*********SQL1**********/
                                //oSmartCard.InsertNewRecord(g_veh_id, g_TimeStamp, g_ref_idx, (int)iAddress1, sAddress2, sData, g_tag_msg);
                                /*********SQL2**********/
                                oSmartCard.InsertNewRecord_Log(g_veh_id, g_TimeStamp, g_log_index, (int)iAddress1, sAddress2, sData, g_tag_msg);
                            }
                            break;
                        case 'E': /// CE                              insert  Invalid
                            {
                                if (spit.Length != 1)  return; 
                                iAddress1 = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT;

                                /*********SQL1**********/
                                //oSmartCard.InsertNewRecord(g_veh_id, g_TimeStamp, g_ref_idx, (int)iAddress1, sAddress2, sData, g_tag_msg);
                                /*********SQL2**********/
                                oSmartCard.InsertNewRecord_Log(g_veh_id, g_TimeStamp, g_log_index, (int)iAddress1, sAddress2, sData, g_tag_msg);
                            }
                            break;
                        case 'p':/// Cp**********,AA,3460200267001   insert  Disable,Valid
                            {
                                if (spit.Length < 2) return; 
                                iAddress1 = SMARTCARDEVENT.VALID_BUT_DISABLE_SMART_CARD;
                                sAddress2 = spit[1];    //AA
                                sData = spit[2];        //3460200267001 

                                /*********SQL1**********/
                                //oSmartCard.InsertNewRecord(g_veh_id, g_TimeStamp, g_ref_idx, (int)iAddress1, sAddress2, sData, g_tag_msg);
                                /*********SQL2**********/
                                oSmartCard.InsertNewRecord_Log(g_veh_id, g_TimeStamp, g_log_index, (int)iAddress1, sAddress2, sData, g_tag_msg);
                            }
                            break;
                        case 'D': /// CD                              remove
                            {
                                if (spit.Length != 1) return;
                                iAddress1 = SMARTCARDEVENT.SMART_CATD_REMOVAL;

                                /*********SQL1**********/
                                //oSmartCard.InsertNewRecord(g_veh_id, g_TimeStamp, g_ref_idx, (int)iAddress1, sAddress2, sData, g_tag_msg);
                                /*********SQL2**********/
                                oSmartCard.InsertNewRecord_Log(g_veh_id, g_TimeStamp, g_log_index, (int)iAddress1, sAddress2, sData, g_tag_msg);
                            }
                            break;
                    }
                }
            }
            catch 
            {
            }
        }
        private void DetectRPMEvent()
        {
            try
            {
                //if (g_tag_msg.Length > 0 && g_tag_msg.IndexOf("ER") > -1)
                //{
                //    int index_start = g_tag_msg.IndexOf("ER");
                //    string[] data = g_tag_msg.Substring(index_start, g_tag_msg.Length - index_start).Split(',');
                //}
            }
            catch
            {
            }
        }

        private void DetectDistance_Over()
        {
            int veh_id, delaytime = 0;
            veh_id = Convert.ToInt32(g_veh_id);
            DataTable CheckDistanceOver = oDistance_Over.GetFleetDistance_Over(veh_id);
            foreach (DataRow drF in CheckDistanceOver.Rows)
            {
                if (g_no_of_satellite >= 3 && g_type_of_fix > 0 && g_speed < 175)
                {
                    double distance = 0.00;
                    double distance_over = 0.00;
                    double get_distance = 0.00;

                    //กำหนดระยะทางห้ามเกิน
                    double over_distance = Convert.ToDouble((String)drF["distance_over"]);

                    DateTime timestart = new DateTime();
                    DateTime timestop = new DateTime();

                    try
                    {
                        //Event Distance Over
                        //กำหนดช่วงเวลาเพื่อหาข้อมูลรถ
                        DateTime curTimestamp = g_TimeStamp;
                        string StartT = (string)drF["start_time"];
                        string StopT = (string)drF["stop_time"];
                        String dtStart = curTimestamp.ToString("MM/dd/yyyy " + StopT);
                        String dtStop = curTimestamp.ToString("MM/dd/yyyy " + StartT);
                        timestart = Convert.ToDateTime(dtStart);
                        timestop = Convert.ToDateTime(dtStop);
                        timestart = timestart.AddDays(-1);
                        distance = oDistance_Over.GetDistance_Now(g_veh_id);
                        get_distance = oDistance_Over.GetDistance_Over(veh_id, timestart, timestop);
                        distance_over = distance - get_distance;
                        if (distance_over > over_distance)
                        {
                            DataTable CheckLastEvent  = oDistance_Over.GetLastEvent(veh_id);
                            delaytime = Convert.ToInt32((string)drF["delay_time"]);
                            foreach (DataRow dr in CheckLastEvent.Rows)
                            {
                                int lastevt = (int)dr["evt_id"];
                                TimeSpan sptimeevt = curTimestamp - (DateTime)dr["local_timestamp"];
                                bool isDistance_Over = false;

                                if (lastevt != (Int32)EVT_TYPE.DISTANCE_OVER && (int)sptimeevt.TotalMinutes > delaytime)
                                {
                                    isDistance_Over = true;
                                }
                                else if (lastevt == (Int32)EVT_TYPE.DISTANCE_OVER && (int)sptimeevt.TotalMinutes > delaytime)
                                {
                                    isDistance_Over = true;
                                }

                                if (isDistance_Over)
                                {
                                    oDistance_Over.InsertNewRecord(g_log_index, g_veh_id, g_TimeStamp);
                                    if (IsEnableBroadcastQ)
                                    {
                                        ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                                        info.idx = -1;
                                        info.type = (int)EVT_TYPE.DISTANCE_OVER;
                                        info.veh_id = g_veh_id;
                                        info.msg = distance_over.ToString() + " km.";
                                        info.timestamp = g_dtTimestampe;
                                        info.location_t = GetLocation(g_lon, g_lat);
                                        info.location_e = string.Empty;
                                        info.lat = g_lat;
                                        info.lon = g_lon;
                                        info.speed = g_speed;
                                        SMSQ.PushQ(info);
                                        EMAILQ.PushQ(info);
                                    }
                                }

                            }
                        }
                    }
                    catch
                    { }
                }
            }
        }
        public string GetLocation(float lon, float lat)
        {
            if (oLoc == null)
            {
                return "?";
            }

            //string location_e = "NO LOCATION";
            string location_e = "ไม่มีสัญญาณ GPS";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    //location_e = oLoc.GetCustomLandmarkName(id, 0);
                    location_e = oLoc.GetCustomLandmarkName(id, 1);
                }
                else
                {
                    id = oLoc.DetectLandmark(g_lon, g_lat);
                    //location_e = oLoc.GetLandmarkName(id, 0);
                    location_e = oLoc.GetLandmarkName(id, 1);
                }

                id = oLoc.DetectAdminPoly(g_lon, g_lat);
                //location_e += " " + oLoc.GetPolyName(id, 0);
                location_e += " " + oLoc.GetPolyName(id, 1);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_e = location_e.TrimStart(new char[] { ' ' });
            }

            return location_e;
        }
        public string GetLocation(float lon, float lat, int lang)
        {
            if (oLoc == null)
            {
                return "?";
            }

            string location_e = "NO LOCATION";
            string location_t = "ไม่มีสัญญาณ GPS";

            if (lon > 0.0 && lat > 0.0)
            {
                int id = oLoc.DetectCustomLandmark(lon, lat);
                if (id > -1)
                {
                    location_e = oLoc.GetCustomLandmarkName(id, 0);
                    location_t = oLoc.GetCustomLandmarkName(id, 1);
                }
                else
                {
                    id = oLoc.DetectLandmark(lon, lat);
                    location_e = oLoc.GetLandmarkName(id, 0);
                    location_t = oLoc.GetLandmarkName(id, 1);
                }

                id = oLoc.DetectAdminPoly(lon, lat);
                location_e += " " + oLoc.GetPolyName(id, 0);
                location_t += " " + oLoc.GetPolyName(id, 1);

                location_e = location_e.TrimEnd(new char[] { ' ' });
                location_t = location_t.TrimEnd(new char[] { ' ' });

                location_e = location_e.TrimStart(new char[] { ' ' });
                location_t = location_t.TrimStart(new char[] { ' ' });
            }

            if (lang == 1)
            {
                return location_t;
            }

            return location_e;
        }

        /// <summary>
        /// True Corp. True IO Status
        /// Modifly : Jirayu Kamsiri
        /// Last : 2012/09/12
        /// </summary>
        /// <param name="enableQ"></param>
        private void DetectTrueIOStatus()
        {
            //True Only For Check Type MSG
            if(!(g_type_of_msg == "i" || g_type_of_msg == "I" || g_type_of_msg == "+" || g_type_of_msg == "X"||g_type_of_msg == "x")){ return; }
            DataTable dtVehInGroup = oTrueZone.True2IsVehicleTrueCorp(g_veh_id);
            if (dtVehInGroup.Rows.Count < 1) 
            { 
                return;
            }
            oTrueIO.DetectStatus(g_log_index, g_veh_id, g_in_out_status, g_type_of_msg, IsEnableBroadcastQTRUE, g_speed, g_dtTimestampe, g_lat, g_lon, SMSQ, EMAILQ, g_tag_msg);
        }

        /// <summary>
        /// True Corp. Event New
        /// TrueFencing,TrueFencing2,True IO
        /// Modifly : Jirayu Kamsiri
        /// Last ModiFly : 2012/09/07
        /// </summary>
        /// 
        private void DetectFencingForTrueCorp()
        {
            if (g_lat == 0.0 || g_lon == 0.0 || g_no_of_satellite <= 3 || g_type_of_fix == 0) return;

            //Check and Update Veh_id and reg
            DataTable dtVehInGroup = oTrueZone.True2IsVehicleTrueCorp(g_veh_id);

            if (dtVehInGroup.Rows.Count < 1) { return; }

            try
            {
                DataTable dtVehZone = oTrueZone.True2GetVehicleGroupZone(g_veh_id);

                foreach (DataRow drZ in dtVehZone.Rows)
                {
                    int zone_id = (int)drZ["zone_id"];
                    if (zone_id == -1) { continue; }

                    GlobalClass.TrueZoneProfile data = new GlobalClass.TrueZoneProfile();

                    if (hzTrueZone.ContainsKey(zone_id))
                    {
                        data = (GlobalClass.TrueZoneProfile)hzTrueZone[zone_id];
                    }
               
                    if (data.zone_id > 0 && data.dtWp.Rows.Count > 0 && data.dtWp != null)
                    {
                        FencingCls.SIDE_TYPE side = FencingCls.SIDE_TYPE.NONE;

                        if (oFencing.DetectFencingTrueCorp(g_veh_id, g_lat, g_lon, (float)data.lat_min, (float)data.lat_max, (float)data.lon_min, (float)data.lon_max, data.clkdir, data.lastedUpdate, data.dtWp, out side))
                        {
                            int last_zone_id = -1;
                            int last_evt_id = -1;

                            /*********SQL2********/
                            oFencing.True2GetZoneLastStatus_Log(g_veh_id, data.zone_id, out last_evt_id, out last_zone_id);

                            if (side == FencingCls.SIDE_TYPE.IN && last_evt_id == (int)EVT_TYPE.IN_ZONE && last_zone_id == data.zone_id)
                            {
                            }
                            else if (side == FencingCls.SIDE_TYPE.OUT && last_evt_id == (int)EVT_TYPE.OUT_ZONE && last_zone_id == data.zone_id)
                            {
                            }
                            else
                            {
                                bool bIsRecord = false;
                                if (last_evt_id != -1)
                                {
                                    bIsRecord = true;
                                }
                                else if ((last_evt_id == -1) && (side == FencingCls.SIDE_TYPE.IN))
                                {
                                    bIsRecord = true;
                                }

                                if (bIsRecord)
                                {
                                    /******SQL2 OK*******/
                                    int id_insert = oFencing.True2InsertMsgZone_Log(g_log_index, data.zone_id);
                                    /******SQL2 OK*******/
                                    oFencing.True2ZoneState_Log(g_log_index, g_veh_id, data.zone_id);

                                    // insert Type Group
                                    if (id_insert > -1)
                                    {
                                        DataTable dtZoneOfType = oTrueZone.True2GetVehicleGroupZoneType(g_veh_id);

                                        if (dtZoneOfType.Rows.Count > 0)
                                        {
                                            DataRow[] dr = dtZoneOfType.Select("zone_id = " + zone_id.ToString());
                                            
                                            if (dr.Length > 0)
                                            {
                                                for (int i = 0; i < dr.Length; i++)
                                                {
                                                    /******SQL2*******/
                                                    oFencing.True2InsertMsgZoneTypeGroup_Log(id_insert, (int)dr[i]["group_id"], (int)dr[i]["type_id"]);
                                                }
                                            }
                                        }
                                    }

                                    if (IsEnableBroadcastQTRUE)
                                    {
                                        ZEBRABROADCASTMQINFO info = new ZEBRABROADCASTMQINFO();
                                        info.idx = -1;
                                        info.type = (int)EVT_TYPE.TRUE_ZONEING;
                                        info.veh_id = g_veh_id;
                                        string msg = side == FencingCls.SIDE_TYPE.IN ? "เข้าโซน" : "ออกโซน";
                                        //evt_id,message,zone_id
                                        int evt_id = side == FencingCls.SIDE_TYPE.IN ? (int)EVT_TYPE.IN_ZONE : (int)EVT_TYPE.OUT_ZONE;
                                        info.msg = evt_id.ToString() + "," + msg + data.zone_desc + "," + zone_id.ToString();
                                        info.timestamp = g_dtTimestampe;
                                        info.location_t = GetLocation(g_lon, g_lat);
                                        info.lat = g_lat;
                                        info.lon = g_lon;
                                        info.speed = g_speed;
                                        SMSQ.PushQ(info);
                                        EMAILQ.PushQ(info);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        public void GetTrueZone()
        {
            try
            {
                DataTable dtAllTrueZone = oTrueZone.True2GetAllTrueZone();

                foreach (DataRow dr in dtAllTrueZone.Rows)
                {
                    int zone_id = (int)dr["zone_id"];

                    GlobalClass.TrueZoneProfile info = GetTrueZoneInfo(zone_id);

                    if (info.zone_id > 0 && info.dtWp.Rows.Count > 0)
                    {
                        if (GlobalClass.hsZoneTrueList.ContainsKey(zone_id))
                        {
                            GlobalClass.hsZoneTrueList[zone_id] = info;
                        }
                        else
                        {
                            GlobalClass.hsZoneTrueList.Add(zone_id,info);
                        }
                    }
                    else
                    {
                        if (GlobalClass.hsZoneTrueList.ContainsKey(zone_id))
                        {
                            GlobalClass.hsZoneTrueList.Remove(zone_id);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
 
            }
        }
        private GlobalClass.TrueZoneProfile GetTrueZoneInfo(int zone_id)
        {
            GlobalClass.TrueZoneProfile info = new GlobalClass.TrueZoneProfile();
           
            try
            {
                DataTable newweb = oFencing.GetZoneProfile_new(zone_id);
                DataTable pfTable = new DataTable();
                if (newweb.Rows.Count > 0)
                {
                    pfTable = oFencing.GetZoneProfile_new(zone_id);
                }
                else
                {
                    pfTable = oFencing.GetZoneProfile(zone_id);
                }

                if (pfTable.Rows.Count > 0)
                {
                    info.zone_id = (int)pfTable.Rows[0]["zone_id"];
                    info.zone_desc = (string)pfTable.Rows[0]["zone_desc"];
                    info.lat_min = (double)pfTable.Rows[0]["lat_min"];
                    info.lat_max = (double)pfTable.Rows[0]["lat_max"];
                    info.lon_min = (double)pfTable.Rows[0]["lon_min"];
                    info.lon_max = (double)pfTable.Rows[0]["lon_max"];
                    info.clkdir = (Boolean)pfTable.Rows[0]["clockwise"];
                    info.lastedUpdate = (DateTime)pfTable.Rows[0]["lasted_update"];

                    DataTable wpTable = oFencing.GetZoneWayPoints(zone_id);
                    DataTable wp = new DataTable();
                    wp.Columns.Add("X", typeof(float));
                    wp.Columns.Add("Y", typeof(float));
                    foreach (DataRow wp_row in wpTable.Rows)
                    {
                        double wpX = (double)wp_row["lon"];
                        double wpY = (double)wp_row["lat"];
                        wp.Rows.Add(new object[] { (float)wpX, (float)wpY });
                    }

                    info.dtWp = wp;
                    info.is_update = false;
                    info.timeExpire = DateTime.Now.AddHours(1);
                }
            }
            catch { }
            return info;
        }
        private String GetTimeStamp(String date, String time)
        {
            try
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                d = d.AddHours(7.0);
                String szTimeStamp = d.ToString("yyyy-MM-dd HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server(Thread) #3" + ex.Message);
            }

            return "";
        }

    }
}
