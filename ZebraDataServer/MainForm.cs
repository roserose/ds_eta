﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Threading;
using System.Globalization;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Diagnostics;
using System.Collections;

using ZebraDataCentre;
using ZebraDataInterface;
using ZebraCommonInterface;
using ZebraPresentationLayer;
using ZebraLogTools;
using System.Net;
using System.Web.Script.Serialization;

namespace ZebraDataServer
{
    #region ENUMUNIC
    public enum CONTROLID
    {
        MAX_SPEED,
        MAX_IDLE, 
        MAX_ZONE_TIMEID,
        MAX_ZONE_CURFEW,
        MAX_ZONE_CURFEWSPEED,
        MAX_LOCATION, 
        MAX_ROUTING,
        MAX_ETA1,
        MAX_ETA2, 
        MAX_IOSTATUS,
        MAX_ENGINEONEVEN,
        MAX_TEMPERATURE, 
        MAX_ENGINEEVENTZONE,
        MAX_SPEEDHAZRD, 
        MAX_ANGLEHAZARD, 
        MAX_TRUEFENCING, 
        MAX_TRUEIO,
        MAX_Genaral,
        MAX_LogError,
        MAX_Magnetic,
        MAX_SmartCard,
        MAX_RPM,

        MAX_LOG_MSG,
        MAX_PROCESS_MSG,
        TOTAL_COMMITLOG_SEC,
        TOTAL_CURTHDID,
        TOTAL_DATA
    };
    public enum PROFILEMENUID
    {
        FLEET,
        FLEET_MENU,
        FLEET_LIST,
        VEHICLE,
        VEHICLE_MENU,
        VEHICLE_LIST,
        FLEETTOOL,
        FLEETTOOL_FENCING,
        FLEETTOOL_FENCING_MENU,
        FLEETTOOL_FENCING_LIST,
        FLEETTOOL_ROUTING,
        FLEETTOOL_ROUTING_MENU,
        FLEETTOOL_ROUTING_LIST,
        FLEETTOOL_MISCTOOL,
        FLEETTOOL_MISCTOOL_ETA,
        FLEETTOOL_MISCTOOL_MARKLOCATION,
        FLEETTOOL_MISCTOOL_EDITFLEET,
        FLEETTOOL_MISCTOOL_EDITVEHICLE,
        FLEETTOOL_MISCTOOL_MODEL,
        FLEETTOOL_MISCTOOL_USER,
        FLEETTOOL_MISCTOOL_DRIVER,
        FLEETTOOL_MISCTOOL_PROFILE,
        FLEETTOOL_MISCTOOL_SELECT_EVENT,
        PREFERRENCE,
        PREFERRENCE_SETEVENTCOLOR,
        PREFERRENCE_SETSOUND,
        PREFERRENCE_SHOWDETAILONMAP,
        REPORT,
        REPORT_ACTIVITIES,
        REPORT_SPEED,
        REPORT_SPEEDING,
        REPORT_IDLE,
        REPORT_INOUTZONE,
        REPORT_SPEEDGRAPH,
        REPORT_FUELLEVEL,
        REPORT_NOGPS,
        REPORT_ALLREPORT,
        REPORT_PLAYBACK,

        FLEETTOOL_MISCTOOL_EDITFLEET_ADDNEW,
        FLEETTOOL_MISCTOOL_EDITFLEET_EDIT,
        FLEETTOOL_MISCTOOL_EDITFLEET_DELETE,

        FLEETTOOL_MISCTOOL_EDITVEHICLE_ADDNEW,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_EDIT,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_DELETE,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_MOVE,
        FLEETTOOL_MISCTOOL_EDITVEHICLE_COPY,

        FLEETTOOL_MISCTOOL_MODEL_ADDNEW,
        FLEETTOOL_MISCTOOL_MODEL_EDIT,
        FLEETTOOL_MISCTOOL_MODEL_DELETE,

        FLEETTOOL_MISCTOOL_USER_ADDNEW,
        FLEETTOOL_MISCTOOL_USER_EDIT,
        FLEETTOOL_MISCTOOL_USER_DELETE,

        FLEETTOOL_MISCTOOL_DRIVER_ADDNEW,
        FLEETTOOL_MISCTOOL_DRIVER_EDIT,
        FLEETTOOL_MISCTOOL_DRIVER_DELETE,
        FLEETTOOL_MISCTOOL_DRIVER_MOVE,

        REPORT_INOUTROUTE,
        REPORT_ENGINEONOFF,

        EVENT,
        EVENT_RESET,
        EVENT_SPEEDING,
        EVENT_IDLE,
        EVENT_HAZARD_SPEED,
        EVENT_HAZARD_ANGLE,
        EVENT_DIGITAL_SENSOR,
        EVENT_ANALOG_SENSOR,
        EVENT_LOCATION,
        EVENT_IN_ZONE,
        EVENT_OUT_ZONE,
        EVENT_NO_ZONE_CURFEW,
        EVENT_IN_ROUTE,
        EVENT_OUT_ROUTE,
        EVENT_ENGINE_ON,
        EVENT_ENGINE_OFF,
        EVENT_NO_INZONE_CURFEW,
        EVENT_NO_OUTZONE_CURFEW,

        EVENT_DFM_MAIN_LIGHT_ON,
        EVENT_DFM_MAIN_LIGHT_OFF,
        EVENT_DFM_PTO_SWITCH_ON,
        EVENT_DFM_PTO_SWITCH_OFF,
        EVENT_DFM_HORN_ON,
        EVENT_DFM_HORN_OFF,
        EVENT_DFM_MINOR_LIGHT_ON,
        EVENT_DFM_MINOR_LIGHT_OFF,
        EVENT_DFM_LEFT_SIGNAL_ON,
        EVENT_DFM_LEFT_SIGNAL_OFF,
        EVENT_DFM_RIGHT_SIGNAL_ON,
        EVENT_DFM_RIGHT_SIGNAL_OFF,
        EVENT_DFM_WIPER_ON,
        EVENT_DFM_WIPER_OFF,
        EVENT_DFM_REVERSE_LIGHT_ON,
        EVENT_DFM_REVERSE_LIGHT_OFF,

        EVENT_NO_ZONE_CURFEW_WITH_SPEED,
        EVENT_NO_INZONE_CURFEW_WITH_SPEED,
        EVENT_NO_OUTZONE_CURFEW_WITH_SPEED,

        EVENT_FUEL_OIL,
        EVENT_FUEL_OIL_ON,
        EVENT_FUEL_OIL_OFF,
        EVENT_FUEL_GAS,
        EVENT_FUEL_GAS_ON,
        EVENT_FUEL_GAS_OFF,

        EXTERNAL_INPUT01_ON,
        EXTERNAL_INPUT01_OFF,
        EXTERNAL_INPUT02_ON,
        EXTERNAL_INPUT02_OFF,
        EXTERNAL_INPUT03_ON,
        EXTERNAL_INPUT03_OFF,
        EXTERNAL_INPUT04_ON,
        EXTERNAL_INPUT04_OFF,
        EXTERNAL_INPUT05_ON,
        EXTERNAL_INPUT05_OFF,
        EXTERNAL_INPUT06_ON,
        EXTERNAL_INPUT06_OFF,
        EXTERNAL_INPUT07_ON,
        EXTERNAL_INPUT07_OFF,
        EXTERNAL_INPUT08_ON,
        EXTERNAL_INPUT08_OFF,
        EXTERNAL_INPUT09_ON,
        EXTERNAL_INPUT09_OFF,
        EXTERNAL_INPUT10_ON,
        EXTERNAL_INPUT10_OFF,

        REPORT_SPEEDINGTIME,
        REPORT_INOUTZONETIME,

        FLEETTOOL_MISCTOOL_ISEDITVEHICLEALL,
        ENABLE_SMS,

        GV_CURLOC,
        GV_CURLOC_VID,
        GV_CURLOC_REG,
        GV_CURLOC_MODEL,
        GV_CURLOC_DRIVER,
        GV_CURLOC_TIMESTAMP,
        GV_CURLOC_SPEED,
        GV_CURLOC_OIL_LEVEL,
        GV_CURLOC_LOCATION,
        GV_CURLOC_ETA,
        GV_CURLOC_CONTACT_NO,
        GV_CURLOC_TEMPERATURE,
        GV_CURLOC_SMARTCARD_DATA,

        EVENT_TEMPERATURE,
        EVENT_SMARTCARD_DATA,
        EVENT_ENGINEON_IN_PROHITBIT_TIME,
        FLEETTOOL_MISCTOOL_MARKLOCATION_SELECTION,
        FLEETTOOL_MISCTOOL_EDITFLEET_REFRESH,

        REPORT_IDLETIME,
        FLEETTOOL_MISCTOOL_POINT_SYSTEM,
        FLEETTOOL_MISCTOOL_POINT_SYSTEM_HOLIDAY,

        REPORT_ZONE_MODEL,

        PREFERRENCE_SEND_SMS,
        EVENT_GPS_DISCONNECTED,
        EVENT_POWER_LINE_DISCONNECTED,
        EVENT_USER_DEFINED1_ON,
        EVENT_USER_DEFINED1_OFF,
        EVENT_USER_DEFINED2_ON,
        EVENT_USER_DEFINED2_OFF,

        EVENT_IGNITION_LINE_ON,
        EVENT_IGNITION_LINE_OFF,
        EVENT_USER_DEFINED1_OUT_ON,
        EVENT_USER_DEFINED1_OUT_OFF,
        EVENT_USER_DEFINED2_OUT_ON,
        EVENT_USER_DEFINED2_OUT_OFF,
        EVENT_USER_DEFINED3_OUT_ON,
        EVENT_USER_DEFINED3_OUT_OFF,

        EVENT_ENGINE_ON_OFF_IN_OUTZONE,

        EVENT_POWER_LINE_CONNECTED,
        EVENT_GPS_CONNECTED
    }
    #endregion

    public partial class frmDataServer : Form
    {
        public delegate void SetTextCallback(Control ctrl, String msg);
        private System.Threading.Semaphore semaphorLock = new System.Threading.Semaphore(1, 1);

        private bool IsClosing = false;
        private bool IsRunThread = true;
        private bool IsCheckThread = false;
        private bool IsEnableForwardIP = false;
        
        private int ResetGCTimer = 0;

        private String szDatabaseServer = "";
        private String szHeaderProgram = "";
        private String szSystemQ = "";
        private CommonQeue MQ;

        private String szForwardQ = "";
        private String szForwardIP = "";
        private CommonQeue MQ_FWD;
  
        private int TotalData = 0;
        private int TotalData_Old = 0;
        private int TotalCurThread = 0;
        private int TotalStartThread = 0;
        private int TotalDataCommit = 0;

        private DateTime nextUpdateTrueZone = new DateTime();

        private Queue LogUpdateQueue;
        private Queue ThreadQueue = new Queue();

        private ExEvent oExEvent = new ExEvent();
        private ExEventIO oExEventIO = new ExEventIO();
        private GlobalClass gbClass = new GlobalClass();

        string pname = "";
        int pid = 0;

        #region Status String
        private string lbMaxGenaral = "";
        private string lbMaxSpeedTimeStr = "";
        private string lbMaxIdleTimeStr = "";
        private string lbMaxZoneTimeStr = "";
        private string lbMaxZoneCurfewTimeStr = "";
        private string lbMaxZoneCurfewWithSpeedStr = "";
        private string lbMaxLocTimeStr = "";
        private string lbMaxRoutingTimeStr = "";

        private string lbMaxEngineOnEventStr = "";
        private string lbMaxTemperatureStr = "";
        private string lbEngineOnOffInOutZoneStr = "";

        private string lbMaxTrueFencingStr = "";
        private string lbMaxTrueIOStr = "";

        private string lbMaxLogError = "";

        private string lbMaxMagnetic = "";
        private string lbMaxSmartCard = "";
        private string lbMaxRpm = "";

        private string lbMaxCommitLog = "";
        private string lbMaxCommitEvent = "";
        #endregion

        

        public frmDataServer()
        {
            try
            {
                LogUpdateQueue = new Queue();
                InitializeComponent();
                InitCfg();
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #1" + ex.Message);
            }
        }

        private void InitCfg()
        {
            try
            {
                //---- Read config. from setting file ------------------------------
                Properties.Settings pf = new ZebraDataServer.Properties.Settings();
 
                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                szHeaderProgram = pf.HeaderProgram;
                IsEnableForwardIP = pf.EnableForwardIP;
                tsSystemQ.Text = String.Format("   SystemQ : {0}", pf.SystemQ);
                v.Text = " DB Host : " + pf.DbSever;
                //this.Text = String.x("Zebra Data Server V{0} {1}", "2.87", szHeaderProgram);//DateTime.Now.ToString("yyyyMMddHHmm", new CultureInfo("en-US")));

                this.Text = "Zebra Data Server V." + System.Reflection.Assembly.GetEntryAssembly().GetName().Version + "  " + szHeaderProgram;

                szDatabaseServer = pf.DbSever;

                szSystemQ = pf.SystemQ;
                
                //---- Init Forward Q ----------------------------------------------
                szForwardIP = pf.ForwardIP;
                szForwardQ = pf.ForwardQ;
                MQ_FWD = new CommonQeue(szForwardIP, szForwardQ);

                //---- Init System Q -----------------------------------------------
                MQ = new CommonQeue(szSystemQ);
                MQ.OnReceiveMQ += new ReceiveMQ(mq_OnReceiveMQ);
                MQ.InitReceiveQMsg();

                //---- Init Remote Port----------------------------------

                if (pf.EnableInterfacePort)
                {
                    int interfacePortNo = Convert.ToInt32(pf.InterfacePort);
                    TcpChannel chanel = new TcpChannel(interfacePortNo);
                    RemotingConfiguration.RegisterWellKnownServiceType(Type.GetType("ZebraDataServer.ExEvent,ZebraDataServer"),
                        "ZebraDataServer", WellKnownObjectMode.Singleton);
                }

                //Init Zone True
                InitTrueAllZone();
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #2" + ex.Message);
            }
        }

        private void InitTrueAllZone()
        {
            try
            {
                oExEvent.GetTrueZone();
                nextUpdateTrueZone = DateTime.Now.AddHours(1);
            }
            catch(Exception ex)
            {
                gbClass.WriteLogEvent(ex.StackTrace, ex.ToString());
            }
        }

        private void InitProcessList()
        {
            ListViewItem itemMaxGenaral = this.lstPerformance.Items.Add("lbMaxGenaral", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxGenaral.SubItems.Add("Genaral"); itemMaxGenaral.SubItems.Add("?");

            ListViewItem itemMaxLogError = this.lstPerformance.Items.Add("lbMaxLogError", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxLogError.SubItems.Add("LogError"); itemMaxLogError.SubItems.Add("?");

            ListViewItem itemMaxSp = this.lstPerformance.Items.Add("lbMaxSpeedTime", (lstPerformance.Items.Count +1).ToString(), null);
            itemMaxSp.SubItems.Add("Speeding"); itemMaxSp.SubItems.Add("?");

            ListViewItem itemMaxIdle = this.lstPerformance.Items.Add("lbMaxIdleTime", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxIdle.SubItems.Add("Idle"); itemMaxIdle.SubItems.Add("?");

            ListViewItem itemMaxZn = this.lstPerformance.Items.Add("lbMaxZoneTime", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxZn.SubItems.Add("Zoning"); itemMaxZn.SubItems.Add("?");

            ListViewItem itemMaxZnCurfew = this.lstPerformance.Items.Add("lbMaxZoneCurfewTime", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxZnCurfew.SubItems.Add("Zoning Curfew"); itemMaxZnCurfew.SubItems.Add("?");
            
            ListViewItem itemMaxZnCurfewWithSpeed = this.lstPerformance.Items.Add("lbMaxZoneCurfewWithSpeed", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxZnCurfewWithSpeed.SubItems.Add("Zoning Curfew With Speed"); itemMaxZnCurfewWithSpeed.SubItems.Add("?");

            ListViewItem itemMaxLoc = this.lstPerformance.Items.Add("lbMaxLocTime", (lstPerformance.Items.Count +1).ToString(), null);
            itemMaxLoc.SubItems.Add("Location"); itemMaxLoc.SubItems.Add("?");

            ListViewItem itemMaxRoute = this.lstPerformance.Items.Add("lbMaxRoutingTime", (lstPerformance.Items.Count + 1).ToString(), null); 
            itemMaxRoute.SubItems.Add("Routing"); itemMaxRoute.SubItems.Add("?");

            ListViewItem itemMaxEngineOnEventStatus = this.lstPerformance.Items.Add("lbMaxEngineOnEventStr", (lstPerformance.Items.Count + 1).ToString(), null); 
            itemMaxEngineOnEventStatus.SubItems.Add("Engine On in Curfew  "); itemMaxEngineOnEventStatus.SubItems.Add("?");

            ListViewItem itemMaxTemperatireStatus = this.lstPerformance.Items.Add("lbMaxTemperatureStr", (lstPerformance.Items.Count + 1).ToString(), null); 
            itemMaxTemperatireStatus.SubItems.Add("Temperature"); itemMaxTemperatireStatus.SubItems.Add("?");

            ListViewItem itemEngineOnOffInOutZoneStatus = this.lstPerformance.Items.Add("lbEngineOnOffInOutZoneStr", (lstPerformance.Items.Count + 1).ToString(), null); 
            itemEngineOnOffInOutZoneStatus.SubItems.Add("Engine Event ON/OFF Zone"); itemEngineOnOffInOutZoneStatus.SubItems.Add("?");

            ListViewItem itemMaxTrueFencingStatus = this.lstPerformance.Items.Add("lbMaxTrueFencingStr", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxTrueFencingStatus.SubItems.Add("True Fencing"); itemMaxTrueFencingStatus.SubItems.Add("?");
           
            ListViewItem itemMaxTrueIOStatus = this.lstPerformance.Items.Add("lbMaxTrueIOStr", (lstPerformance.Items.Count + 1).ToString(), null); 
            itemMaxTrueIOStatus.SubItems.Add("True IO Status"); itemMaxTrueIOStatus.SubItems.Add("?");

            ListViewItem itemMaxMagnetic = this.lstPerformance.Items.Add("lbMaxMagnetic", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxMagnetic.SubItems.Add("Magnetic Event"); itemMaxMagnetic.SubItems.Add("?");

            ListViewItem itemMaxSmartCard = this.lstPerformance.Items.Add("lbMaxSmartCard", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxSmartCard.SubItems.Add("Smart Card Event"); itemMaxSmartCard.SubItems.Add("?");

            ListViewItem itemMaxRpm = this.lstPerformance.Items.Add("lbMaxRpm", (lstPerformance.Items.Count + 1).ToString(), null);
            itemMaxRpm.SubItems.Add("RPM Event"); itemMaxRpm.SubItems.Add("?");

            ListViewItem itemsp1 = this.lstPerformance.Items.Add("sp1", "", null); itemsp1.SubItems.Add(""); itemsp1.SubItems.Add("");

            ListViewItem itemCommitLog = this.lstPerformance.Items.Add("lbTimeCommitLog", "", null); itemCommitLog.SubItems.Add("Log Msg/Sec"); itemCommitLog.SubItems.Add("0");
            ListViewItem itemCommitProcess = this.lstPerformance.Items.Add("lbTimeCommitEvent", "", null); itemCommitProcess.SubItems.Add("Event Msg/Sec"); itemCommitProcess.SubItems.Add("0");
            ListViewItem itemTotalDataCommit = this.lstPerformance.Items.Add("lbTotalOfDataCommit", "", null); itemTotalDataCommit.SubItems.Add("Data Commit/Sec"); itemTotalDataCommit.SubItems.Add("0");

            ListViewItem itemTotalThrd = this.lstPerformance.Items.Add("lbTotalCurThd", "", null); itemTotalThrd.SubItems.Add("Total Current Thread : "); itemTotalThrd.SubItems.Add("?");
            ListViewItem itemTotalData = this.lstPerformance.Items.Add("lbTotalOfData", "", null); itemTotalData.SubItems.Add("Total of Data"); itemTotalData.SubItems.Add("?");

        }

        private void frmDataServer_Load(object sender, EventArgs e)
        {
            InitProcessList();
        }
        private void frmDataAgency_Shown(object sender, EventArgs e)
        {
            if (MQ != null)
            {
                TotalStartThread = Process.GetCurrentProcess().Threads.Count;
               


                MQ.BeginReceiveQMsg();
                dataServerTimer.Start();
            }
            else
            {
                MessageBox.Show("Have no Queue " + szSystemQ + " in system", "Warning!!!");
            }
        }

        private void frmDataServer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsClosing)
            {
                Process p = Process.GetCurrentProcess();
                p.Kill();
            }

            e.Cancel = !IsClosing;
            IsClosing = true;
        }

        private void mq_OnReceiveMQ(object owner, XmlReader data, Stream stmData)
        {
            try
            {
                //--- Forward Q
                if (IsEnableForwardIP) { MQ_FWD.ForwardQ(stmData); }
                //-------------
                DateTime d1 = DateTime.Now;
                XmlSerializer ser = new XmlSerializer(typeof(Max2FmtMsg));                
                Max2FmtMsg msg = (Max2FmtMsg)ser.Deserialize(data);
                //convert veh_id meitrack
                SQL2 sql = new SQL2(szDatabaseServer);
                if (msg.conInfo.type_of_box == "T")
                {
                    msg.vid = sql.Getvid(msg.vid);
                   // msg.vid = "1000000";
                }               
                msg = MsgCheckFormat(msg);
                //--- Do IO Status Event Before Throw to Threading           



                decimal last_log_index = InsertDataLog(msg);

                /******go to clase ProcessThreadClass********/
                ProcessThreadClass thrd = new ProcessThreadClass(msg, szDatabaseServer, last_log_index);
                thrd.OnCompleted += new CompletedHandler(thrd_OnCompleted);
                thrd.OnReportStatus += new ReportStatusHandler(thrd_OnReportStatus);
                thrd.OnProcessTimeCompleted += new ElapedTimeProcess(thrd_OnProcessTimeCompleted);
               // th
                TotalData++;
                TimeSpan sp = DateTime.Now - d1;

                lbMaxCommitLog = sp.TotalMilliseconds.ToString();

                if (!IsClosing && MQ != null && IsRunThread && !IsCheckThread)
                {
                    MQ.BeginReceiveQMsg(); //ต้อง UnComment
                }
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #3" + ex.Message);
            }
        }

        void thrd_OnProcessTimeCompleted(string zMsg)
        {
            semaphorLock.WaitOne();
            try {  }
            catch { }
            semaphorLock.Release();
        }
        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process p = Process.GetCurrentProcess();
            p.Kill();
        }
        private void thrd_OnReportStatus(object sender, CONTROLID ControlID, string data)
        {
        //private void thrd_OnReportStatus(object sender, CONTROLID ControlID, string data, string msg)
        //{
            //write text data > 1000.00
            //if (Convert.ToDouble(data) > 1000.00)
            //{
            //    string textalert = ControlID.ToString();
            //    textalert = msg + "  :  " + textalert + "  :  " + data + " ms.";
            //    String fileName = "C:\\Users\\Administrator\\Desktop\\Zebra\\Over1000ms.txt";
            //    //String fileName = "D:\\Over1000ms.txt";
            //    if (File.Exists(fileName))
            //    {
            //        StreamWriter sr = File.AppendText(fileName);
            //        sr.WriteLine(textalert);
            //        sr.Close();
            //        sr.Dispose();
            //    }
            //    else
            //    {
            //        StreamWriter sr = File.CreateText(fileName);
            //        sr.WriteLine(textalert);
            //        sr.Close();
            //        sr.Dispose();
            //    }
            //}
            //

            switch (ControlID)
            {
                case CONTROLID.MAX_Genaral:
                    lbMaxGenaral = data;
                    break;
                case CONTROLID.MAX_LogError:
                    lbMaxLogError = data;
                    break;
                case CONTROLID.MAX_SPEED:
                    lbMaxSpeedTimeStr = data;
                    break;
                case CONTROLID.MAX_IDLE:
                    lbMaxIdleTimeStr = data;
                    break;
                case CONTROLID.MAX_ZONE_TIMEID:
                    lbMaxZoneTimeStr = data;
                    break;
                case CONTROLID.MAX_ZONE_CURFEW:
                    lbMaxZoneCurfewTimeStr = data;
                    break;
                case CONTROLID.MAX_ZONE_CURFEWSPEED:
                    lbMaxZoneCurfewWithSpeedStr = data;
                    break;
                case CONTROLID.MAX_LOCATION:
                    lbMaxLocTimeStr = data;
                    break;
                case CONTROLID.MAX_ROUTING:
                    lbMaxRoutingTimeStr = data;
                    break;
                case CONTROLID.MAX_ENGINEONEVEN:
                    lbMaxEngineOnEventStr = data;
                    break;
                case CONTROLID.MAX_TEMPERATURE:
                    lbMaxTemperatureStr = data;
                    break;
                case CONTROLID.MAX_ENGINEEVENTZONE:
                    lbEngineOnOffInOutZoneStr = data;
                    break;

                case CONTROLID.MAX_TRUEFENCING:
                    lbMaxTrueFencingStr = data;
                    break;
                case CONTROLID.MAX_TRUEIO:
                    lbMaxTrueIOStr = data;
                    break;
             
                case CONTROLID.MAX_RPM:
                    lbMaxRpm = data;
                    break;
                case CONTROLID.MAX_SmartCard:
                    lbMaxSmartCard = data;
                    break;
                case CONTROLID.MAX_Magnetic:
                    lbMaxMagnetic = data;
                    break;
                case CONTROLID.MAX_PROCESS_MSG:
                    lbMaxCommitEvent = data;
                    break;
                default:
                    break;
            }
        }
        private void thrd_OnCompleted(object sender)
        {
            if (TotalCurThread < 0) TotalCurThread = 0;
        }

        private void WriteCtrlMsg(String ctrlStr, String msg)
        {
            try
            {
                if (lstPerformance.InvokeRequired)
                {
                    
                    //SetTextCallback proc = new SetTextCallback(WriteCtrlMsg);
                    //this.Invoke(proc, new object[] { ctrlStr, msg });
                    
                }
                else
                {
                    lstPerformance.Items[ctrlStr].SubItems[2].Text = msg;
                }
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server #4" + ex.Message);
            }
        }
        private void dataServerTimer_Tick(object sender, EventArgs e)
        {
            //หาค่า PID ถ้าไม่เหมือนเดิมให้รีสตาร์ท
            foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcesses())
            {
                pname = proc.ProcessName;
                if (pname == "mqsvc")
                { 
                    int pid2 = proc.Id;
                    if (pid == 0)
                    {
                        pid = proc.Id;
                    }
                    if (pid != pid2)
                    {
                        Application.Exit();
                    }
                    pid = pid2;
                }                
            }
            //
            if (TotalCurThread > 500)
            {
                IsRunThread = false;

                Process p = Process.GetCurrentProcess();
                p.Kill();
            }

            if (!IsRunThread && TotalCurThread < 200)
            {
                IsRunThread = true;
                MQ.BeginReceiveQMsg();
            }
       
            
            TotalCurThread = Process.GetCurrentProcess().Threads.Count - TotalStartThread;
            TotalDataCommit = TotalData - TotalData_Old;

            if (TotalCurThread < 0) TotalCurThread = 0;

            WriteCtrlMsg("lbMaxGenaral", lbMaxGenaral);
            WriteCtrlMsg("lbMaxLogError", lbMaxLogError);

            WriteCtrlMsg("lbMaxSpeedTime", lbMaxSpeedTimeStr);
            WriteCtrlMsg("lbMaxIdleTime", lbMaxIdleTimeStr);
            WriteCtrlMsg("lbMaxZoneTime", lbMaxZoneTimeStr);
            WriteCtrlMsg("lbMaxZoneCurfewTime", lbMaxZoneCurfewTimeStr);
            WriteCtrlMsg("lbMaxZoneCurfewWithSpeed", lbMaxZoneCurfewWithSpeedStr);
            WriteCtrlMsg("lbMaxLocTime", lbMaxLocTimeStr);
            WriteCtrlMsg("lbMaxRoutingTime", lbMaxRoutingTimeStr);

            WriteCtrlMsg("lbMaxEngineOnEventStr", lbMaxEngineOnEventStr);
            WriteCtrlMsg("lbMaxTemperatureStr", lbMaxTemperatureStr);

            WriteCtrlMsg("lbEngineOnOffInOutZoneStr", lbEngineOnOffInOutZoneStr);

            WriteCtrlMsg("lbMaxTrueFencingStr", lbMaxTrueFencingStr);
            WriteCtrlMsg("lbMaxTrueIOStr", lbMaxTrueIOStr);

            WriteCtrlMsg("lbMaxMagnetic", lbMaxMagnetic);
            WriteCtrlMsg("lbMaxSmartCard", lbMaxSmartCard);
            WriteCtrlMsg("lbMaxRpm", lbMaxRpm);

            WriteCtrlMsg("lbTimeCommitLog", lbMaxCommitLog);
            WriteCtrlMsg("lbTimeCommitEvent", lbMaxCommitEvent);

            WriteCtrlMsg("lbTotalCurThd", TotalCurThread.ToString());
            WriteCtrlMsg("lbTotalOfData", TotalData.ToString());
            WriteCtrlMsg("lbTotalOfDataCommit", TotalDataCommit.ToString());

            if (IsClosing)
            {
                if (TotalCurThread <= 0)
                {
                    dataServerTimer.Stop();
                    Close();
                    Application.Exit();
                }
                else
                {
                    tsTimerStatus.Text = "Please wait, still threre are running Thread left !!!";
                }
            }
            else
            {
                tsTimerStatus.Text = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
                TotalData_Old = TotalData;
            }

            if (++ResetGCTimer > 60)
            {
                ResetGCTimer = 0;
                GC.Collect();
            }

            if (DateTime.Now > nextUpdateTrueZone)
            {
                InitTrueAllZone();
            }
        }
    
        private void stopMQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IsCheckThread = true;
        }

        private void startMQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IsCheckThread = false;
            MQ.BeginReceiveQMsg();
        }
     
        private Max2FmtMsg MsgCheckFormat(Max2FmtMsg msg)
        {
            try
            {
                if (msg.tag == null) { msg.tag = ""; }
                if (msg.odometor == null) { msg.odometor = ""; }
                if (msg.internal_power == null) { msg.internal_power = ""; }
                if (msg.external_power == null) { msg.external_power = ""; }
            }
            catch
            { }
            return msg;
        }

        //á¡éàÇÅÒ Í´Õµ -1000 ÇÑ¹
        private decimal InsertDataLog(object obj)
        {
            decimal  last_log_index = -1.0m;

            try
            {
                if (obj.GetType() == typeof(Max2FmtMsg))
                {
                    Max2FmtMsg msg = (Max2FmtMsg)obj;

                    if (msg.gps_date_time == null || msg.utc == null)
                    {
                        return last_log_index;
                    }

                    SQL sql = new SQL(szDatabaseServer);
                    SQL2 sql2 = new SQL2(szDatabaseServer);                    

                    String timestamp = GetTimeStamp(msg.gps_date_time, msg.utc);
                    DateTime testDate = DateTime.Parse(timestamp);
                    DateTime validDate1 = DateTime.Now.Add(new TimeSpan(-1000, 0, 0, 0));
                    DateTime validDate2 = DateTime.Now.Add(new TimeSpan(2, 0, 0, 0));

                    String szValidDate1 = validDate1.ToString("yyyy-MM-dd", new CultureInfo("en-US"));
                    String szValidDate2 = validDate2.ToString("yyyy-MM-dd", new CultureInfo("en-US"));

                    validDate1 = DateTime.Parse(szValidDate1);
                    validDate2 = DateTime.Parse(szValidDate2);
///////////////////  àÇÅÒÍ¹Ò¤µ »ÃÑº à»ç¹àÇÅÒ»Ñ¨¨ØºÑ¹  ///////////////////
                    if (!(validDate1 <= testDate && testDate <= validDate2))
                    {
                        DateTime d = DateTime.Now;
                        timestamp = d.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        testDate = DateTime.Parse(timestamp);

                        d = d.AddHours(-7);
                        msg.gps_date_time = d.ToString("ddMMyy");
                        msg.utc = d.ToString("HHmmss");
                        msg.lon = "0.0";
                        msg.lat = "0.0";
                    }
/////////////////////////////////////////////////////////////////
//á¡éäÁèãËéÅ§ veh_cur_loc
                    #region DEVICE/LogMsg
                    if (validDate1 <= testDate && testDate <= validDate2)
                    {
                        try
                        {
                            System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                            if (msg.conInfo.type_of_box != null)
                            {
                                //sql.InsertNewBoxStatus(int.Parse(msg.vid),
                                //    msg.conInfo.source_ip,
                                //    int.Parse(msg.conInfo.source_port), true,
                                //    msg.conInfo.destination_ip,
                                //    int.Parse(msg.conInfo.destination_port),
                                //    msg.conInfo.type_of_box);
                                /**SQL2***/
                                sql2.InsertNewBoxStatus(int.Parse(msg.vid),
                                    msg.conInfo.source_ip,
                                    int.Parse(msg.conInfo.source_port), true,
                                    msg.conInfo.destination_ip,
                                    int.Parse(msg.conInfo.destination_port),
                                    msg.conInfo.type_of_box);
                            }

                            if (msg.type_of_message == "D")
                            {
                                #region  Device
                                ///temperature
                                ///T0+2506,125,020,240
                                if (msg.tag[0] == 'T' || msg.tag[0] == 't')
                                {
                                    string[] f = msg.tag.Split(new char[] { ',' });
                                    if (f.Length >= 4)
                                    {
                                        int temp_id = Convert.ToInt32(f[0][1].ToString());
                                        float temp_value = (float)Convert.ToDouble(f[0].Substring(2)) / 100.0F;
                                        float max_temp = (float)Convert.ToDouble(f[1]) - 55.0F;
                                        float min_temp = (float)Convert.ToDouble(f[2]) - 55.0F;
                                        
                                        /**SQL1***/
                                        //sql.InsertTemperatureMsg(Convert.ToInt32(msg.vid), temp_id, Convert.ToDateTime(timestamp), temp_value, max_temp, min_temp);

                                        /**SQL2***/
                                        sql2.InsertTemperatureMsg(Convert.ToInt32(msg.vid), temp_id, Convert.ToDateTime(timestamp), temp_value, max_temp, min_temp);
                                        last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                    }
                                }
                                ///smart card
                                else if (msg.tag[0] == 'C')
                                {
                                    // CP**********,AA,3460200267001   inser   Enanble,Valid
                                    // CE                              insert  Invalid
                                    // Cp**********,AA,3460200267001   insert  Disable,Valid
                                    // CD                              remove

                                    /**SQL1***/
                                    //lasted_index = InsertLogMsg(sql, msg, timestamp);

                                    /**SQL2***/
                                    last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                    if (last_log_index > -1 && msg.tag.Length > 1)
                                    {
                                        SMARTCARDEVENT evt = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT;
                                        switch (msg.tag[1])
                                        {
                                            case 'P': evt = SMARTCARDEVENT.VALID_SMARTCARD_INSERT; break;
                                            case 'E': evt = SMARTCARDEVENT.INVALID_SMART_CARD_INSERT; break;
                                            case 'p': evt = SMARTCARDEVENT.VALID_BUT_DISABLE_SMART_CARD; break;
                                            case 'D': evt = SMARTCARDEVENT.SMART_CATD_REMOVAL; break;
                                        }

                                        string data = msg.tag.Substring(2);
                                        /**SQL1***/
                                        //sql.InsertSmartCardMsg(Convert.ToInt32(msg.vid), Convert.ToDateTime(timestamp), data, (int)evt, lasted_index);
                                        /**SQL2***/
                                        sql2.InsertSmartCardMsg(Convert.ToInt32(msg.vid), Convert.ToDateTime(timestamp), data, (int)evt, last_log_index);
                                    }
                                }
                                ///analog level
                                else if (msg.tag[0] == 'V')
                                {
                                    //V0,410,060,060
                                    string[] f = msg.tag.Split(new char[] { ',' });
                                    if (f.Length == 4)
                                    {
                                        /**SQL1***/
                                        //lasted_index = InsertLogMsg(sql, msg, timestamp);

                                        /**SQL2***/
                                        last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                        if (last_log_index > 0)
                                        {
                                            int vo_id = Convert.ToInt32(f[0][1].ToString());
                                            float analog_2 = (float)Convert.ToDouble(f[1]);
                                            float analog_3 = (float)Convert.ToDouble(f[2]);
                                            float interval = (float)Convert.ToDouble(f[3]);
                                           
                                            /**SQL1***/
                                            //sql.InsertMsgAnalog(Convert.ToInt32(msg.vid), timestamp, lasted_index, analog_2, analog_3);
                                            
                                            /**SQL2***/
                                            sql2.InsertMsgAnalog(Convert.ToInt32(msg.vid), timestamp, last_log_index, analog_2, analog_3);
                                        }
                                    }
                                }
                                else if (msg.tag[0] == 'I')
                                {
                                    //I0,C,0,FF,300,01
                                    //External Input 6 port
                                    string[] inputStr = msg.tag.Split(new char[] { ',' });
                                    if (inputStr.Length == 6)
                                    {
                                        /**SQL1***/
                                        //lasted_index = InsertLogMsg(sql, msg, timestamp);
                                        /**SQL2***/
                                        last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                        if (last_log_index > 0)
                                        {
                                            int input_id = Convert.ToInt32(inputStr[0][1].ToString());
                                            int msgType = sql.GetOptionalMsgTypeIDByMsgType(inputStr[1]);
                                            int status_byte = Convert.ToInt32(GetHexToDex2(inputStr[2]));
                                            int input_mark = Convert.ToInt32(GetHexToDex2(inputStr[3]));
                                            int report_time = Convert.ToInt32(inputStr[4]);
                                            int input_status = Convert.ToInt32(GetHexToDex2(inputStr[5]));
                                            DateTime local_timestamp = DateTime.Parse(timestamp);

                                            /**SQL1***/
                                            //sql.InsertNewLogOptional(Convert.ToInt32(msg.vid), local_timestamp, input_id, msgType, status_byte, input_mark, report_time, input_status, lasted_index);

                                            /**SQL2***/
                                            sql2.InsertNewLogOptional(Convert.ToInt32(msg.vid), local_timestamp, input_id, msgType, status_byte, input_mark, report_time, input_status, last_log_index);
                                        }
                                    }
                                }
                                else if (msg.tag[0] == 'M' && msg.tag[1] == 'C')
                                {
                                    /**SQL1***/
                                    //lasted_index = InsertLogMsg(sql, msg, timestamp);

                                    /**SQL2***/
                                    last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                    if (last_log_index > 0)
                                    {
                                        //MC,24,2,0004552,00100
                                        //MC,24,
                                        //2,
                                        //0004552,
                                        //00100
                                        /*
                                         * SUKSAWADDEE SAITHARN MISS,6007643100500157891,150619800909,24,2,0004552,00100
                                        */
                                        string[] f = msg.tag.Split(new char[] { ',' });
                                        if (f.Length > 4)
                                        {
                                            string driver_type = f[1];
                                            string driver_sex = f[2];
                                            string driver_number = f[3];
                                            string code = f[4];
                                            string province_code = code.Substring(0, 3);
                                            string distic_code = code.Substring(3, 2);
                                            string driver_name = "";
                                            string id_country = "";
                                            string id_card = "";
                                            string expiry_date = "";
                                            string birthday = "";
                                            if (f.Length > 5)
                                            {
                                                driver_name = f[5];
                                                id_country = f[6];
                                                id_card = f[7];
                                                expiry_date = f[8];
                                                birthday = f[9];
                                            }

                                            //sql.InsertNewDriverLicence(lasted_index,
                                            //    Convert.ToInt32(msg.vid),
                                            //    driver_type,
                                            //    driver_sex,
                                            //    driver_number,
                                            //    province_code,
                                            //    distic_code,
                                            //    driver_name,
                                            //    id_country,
                                            //    id_card,
                                            //    expiry_date,
                                            //    birthday);

                                            /**SQL2***/
                                            sql2.InsertNewDriverLicence(last_log_index,
                                            Convert.ToInt32(msg.vid),
                                            driver_type,
                                            driver_sex,
                                            driver_number,
                                            province_code,
                                            distic_code,
                                            driver_name,
                                            id_country,
                                            id_card,
                                            expiry_date,
                                            birthday);
                                        }
                                    }
                                }
                                else if (msg.tag[0] == 'M' && msg.tag[1] == 'G')
                                {
                                    last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                    if (last_log_index > 0)
                                    {
                                        string[] f = msg.tag.Split(new char[] { ',' });
                                        if (f.Length > 4)
                                        {
                                            string driver_type = f[1];
                                            string driver_sex = f[2];
                                            string driver_number = f[3];
                                            string code = f[4];
                                            string province_code = code.Substring(0, 3);
                                            string distic_code = code.Substring(3, 2);
                                            string driver_name = "";
                                            string id_country = "";
                                            string id_card = "";
                                            string expiry_date = "";
                                            string birthday = "";
                                            if (f.Length > 5)
                                            {
                                                driver_name = f[5];
                                                id_country = f[6];
                                                id_card = f[7];
                                                expiry_date = f[8];
                                                birthday = f[9];
                                            }
                                            sql2.InsertNewDriverLicence(last_log_index,
                                            Convert.ToInt32(msg.vid),
                                            driver_type,
                                            driver_sex,
                                            driver_number,
                                            province_code,
                                            distic_code,
                                            driver_name,
                                            id_country,
                                            id_card,
                                            expiry_date,
                                            birthday);
                                        }
                                    }
                                }
                                else if (msg.tag[0] == 'R' && msg.tag[1] == 'F' && msg.tag[2] == 'I' && msg.tag[3] == 'D')
                                {
                                    last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                    if (last_log_index > 0)
                                    {
                                        string driver_type = msg.tag.Substring(4, 2);
                                        string driver_sex = msg.tag.Substring(20, 1);
                                        string driver_number = msg.tag.Substring(33, 8);// 7 or 8
                                        string code = msg.tag.Substring(43, 5);
                                        string province_code = code.Substring(0, 3);
                                        string distic_code = code.Substring(3, 2);
                                        string driver_name = "";
                                        string id_country = "";
                                        string id_card = "";
                                        string expiry_date = "";
                                        string birthday = "";

                                        sql2.InsertNewDriverLicence(last_log_index,
                                        Convert.ToInt32(msg.vid),
                                        driver_type,
                                        driver_sex,
                                        driver_number,
                                        province_code,
                                        distic_code,
                                        driver_name,
                                        id_country,
                                        id_card,
                                        expiry_date,
                                        birthday); 
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                /**SQL1***/
                                //lasted_index = InsertLogMsg(sql, msg, timestamp);

                                /**SQL2***/
                                last_log_index = InsertLogMsg2(sql2, msg, timestamp);

                                #region GSM Signal
                                //--- For MBOX
                                if (last_log_index > 0 && msg.type_of_message == "Q")
                                {
                                    string[] f = msg.tag.Split(new char[] { ',' });
                                    if (f.Length >= 7)
                                    {
                                        string mcc = f[0];
                                        string mnc = f[1];
                                        string lac = GetHexToDex(f[2]);
                                        string cell = GetHexToDex(f[3]);
                                        string bsic = f[4];
                                        int arfcn = Convert.ToInt32(f[5]);
                                        int rxlev = Convert.ToInt32(f[6]);

                                        /**SQL1***/
                                        //sql.InsertGSMMsg(lasted_index, mcc, mnc, lac, cell, bsic, arfcn, rxlev);
                                        if (!(int.Parse(mcc) > 0 && int.Parse(mnc) > 0 && int.Parse(lac) > 0 && int.Parse(cell) > 0 && int.Parse(cell) > 0
                                              && int.Parse(bsic) > 0 && arfcn > 0 && rxlev > 0))
                                        {
                                            /**SQL2***/
                                            sql2.InsertGSMMsg(last_log_index, mcc, mnc, lac, cell, bsic, arfcn, rxlev);
                                        }
                                    }
                                }
                                //--- For GBOX
                                else if (last_log_index > 0 && msg.tag.Length > 0) //Tag[0] = Q : Is Addition format for GBox GSM. EX: Tag = Q,520,01,36BA,0233,13,116,57
                                {
                                    if (msg.tag[0] == 'Q')
                                    {
                                        string[] f = msg.tag.Split(new char[] { ',' });
                                        if (f.Length >= 7)
                                        {
                                            string mcc = f[1];
                                            string mnc = f[2];
                                            string lac = f[3];
                                            string cell = f[4];
                                            string bsic = f[5];
                                            int arfcn = Convert.ToInt32(f[6]);
                                            int rxlev = Convert.ToInt32(f[7]);

                                            /**SQL1***/
                                            //sql.InsertGSMMsg(lasted_index, mcc, mnc, lac, cell, bsic, arfcn, rxlev);
                                            if (!(int.Parse(mcc) > 0 && int.Parse(mnc) > 0 && int.Parse(lac) > 0 && int.Parse(cell) > 0 && int.Parse(cell) > 0
                                                && int.Parse(bsic) > 0 && arfcn > 0 && rxlev > 0))
                                            {
                                                /**SQL2***/
                                                sql2.InsertGSMMsg(last_log_index, mcc, mnc, lac, cell, bsic, arfcn, rxlev);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                        }
                        catch
                        {
                        }
                    }

                    #endregion
                    #region  IO/Odometer-Distance/Power/CurLoc
                    if (last_log_index > -1)
                    {
                        if (Validate(msg))
                        {
                            if (int.Parse(msg.no_of_satellite) >= 3 && int.Parse(msg.type_of_fix) > 0 && int.Parse(msg.speed) < 175)
                            {
                                //Call Distance
                                DistanceCls distCls = new DistanceCls(szDatabaseServer);
                                double distance = 0.00;

                                if (distCls.CalDistance(int.Parse(msg.vid), float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)), out distance))
                                {
                                    /**SQL1***/
                                    //sql.UpdateCurLocDistance(int.Parse(msg.vid), distance);
                                    //sql.InsertLocationDistance(lasted_index, distance);

                                    /******UPDATE Distance,Insert Distance******/
                                    /**SQL2***/
                                    //ไม่ให้อัพ currentlocation
                                    sql2.UpdateCurLocDistance(int.Parse(msg.vid), distance);
                                    sql2.InsertLogDistance(last_log_index, distance, 0.0);                                    
                                }

                                /**SQL1***/
                                //sql.UpdateVehCurLoc2(int.Parse(msg.vid), lasted_index, float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)));
                               
                                /***UPDATE LAT LON INDEX**/
                                /**SQL2***/
                                //ไม่ให้อัพ currentlocation
                                sql2.UpdateVehCurLoc2(int.Parse(msg.vid), last_log_index, float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)));
                            }
                            else
                            {
                                /**SQL1***/
                                //sql.UpdateVehCurLoc(int.Parse(msg.vid), lasted_index);
                                
                                /**UPDATE INDEX****/
                                /**SQL2***/
                                //ไม่ให้อัพ currentlocation
                                sql2.UpdateVehCurLoc(int.Parse(msg.vid), last_log_index);
                            }

                            int internal_power = 0;
                            int external_power = 0;

                            if (int.TryParse(msg.internal_power, out internal_power)) { }
                            if (int.TryParse(msg.external_power, out external_power)) { }

                            oExEventIO.DetectEventIO
                            (
                                last_log_index,
                                int.Parse(msg.vid),
                                timestamp,
                                float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                                (int)float.Parse(msg.speed), (int)float.Parse(msg.course), int.Parse(msg.type_of_fix),
                                int.Parse(msg.no_of_satellite), Convert.ToInt32(msg.gpio_status, 16), int.Parse(msg.analog_level),
                                msg.type_of_message.Substring((msg.type_of_message.Length - 1), 1), msg.tag,
                                internal_power, external_power
                            );
                        }
                    }                   

                  


                }
                    #endregion
            }
            catch
            {
            }
            return last_log_index;
        }

        private int InsertLogMsg(SQL sql, Max2FmtMsg msg, string timestamp)
        {
            int lasted_index = -1;
            try
            {
                //InsertMsg_Check
                if (int.Parse(msg.type_of_fix) == 0 || int.Parse(msg.no_of_satellite) < 3)
                {
                    lasted_index = sql.InsertMsg_Error
                        (
                                int.Parse(msg.vid),
                                timestamp,
                                float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                                (int)float.Parse(msg.speed), (int)float.Parse(msg.course), int.Parse(msg.type_of_fix),
                                int.Parse(msg.no_of_satellite), Convert.ToInt32(msg.gpio_status, 16), int.Parse(msg.analog_level),
                                msg.type_of_message
                        );
                }
                else
                {
                    lasted_index = sql.InsertMsg
                        (
                            int.Parse(msg.vid),
                            timestamp,
                            float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                            int.Parse(msg.speed), int.Parse(msg.course), int.Parse(msg.type_of_fix),
                            int.Parse(msg.no_of_satellite), Convert.ToInt32(msg.gpio_status, 16), int.Parse(msg.analog_level),
                            msg.type_of_message
                        );
                }
            }
            catch
            {
            }
            return lasted_index;
         
        }

        private decimal InsertLogMsg2(SQL2 sql2, Max2FmtMsg msg, string timestamp)
        {
            decimal index = -1.0m;
            bool is_complete = false;
            try
            {
                index = CalculatorLogIndex(sql2, msg, timestamp);

                if (index < 1.0m) return -1.0m;
                //InsertMsg_Check
                if (int.Parse(msg.type_of_fix) == 0 || int.Parse(msg.no_of_satellite) < 3)
                {
                    is_complete = sql2.InsertMsg_Error(
                                index,
                                int.Parse(msg.vid),
                                timestamp,
                                float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                                (int)float.Parse(msg.speed), (int)float.Parse(msg.course), int.Parse(msg.type_of_fix),
                                int.Parse(msg.no_of_satellite), Convert.ToInt32(msg.gpio_status, 16), int.Parse(msg.analog_level),
                                msg.type_of_message
                        );
                }
                else
                {
                    is_complete = sql2.InsertMsg
                        (
                            index,
                            int.Parse(msg.vid),
                            timestamp,
                            float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)), float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                            int.Parse(msg.speed), int.Parse(msg.course), int.Parse(msg.type_of_fix),
                            int.Parse(msg.no_of_satellite), Convert.ToInt32(msg.gpio_status, 16), int.Parse(msg.analog_level),
                            msg.type_of_message
                        );
                }

                if (is_complete)
                {
                    return index;
                }
            }
            catch (Exception ex)
            {
                string msgxx = ex.Message;
            }
            return -1.0m;
        }
        private decimal CalculatorLogIndex(SQL2 sql2, Max2FmtMsg msg, string timestamp)
        {
            decimal tmp_index = -1.0m;
            try
            {
                //1403190043352.190809010
                //yyMMdd**12345.HHmmss***
                //** = YY > 13,14,15,00
                //12345 = Vid 5 digi
                //*** = Type Of Msg = G - 012
                string temp_index = string.Empty;
                DateTime dtTime = Convert.ToDateTime(timestamp);
                temp_index = dtTime.ToString("yyMMdd");
                string temp_vid = msg.vid.Trim();
                string year1 = DateTime.Now.AddYears(-1).ToString("yy");
                string year2 = DateTime.Now.ToString("yy");
                string year3 = DateTime.Now.AddYears(+1).ToString("yy");

                string tm_vid = "00000";

                if (temp_vid.Length == 10 && (temp_vid.StartsWith(year1) || temp_vid.StartsWith(year2) || temp_vid.StartsWith(year3)))
                {
                    temp_index += temp_vid[0].ToString() + temp_vid[1].ToString();

                    if (int.Parse(temp_vid) > 0)
                    {
                        tm_vid = (temp_vid.Substring((temp_vid.Length - 4), 4)).PadLeft(5, '0');
                    }
                }
                else
                {
                    temp_index += "00";

                    if (int.Parse(temp_vid) > 0)
                    {
                        if (temp_vid.Length < 5)
                            tm_vid = temp_vid.PadLeft(5, '0');
                        else
                            tm_vid = temp_vid.Substring((temp_vid.Length - 5), 5);
                    }
                }

                temp_index += tm_vid + "." + dtTime.ToString("HHmmss");
                string temp_key_1 = "000";
                #region calculator_type_of_msg_index
                int int_type_of_msg = GetTypeOfMsg(msg.type_of_message.Trim());
                string key_tag = (msg.header == null ? "$" : msg.header) + (msg.type_of_message == null ? "" : msg.type_of_message) + (msg.gpio_status == null ? "" : msg.gpio_status) + (msg.tag == null ? "" : msg.tag);
                string key_tagi64 = ELFHash(key_tag).ToString();

                string key_i3digi = key_tagi64;
                if (key_i3digi.Length < 3)
                    key_i3digi = key_i3digi.PadLeft(3, '0');
                else
                    key_i3digi = key_i3digi.Substring((key_i3digi.Length - 3), 3);

                int temp_key_2 = Convert.ToInt16(key_i3digi);
                if (temp_key_2 > 800)
                {
                    temp_key_2 = temp_key_2 - 200;
                }
                else if (temp_key_2 < 0)
                {
                    temp_key_2 = temp_key_2 * -1;
                }

                temp_key_2 = temp_key_2 + int_type_of_msg;
                #endregion
                if (temp_key_2.ToString().Length < 3)
                    temp_key_1 = temp_key_2.ToString().PadLeft(3, '0');
                else
                    temp_key_1 = temp_key_2.ToString();
                temp_index += temp_key_1; 
                tmp_index = Convert.ToDecimal(temp_index);
                //1403210000181.094232013
            }
            catch { }
            return tmp_index;
        }

        private int GetTypeOfMsg(string type)
        {
            int id = 0;
            try
            {
                switch (type)
                {
                    case "I": id = 1; break;
                    case "K": id = 2; break;
                    case "M": id = 3; break;
                    case "V": id = 4; break;
                    case "A": id = 5; break;
                    case "-": id = 6; break;
                    case "+": id = 8; break;
                    case "S": id = 9; break;
                    case "G": id = 10; break;
                    case "R": id = 11; break;
                    case "L": id = 12; break;
                    case "D": id = 13; break;
                    case "O": id = 15; break;
                    case "F": id = 16; break;
                    case "X": id = 17; break;
                    case "x": id = 18; break;
                    case "Q": id = 19; break;
                    case "C": id = 20; break;
                    case "c": id = 21; break;
                    case "W": id = 22; break;
                    case "k": id = 23; break;
                    case "B": id = 24; break;
                    case "b": id = 25; break;
                    case "1": id = 26; break;
                    case "2": id = 27; break;
                    case "3": id = 28; break;
                    case "4": id = 29; break;
                    case "5": id = 30; break;
                    case "6": id = 31; break;
                    case "7": id = 32; break;
                    case "8": id = 33; break;
                    case "v": id = 34; break;
                    case "*L": id = 35; break;
                    case "*1": id = 36; break;
                    case "*2": id = 37; break;
                    case "*3": id = 38; break;
                    case "*4": id = 39; break;
                    case "*5": id = 40; break;
                    case "*6": id = 41; break;
                    case "*7": id = 42; break;
                    case "*8": id = 43; break;
                }
            }
            catch { }
            return id;
        }

        private Int64 ELFHash(String str)
        {
            Int64 hash = 0;
            Int64 x = 0;

            for (Int32 i = 0; i < str.Length; i++)
            {
                hash = (hash << 4) + str[i];

                if ((x = hash & 0xF0000000L) != 0)
                {
                    hash ^= (x >> 24);
                }
                hash &= ~x;
            }

            return hash;
        }
        private bool Validate(Max2FmtMsg msg)
        {
            bool bIsOk = true; 
            //SQL sql = new SQL(szDatabaseServer);

            //int maxSpeed = 200; // Km/Hrs 
            //int totalSec = 2 * 60;
            string eventStr = "K,M,V,A,S,G,L,O,F,X,x"; // "2,3,4,5,9,10,12,15,16,17,18"; msg type id.

            //double lat = 0.0;
            //double lon = 0.0;
            DateTime timestamp = new DateTime();

            try
            {
                String szTimeStamp = GetTimeStamp(msg.gps_date_time, msg.utc);
                DateTime curTimestamp = Convert.ToDateTime(szTimeStamp);

                String d1 = curTimestamp.ToString("MM/dd/yyyy HH:mm:ss");
                String d2 = timestamp.ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));
                String d3 = DateTime.Now.AddHours(1).ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));

                if (Convert.ToDateTime(d3) < Convert.ToDateTime(d1))
                {
                    return false;
                }

                int idx = eventStr.IndexOf(msg.type_of_message);
                if (idx < 0)
                {
                    return bIsOk;
                }
            }
            catch
            {
            }

            return bIsOk;
        }
        private String GetTimeStamp(String date, String time)
        {
            try
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                d = d.AddHours(7.0);
                String szTimeStamp = d.ToString("yyyy-MM-dd HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server(Thread) #3" + ex.Message);
            }

            return "";
        }
        private string GetHexToDex(string zHexStr)
        {
            string retDec = zHexStr;
            try
            {
                int decValue = 0;
                if (!(int.TryParse(zHexStr, out decValue)))
                {
                    decValue = Convert.ToInt32(zHexStr, 16);
                    retDec = decValue.ToString("0");
                }
            }
            catch
            {

            }
            return retDec;
        }
        private string GetHexToDex2(string zHexStr)
        {
            string retDec = "0";
            try
            {
                int decValue = Convert.ToInt32(zHexStr, 16);
                retDec = decValue.ToString("0");
            }
            catch
            {
            }
            return retDec;
        }

        private void AddFileArchive()
        {
            string fileNameOld = string.Empty;
            string src_file_name = fileNameOld;
            string dst_file_name = fileNameOld + ".rar";

            System.IO.FileStream file_input = null;
            System.IO.FileStream file_output = null;
            System.IO.Compression.GZipStream rar = null;
            byte[] buffer;

            try
            {
                file_output = new System.IO.FileStream(dst_file_name, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);
                rar = new System.IO.Compression.GZipStream(file_output, System.IO.Compression.CompressionMode.Compress, true);

                file_input = new System.IO.FileStream(src_file_name, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                buffer = new byte[file_input.Length];
                int iBuff = file_input.Read(buffer, 0, buffer.Length);
                file_input.Close();
                file_input = null;

                rar.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (rar != null)
                {
                    rar.Close();
                    rar = null;
                }
                if (file_output != null)
                {
                    file_output.Close();
                    file_output = null;
                }
                if (file_input != null)
                {
                    file_input.Close();
                    file_input = null;
                }
            }
        }



        private void SendJsonDaata(string json)
        {

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://10.0.100.150/kt_tracking/apps/realtimedb/realtime_db.php");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    //string json = "{\"evt_desc\":\"ดับเครื่องยนต์\",\"licenp\":\"กข-5678\", \"loc_th\":\"สวนลุมพินี\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

            }
            catch (Exception)
            {
                
                throw;
            }

            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    var result = streamReader.ReadToEnd();
            //}

        }

        private void frmDataServer_Load_1(object sender, EventArgs e)
        {
            InitProcessList();
        }

    }
}