using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Data;
using System.Collections;
using ZebraCommonInterface;

namespace ZebraDataServer
{
    public class GlobalClass
    {
        static public Hashtable hsZoneTrueList = new Hashtable();
    
        public struct TrueZoneProfile
        {
            public int zone_id;
            public string zone_desc;
            public double lat_min;
            public double lat_max;
            public double lon_min;
            public double lon_max;
            public Boolean clkdir;
            public DateTime lastedUpdate;
            public DataTable dtWp;
            public DateTime timeExpire;
            public bool is_update;
        }

        static public void WriteLogEvent(String msg)
        {
            msg = DateTime.Now.ToString() + " : " + msg;
            String fileName = "GlobalLogEvent.txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
             
        }

        static public void WriteLog(String msg)
        {
            //msg = DateTime.Now.ToString() + " : " + msg;
            //String fileName = "C:\test.txt";
            //if (File.Exists(fileName))
            //{
            //    StreamWriter sr = File.AppendText(fileName);
            //    sr.WriteLine(msg);
            //    sr.Close();
            //    sr.Dispose();
            //}
            //else
            //{
            //    StreamWriter sr = File.CreateText(fileName);
            //    sr.WriteLine(msg);
            //    sr.Close();
            //    sr.Dispose();
            //}
        }

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        public void WriteLogEvent(String Threading, String Msg)
        {
            try
            {
                semaphor.WaitOne();

                String logPath = Directory.GetCurrentDirectory() + "\\ThreadExceptionlog";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                String fileName = logPath + "\\" + "ThreadExceptionlog" + DateTime.Now.ToString("_yyyy_MM_dd") + ".txt";

                Msg = DateTime.Now.ToString() + "," + Threading + "," + Msg;

                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(Msg);
                    sr.Close();
                    sr.Dispose();
                }
                semaphor.Release();
            }
            catch { }
        }


        static public double GetFromTheGreatCircle(double lat1, double lon1, double lat2, double lon2)
        {
            double R = 6371.0;
            double rad = 57.2958;

            lat1 = lat1 / rad;
            lat2 = lat2 / rad;
            lon1 = lon1 / rad;
            lon2 = lon2 / rad;

            double a = Math.Sin(lat1) * Math.Sin(lat2);
            double b = Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(lon2 - lon1);
            double c = Math.Acos(a + b);
            double d = R * c;

            return d;
        }
    }
}
