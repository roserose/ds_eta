namespace ZebraDataServer
{
    partial class frmDataServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataServer));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopMQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startMQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsTimerStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsSystemQ = new System.Windows.Forms.ToolStripStatusLabel();
            this.v = new System.Windows.Forms.ToolStripStatusLabel();
            this.messageQueue1 = new System.Messaging.MessageQueue();
            this.tbPerformance = new System.Windows.Forms.TabControl();
            this.pgPerformance = new System.Windows.Forms.TabPage();
            this.lstPerformance = new System.Windows.Forms.ListView();
            this.colNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colProcess = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colElapsedTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dataServerTimer = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tbPerformance.SuspendLayout();
            this.pgPerformance.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(528, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitToolStripMenuItem,
            this.stopMQToolStripMenuItem,
            this.startMQToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.quitToolStripMenuItem.Text = "&Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // stopMQToolStripMenuItem
            // 
            this.stopMQToolStripMenuItem.Name = "stopMQToolStripMenuItem";
            this.stopMQToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.stopMQToolStripMenuItem.Text = "Stop MQ";
            this.stopMQToolStripMenuItem.Click += new System.EventHandler(this.stopMQToolStripMenuItem_Click);
            // 
            // startMQToolStripMenuItem
            // 
            this.startMQToolStripMenuItem.Name = "startMQToolStripMenuItem";
            this.startMQToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.startMQToolStripMenuItem.Text = "Start MQ";
            this.startMQToolStripMenuItem.Click += new System.EventHandler(this.startMQToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutUsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutUsToolStripMenuItem
            // 
            this.aboutUsToolStripMenuItem.Name = "aboutUsToolStripMenuItem";
            this.aboutUsToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.aboutUsToolStripMenuItem.Text = "&About us";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTimerStatus,
            this.tsSystemQ,
            this.v});
            this.statusStrip1.Location = new System.Drawing.Point(0, 488);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(528, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsTimerStatus
            // 
            this.tsTimerStatus.Name = "tsTimerStatus";
            this.tsTimerStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsTimerStatus.Size = new System.Drawing.Size(110, 17);
            this.tsTimerStatus.Text = "00-00-0000 00:00:00";
            // 
            // tsSystemQ
            // 
            this.tsSystemQ.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.tsSystemQ.ImageTransparentColor = System.Drawing.Color.Gray;
            this.tsSystemQ.Name = "tsSystemQ";
            this.tsSystemQ.Size = new System.Drawing.Size(80, 17);
            this.tsSystemQ.Text = "    SystemQ : -";
            // 
            // v
            // 
            this.v.Name = "v";
            this.v.Size = new System.Drawing.Size(67, 17);
            this.v.Text = " DB Host : -";
            // 
            // messageQueue1
            // 
            this.messageQueue1.MessageReadPropertyFilter.LookupId = true;
            this.messageQueue1.SynchronizingObject = this;
            // 
            // tbPerformance
            // 
            this.tbPerformance.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPerformance.Controls.Add(this.pgPerformance);
            this.tbPerformance.Location = new System.Drawing.Point(3, 43);
            this.tbPerformance.Name = "tbPerformance";
            this.tbPerformance.SelectedIndex = 0;
            this.tbPerformance.Size = new System.Drawing.Size(524, 444);
            this.tbPerformance.TabIndex = 3;
            // 
            // pgPerformance
            // 
            this.pgPerformance.Controls.Add(this.lstPerformance);
            this.pgPerformance.Location = new System.Drawing.Point(4, 22);
            this.pgPerformance.Name = "pgPerformance";
            this.pgPerformance.Padding = new System.Windows.Forms.Padding(3);
            this.pgPerformance.Size = new System.Drawing.Size(516, 418);
            this.pgPerformance.TabIndex = 0;
            this.pgPerformance.Text = "Performance";
            this.pgPerformance.UseVisualStyleBackColor = true;
            // 
            // lstPerformance
            // 
            this.lstPerformance.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPerformance.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNo,
            this.colProcess,
            this.colElapsedTime});
            this.lstPerformance.FullRowSelect = true;
            this.lstPerformance.Location = new System.Drawing.Point(2, 2);
            this.lstPerformance.Name = "lstPerformance";
            this.lstPerformance.Size = new System.Drawing.Size(511, 412);
            this.lstPerformance.TabIndex = 0;
            this.lstPerformance.UseCompatibleStateImageBehavior = false;
            this.lstPerformance.View = System.Windows.Forms.View.Details;
            // 
            // colNo
            // 
            this.colNo.Text = "No";
            this.colNo.Width = 40;
            // 
            // colProcess
            // 
            this.colProcess.Text = "Process";
            this.colProcess.Width = 310;
            // 
            // colElapsedTime
            // 
            this.colElapsedTime.Text = "Elapsed Time (ms)";
            this.colElapsedTime.Width = 108;
            // 
            // dataServerTimer
            // 
            this.dataServerTimer.Interval = 1000;
            this.dataServerTimer.Tick += new System.EventHandler(this.dataServerTimer_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(528, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // frmDataServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 510);
            this.Controls.Add(this.tbPerformance);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmDataServer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zebra Data Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmDataServer_FormClosing);
            this.Load += new System.EventHandler(this.frmDataServer_Load_1);
            this.Shown += new System.EventHandler(this.frmDataAgency_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tbPerformance.ResumeLayout(false);
            this.pgPerformance.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutUsToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Messaging.MessageQueue messageQueue1;
        private System.Windows.Forms.TabControl tbPerformance;
        private System.Windows.Forms.TabPage pgPerformance;
        private System.Windows.Forms.Timer dataServerTimer;
        private System.Windows.Forms.ToolStripStatusLabel tsTimerStatus;
        private System.Windows.Forms.ListView lstPerformance;
        private System.Windows.Forms.ColumnHeader colProcess;
        private System.Windows.Forms.ColumnHeader colElapsedTime;
        private System.Windows.Forms.ColumnHeader colNo;
        private System.Windows.Forms.ToolStripStatusLabel tsSystemQ;
        private System.Windows.Forms.ToolStripStatusLabel v;
        private System.Windows.Forms.ToolStripMenuItem stopMQToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startMQToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
    }
}

