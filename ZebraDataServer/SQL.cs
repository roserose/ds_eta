using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using ZebraCommonInterface;
using System.IO;

namespace ZebraDataServer
{
    public class SQL
    {
       // private int totalElapedTime = 50;
        private String connStr = "";

        public SQL(String dbServer)
        {
            //connStr = String.Format("Data Source={0}; Database=ZebraDB;  UID=sa; PWD=aab-gps-gtt", dbServer);            
            //connStr = String.Format("initial catalog=ZebraDB; Data Source={0}; User Id=sa; Password=aab-gps-gtt; Min Pool Size=20; Max Pool Size=500", dbServer);                        
            connStr = String.Format("Data Source={0}; Database=ZebraDB;  UID=sa; PWD=aab-gps-gtt; Pooling=true; Min Pool Size=20; Max Pool Size=500", dbServer);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////

        public bool InsertSmartCardMsg(int veh_id, DateTime dtTimestampe, String data, int evt,int ref_idx)
        {
            bool ret = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewSmartCardData";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@data", data);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }
            return ret;
        }
        //rose
        public DataTable GetDataTable(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = sql;
                    cm.CommandType = System.Data.CommandType.Text;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    cn.Close();

                }
            }
            catch(Exception ex)
            {
              //  Console.WriteLine(ex.ToString());
            }

            return dt;
        }
        public bool InsertTemperatureMsg(int veh_id, int temp_id, DateTime dtTimestampe, float temp_value, float max_temp, float min_temp)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewTemperature";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temp_id", temp_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temperature", temp_value);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@max_temperature", max_temp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@min_temperature", min_temp);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }

            return ret;
        }
        public bool IsTemperatureConnected(int veh_id)
        {
            bool ret = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spIsTempConnected";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ret", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    int iRet = (int)cm.Parameters["@ret"].Value;
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return ret;
        }
        public bool InsertGSMMsg(long idx, string mcc, string mnc, string lac, string cell, string bsic, int arfcn, int rxlev)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertGSM";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mcc", mcc);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mnc", mnc);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lac", lac);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@cell", cell);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@bsic", bsic);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@arfcn", arfcn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@rxlev", rxlev);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }

            return ret;
        }
        public void InsertMsgAnalog(int veh_id, string timestamp, int ref_idx, double analog2, double analog3)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewAnalog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", timestamp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog2", analog2);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog3", analog3);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch 
            {
            }
        }

        public int InsertMsg(int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg)
        {
            int lasted_index = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lon", lon);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@course", course);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_fix", type_of_fix);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@no_of_satellite", no_of_satellite);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@in_out_status", in_out_status);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog_level", analog_level);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_msg", type_of_msg);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lasted_index", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        lasted_index = (int)cm.Parameters["@lasted_index"].Value;
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }

            return lasted_index;
        }

        public Boolean InsertMsgEvt(int refIdx, int evtId)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spEvt_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evtId);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return bRet;
        }
        public Boolean InsertLogUserDefined1(int vehID, int refIdx, int evtId)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertUserDefined1";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_id", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", (int)evtId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", (int)vehID);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("InsertUserDefined1() : " + ex.Message);
            }

            return bRet;
        }
        public Boolean InsertLogUserDefined2(int vehID, int refIdx, int evtId)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertUserDefined2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_id", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", (int)evtId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", (int)vehID);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("InsertUserDefined2() : " + ex.Message);
            }
            return bRet;
        }
        public Boolean InsertMsgZone(int refIdx, int zoneId, int evt_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgZone_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zoneId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public Boolean InsertMsgZoneCurfew(int veh_id, int refIdx, int zoneId, int evt_id, int marked)
        {
            Boolean bRet = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spZone_UpdateZoneCurfew";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zoneId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@marked", marked);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        public Boolean InsertMsgZoneCurfewWithSpeed(int veh_id, int refIdx, int zoneId, int evt_id, int marked, int speed)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spZone_UpdateZoneCurfewWithSpeed";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zoneId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@marked", marked);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public Boolean InsertMsgLoc(int refIdx, LOCATION_TYPE loc_type, int locId)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertLocation";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@loc_type", (int)loc_type);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@loc_id", locId);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public int InsertLoc2(string location_t, string location_e)
        {
            int id = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLocation2_Insert";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@location_t", location_t);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@location_e", location_e);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ret", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        id = (int)cm.Parameters["@ret"].Value;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return id;
        }
        public void InsertPrvZoneState(int veh_id, int zone_id, int evt_id, int ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgZone_InsertPrvZoneState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertPrvIOState(int veh_id, int port, int ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spIOPort_SavePrvState]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertPrvIOState_External(int veh_id, int port, int ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spIOPort_External_SavePrvState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public Boolean InsertExternalInputEvt(int veh_id, int refIdx, EVT_TYPE evtId)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spEvt_InsertExternalInput";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", (int)evtId);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return bRet;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////

        public bool IsEngineOn(int veh_id)
        {
            bool bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_IsEngineOn]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_engine_on", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    int iRet = (int)cm.Parameters["@is_engine_on"].Value;

                    if (iRet == 1)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }

            return bRet;
        }

        public void GetPrvLatLon(int veh_id, out double lat, out double lon)
        {
            lat = lon = 0.0;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_PreviousLastLatLon]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        lat = (double)dt.Rows[0]["prv_lat"];
                        lon = (double)dt.Rows[0]["prv_lon"];
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }
        }

        public void UpdateVehCurLoc(int veh_id, int ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_UpdateCurLoc]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch 
            {
            }
        }
        public void UpdateVehCurLoc2(int veh_id, int ref_idx,float lat, float lon)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_UpdateCurLoc2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_lon", lon);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public int GetVehSpeedLimit(int veh_id)
        {
            int iMaxSpeed = -1;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetSpeedViolate]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    try
                    {
                        foreach (DataRow r in dt.Rows)
                        {
                            iMaxSpeed = (int)r["max_speed"];
                            break;
                        }
                    }
                    catch
                    {
                        iMaxSpeed = 1000;
                    }
                    cn.Close();
                }
            }
            catch
            {
                iMaxSpeed = 1000;
            }

            return iMaxSpeed;
        }

        public Boolean GetVehIdleTimeLimit(int veh_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetIdleTime]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@return_value", SqlDbType.Bit);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    if ((Boolean)cm.Parameters["@return_value"].Value)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
  
            return bRet;
        }

        public Boolean GetVehEngineOn(int veh_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_IsEngineOn]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@return_value", SqlDbType.Bit);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        bRet = (bool)cm.Parameters["@is_engine_on"].Value;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return bRet;
        }

        public DataTable DetectIdleVeh(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetIdle]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    try
                    {
                        SqlDataAdapter adp = new SqlDataAdapter(cm);
                        adp.Fill(fzTable);
                    }
                    catch 
                    {
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }

            return fzTable;
        }

        public DateTime GetVehCurTime(int veh_id)
        {
            DateTime curDateTime = new DateTime();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetCurTime]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    try
                    {
                        foreach (DataRow r in dt.Rows)
                        {
                            curDateTime = (DateTime)r["local_timestamp"];
                            break;
                        }
                    }
                    catch
                    { }
                    cn.Close();
                }
            }
            catch
            {
            }

            return curDateTime;
        }
        public void GetVehLatWP(int veh_id, out double lat, out double lon)
        {
            lat = 0.0;
            lon = 0.0;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetLastWP]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    try
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            lat = (double)dt.Rows[0]["lat"];
                            lon = (double)dt.Rows[0]["lon"];
                        }
                    }
                    catch { }

                  
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        public void GetLastVehicleLatLon(int veh_id, out double lat, out double lon, out DateTime timestamp)
        {
            lat = -1;
            lon = -1;
            timestamp = new DateTime();

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetLastLatLon]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    try
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            lat = (double)dt.Rows[0]["lat"];
                            lon = (double)dt.Rows[0]["lon"];
                            timestamp = (DateTime)dt.Rows[0]["local_timestamp"];
                        }
                    }
                    catch
                    {
                        lat = -1;
                        lon = -1;
                    }
                    cn.Close();
                }
            }
            catch
            {
                lat = -1;
                lon = -1;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////

        public DataTable GetFleetZoneByVeh(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetFleetZone]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public DataTable GetZoneProfile(int zone_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_GetProfile]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }
        //�ѧ�������
        public DataTable GetZoneProfile_new(int zone_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_GetProfile_New_TEST]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }
        public DataTable GetZoneWayPoints(int zone_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[zpZone_GetWayPoints]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public void GetLastZoneStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            evt_id = -1;
            zone_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_GetLastStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        zone_id = -1;
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }
        }
        public void GetLastZoneCurfewStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark)
        {
            evt_id = -1;
            zone_id = -1;
            mark = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_GetLastZoneCurfewStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mark", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                        mark = (int)cm.Parameters["@mark"].Value;
                    }
                    catch 
                    {
                        evt_id = -1;
                        zone_id = -1;
                        mark = -1;
                        local_timestamp = DateTime.Now.AddDays(-1);
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }
        }

        public void GetLastZoneCurfewWithSpeedStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark, out int speed)
        {
            evt_id = -1;
            zone_id = -1;
            mark = -1;
            speed = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_GetLastZoneCurfewWithSpeedStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mark", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                        mark = (int)cm.Parameters["@mark"].Value;
                        speed = (int)cm.Parameters["@speed"].Value;
                    }
                    catch 
                    {
                        evt_id = -1;
                        zone_id = -1;
                        mark = -1;
                        local_timestamp = DateTime.Now.AddDays(-1);
                        speed = -1;
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }
        }

        public bool IsZonePortOutOfPolicy(int veh_id, int last_zone_id, int zone_evt, int last_io, int delay_time)
        {
            bool bRet = false;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_IsZonePortOutOfPolicy]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", zone_evt);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_io_status", last_io);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@delay_time", delay_time);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ret", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        int ret = (int)cm.Parameters["@ret"].Value;
                        if (ret > 0)
                        {
                            bRet = true;
                        }
                    }
                    catch 
                    {
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }

            return bRet;
        }

        public Boolean GetVehicleMagneticType(int veh_id)
        {
            bool isSet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spGet_vehmagnetic_setting";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    try
                    {
                        DataTable fzTable = new DataTable();
                        SqlDataAdapter adp = new SqlDataAdapter(cm);
                        adp.Fill(fzTable);
                        if (fzTable.Rows.Count > 0)
                        {
                            isSet = true;
                        }
                    }
                    catch
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return isSet;
        }

        public DataTable GetVehicleMagneticParam(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spGet_vehmagnetic_param";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        //����¹ table
        //�ѧ�������
        public DataTable GetVehicleMagneticParam_new(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spGet_vehmagnetic_param_New";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }
        public DataTable GetVehicleMagneticLastNoCard(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spGet_vehmagnetic_lastNoCard2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        /// <summary>
        /// /
        /// </summary>
        /// <param name="veh_id"></param>
        /// <returns></returns>
        public bool GetTrueVehicle(int veh_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue_GetVehicle]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ret", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    int cnt = (int)cm.Parameters["@ret"].Value;
                    if (cnt > 0) bRet = true;

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        //1 Get True veh and update veh data automatic
        public DataTable True2GetTrueCorpVehicle(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_GetVehicle]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        //2 GET ZONE BY GROUP
        public DataTable True2GetVehicleZone(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_GetVehZone";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
     
        //3
        public void True2GetZoneLastStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            evt_id = -1;
            zone_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_GetZone_GetLastStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        zone_id = -1;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        //4 GET ZONE BY GROUP Type
        public DataTable True2GetVehicleZoneGroupType(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_GetVehZoneGroupType]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }

        //5 Updata zone state
        public void True2InsertPrvZoneState(int veh_id, int zone_id, int evt_id, int ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_MsgZone_InsertPrvZoneState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }

        //5 insert log zone
        public int True2InsertMsgZone(int refIdx, int zoneId, int evt_id)
        {
            int lasted_index = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_MsgZone_InsertLog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zoneId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lasted_index", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        lasted_index = (int)cm.Parameters["@lasted_index"].Value;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return lasted_index;
        }

        //6 insert type group
        public Boolean True2InsertMsgZoneTypeGroup(int id_insert, int group_id, int type_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_MsgZone_InsertLog_Type_Group";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@id_insert", id_insert);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@group_id", group_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_id", type_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        //7 insert log IO
        public Boolean True2InsertMsgIO(int refIdx, int evt_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_IOstatus_InsertLog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        public DataTable True2GetAllTrueZone()
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_GetAllZone";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        /// <summary>
        ///  7 Update Status IO
        /// </summary>
        /// <param name="refIdx"></param>
        /// <param name="evt_id"></param>
        /// <returns></returns>
        public Boolean True2InsertTruePrvIOState(int veh_id ,int port, int refIdx)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_IOstatus_InsertPrvState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port_ref_idx", refIdx);
                    cm.Parameters.Add(param1);


                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////

        public DataTable GetTrueZone()
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue_GetZone]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public void GetLastTrueZoneStatus(int veh_id, int zone_id, out int evt_id)
        {
            evt_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue_GetLastZoneStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                    }
                    catch 
                    {
                        evt_id = -1;
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }
      
        }
        public void InsertPrvTrueZoneState(int veh_id, int zone_id, int evt_id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue_InsertPrvZoneState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
       
        }
        public void InsertTrue2TempReport(int ref_idx, int zone_id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_InsertTempReport";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void GetLastTrueIOStatus(int veh_id, out int state, out int ref_idx)
        {
            state = 0;
            ref_idx = 0;
         
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_IOstatus_GetLastStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (int)cm.Parameters["@ref_idx"].Value;
                    }
                    catch 
                    {
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }
        }
        public void InsertTruePrvIOState(int veh_id, int port, int ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue_SavePrvIOState]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //����¹ table **������
        public DataTable GetFleetZoneCurfewByVeh(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetFleetZoneCurfew]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    
                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }
        //����¹ Table
        //�ѧ�������
        public DataTable GetFleetZoneCurfewByVeh_new(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetFleetZoneCurfew_New]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //����¹ table **������
        public DataTable GetFleetZoneCurfewWithSpeedByVeh(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetFleetZoneCurfewWithSpeed]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        //����¹ Table
        //�ѧ�������
        public DataTable GetFleetZoneCurfewWithSpeedByVeh_new(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetFleetZoneCurfewWithSpeed_New]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        public DataTable GetRouteByVeh(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetRoute]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public DataTable GetRouteProfile(int route_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spRoute_GetProfile]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@route_id", route_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        public DataTable GetRouteWayPoints_M(int route_id,int number)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spRouteM_GetWayPoints]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@route_id", route_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@no", number);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public DataTable GetRouteWayPointsM(int route_id, int route_no)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spRouteM_GetWayPoints]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@route_id", route_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@route_id", route_no);
                    cm.Parameters.Add(param1);


                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public DataTable GetRouteWayPoints(int route_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spRouteM_GetWayPoints]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@route_id", route_id);
                    cm.Parameters.Add(param1);

                   
            

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public Boolean InsertMsgRoute(int refIdx, int routeId, int evt_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgRoute_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@route_id", routeId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                   
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        public void GetLastRouteStatus(int veh_id, int last_route_id, out int evt_id, out int route_id)
        {
            evt_id = -1;
            route_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spRoute_GetLastStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_route_id", last_route_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@route_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        route_id = (int)cm.Parameters["@route_id"].Value;
                    }
                    catch 
                    {
                        evt_id = -1;
                        route_id = -1;
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }
        }
        public void InsertPrvRouteState(int veh_id, int route_id, int evt_id, int ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgRoute_InsertPrvRouteState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@route_id", route_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { } 
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        public void GetLastIOStatus(int veh_id, out int state, out int ref_idx)
        {
            state = 0;
            ref_idx = 0;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetLastIOStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (int)cm.Parameters["@ref_idx"].Value;
                    }
                    catch 
                    {
                    }
                 
                    cn.Close();
                }
            }
            catch 
            {
            }
       
        }
        public void GetLastIOStatus_External(int veh_id, out int state, out decimal ref_idx)
        {
            state = 0;
            ref_idx = -1.0m;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastIOStatus_External";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Decimal);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (decimal)cm.Parameters["@ref_idx"].Value;
                    }
                    catch 
                    {
                    } 
                
                    cn.Close();
                }
            }
            catch 
            {
            }
        }
        public void GetLastIOStatusTime(int veh_id, out int state, out int ref_idx, out DateTime timestamp)
        {
            state = 0;
            ref_idx = 0;
            timestamp = DateTime.Now;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_GetLastIOStatusTime";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);
                   
                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (int)cm.Parameters["@ref_idx"].Value;
                        timestamp = (DateTime)cm.Parameters["@timestamp"].Value;
                    }
                    catch 
                    {
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }
        }

        //GET_MAP////////////////////////////////////////////////////////////////////////////////////////
        public DataTable GetMapAdminPolyWayPoints(int id)
        {
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spMap_GetAreaWayPoints]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@id", id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable); 

                    cn.Close();
                }
            }
            catch 
            {
            }

            return areaTable;
        }
        public DataTable GetMapAdminPolyIDs(float lat, float lon)
        {
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spMap_GetAreaIDs]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lon", lon);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable);

                    cn.Close();
                }
            }
            catch 
            {
            }
          
            return areaTable;
        }
        public DataTable GetMapLandmark(float lon, float lat)
        {
           
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spMap_GetLandmarkIDs]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lon", lon);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable);
                 
                    cn.Close();
                }
            }
            catch 
            {
            }
       
            return areaTable;
        }

        public DataTable GetProvince_id(int id)
        {
         
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spMap_GetProvince_id]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@id", id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable);
                    
                    cn.Close();
                }
            }
            catch 
            {
            }

            return areaTable;
        }
        public DataTable GetMapAdminName(int id)
        {
         
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spMap_GetAdminName]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@id", id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable);
                    
                    cn.Close();
                }
            }
            catch 
            {
            }

            return areaTable;
        }
        public DataTable GetMapLandmarkName(int id)
        {
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spMap_GetLandmarkName]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@id", id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable);
                    cn.Close();
                }
            }
            catch
            {
            }
          
            return areaTable;
        }
        public DataTable GetMapCustomerLandmark(float lon, float lat)
        {
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spMap_GetCustomerLandmarkIDs]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lon", lon);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable); 
                    cn.Close();
                }
            }
            catch 
            {
            }

          return areaTable;
        }
        public DataTable GetMapCustomerLandmarkName(int id)
        {
            DataTable areaTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spMap_GetCustomerLandmarkName";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@id", id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(areaTable);
                    

                    cn.Close();
                }
            }
            catch 
            {
            }
            return areaTable;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        public DataTable GetETAListByVeh1(int VehID)
        {
            DataTable dtETA = new DataTable();

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spETA_GetListByVeh1]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", VehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtETA);

                    cn.Close();
                }
            }
            catch 
            {
            }
         
            return dtETA;
        }
        public DataTable GetETAListByVeh2(int VehID)
        {
            DataTable dtETA = new DataTable();

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spETA_GetListByVeh2]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", VehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtETA);
                

                    cn.Close();
                }
            }
            catch 
            {
            }
          
            return dtETA;
        }
        public DataTable GetLastZoneByVeh(int VehID)
        {
            DataTable dtZone = new DataTable();

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_GetLastZoneByVeh]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", VehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtZone);

                    cn.Close();
                }
            }
            catch 
            {
            }
        
            return dtZone;
        }

        public void GetZoneFirstPoint(int ZoneID, out double lat, out double lon)
        {
            lat = 0.0;
            lon = 0.0;
         
            DataTable dtWP = new DataTable();

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spZone_GetZoneFirstPoint]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@zone_id", ZoneID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtWP);
                    if (dtWP.Rows.Count > 0)
                    {
                        lat = (double)dtWP.Rows[0]["lat"];
                        lon = (double)dtWP.Rows[0]["lon"];
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }

        }
        public Boolean InSertUpdateETA_M(decimal ref_idx,int veh_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgETA_InsertCurrent";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                  
                    
                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }

            }catch(Exception ex)
            {

            }
            return bRet;
        }
        public Boolean InsertMsgETA_M(decimal ref_idx, int eta_id, decimal time_left, int destination_id, int eta_route_no,decimal distance)
        {

            Boolean bRet = false;
            try
            {
              
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgETA_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_id", eta_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@time_left", time_left);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@destination_id", destination_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_route_no", eta_route_no);
                    cm.Parameters.Add(param1);
                    param1 = new SqlParameter("@distance", distance);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public Boolean InsertMsgETA(decimal ref_idx, int eta_id, double time_left, int destination_id)
        {
   
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgETA_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_id", eta_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@time_left", time_left);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@destination_id", destination_id);
                    cm.Parameters.Add(param1);

                    
                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    } 
                  
                    cn.Close();
                }
            }
            catch
            {
            }
           
            return bRet;
        }
        public int GetLastETAState(int veh_id)
        {
            int last_no = -1;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spETA_GetLastState]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_no", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    last_no = (int)cm.Parameters["@last_no"].Value;
            
                    cn.Close();
                }
            }
            catch 
            {
            }
            return last_no;
        }
        public int GetRouteNo(int veh_id)
        {
            int rid = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spRouteM_GetRouteNo]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);


                    cm.ExecuteNonQuery();
                    rid = (int)cm.Parameters["@rid"].Value;

                    cn.Close();
                }
            }
            catch (Exception ex)
            {


            }
            return rid;
        }
        public decimal GetDistanceETAFirstCarToMe(int first_rid,int my_rid,int eta_id)
        {
            decimal etadis = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spRouteDiff_FirstToMe]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@f_rid", first_rid);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@m_rid", my_rid);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_id", eta_id);
                    cm.Parameters.Add(param1);


                    cm.ExecuteNonQuery();
                    etadis = (decimal)cm.Parameters["@difdata"].Value;

                    cn.Close();
                }
            }catch(Exception ex){


            }
            return etadis;
        }
        public DataTable GetETADropZone(int veh_id, int route_id)
        {
            DataTable dpZoneTable = new DataTable();

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spETA_GetDropZone]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@route_id", route_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dpZoneTable);
                    cn.Close();
                }
            }
            catch 
            {
            }
            return dpZoneTable;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //����¹ table **������
        public DataTable GetEngineOnEventByVeh(int veh_id)
        {
         
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetEngineOnEvent]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
         
            return fzTable;
        }
        //�ѧ�������
        public DataTable GetEngineOnEventByVeh_new(int veh_id)
        {

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetEngineOnEvent_New]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public void GetLastEngineOnEventStatus(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            evt_id = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetLastEngineOnEventStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch 
                    {
                        evt_id = -1;
                        local_timestamp = DateTime.Now;
                    }
                  
                    cn.Close();
                }
            }
            catch 
            {
            }
        }
        public void GetLastEngineOnCurfewEventStatus(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            evt_id = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_GetLastEngineOnCurfewEventStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 30;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch 
                    {
                        evt_id = -1;
                        local_timestamp = DateTime.Now;
                    } 

                    cn.Close();
                }
            }
            catch 
            {
            }
         
        }
        public Boolean InsertMsgEngineOnEvent(int refIdx, int evt_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgEngineOnEvent_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        //����¹ table **������
        public DataTable GetTemperatureByVeh(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetTemperatureByVID]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    cn.Close();
                }
            }
            catch
            {
            }
     
            return fzTable;
        }
        //�ѧ������� 
        public DataTable GetTemperatureByVeh_new(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    //rose
                    //old
                    //spVeh_GetTemperatureByVID_New
                    cm.CommandText = "[spVeh_GetTemperatureByVID_Param_type]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);
                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public void GetLastTemperatureEventStatus(int veh_id, out int evt_id, out int last_temperature, out int cur_temperature, out DateTime local_timestamp)
        {
            evt_id = -1;
            last_temperature = cur_temperature = 0;
            local_timestamp = DateTime.Now.AddDays(-1);
        
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetLastTemperatureEventStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_temperature", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@cur_temperature", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);


                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        last_temperature = (int)cm.Parameters["@last_temperature"].Value;
                        cur_temperature = (int)cm.Parameters["@cur_temperature"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        last_temperature = 0;
                        cur_temperature = 0;
                        local_timestamp = DateTime.Now;
                    }
                
                    cn.Close();
                }
            }
            catch 
            {
            }
          
        }
        public Boolean InsertMsgTemperatureEvent(decimal refIdx, int evt_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsgTemperatureEvent_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                
                    cn.Close();
                }
            }
            catch
            {
            }
         
            return bRet;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        public int GetVehSpeedHazard(int veh_id, int speed, int type_of_msg)
        {
            int iMaxSpeed = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetSpeedHazard]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_msg", type_of_msg);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);
                    
                    cn.Close();
                }
            }
            catch
            {
                iMaxSpeed = 1000;
            }
       
            return iMaxSpeed;
        }
        public int GetVehAngleHazard(int veh_id, int speed, int type_of_msg)
        {
         
            int iMaxSpeed = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetAngleHazard]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_msg", type_of_msg);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);
             
                    cn.Close();
                }
            }
            catch
            {
                iMaxSpeed = 1000;
            }
     
            return iMaxSpeed;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        public int GetFeatureExistence(int veh_id, int evt_id)
        {
            return 1;
        }
        //����¹ table 
        public DataTable GetFeatureEvtParam(int veh_id, int evt_id)
        {
            DataTable dtRet = new DataTable();

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spFeature_GetEvtParam]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    try
                    {
                        SqlDataAdapter adp = new SqlDataAdapter(cm);
                        adp.Fill(dtRet);
                    }
                    catch 
                    {
                    }
                    
                    cn.Close();
                }
            }
            catch 
            {
            }
        
            return dtRet;
        }
        //�ѧ�������
        public DataTable GetFeatureEvtParam_new(int veh_id, int evt_id)
        {
            DataTable dtRet = new DataTable();

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spFeature_GetEvtParam_New]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    try
                    {
                        SqlDataAdapter adp = new SqlDataAdapter(cm);
                        adp.Fill(dtRet);
                    }
                    catch
                    {
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return dtRet;
        }
        //����¹ table 
        public int GetDisconnectedPowerEvtParam(int veh_id)
        {
            int delay_time = 0;
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spDiscnPowerLineFeature_GetEvtParam";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@return_value", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        delay_time = (int)cm.Parameters["@return_value"].Value;
                    }
                    catch
                    {
                    } 
                    cn.Close();
                }
            }
            catch 
            {
            }
        
            return delay_time;
        }
        //�ѧ�������
        public int GetDisconnectedPowerEvtParam_new(int veh_id)
        {
            int delay_time = 0;
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spDiscnPowerLineFeature_GetEvtParam_New";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@return_value", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        delay_time = (int)cm.Parameters["@return_value"].Value;
                    }
                    catch
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return delay_time;
        }
        public int GetTestVehicle(int zVehID)
        {
            int iRet = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTest_GetVehicle";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ret", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        iRet = (int)cm.Parameters["@ret"].Value;

                    }
                    catch
                    {
                    }
 

                    cn.Close();
                }
            }
            catch 
            {
            }
        
            return iRet;
        }
        public DataTable GetTestZone()
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTest_GetZone";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);


                    cn.Close();

                }
            }
            catch 
            {
            }

            return dtRet;
        }
        public void GetLastTestZoneStatus(int veh_id, int zone_id, out int evt_id)
        {
       
            evt_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTest_GetLastZoneStatus]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                    }

                    cn.Close();
                }
            }
            catch 
            {
            }
        }

        public void InsertPrvTestZoneState(int veh_id, int zone_id, int evt_id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTest_InsertPrvZoneState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { } 

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public DataTable GetTrueZoneAlert2(int veh_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_GetZoneAlert";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);

                    cn.Close();
                }
            }
            catch
            {
            }
        
            return dtRet;
        }

        public void InsertNewLogOptional(int zVehID, DateTime zLocalTimestamp, int zInputID, int zMessageType, int zStatusByte, int zInputMark, int zReportTime, int zInputStatus, int zRefIdx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewOptional";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", zLocalTimestamp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input_id", zInputID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@message_type", zMessageType);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@status_byte", zStatusByte);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input_mark", zInputMark);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@report_time", zReportTime);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input_status", zInputStatus);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", zRefIdx);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
      }
      //lasted_index, Convert.ToInt32(msg.vid), driver_type, driver_sex, driver_number, country_code
        public void InsertNewDriverLicence(int zRefIdx, int zVehID, string zDriver_type, string zDriver_sex,
            string zDriver_number, string zProvince, string zDistic,
            string zDriver_name, string zId_country, string zId_card, string zExpiry_date, string zBirthday) // province_code, distic_code
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertDriverLicence";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@id_ref", zRefIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_type", zDriver_type);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_sex", zDriver_sex);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_number", zDriver_number);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@province", zProvince);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@distic", zDistic);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_name", zDriver_name);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@id_country", zId_country);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@id_card", zId_card);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@expiry_date", zExpiry_date);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@birthday", zBirthday);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch(Exception ex)
            {
            }
        }

        public bool GetVehTypeOfBox(string zBoxType, int zVehID)
        {
            bool bRet = false;
            try
            {
                DataTable dtRet = new DataTable();
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spGetTypeOfBox";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 30;

                    SqlParameter param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);

                    cn.Close();

                    if (dtRet.Rows.Count > 0)
                    {
                        if (dtRet.Rows[0]["type_of_box"].ToString().ToUpper() == zBoxType.ToUpper())
                        {
                            bRet = true;
                        }
                    }
                }
            }
            catch
            {
            }

            return bRet;
        }
        public void InsertLogTagMsg(int zRefIdx, int zVeh,string zTag_msg)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertTagMsg";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 30;

                    SqlParameter
                    param1 = new SqlParameter("@ref_idx", zRefIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", zVeh);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@tag_msg", zTag_msg);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        public int GetLastEngineEventStatus(int veh_id)
        {
            int evt_id = -1;
            DateTime local_timestamp = new DateTime();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_GetLastEngineEventStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);
                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return evt_id;
        }
        public int GetOptionalMsgTypeIDByMsgType(string zMessageType)
        {
            int retID = -1;
            try
            {
                DataTable dtRet = new DataTable();
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spOptional_GetMsgType";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@msg_type", zMessageType);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
 
                    if (dtRet.Rows.Count > 0)
                    {
                        retID = (int)dtRet.Rows[0]["msg_id"];
                    }
                }
            }
            catch
            {
            }
        
            return retID;
        }
       
        public bool IsVehFromDFM_TOR(int zVehID)
        {
            bool bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spDFM_GetVehicle";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@iRet", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);
                    cm.ExecuteNonQuery();
 
                    int iRet = (int)cm.Parameters["@iRet"].Value;
                    if (iRet > 0)
                    {
                        bRet = true;
                    }
                   
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
   
        public int GetExternalInput(int zRefIdx, int zVehID)
        {
            int input = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spDFM_GetExternalIOStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@ref_idx", zRefIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);
                    cm.ExecuteNonQuery();

                    input = (int)cm.Parameters["@input"].Value;
                    cn.Close();
                }
            }
            catch
            {
            }

            return input;
        }
        public int GetExternalInput2(int zRefIdx, int zVehID)
        {
            int input = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spDFM_GetExternalIOStatus2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@ref_idx", zRefIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);
                    cm.ExecuteNonQuery();

                    input = (int)cm.Parameters["@input"].Value;
                    cn.Close();
                }
            }
            catch
            {
            }
         
            return input;
        }
        public void InsertDFM_AllInput(int zVehID, int zRefID, int isEngineOn, int isMainLightOn, int isPTOSwitchOn, int isHornOn, int isMinorLightOn, int isLeftSignalOn, int isRightSignalOn, int isWiperOn, int isReverseLightOn)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spDFM_InsertAllInput";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", zRefID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_engineOn", isEngineOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_mainLightOn", isMainLightOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_PTOSwitchOn", isPTOSwitchOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_hornOn", isHornOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_minorLightOn", isMinorLightOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_leftSignalOn", isLeftSignalOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_rightSignalOn", isRightSignalOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_wiperOn", isWiperOn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_reverseLightOn", isReverseLightOn);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        public void InsertExternalAllInput(int zVeh, int zRefID, int input01, int input02, int input03, int input04, int input05, int input06)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertExternalInput";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", zVeh);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", zRefID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input01", input01);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input02", input02);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input03", input03);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input04", input04);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input05", input05);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input06", input06);
                    cm.Parameters.Add(param1);
              
                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public EVT_TYPE GetExternalInputSettingEvent(int veh_id, int port)
        {
            EVT_TYPE retID = EVT_TYPE.NONE;
            try
            {
                DataTable dtRet = new DataTable();
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spOptional_GetMsgType";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();


                    if (dtRet.Rows.Count > 0)
                    {
                        retID = (EVT_TYPE)dtRet.Rows[0]["msg_id"];
                    }
                }
            }
            catch
            {
            }

            return retID;
        }
        public EVT_TYPE GetIOSettingEvent(int veh_id, int port, int status_onoff, char io_type, int internal_port)
        {
            EVT_TYPE retID = EVT_TYPE.NONE;
            try
            {
                DataTable dtRet = new DataTable();
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_IOConfigEvent";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@status_onoff", status_onoff);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@io_type", io_type);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@internal_port", internal_port);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();

                    if (dtRet.Rows.Count > 0)
                    {
                        retID = (EVT_TYPE)dtRet.Rows[0]["evt_setting"];
                    }
                }
            }
            catch
            {
            }

            return retID;
        }

        public void UpdateVehAndRegConfigIO(int zVeh)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_UpdateVehAndRegConfigIO";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", zVeh);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////

        public void WriteLogEvent(String msg)
        {
            /*
            msg = DateTime.Now.ToString() + " : " + msg;
            String fileName = "SQLERROR.txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
             */
        }
        ////////////////////////////////////Insert_Log_Msg_Error///////////////////////////////////////////
        public int InsertMsg_Error(int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg)
        {
            int lasted_index = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewRecordError";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lon", lon);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@course", course);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_fix", type_of_fix);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@no_of_satellite", no_of_satellite);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@in_out_status", in_out_status);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog_level", analog_level);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_msg", type_of_msg);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lasted_index", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);
                    
                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        lasted_index = (int)cm.Parameters["@lasted_index"].Value;
                    }
                    cn.Close();
                }
            }
            catch 
            {
            }
        
            return lasted_index;
        }
        public Boolean UpdateMsg_Error( int idx, int last_idx, double last_lat, double last_lon, int no_of_Satellite)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_UpdateError";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_idx", last_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_lat", last_lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_lon", last_lon);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@no_of_Satellite", no_of_Satellite);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                   
                    cn.Close();
                }
            }
            catch 
            {
            }
          
            return ret;
        }
        public DataTable GetMsgLatLon(int idx,int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spLogMsg_GetLatLon]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch 
            {
            }
        
            return fzTable;
        }
        public Boolean GetEngineOnByTime(int veh_id)
        {
            Boolean ret = false;
        
            try
            {
                {
                    DataTable dtRet = new DataTable();
                    using (SqlConnection cn = new SqlConnection(connStr))
                    {
                        cn.Open();

                        SqlCommand cm = new SqlCommand();
                        cm.CommandText = "spVeh_GetLastEngineOnCurfew";
                        cm.CommandType = System.Data.CommandType.StoredProcedure;
                        cm.Connection = cn;
                        cm.CommandTimeout = 60 * 5;

                        SqlParameter
                        param1 = new SqlParameter("@veh_id", veh_id);
                        cm.Parameters.Add(param1);

                        SqlDataAdapter adp = new SqlDataAdapter(cm);
                        adp.Fill(dtRet);
                        cn.Close();

                        if (dtRet.Rows.Count > 0)
                        {
                            ret = (bool)dtRet.Rows[0]["is_engine_on"];
                        }
                    }
                }
            }
            catch { }
          
            return ret;
        }
        public void GetFuleSeting(int veh_id, int port_id,bool status,out int evt_id )
        {
            evt_id = -1;
            try
            {
                DataTable fzTable = new DataTable();

                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_GetFuleSeting";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@status", status);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();

                    if (fzTable.Rows.Count > 0)
                    {
                        evt_id = (int)fzTable.Rows[0]["evt_id"];
                    }
                }

            }
            catch { }
        }
        public void InsertOdometer(int ref_idx, double odometer)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertOdometer";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx_ref", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@odometer", odometer);
                    cm.Parameters.Add(param1);


                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertPowerLevel(int ref_idx, int veh_id, int internal_power,int external_power)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertPowerlevel";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@idx_ref", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@internal_volts", internal_power);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@external_volts", external_power);
                    cm.Parameters.Add(param1);


                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public DataTable GetCurLoc(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_GetCurLoc";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public bool InsertLocationDistance(int ref_idx, double distance)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertDistance";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx_ref", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@distance", distance);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }
        public bool UpdateCurLocDistance(int veh_id, double distance)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_UpdateCurLocDistance";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@distance", distance);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }

        ///
        /// FW Version
        ///
        public void InsertFwVersion(int veh_id, int ref_idx, string fw)
        {
           // bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertFW_Version";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@idx_ref", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@fw_vs", fw);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                     //   ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// KPI Detect Event
        /// </summary>
        /// 
        public DataTable KPIGetZoneByVeh(int VehID, int ZoneID)
        {
            DataTable dtZone = new DataTable();

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spKPI_Veh_GetZone";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", VehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", ZoneID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtZone);

                    cn.Close();
                }
            }
            catch
            {
            }

            return dtZone;
        }
        public DataTable KPIGetLastZone(int veh_id, int zone_id, int evt_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spKPI_Veh_GetLastZone";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }
        public void KPIInsertLogZone(int ref_idx, int ref_veh_id, string recsts, int kpi_type, int ref_log, DateTime ref_log_timestamp)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spKPI_LogMsg_InsertZone";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", ref_veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@recsts", recsts);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@KPI_type", kpi_type);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_log", ref_log);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_log_timestamp", ref_log_timestamp);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch(Exception ex)
            {
            }
        }
        public void InsertNewBoxStatus(int zVehID, string zIP, int zPort, bool isConnected, string zLocalIP, int zLocalPort,string zType_of_box)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connStr);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "spBox_InsertNewStatus";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", zVehID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@ip", zIP);
                cm.Parameters.Add(p);

                p = new SqlParameter("@port", zPort);
                cm.Parameters.Add(p);

                p = new SqlParameter("@isConnected", isConnected);
                cm.Parameters.Add(p);

                p = new SqlParameter("@local_port", zLocalPort);
                cm.Parameters.Add(p);

                p = new SqlParameter("@local_ip", zLocalIP);
                cm.Parameters.Add(p);

                p = new SqlParameter("@type_of_box", zType_of_box);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
            }
        }

        public bool GetTypeOfBox(int veh_id)
        {
            bool isExcept = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spGetTypeOfBoxIsExcept";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        if ((int)dt.Rows[0]["cnt"] > 0)
                        {
                            isExcept = true;
                        }
                    }
                     
                    cn.Close();
                }
            }
            catch
            {
            }

            return isExcept;
        }

        public bool InsertSmartCardEvent(int veh_id, DateTime dtTimestampe, int ref_idx, int iAddress1, string sAddress2, string sData, string sTag)
        {
            bool ret = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewSmartCardEvent";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@iAddress1", iAddress1);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@sAddress2", sAddress2);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@sData", sData);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@sTag", sTag);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }

        public DataTable GetRPM_Config(int veh_id ,int raw_rpm)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spVeh_GetRPM_Setting";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@raw_rpm", raw_rpm);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }
            return fzTable;
        }

        public bool InsertLogRPM(int ref_idx, int raw_data, int rpm)
        {
            bool ret = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spLogMsg_InsertNewRPM";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@raw_data", raw_data);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@rpm", rpm);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }

        public void InsertNewExternal_Input(int veh_id, int External01, int External02, int External03, int External04)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connStr);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "Insert_External_Input2";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", veh_id);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External01", External01);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External02", External02);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External03", External03);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External04", External04);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
            }
        }

        public void InsertNewExternal_Input2(int veh_id, int External05, int External06, int External07, int External08)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connStr);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "Insert_External_Inputs2";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", veh_id);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External05", External05);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External06", External06);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External07", External07);
                cm.Parameters.Add(p);

                p = new SqlParameter("@External08", External08);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
            }
        }


        public int getDefaultInput(int veh_id)
        {
            int value = 0;
            try
            {
                /*
                DataTable dtRet = new DataTable();
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "Check_Externalinput";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }*/

                return -12;
            }
            catch
            {
            }
            return 0;
        }
 
        /// <summary>
        /// ///////////////
        /// </summary>

        System.Threading.Semaphore semaphor = new System.Threading.Semaphore(1, 1);
        public void WriteLogStoredProcedure(String StorName, DateTime zStart, DateTime zEnd, string total_of_millisec)
        {
            try
            {
                semaphor.WaitOne();

                string msg = zStart.ToLongTimeString() + "," + zEnd.ToLongTimeString() + "," + StorName + "," + total_of_millisec + ",Ms";

                String logPath = Directory.GetCurrentDirectory() + "\\Log";
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                String fileName = logPath + "\\" + "ElapsedTimeStor" + DateTime.Now.ToString("_yyyy_MM_dd") + ".txt";

                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }

                semaphor.Release();
            }
            catch { }
        }

        public DataTable GetFleetDistanceOver(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spVeh_GetFleetDistanceOver]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }


    }
}
