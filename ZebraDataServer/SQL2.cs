using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using ZebraCommonInterface;
using System.IO;

namespace ZebraDataServer
{
    public class SQL2
    {
        private String connStr = "";
        private String connStrDLT = "";
        public SQL2(String dbServer)
        {
            connStr = String.Format("Data Source={0}; Database=ZebraDB;  UID=sa; PWD=aab-gps-gtt; Pooling=true; Min Pool Size=20; Max Pool Size=500", dbServer);
        }
        
        public void InsertID_DLTESRI(int VehID, string Driver_ID, string License)
        {
            try
            {
                connStrDLT = String.Format("Data Source={0}; Database=DLTDB;  UID=sa; PWD=1q2w&dltesri; Pooling=true; Min Pool Size=20; Max Pool Size=500", "10.0.10.76");
                SqlConnection cn = new SqlConnection(connStrDLT);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "Update_driver";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", VehID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@driver_id", Driver_ID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@license", License);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public bool GetAccessorieByVid(int veh_id)
        {
            //rose create method from date 2017-05-11
            DataTable tb = new DataTable();
            bool ch= false;
            try
            {
                using(SqlConnection cn =new SqlConnection(connStr))
                {
                    cn.Open();
                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "Get_VID";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(tb);

                    cn.Close();


                }
                if (tb.Rows.Count != 0 && tb != null)
                {
                   int id = Convert.ToInt32(tb.Rows[0]["Acc"]);

                    if(id == 1)
                    {
                        ch=false;
                    }
                }
            }
            catch
            {

            }

            return ch;
        }
        public String Getvid(string Imei)
        {
            DataTable vid = new DataTable();
            string veh_id = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "Get_VID";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@Imei", Imei);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(vid);

                    cn.Close();
                    if (vid.Rows.Count != 0 && vid != null)
                    {
                        veh_id = Convert.ToString(vid.Rows[0]["veh_id"]);
                    }
                 
                }
            }
            catch (Exception ex)
            {
            }

            return veh_id;
        }

        public string GetDriverID(int VehID)
        {
            DataTable driver_id= new DataTable();
            string driver = "";
            try
            {
                SqlConnection cn = new SqlConnection(connStr);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "Get_DriverID";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", VehID);
                cm.Parameters.Add(p);

                SqlDataAdapter adp = new SqlDataAdapter(cm);
                adp.Fill(driver_id);

                cm.ExecuteNonQuery();

                if (driver_id.Rows.Count != 0 && driver_id != null)
                {
                    driver = Convert.ToString(driver_id.Rows[0]["ID"]);
                }
                cn.Close();
            }
            catch (Exception ex)
            {
            }

            return driver;
        }

        public void updateDriverID(string DriverID, int veh_id)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connStr);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "UpdateDriverID_Meitrack";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@ID", DriverID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@veh_id", veh_id);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
            }
        }

        public void InsertNewBoxStatus(int zVehID, string zIP, int zPort, bool isConnected, string zLocalIP, int zLocalPort, string zType_of_box)
        {
            try
            {
                SqlConnection cn = new SqlConnection(connStr);
                cn.Open();

                SqlCommand cm = new SqlCommand();
                cm.CommandText = "DS2_Box_InsertNewStatus";
                cm.CommandType = CommandType.StoredProcedure;
                cm.CommandTimeout = 30;
                cm.Connection = cn;

                SqlParameter p;
                p = new SqlParameter("@veh_id", zVehID);
                cm.Parameters.Add(p);

                p = new SqlParameter("@ip", zIP);
                cm.Parameters.Add(p);

                p = new SqlParameter("@port", zPort);
                cm.Parameters.Add(p);

                p = new SqlParameter("@isConnected", isConnected);
                cm.Parameters.Add(p);

                p = new SqlParameter("@local_port", zLocalPort);
                cm.Parameters.Add(p);

                p = new SqlParameter("@local_ip", zLocalIP);
                cm.Parameters.Add(p);

                p = new SqlParameter("@type_of_box", zType_of_box);
                cm.Parameters.Add(p);

                cm.ExecuteNonQuery();
                cn.Close();
            }
            catch
            {
            }
        }
        public bool InsertTemperatureMsg(int veh_id, int temp_id, DateTime dtTimestampe, float temp_value, float max_temp, float min_temp)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewTemperature";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temp_id", temp_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temperature", temp_value);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@max_temperature", max_temp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@min_temperature", min_temp);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return ret;
        }
        public bool InsertTemperatureMsg_New(int veh_id, int temp_id, DateTime dtTimestampe, float temp_value, float temp1_value, float temp2_value, float max_temp, float min_temp)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewTemperature";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temp_id", temp_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temperature", temp_value);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temp1", temp1_value);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@temp2", temp2_value);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@max_temperature", max_temp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@min_temperature", min_temp);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return ret;
        }
        public bool InsertSmartCardMsg(int veh_id, DateTime dtTimestampe, String data, int evt, decimal ref_idx)
        {
            if (ref_idx < 1.0m) return false;

            bool ret = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewSmartCardData";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@data", data);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }
        public bool InsertMsg_Error(decimal idx, int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg)
        {
            if (idx < 1.0m) return false;

            bool is_complete = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewRecordError";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lon", lon);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@course", course);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_fix", type_of_fix);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@no_of_satellite", no_of_satellite);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@in_out_status", in_out_status);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog_level", analog_level);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_msg", type_of_msg);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        is_complete = true;
                    }

                    cn.Close();


                }
            }
            catch
            {
            }

            return is_complete;
        }
        public bool InsertMsg(decimal idx, int veh_id, String dtTimestampe, float lat, float lon, int speed, int course, int type_of_fix, int no_of_satellite, int in_out_status, int analog_level, string type_of_msg)
        {
            if (idx < 1.0m) return false;

            bool is_complete = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewRecord"; //DS2_LogMsg_InsertNewRecord_tls
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lon", lon);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@course", course);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_fix", type_of_fix);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@no_of_satellite", no_of_satellite);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@in_out_status", in_out_status);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog_level", analog_level);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_of_msg", type_of_msg);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        is_complete = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return is_complete;
        }
        public void InsertMsgAnalog(int veh_id, string timestamp, decimal ref_idx, double analog2, double analog3)
        {
            if (ref_idx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewAnalog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", timestamp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog2", analog2);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@analog3", analog3);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertNewLogOptional(int zVehID, DateTime zLocalTimestamp, int zInputID, int zMessageType, int zStatusByte, int zInputMark, int zReportTime, int zInputStatus, decimal ref_idx)
        {
            if (ref_idx < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewOptional";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", zLocalTimestamp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input_id", zInputID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@message_type", zMessageType);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@status_byte", zStatusByte);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input_mark", zInputMark);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@report_time", zReportTime);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input_status", zInputStatus);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertNewDriverLicence(decimal zRefIdx, int zVehID, string zDriver_type, string zDriver_sex,
        string zDriver_number, string zProvince, string zDistic,
        string zDriver_name, string zId_country, string zId_card, string zExpiry_date, string zBirthday) // province_code, distic_code
        {
            if (zRefIdx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertDriverLicence";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@ref_idx", zRefIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_type", zDriver_type);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_sex", zDriver_sex);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_number", zDriver_number);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@province", zProvince);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@distic", zDistic);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@driver_name", zDriver_name);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@id_country", zId_country);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@id_card", zId_card);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@expiry_date", zExpiry_date);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@birthday", zBirthday);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public bool InsertGSMMsg(Decimal idx, string mcc, string mnc, string lac, string cell, string bsic, int arfcn, int rxlev)
        {
            if (idx < 1.0m) return false;

            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertGSM";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mcc", mcc);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mnc", mnc);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lac", lac);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@cell", cell);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@bsic", bsic);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@arfcn", arfcn);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@rxlev", rxlev);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return ret;
        }
        public void InsertFwVersion(int veh_id, Decimal ref_idx, string fw)
        {
            if (ref_idx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertFW_Version";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@idx_ref", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@fw_vs", fw);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertLogDistance(Decimal ref_idx, double distance, double odometer)
        {
            if (ref_idx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertLogDistance";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx_ref", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@distance", distance);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@odometer", odometer);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertLogTagMsg(Decimal zRefIdx, int zVeh, string zTag_msg)
        {
            if (zRefIdx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertTagMsg";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 30;

                    SqlParameter
                    param1 = new SqlParameter("@ref_idx", zRefIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", zVeh);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@tag_msg", zTag_msg);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public bool InsertLogRPM2(decimal ref_idx, int raw_data, int rpm)
        {
            if (ref_idx < 1.0m) return false;

            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewRPM";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@raw_data", raw_data);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@rpm", rpm);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }
        public void InsertPowerLevel(Decimal ref_idx, int veh_id, int internal_power, int external_power)
        {
            if (ref_idx < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertPowerlevel";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@internal_volts", internal_power);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@external_volts", external_power);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertPowerLevelKBox(Decimal ref_idx, int veh_id, int internal_power, int external_power, string charging, string capacity)
        {
            if (ref_idx < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertPowerlevel_KBox";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@internal_volts", internal_power);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@external_volts", external_power);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@charging", charging);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@capacity", capacity);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public DataTable GetCurLoc(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetCurLoc";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        public bool UpdateCurLocDistance(int veh_id, double distance)
        {
            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_UpdateCurLocDistance";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@distance", distance);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }
        public void UpdateVehCurLoc2(int veh_id, Decimal ref_idx, float lat, float lon)
        {
            if (ref_idx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_UpdateCurLoc2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_lat", lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_lon", lon);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void UpdateVehCurLoc(int veh_id, Decimal ref_idx)
        {
            if (ref_idx < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_UpdateCurLoc";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public Boolean InsertMsgEvt(Decimal refIdx, int veh_id, DateTime timestamp, int evtId, int remark, int evt_back_side)
        {
            if (refIdx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    //�������Ѿ lasteventtime �� DS2_Evt_InsertNewRecord3  , ������ DS2_Evt_InsertNewRecord2
                    cm.CommandText = "DS2_Evt_InsertNewRecord2";  //DS2_Evt_InsertNewRecord2_tls
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestamp", timestamp);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evtId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@remark", remark);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_back_side", evt_back_side);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return bRet;
        }
        public void GetLastIOStatus_External(int veh_id, out int state, out Decimal ref_idx)
        {
            state = 0;
            ref_idx = 0;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastIOStatus_External";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Decimal);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (Decimal)cm.Parameters["@ref_idx"].Value;
                    }
                    catch
                    {
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public Boolean InsertExternalInputEvt(int veh_id, Decimal refIdx, EVT_TYPE evtId)
        {
            if (refIdx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Evt_InsertExternalInput";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", (int)evtId);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return bRet;
        }
        public void GetLastIOStatus(int veh_id, out int state, out Decimal ref_idx)
        {
            state = 0;
            ref_idx = 0;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastIOStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@state", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", SqlDbType.Decimal);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        state = (int)cm.Parameters["@state"].Value;
                        ref_idx = (Decimal)cm.Parameters["@ref_idx"].Value;
                    }
                    catch
                    {
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertPrvIOState_External(int veh_id, int port, Decimal ref_idx)
        {
            if (ref_idx < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_IOPort_External_SavePrvState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertExternalAllInput(int zVeh, Decimal zRefID, int input01, int input02, int input03, int input04, int input05, int input06)
        {
            if (zRefID < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertExternalInput";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", zVeh);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", zRefID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input01", input01 == 0 ? false : true);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input02", input02 == 0 ? false : true);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input03", input03 == 0 ? false : true);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input04", input04 == 0 ? false : true);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input05", input05 == 0 ? false : true);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@input06", input06 == 0 ? false : true);
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        public void InsertPrvIOState(int veh_id, int port, Decimal ref_idx)
        {
            if (ref_idx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_IOPort_SavePrvState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public Boolean InsertLogUserDefined(int vehID, Decimal refIdx, int evtId, int defined)
        {
            if (refIdx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertUserDefined";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_id", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evtId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", vehID);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@defined", defined);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true; 
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public bool IsEngineOn(int veh_id)
        {
            bool bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_IsEngineOn";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@is_engine_on", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    int iRet = (int)cm.Parameters["@is_engine_on"].Value;

                    if (iRet == 1)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public void GetLastEventStatus(int veh_id, int evt_id, out decimal ref_idx, out DateTime timestamp)
        {
            ref_idx = -1.0M;
            timestamp = DateTime.Now;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastEvent";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);


                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        ref_idx = (decimal)cm.Parameters["@ref_idx"].Value;
                        timestamp = (DateTime)cm.Parameters["@timestamp"].Value;
                    }
                    catch
                    {
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public bool GetVehTypeOfBox(string zBoxType, int zVehID)
        {
            bool bRet = false;
            try
            {
                DataTable dtRet = new DataTable();
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetTypeOfBox";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 30;

                    SqlParameter param1 = new SqlParameter("@veh_id", zVehID);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);

                    cn.Close();

                    if (dtRet.Rows.Count > 0)
                    {
                        if (dtRet.Rows[0]["type_of_box"].ToString().ToUpper() == zBoxType.ToUpper())
                        {
                            bRet = true;
                        }
                    }
                }
            }
            catch
            {
            }

            return bRet;
        }
        public int GetLastEngineEventStatus(int veh_id)
        {
            int evt_id = -1;
            DateTime local_timestamp = new DateTime();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastEngineOnStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);
                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return evt_id;
        }
        /*****Event******/

        #region UpdateLogError
        public DataTable GetMsgLatLon(Decimal idx, int veh_id)
        {
            if (idx < 1.0m) return new DataTable();

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_GetLatLon";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        public Boolean UpdateMsg_ErrorLog(Decimal idx, Decimal last_idx, double last_lat, double last_lon, int no_of_Satellite)
        {
            if (idx < 1.0m) return false;

            bool ret = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_UpdateError";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_idx", last_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_lat", last_lat);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_lon", last_lon);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@no_of_Satellite", no_of_Satellite);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return ret;
        }

        #endregion

        #region Idle
        public Boolean GetVehIdleTimeLimit(int veh_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetIdleTime";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@return_value", SqlDbType.Bit);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    int a = cm.ExecuteNonQuery();
                    if ((Boolean)cm.Parameters["@return_value"].Value)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        //edit idle for speed commit
        public Boolean GetVehIdleTimeLimit_New(int veh_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetIdleTime_New";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    //SqlParameter param1 = new SqlParameter("@veh_id", lat);
                    //cm.Parameters.Add(param1);

                    //SqlParameter param1 = new SqlParameter("@veh_id", lon);
                    //cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@return_value", SqlDbType.Bit);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    cm.ExecuteNonQuery();
                    if ((Boolean)cm.Parameters["@return_value"].Value)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        #endregion

        #region Speeding
        public int GetVehSpeedLimit(int veh_id)
        {
            int iMaxSpeed = -1;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetSpeedViolate";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    try
                    {
                        foreach (DataRow r in dt.Rows)
                        {
                            iMaxSpeed = (int)r["max_speed"];
                            break;
                        }
                    }
                    catch
                    {
                        iMaxSpeed = 1000;
                    }
                    cn.Close();
                }
            }
            catch
            {
                iMaxSpeed = 1000;
            }

            return iMaxSpeed;
        }

        #endregion

        #region Zone
        public void GetLastZoneStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            evt_id = -1;
            zone_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Zone_GetLastStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        zone_id = -1;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        public void InsertPrvZoneState(int veh_id, int zone_id, int evt_id, Decimal ref_idx)
        {
            if (ref_idx < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsgZone_InsertPrvZoneState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        #endregion

        public void InsertPrvZoneState_Curfew(int veh_id, int zone_id, int evt_id, Decimal ref_idx)
        {
            if (ref_idx < 1.0m) return;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsgZone_InsertPrvZoneState_New";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }

        #region ZoneCurfew
        public void GetLastZoneCurfewStatus(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark)
        {
            evt_id = -1;
            zone_id = -1;
            mark = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "Ds2_Zone_GetLastZoneCurfewStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mark", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                        mark = (int)cm.Parameters["@mark"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        zone_id = -1;
                        mark = -1;
                        local_timestamp = DateTime.Now.AddDays(-1);
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }

        public Boolean InsertMsgZoneCurfew(int veh_id, decimal refIdx, int zoneId, int evt_id, int marked)
        {
            if (refIdx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Zone_UpdateZoneCurfew";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zoneId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@marked", marked);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        #endregion

        #region Route
        public void GetLastRouteStatus(int veh_id, int last_route_id, out int evt_id, out int route_id)
        {
            evt_id = -1;
            route_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Route_GetLastStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_route_id", last_route_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@route_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        route_id = (int)cm.Parameters["@route_id"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        route_id = -1;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void InsertPrvRouteState(int veh_id, int route_id, int evt_id, Decimal ref_idx)
        {
            if (ref_idx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsgRoute_InsertPrvRouteState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@route_id", route_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }
                    cn.Close();
                }
            }
            catch
            {

            }
        }
        #endregion

        #region Location
        public void UpdateLoc(decimal idx, string location_t, string location_e)
        {
            if (idx < 1.0m) return;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Location_Update";  //DS2_Location_Update_tls
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@location_t", location_t);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@location_e", location_e);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        #endregion

        #region  ETA
        public Boolean InsertMsgETAM(Decimal ref_idx, int eta_id, float time_left, int destination_id, int eta_route_no)
        {
            if (ref_idx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsgETA_InsertNewRecord_M";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_id", eta_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@time_left", time_left);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@destination_id", destination_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_route_no", eta_route_no);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }

        public Boolean InsertMsgETA_M(Decimal ref_idx, int eta_id, decimal time_left, int destination_id, int eta_route_no,decimal distance,decimal diffirst)
        {
            if (ref_idx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsgETA_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_id", eta_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@time_left", time_left);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@destination_id", destination_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_route_no", eta_route_no);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@distance", distance);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_diff", diffirst);
                    cm.Parameters.Add(param1);


                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public Boolean InsertMsgETA(Decimal ref_idx, int eta_id, double time_left, int destination_id)
        {
            if (ref_idx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsgETA_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@eta_id", eta_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@time_left", time_left);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@destination_id", destination_id);
                    cm.Parameters.Add(param1);

                  

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        #endregion

        #region EngineOnEvent
        public Boolean GetEngineOnByTime(int veh_id)
        {
            Boolean ret = false;

            try
            {
                {
                    DataTable dtRet = new DataTable();
                    using (SqlConnection cn = new SqlConnection(connStr))
                    {
                        cn.Open();

                        SqlCommand cm = new SqlCommand();
                        cm.CommandText = "DS2_Veh_GetLastEngineOnByTime";
                        cm.CommandType = System.Data.CommandType.StoredProcedure;
                        cm.Connection = cn;
                        cm.CommandTimeout = 60 * 5;

                        SqlParameter
                        param1 = new SqlParameter("@veh_id", veh_id);
                        cm.Parameters.Add(param1);

                        SqlDataAdapter adp = new SqlDataAdapter(cm);
                        adp.Fill(dtRet);
                        cn.Close();

                        if (dtRet.Rows.Count > 0)
                        {
                            ret = (bool)dtRet.Rows[0]["is_engine_on"];
                        }
                    }
                }
            }
            catch { }

            return ret;
        }
        public void GetLastStatusEngineOnEvent(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            evt_id = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastEngineEventStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 30;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        local_timestamp = DateTime.Now;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public void GetLastEngineOnStatus(int veh_id, out int evt_id, out DateTime local_timestamp)
        {
            evt_id = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastEngineOnStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        local_timestamp = DateTime.Now;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        #endregion

        #region Temperature
        public void GetLastTemperatureEventStatus(int veh_id, out int evt_id, out int last_temperature, out int cur_temperature, out DateTime local_timestamp)
        {
            evt_id = -1;
            last_temperature = cur_temperature = 0;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetLastTemperatureEventStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_temperature", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@cur_temperature", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);


                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        last_temperature = (int)cm.Parameters["@last_temperature"].Value;
                        cur_temperature = (int)cm.Parameters["@cur_temperature"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        last_temperature = 0;
                        cur_temperature = 0;
                        local_timestamp = DateTime.Now;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        #endregion

        #region EngineOnOffInOutZone
        public bool IsZonePortOutOfPolicy(int veh_id, int last_zone_id, int zone_evt, int last_io, int delay_time)
        {
            bool bRet = false;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Zone_IsZonePortOutOfPolicy";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", zone_evt);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_io_status", last_io);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@delay_time", delay_time);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ret", SqlDbType.Int);
                    param1.Direction = ParameterDirection.ReturnValue;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        int ret = (int)cm.Parameters["@ret"].Value;
                        if (ret > 0)
                        {
                            bRet = true;
                        }
                    }
                    catch
                    {
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        #endregion

        #region ZoneCurfewWithSpeed
        public void GetLastStatusZoneCurfewWithSpeed(int veh_id, int last_zone_id, out int evt_id, out int zone_id, out DateTime local_timestamp, out int mark, out int speed)
        {
            evt_id = -1;
            zone_id = -1;
            mark = -1;
            speed = -1;
            local_timestamp = DateTime.Now.AddDays(-1);

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Zone_GetLastStatusZoneCurfewWithSpeed";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", SqlDbType.DateTime);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@mark", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                        local_timestamp = (DateTime)cm.Parameters["@local_timestamp"].Value;
                        mark = (int)cm.Parameters["@mark"].Value;
                        speed = (int)cm.Parameters["@speed"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        zone_id = -1;
                        mark = -1;
                        local_timestamp = DateTime.Now.AddDays(-1);
                        speed = -1;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public Boolean UpSertStatusZoneCurfewWithSpeed(int veh_id, Decimal refIdx, int zoneId, int evt_id, int marked, int speed)
        {
            if (refIdx < 1.0m) return false;

            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Zone_UpdateZoneCurfewWithSpeed";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zoneId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@marked", marked);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@speed", speed);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        #endregion

        #region Magnatic Event
        public DataTable GetVehicleMagneticLastNoCard(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetMagnetic_lastNoCard";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        public DataTable GetMagnetic(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_Veh_GetMagneticType";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }
        #endregion

        #region  SmartCard Event
        public bool InsertSmartCardEvent(int veh_id, DateTime dtTimestampe, Decimal ref_idx, int iAddress1, string sAddress2, string sData, string sTag)
        {
            if (ref_idx < 1.0m) return false;

            bool ret = false;

            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_LogMsg_InsertNewSmartCardEvent";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@local_timestamp", dtTimestampe);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@iAddress1", iAddress1);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@sAddress2", sAddress2);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@sData", sData);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@sTag", sTag);
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        ret = true;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
            return ret;
        }

        #endregion

        #region True Zone
        public void True2GetZoneLastStatus_Log(int veh_id, int last_zone_id, out int evt_id, out int zone_id)
        {
            evt_id = -1;
            zone_id = -1;

            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_True_Zone_GetCurrentStatus";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@last_zone_id", last_zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    try
                    {
                        cm.ExecuteNonQuery();
                        evt_id = (int)cm.Parameters["@evt_id"].Value;
                        zone_id = (int)cm.Parameters["@zone_id"].Value;
                    }
                    catch
                    {
                        evt_id = -1;
                        zone_id = -1;
                    }
                    cn.Close();
                }
            }
            catch
            {
            }
        }

        public int True2InsertMsgZone_Log(Decimal refIdx, int zoneId, int evt_id)
        {
            int lasted_index = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_True_Zone_InsertNewRecord";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zoneId);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@lasted_index", SqlDbType.Int);
                    param1.Direction = ParameterDirection.Output;
                    cm.Parameters.Add(param1);

                    int iRet = cm.ExecuteNonQuery();
                    if (iRet > 0)
                    {
                        lasted_index = (int)cm.Parameters["@lasted_index"].Value;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return lasted_index;
        }

        public void True2InsertPrvZoneState_Log(int veh_id, int zone_id, int evt_id, Decimal ref_idx)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_True_Zone_UpSertCurrentState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@ref_idx", ref_idx);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0) { }

                    cn.Close();
                }
            }
            catch
            {
            }
        }
        public Boolean True2InsertMsgZoneTypeGroup_Log(int id_insert, int group_id, int type_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_True_Zone_InsertMsg_TypeGroup";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@id_insert", id_insert);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@group_id", group_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_id", type_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        #endregion

        #region True IO
        public Boolean True2InsertMsgIO(Decimal refIdx, int evt_id)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_IOstatus_InsertLog";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@ref_idx", refIdx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param1);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        public Boolean True2InsertTruePrvIOState(int veh_id, int port, Decimal refIdx)
        {
            Boolean bRet = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue2_Alert_IOstatus_InsertPrvState";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port", port);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@port_ref_idx", refIdx);
                    cm.Parameters.Add(param1);


                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bRet = true;
                    }

                    cn.Close();
                }
            }
            catch
            {
            }

            return bRet;
        }
        #endregion

        #region Distance Over
        public double Getdistance(int veh_id, DateTime timestart, DateTime timestop)
        {
            double get_distance = 0.00;

            DataTable distanceTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_GetDistance";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestart", timestart);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@timestop", timestop);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(distanceTable);

                    cn.Close();
                    try
                    {
                        foreach (DataRow dr in distanceTable.Rows)
                        {
                            if (distanceTable.Rows.Count > 0)
                            {
                                get_distance = Convert.ToDouble((decimal)dr["distance"]);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }

            return get_distance;
        }

        public DataTable GetLastEvent(int veh_id)
        {
            DataTable dtLastEvent = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "DS2_GetLastEvent";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtLastEvent);

                    cn.Close();
                }
            }
            catch
            {
            }

            return dtLastEvent;
        }
        #endregion
    }
}

/**
        MAX_SPEED,                  lbMaxSpeedTime
        MAX_IDLE,                   lbMaxIdleTime
        MAX_ZONE_TIMEID,            lbMaxZoneTime
        MAX_ZONE_CURFEW,            lbMaxZoneCurfewTime
        MAX_ZONE_CURFEWSPEED,       lbMaxZoneCurfewWithSpeed
        MAX_LOCATION,               lbMaxLocTime
        MAX_ROUTING,                lbMaxRoutingTime
        MAX_ETA1,                   
        MAX_ETA2, 
        MAX_IOSTATUS,           
        MAX_ENGINEONEVEN,           lbMaxEngineOnEventStr
        MAX_TEMPERATURE,            lbMaxTemperatureStr
        MAX_ENGINEEVENTZONE,        lbEngineOnOffInOutZoneStr
        MAX_SPEEDHAZRD,             
        MAX_ANGLEHAZARD, 
        MAX_TRUEFENCING,            lbMaxTrueFencingStr
        MAX_TRUEIO,                 lbMaxTrueIOStr
        ,

        MAX_LOG_MSG,                lbMaxCommitLog
        MAX_PROCESS_MSG,            lbMaxCommitEvent
        TOTAL_COMMITLOG_SEC,        lbTotalOfDataCommit
        TOTAL_CURTHDID              lbTotalCurThd
        TOTAL_DATA                  lbTotalOfData
 * 
 * 
 * 
 * 
 *      private string lbMaxGenaral = "";
 *      private string lbMaxLogError = "";
 *      
 *      private string lbMaxSpeedTimeStr = "";
        private string lbMaxIdleTimeStr = "";
        private string lbMaxZoneTimeStr = "";
        private string lbMaxZoneCurfewTimeStr = "";
        private string lbMaxZoneCurfewWithSpeedStr = "";
        private string lbMaxLocTimeStr = "";
        private string lbMaxRoutingTimeStr = "";

        private string lbMaxEngineOnEventStr = "";
        private string lbMaxTemperatureStr = "";
        private string lbEngineOnOffInOutZoneStr = "";

        private string lbMaxTrueFencingStr = "";
        private string lbMaxTrueIOStr = "";

        private string lbMaxMagnetic = "";
        private string lbMaxSmartCard = "";
        private string lbMaxRpm = "";


        private string lbMaxCommitLog = "";
        private string lbMaxCommitEvent = "";

        private string lbTotalOfDataCommit = "";
        private string lbTotalCurThd = "";
        private string lbTotalOfData = "";
 * 


ProcessGenaralMsg
UpdateLogError
DetectMagneticEvent
DetectSmartCardEvent
DetectRPMEvent

 public struct ExEventCompleteStruc
    {
        public double speeding_elapsed_time_maximum;
        public double idle_elapsed_time_maximum;
        public double fencing_elapsed_time_maximum;
        public double zone_in_curfew_elapsed_time_maximum;
        public double zone_in_curfew_with_speed_elapsed_time_maximum;  
 * 
 *      public double routing_elapsed_time_maximum;
        public double location_elapsed_time_maximum;
        public double eta1_elapsed_time_maximum;
        public double eta2_elapsed_time_maximum;
        public double io_status_elapsed_time_maximum;
        public double engine_on_elapsed_time_maximum;
        public double speed_hazard_elapsed_time_maximum;
        public double angle_hazard_elapsed_time_maximum;
        public double temperature_elapsed_time_maximum;
        public double engine_onoff_inout_zone_elapsed_time_maximun;
        public double true_fencing_elapsed_time_maximum;
        public double true_io_elapsed_time_maximum_;
        public double DFM_io_status_elapsed_time_maximum;
        public double zone_in_curfew_with_speed_elapsed_time_maximum;
        public double magnetic_elapsed_time_maximum;

        public double genaral_elapsed_time_maximum;
        public double logerror_elapsed_time_maximum;
        public double rpm_elapsed_time_maximum;
        public double smart_card_elapsed_time_maximum;

        public double Commit_event_elapsed_time_maximum;
    }
**/