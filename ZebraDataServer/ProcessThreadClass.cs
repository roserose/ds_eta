using System;
using System.Collections.Generic;
using System.Text;
using ZebraPresentationLayer;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using ZebraCommonInterface;
using System.Collections;
using System.Diagnostics;
using System.IO;

namespace ZebraDataServer
{
    public delegate void ReportStatusHandler(object sender, CONTROLID ControlID, string data);
    //public delegate void ReportStatusHandler(object sender, CONTROLID ControlID, string data, string msg);
    public delegate void CompletedHandler(object sender);

    public class ProcessThreadClass
    {
        public event ReportStatusHandler OnReportStatus;
        public event CompletedHandler OnCompleted;
        public event ElapedTimeProcess OnProcessTimeCompleted;
        private String szDatabaseServer = "";
        private ExEvent oExEvent = new ExEvent();

        private System.Threading.Semaphore runLock = new Semaphore(1, 1);
        string vid_text = "";
        public struct PrcObj
        {
            public Max2FmtMsg msg;
            public int lasted_index;
            public decimal lasted_log_index;
        }

        public ProcessThreadClass(Max2FmtMsg msg, String szDBServer,decimal zLastLogIdx)
        {
            szDatabaseServer = szDBServer;
            vid_text = msg.vid.ToString();
            oExEvent.OnProcessCompleted += new ProcessCompleted(oExEvent_OnProcessCompleted);
            oExEvent.OnProcessTime += new ElapedTimeProcess(oExEvent_OnProcessTime);

            try
            {
                PrcObj prcObj = new PrcObj();
                prcObj.msg = msg;
                prcObj.lasted_index = -1;
                prcObj.lasted_log_index = zLastLogIdx;
                
                Thread thread = new Thread(new ParameterizedThreadStart(ReadQCallBack));
                thread.Start(prcObj);
                Thread.Sleep(10);
            }
            catch { }
        }

        void oExEvent_OnProcessTime(string zMsg)
        {
            if (OnProcessTimeCompleted != null)
            {
                OnProcessTimeCompleted(zMsg);
            }
        }


/////////////////////////// ���������������ʴ� location ��͹��ѧ�� 1000 �ѹ
 

        private void ReadQCallBack(object obj)
        {
            try
            {
                PrcObj prcObj = (PrcObj)obj;
                int lasted_index = prcObj.lasted_index;
                decimal last_log_index = prcObj.lasted_log_index;
                Max2FmtMsg msg = prcObj.msg;
                //rose delete last_log_index < 1
         //      if (msg.gps_date_time == null || msg.utc == null || last_log_index < 1)//old
             if (msg.gps_date_time == null || msg.utc == null)
                {
                    return;
                }

                if (msg.GetType() == typeof(Max2FmtMsg))
                {
                    String timestamp = GetTimeStamp(msg.gps_date_time, msg.utc);

                    DateTime testDate = DateTime.Parse(timestamp);
                    DateTime validDate1 = DateTime.Now.Add(new TimeSpan(-1000, 0, 0, 0));
                    DateTime validDate2 = DateTime.Now.Add(new TimeSpan(2, 0, 0, 0));

                    String szValidDate1 = validDate1.ToString("yyyy-MM-dd", new CultureInfo("en-US"));
                    String szValidDate2 = validDate2.ToString("yyyy-MM-dd", new CultureInfo("en-US"));

                    validDate1 = DateTime.Parse(szValidDate1);
                    validDate2 = DateTime.Parse(szValidDate2);

                    if (!(validDate1 <= testDate && testDate <= validDate2))
                    {
                        timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-US"));
                        testDate = DateTime.Parse(timestamp);
                        msg.lon = "0.0";
                        msg.lat = "0.0";
                    }


                    if (Validate(msg))
                    {
                        int internal_power = 0;
                        int external_power = 0;

                        if (int.TryParse(msg.internal_power, out internal_power)) { }
                        if (int.TryParse(msg.external_power, out external_power)) { }

                        Thread.Sleep(10);

                        oExEvent.DetectEvent(
                                last_log_index,
                                int.Parse(msg.vid),
                                timestamp,
                                float.Parse(msg.lat.Substring(0, msg.lat.Length - 1)),
                                float.Parse(msg.lon.Substring(0, msg.lon.Length - 1)),
                                (int)float.Parse(msg.speed),
                                (int)float.Parse(msg.course),
                                int.Parse(msg.type_of_fix),
                                int.Parse(msg.no_of_satellite),
                                Convert.ToInt32(msg.gpio_status, 16),
                                int.Parse(msg.analog_level),
                                msg.type_of_message.Substring(msg.type_of_message.Length - 1, 1),
                                msg.tag,
                                testDate,
                                internal_power,
                                external_power
                            );
                    }
                    else
                    {
                    }
                }
                if (OnCompleted != null) { OnCompleted(this); }
            }
            catch
            {
            }
        }
        private string GetHexToDex(string zHexStr)
        {
            string retDec = zHexStr;
            try
            {
                int decValue = 0;
                if (!(int.TryParse(zHexStr, out decValue)))
                {
                    decValue = Convert.ToInt32(zHexStr, 16);
                    retDec = decValue.ToString("0");
                }
            }
            catch
            {

            }
            return retDec;
        }
        private string GetHexToDex2(string zHexStr)
        {
            string retDec = "0";
            try
            {
                int decValue = Convert.ToInt32(zHexStr, 16);
                retDec = decValue.ToString("0");
            }
            catch
            {

            }
            return retDec;
        }
        private void oExEvent_OnProcessCompleted(object sender, ExEventCompleteStruc msg)
        {
        //private void oExEvent_OnProcessCompleted(object sender, ExEventCompleteStruc msg, string data)
        //{   (, vid_text)
            WriteCtrlMsg(CONTROLID.MAX_Genaral, msg.genaral_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_LogError, msg.logerror_elapsed_time_maximum.ToString());

            WriteCtrlMsg(CONTROLID.MAX_SPEED, msg.speeding_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_IDLE, msg.idle_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_ZONE_TIMEID, msg.fencing_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_ZONE_CURFEW, msg.zone_in_curfew_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_ZONE_CURFEWSPEED, msg.zone_in_curfew_elapsed_time_maximum.ToString());

            WriteCtrlMsg(CONTROLID.MAX_LOCATION, msg.location_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_ROUTING, msg.routing_elapsed_time_maximum.ToString());

            WriteCtrlMsg(CONTROLID.MAX_ENGINEONEVEN, msg.engine_on_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_TEMPERATURE, msg.temperature_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_ENGINEEVENTZONE, msg.engine_onoff_inout_zone_elapsed_time_maximun.ToString());

            WriteCtrlMsg(CONTROLID.MAX_TRUEFENCING, msg.true_fencing_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_TRUEIO, msg.true_io_elapsed_time_maximum_.ToString());


            WriteCtrlMsg(CONTROLID.MAX_Magnetic, msg.magnetic_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_SmartCard, msg.smart_card_elapsed_time_maximum.ToString());
            WriteCtrlMsg(CONTROLID.MAX_RPM, msg.rpm_elapsed_time_maximum.ToString());

            WriteCtrlMsg(CONTROLID.MAX_PROCESS_MSG, msg.Commit_event_elapsed_time_maximum.ToString());
        }

        private bool Validate(Max2FmtMsg msg)
        {
            bool bIsOk = true;
           // SQL sql = new SQL(szDatabaseServer);

            //int maxSpeed = 200; // Km/Hrs 
            //int totalSec = 2 * 60;
            string eventStr = "K,M,V,A,S,G,L,O,F,X,x"; // "2,3,4,5,9,10,12,15,16,17,18"; msg type id.

            //double lat = 0.0;
            //double lon = 0.0;
            DateTime timestamp = new DateTime();

            try
            {
                String szTimeStamp = GetTimeStamp(msg.gps_date_time, msg.utc);
                DateTime curTimestamp = Convert.ToDateTime(szTimeStamp);

                String d1 = curTimestamp.ToString("MM/dd/yyyy HH:mm:ss");
                String d2 = timestamp.ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));
                String d3 = DateTime.Now.AddHours(1).ToString("MM/dd/yyyy HH:mm:ss", new CultureInfo("en-US"));

                if (Convert.ToDateTime(d3) < Convert.ToDateTime(d1))
                {
                    //edit for event magnetic after data error 
                    return false;
                }

                //int idx = eventStr.IndexOf(msg.type_of_message.ToUpper());
                int idx = eventStr.IndexOf(msg.type_of_message);
                if (idx < 0)
                {
                    return bIsOk;
                }

                /*
                if (Convert.ToInt32(msg.no_of_satellite) < 4)
                {
                    return false;
                }

                if (Convert.ToInt32(msg.speed) > 190)
                {
                    return false;
                }
                */
            }
            catch
            {
            }
            return bIsOk;
        }
        private String GetTimeStamp(String date, String time)
        {
            try
            {
                System.Globalization.CultureInfo curCul = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

                int dd = Convert.ToInt32(date.Substring(0, 2));
                int MM = Convert.ToInt32(date.Substring(2, 2));
                int yy = Convert.ToInt32(date.Substring(4, 2));

                int HH = Convert.ToInt32(time.Substring(0, 2));
                int mm = Convert.ToInt32(time.Substring(2, 2));
                int ss = Convert.ToInt32(time.Substring(4, 2));

                DateTime d = new DateTime(2000 + yy, MM, dd, HH, mm, ss);
                d = d.AddHours(7.0);
                String szTimeStamp = d.ToString("yyyy-MM-dd HH:mm:ss");

                System.Threading.Thread.CurrentThread.CurrentCulture = curCul;
                return szTimeStamp;
            }
            catch (Exception ex)
            {
                GlobalClass.WriteLogEvent("Zebra Data Server(Thread) #3" + ex.Message);
            }

            return "";
        }
        private void WriteCtrlMsg(CONTROLID CtrlID, String msg)
        {
        //private void WriteCtrlMsg(CONTROLID CtrlID, String msg, string vid)
        //{
            if (OnReportStatus != null)
            {
                OnReportStatus(this, CtrlID, msg);
                //OnReportStatus(this, CtrlID, msg, vid);
            }
        }

        private double GetODOFromTheGreatCircle(double lat1, double lon1, double lat2, double lon2)
        {
            double R = 6371.0;
            double rad = 57.2958;

            lat1 = lat1 / rad;
            lat2 = lat2 / rad;
            lon1 = lon1 / rad;
            lon2 = lon2 / rad;

            double a = Math.Sin(lat1) * Math.Sin(lat2);
            double b = Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(lon2 - lon1);
            double c = Math.Acos(a + b);
            double d = R * c;

            return d;
        }
    }
}
