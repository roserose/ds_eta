﻿namespace ZebraBroadcast
{
    partial class MySQL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btConnect = new System.Windows.Forms.Button();
            this.btInsert = new System.Windows.Forms.Button();
            this.tbConnect = new System.Windows.Forms.TextBox();
            this.tbInsert = new System.Windows.Forms.TextBox();
            this.tbARM = new System.Windows.Forms.TextBox();
            this.btInsertARM = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btConnect
            // 
            this.btConnect.Location = new System.Drawing.Point(12, 12);
            this.btConnect.Name = "btConnect";
            this.btConnect.Size = new System.Drawing.Size(92, 25);
            this.btConnect.TabIndex = 0;
            this.btConnect.Text = "Connect";
            this.btConnect.UseVisualStyleBackColor = true;
            this.btConnect.Click += new System.EventHandler(this.btConnect_Click);
            // 
            // btInsert
            // 
            this.btInsert.Location = new System.Drawing.Point(12, 43);
            this.btInsert.Name = "btInsert";
            this.btInsert.Size = new System.Drawing.Size(92, 25);
            this.btInsert.TabIndex = 1;
            this.btInsert.Text = "Insert";
            this.btInsert.UseVisualStyleBackColor = true;
            this.btInsert.Click += new System.EventHandler(this.btInsert_Click);
            // 
            // tbConnect
            // 
            this.tbConnect.Location = new System.Drawing.Point(122, 13);
            this.tbConnect.Name = "tbConnect";
            this.tbConnect.Size = new System.Drawing.Size(219, 20);
            this.tbConnect.TabIndex = 2;
            // 
            // tbInsert
            // 
            this.tbInsert.Location = new System.Drawing.Point(122, 46);
            this.tbInsert.Name = "tbInsert";
            this.tbInsert.Size = new System.Drawing.Size(219, 20);
            this.tbInsert.TabIndex = 3;
            // 
            // tbARM
            // 
            this.tbARM.Location = new System.Drawing.Point(122, 98);
            this.tbARM.Name = "tbARM";
            this.tbARM.Size = new System.Drawing.Size(219, 20);
            this.tbARM.TabIndex = 5;
            // 
            // btInsertARM
            // 
            this.btInsertARM.Location = new System.Drawing.Point(12, 95);
            this.btInsertARM.Name = "btInsertARM";
            this.btInsertARM.Size = new System.Drawing.Size(92, 25);
            this.btInsertARM.TabIndex = 4;
            this.btInsertARM.Text = "Insert";
            this.btInsertARM.UseVisualStyleBackColor = true;
            this.btInsertARM.Click += new System.EventHandler(this.btInsertARM_Click);
            // 
            // MySQL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 132);
            this.Controls.Add(this.tbARM);
            this.Controls.Add(this.btInsertARM);
            this.Controls.Add(this.tbInsert);
            this.Controls.Add(this.tbConnect);
            this.Controls.Add(this.btInsert);
            this.Controls.Add(this.btConnect);
            this.Name = "MySQL";
            this.Text = "MySQL";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btConnect;
        private System.Windows.Forms.Button btInsert;
        private System.Windows.Forms.TextBox tbConnect;
        private System.Windows.Forms.TextBox tbInsert;
        private System.Windows.Forms.TextBox tbARM;
        private System.Windows.Forms.Button btInsertARM;
    }
}