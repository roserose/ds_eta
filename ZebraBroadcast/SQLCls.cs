using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using ZebraCommonInterface;

namespace ZebraBroadcast
{
    class SQLCls
    {
        private String connStr = "";
        public SQLCls(String dbServer)
        {
            connStr = String.Format("Data Source={0}; Database=ZebraDB;  UID=sa; PWD=aab-gps-gtt; Pooling=true; Min Pool Size=20; Max Pool Size=500", dbServer);
        }
        //Tabel ���� 
        public DataTable GetFeatureProfile_New_Email(int idx, int veh_id, int evt_type)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetProfile_New_Email";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        //Tabel ���� 
        public DataTable GetFeatureProfile_New_SMS(int idx, int veh_id, int evt_type)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetProfile_New_SMS";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        //Tabel ���� 
        public DataTable GetFeatureProfile_New(int idx, int veh_id, int evt_type, string sday_no)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetProfile_New_day";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@sdayno", sday_no);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch(Exception e)
            {
            }

            return pfTable;
        }
        //����¹ Tabel ������
        public DataTable GetFeatureProfile_New_Email(int idx, int veh_id, int evt_type, int msg)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE)
                    {
                        cm.CommandText = "[spFeature_GetProfileZoneVehicle_New_Email]";
                    }
                    else if (evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                    {
                        cm.CommandText = "[spFeature_GetProfileRouteVehicle_New_Email"; 
                    }
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE)
                    {
                        param = new SqlParameter("@zone_id", msg);
                    }
                    else if (evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                    {
                        param = new SqlParameter("@route_id", msg);
                    }
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        //Tabel ���� 
        public DataTable GetFeatureProfile_New_SMS(int idx, int veh_id, int evt_type, int msg)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE)
                    {
                        cm.CommandText = "[spFeature_GetProfileZoneVehicle_New_SMS]";
                    }
                    else if (evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                    {
                        cm.CommandText = "[spFeature_GetProfileRouteVehicle_New_SMS";
                    }
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE)
                    {
                        param = new SqlParameter("@zone_id", msg);
                    }
                    else if (evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                    {
                        param = new SqlParameter("@route_id", msg);
                    }
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        //Tabel ���� 
        public DataTable GetFeatureProfileByDay_New_Email(int veh_id, int evt_type, int day_id)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetProfileByDay_New_Email";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@day_id", (int)day_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        //Tabel ���� 
        public DataTable GetFeatureProfileByDay_New_SMS(int veh_id, int evt_type, int day_id)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetProfileByDay_New_SMS";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@day_id", (int)day_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        //Tabel ���� 
        public DataTable GetBroadcastTypes_New(int veh_id, int evt_type)
        {
            DataTable pfTable = new DataTable();
            int a = (int)evt_type;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetBroadcastTypes_New";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id",(int)evt_type);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch(Exception e)
            {
            }

            return pfTable;
        }

        public DataTable GetFeatureProfile(int idx, int veh_id, int evt_type)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetProfile";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        public DataTable GetFeatureProfile(int idx, int veh_id, int evt_type, int msg)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE)
                    {
                        cm.CommandText = "[spFeature_GetProfileZoneVehicle]";
                    }
                    else if (evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                    {
                        cm.CommandText = "[spFeature_GetProfileRouteVehicle]";
                    }
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE)
                    {
                        param = new SqlParameter("@zone_id", msg);
                    }
                    else if (evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                    {
                        param = new SqlParameter("@route_id", msg);
                    }
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        public DataTable GetFeatureProfileByDay(int veh_id, int evt_type, int day_id)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetProfileByDay";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@day_id", (int)day_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }
        public DataTable GetBroadcastTypes(int veh_id, int evt_type)
        {
            DataTable pfTable = new DataTable();
            int a = (int)evt_type;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetBroadcastTypes";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }


        public bool InsertNewBroadcastLog(BROADCASTLOGTYPE type, PROFILEINFO info)
        {
            bool bIsSuccess = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spInsertNewBroadcastLog]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@broadcast_id", (int)type);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@fleet_id", info.fleet_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@veh_id", info.veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@user_id", info.staff_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@timestamp", info.timestamp);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", info.evt_id);
                    cm.Parameters.Add(param);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bIsSuccess = true;
                    }
                    cn.Close();
                }
            }
            catch(Exception e)
            {
            }

            return bIsSuccess;
        }
        public bool InsertBulkSmsNewBroadcastLog(PROFILEINFO info, Int64 zSMID ,string tel)
        {
            bool bIsSuccess = false;
            try
            {
                    using (SqlConnection cn = new SqlConnection(connStr))
                    {
                        cn.Open();

                        SqlCommand cm = new SqlCommand();
                        cm.CommandText = "[spInsertBulkSmsNewBroadcastLog]";
                        cm.CommandType = System.Data.CommandType.StoredProcedure;
                        cm.Connection = cn;
                        cm.CommandTimeout = 60 * 5;

                        SqlParameter
                        param = new SqlParameter("@smid", zSMID.ToString());
                        cm.Parameters.Add(param);

                        param = new SqlParameter("@tel", tel);
                        cm.Parameters.Add(param);

                        param = new SqlParameter("@timestamp", info.timestamp);
                        cm.Parameters.Add(param);

                        param = new SqlParameter("@fleet_id", info.fleet_id);
                        cm.Parameters.Add(param);

                        param = new SqlParameter("@veh_id", info.veh_id);
                        cm.Parameters.Add(param);

                        param = new SqlParameter("@user_id", info.staff_id);
                        cm.Parameters.Add(param);

                        param = new SqlParameter("@evt_id", info.evt_id);
                        cm.Parameters.Add(param);

                        if (cm.ExecuteNonQuery() > 0)
                        {
                            bIsSuccess = true;
                        }
                        cn.Close();
                    }
            }
            catch (Exception e)
            {
                bIsSuccess = false;
            }
            return bIsSuccess;
        }

        public String GetZoneLabel(int ZoneID)
        {
            String zoneDesc = "?";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "zpZone_GetZoneLabel";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@zone_id", ZoneID);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt != null || dt.Rows.Count > 0)
                    {
                        zoneDesc = (string)dt.Rows[0]["zone_desc"];
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return zoneDesc;
        }

        public String GetMessage(int fleet_id, int veh_id, int evt_type)
        {
            String Message = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetMessageBulkSms";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@fleet_id", fleet_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        Message = (string)dt.Rows[0]["message"];
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return Message;
        }
        //Tabel ���� 
        public String GetMessageNew(int fleet_id, int veh_id, int evt_type, int contact_id, int no_day)
        {
            String Message = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetMessageBulkSms_New";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@fleet_id", fleet_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", evt_type);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@contact_id", contact_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@no_day", no_day);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        Message = (string)dt.Rows[0]["message"];
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return Message;
        }
        //Tabel ���� 
        public String GetMessageNew(int fleet_id, int veh_id, int evt_type, int contact_id, int noday, string start_time, string stop_time)
        {
            String Message = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetMessageBulkSms_New_day";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@fleet_id", fleet_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", evt_type);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@contact_id", contact_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@noday", noday);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@start_time", start_time);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@stop_time", stop_time);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        Message = (string)dt.Rows[0]["message"];
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return Message;
        }

        public String GetRouteLabel(int RouteID)
        {
            String routeDesc = "?";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "zpRoute_GetRouteLabel";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@route_id", RouteID);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt != null || dt.Rows.Count > 0)
                    {
                        routeDesc = (string)dt.Rows[0]["route_desc"];
                    }

                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return routeDesc;
        }

        public String GetSOSLabel(int veh_id, bool status, int port, EVT_TYPE evt_type)
        {
            String SOSDesc = "?";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetSOSLabel";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@status", status);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt != null || dt.Rows.Count > 0)
                    {
                        if (evt_type == EVT_TYPE.EVENT_USER_DEFINED1_ON || evt_type == EVT_TYPE.EVENT_USER_DEFINED1_OFF || evt_type == EVT_TYPE.EVENT_USER_DEFINED2_ON || evt_type == EVT_TYPE.EVENT_USER_DEFINED2_OFF)
                        {
                            if (dt.Rows.Count == 1)
                            {
                                SOSDesc = "SOS";
                            }
                            else if (dt.Rows.Count == 2)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    int port_ = (int)dr["port"];

                                    if (port == port_)
                                    {
                                        if (port == 2)
                                        {
                                            SOSDesc = "SOS1";
                                        }
                                        else if (port == 3)
                                        {
                                            SOSDesc = "SOS2";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (evt_type == EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON ||  evt_type == EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON || evt_type == EVT_TYPE.EVENT_USER_DEFINED3_OUT_ON)
                            {
                                SOSDesc = "�ѹ��觶١���";
                            }
                            else if (evt_type == EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF || evt_type == EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF || evt_type == EVT_TYPE.EVENT_USER_DEFINED3_OUT_OFF)
                            {
                                SOSDesc = "�ѹ��觶١�Ѵ";
                            }
                        }
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return SOSDesc;
        }

        public DataTable GetTrueZoneProfile(int zone_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue_GetZoneProfile";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill( dtRet );
                    cn.Close();
                }
            }
            catch (Exception e) {
            }
            return dtRet;
        }
        public DataTable GetTrueVehProfile(int veh_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue_GetVehProfile";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }

        public DataTable GetTrueUserProfile(int zone_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue_GetUserZoneProfile";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }
        public DataTable GetTrueUserProfile()
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue_GetUserProfile]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }
        public DataTable GetTestVehProfile(int veh_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTest_GetVehProfile";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }
        public DataTable GetTestUserProfile(int zone_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTest_GetUserZoneProfile";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }
        public DataTable GetTestZoneProfile(int zone_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTest_GetZoneProfile";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }
        public DataTable GetTrueVehProfile2(int veh_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue_GetVehProfile2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }
        public DataTable GetTrueUserProfile2(int zone_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue_GetUserZoneProfile2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }
        public DataTable GetTrueZoneProfile2(int zone_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spTrue_GetZoneProfile2";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@zone_id", zone_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }

        public String GetEvtDesc(int veh_id, EVT_TYPE evt_type)
        {
            String Message = "?";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetMessageBulkSms";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@evt_id", (int)evt_type);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt != null || dt.Rows.Count > 0)
                    {
                        Message = (string)dt.Rows[0]["message"];
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return Message;
        }
        public String GetEvtAbbreviationDesc(int evt_type)
        {
            String Message = "?";
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "spFeature_GetSMSEventAbbreviation";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@evt_id", evt_type);
                    cm.Parameters.Add(param);

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dt);

                    if (dt != null || dt.Rows.Count > 0)
                    {
                        Message = (string)dt.Rows[0]["message"];
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return Message;
        }
        /// <summary>
        /// True Corp. Event
        /// Create : Jirayu Kamsiri
        /// 2012/09/12
        /// </summary>
        /// <param name="veh_id"></param>
        /// <returns></returns>
        
        //1.Get True veh : E-Mail - SMS
        public DataTable True2GetTrueCorpVehicle(int veh_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_GetVehicle]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        //2.Get ProFile
        public DataTable True2GetTrueVehProfile(int veh_id)
        {
            DataTable dtRet = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_GetVehProfile]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(dtRet);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }
            return dtRet;
        }

        //2.Get Profile For Send : E-Mail - SMS
        public DataTable True2GetSendProfile(int group_id,int event_id, int idx,int type_broadcast,int value)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_GetProfileBroadcast]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@group_id", group_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@event_type", event_id);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@idx", idx);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@type_broadcast", type_broadcast);
                    cm.Parameters.Add(param1);

                    param1 = new SqlParameter("@value", value);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        //3.Get User Profile : E-Mail-SMS
        public DataTable True2GetUserProfile(int user_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[spTrue2_Alert_GetUserInfo]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@user_id", user_id);
                    cm.Parameters.Add(param1);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        //Tabel ���� NEW WEB
        public DataTable Check_param_newweb(int veh_id, int evt_id)
        {
            DataTable fzTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "Check_param_newweb";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter param1 = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param1);

                    SqlParameter param2 = new SqlParameter("@evt_id", evt_id);
                    cm.Parameters.Add(param2);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(fzTable);

                    cn.Close();
                }
            }
            catch
            {
            }

            return fzTable;
        }

        public DataTable GetTelno_DistanceOver(int veh_id)
        {
            DataTable pfTable = new DataTable();
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "Telno_DistanceOver";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@veh_id", veh_id);
                    cm.Parameters.Add(param);

                    SqlDataAdapter adp = new SqlDataAdapter(cm);
                    adp.Fill(pfTable);
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return pfTable;
        }

        public bool Insert_text(DateTime zTimestamp, string text)
        {
            bool bIsSuccess = false;
            try
            {
                using (SqlConnection cn = new SqlConnection(connStr))
                {
                    cn.Open();

                    SqlCommand cm = new SqlCommand();
                    cm.CommandText = "[Watchdog_Alert_Mobile]";
                    cm.CommandType = System.Data.CommandType.StoredProcedure;
                    cm.Connection = cn;
                    cm.CommandTimeout = 60 * 5;

                    SqlParameter
                    param = new SqlParameter("@timestamp", zTimestamp);
                    cm.Parameters.Add(param);

                    param = new SqlParameter("@text", text);
                    cm.Parameters.Add(param);

                    if (cm.ExecuteNonQuery() > 0)
                    {
                        bIsSuccess = true;
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
            }

            return bIsSuccess;
        }

    }
}
