﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Messaging;
using System.Xml.Serialization;
using System.Runtime.Remoting.Channels;
using System.Collections;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Runtime.Remoting.Messaging;
//using MySql.Data.MySqlClient;


using ZebraDataCentre;
using ZebraCommonInterface;

namespace ZebraBroadcast
{
    public delegate void WriteFileProc(String msg);
    public delegate void PrintMagCallBack(string msg);
    public partial class MainFrm : Form
    {
        public struct TRUEUSERINFO
        {
            public string name;
            public string mobile;
            public string email;
        }
        public struct TESTUSERINFO
        {
            public string name;
            public string mobile;
            public string email;
        }

        public struct TRUEUSERINFO2
        {
            public string name;
            public string mobile;
            public string email;
        }

        private int _portNo = 6602;
        public int gPortNo
        {
            get { return _portNo; }
            set { _portNo = value; }
        }

        bool bCanClose = false;
        String szBroadcastStatus = "";
        bool gTrueFleetSendZoneSMS = false;
        bool gTrueFleetSendIOSMS = false;

        CommonQeue SMSQ;
        CommonQeue EMAILQ;

        String gDbHost = "";
        private EmailCls emailObj;
        private SMSCls smsObj;
        private SQLCls sql;

        public MainFrm()
        {
            InitializeComponent();
            InitCfg();
        }

        private void InitCfg()
        {
            try
            {
                Properties.Settings pf = new ZebraBroadcast.Properties.Settings();

                //---- Check Service ----------------------------------------------
                DataToolService objService = new DataToolService();
                bool sv_pass = false;
                string sv_message = "";
                ServiceStatus sv_status = ServiceStatus.None;
                objService.GetServiceState("MSMQ", out sv_pass, out sv_message, out sv_status);
                if (!sv_pass)
                {
                    if (sv_status == ServiceStatus.Not_Install)
                    {
                        MessageBox.Show("MSMQ Services fail!!!\r\nMay due to MSMQ services no Install.\r\nPlease Intall MSMQ services.\r\nClick OK to Exit.", "MSMQ Services no found.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    Environment.Exit(0);
                }

                gDbHost = pf.HostDB;
                gPortNo = pf.BroadcastRemotePort;
                gTrueFleetSendZoneSMS = pf.TrueFleetSendZoneSMS;
                gTrueFleetSendIOSMS = pf.TrueFleetSendIOSMS;

                emailObj = new EmailCls(gDbHost);
                emailObj.OnSendEmailCompleted += new SendEmailCompletedHandler(emailObj_OnSendEmailCompleted);

                bool sms_status = false;
                smsObj = new SMSCls(gDbHost, out sms_status);
                smsObj.OnSendSMSCompleted += new SendSMSCompletedHandler(smsObj_OnSendSMSCompleted);
                smsObj.OnSendBulkSMSCompleted += new SendBulkSMSCompletedHandler(smsObj_OnSendBulkSMSCompleted);
                smsObj.OnSMSQueueMonitor += new SMSQueueMonitorHandler(smsObj_OnSMSQueueMonitor);

                InitRemoteObj();

                sql = new SQLCls(gDbHost);

                sms_status = true; // test

                if (sms_status) { szBroadcastStatus = "Ready to broadcast ..."; }
                else { szBroadcastStatus = "Not ready!!!"; }
            }
            catch { }
        }
        private void smsObj_OnSMSQueueMonitor(string msg)
        {
            Properties.Settings pf = new ZebraBroadcast.Properties.Settings();
            tslbStatus.Text = "Total SMS left " + msg;
            lbl_SystemQ.Text = "SystemsQueue :" + pf.Broadcast_EMAILQ;

               


               
              
        }
        private void InitRemoteObj()
        {
            try
            {
                BinaryClientFormatterSinkProvider clientProvider = null;
                BinaryServerFormatterSinkProvider serverProvider = new BinaryServerFormatterSinkProvider();
                serverProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

                IDictionary props = new Hashtable();
                props["port"] = gPortNo;
                props["typeFilterLevel"] = TypeFilterLevel.Full;
                TcpChannel chan = new TcpChannel(
                    props, clientProvider, serverProvider);

                ChannelServices.RegisterChannel(chan);

                RemotingConfiguration.RegisterWellKnownServiceType(
                    Type.GetType("ZebraBroadcast.SMSCls,ZebraBroadcast"),  // namespace.class, assembly
                    "SMSObj",
                    WellKnownObjectMode.Singleton);
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Broadcast Error #1" + ex.Message);
            }
        }

        //private void TestRemotingObj()
        //{
        //    IPHostEntry ep = Dns.GetHostByName(Dns.GetHostName());
        //    if (ep.AddressList.Length < 1) return;
        //    String connStr = String.Format("tcp://{0}:{1}/SMSObj", ep.AddressList[0], gPortNo);

        //    BroadcastInterface obj = (BroadcastInterface)Activator.GetObject(
        //        typeof(BroadcastInterface),
        //        connStr);

        //    if (obj.IsAlive())
        //    {
        //        return;
        //    }
        //}

        private void emailObj_OnSendEmailCompleted(object sender, PROFILEINFO info)
        {
            string msg = "Email: " + info.timestamp + " : " + info.veh_reg + " : " + info.contact_name + " : " + info.tel_no + " Evt : " + info.evt_desc;
            PrintMsg(msg);
        }

        private void smsObj_OnSendSMSCompleted(object sender, PROFILEINFO info)
        {
            string msg = "SMS: " + info.timestamp + " : " + info.veh_reg + " : " + info.contact_name + " : " + info.tel_no + " Evt : " + info.evt_desc;
            PrintMsg(msg);
        }

        private void smsObj_OnSendBulkSMSCompleted(object sender, PROFILEINFO info, int count)
        {
            string msg = "BULK SMS: " + info.timestamp + " : " + info.veh_reg + " : " + info.contact_name + " : " + info.tel_no + " : " + count + " Message" + " Evt : " + info.evt_desc;
            PrintMsg(msg);
        }

        private void PrintMsg(string msg)
        {
            if (this.richTextBox1.InvokeRequired)
            {
                if (!bCanClose)
                {
                    PrintMagCallBack proc = new PrintMagCallBack(PrintMsg);
                    Invoke(proc, new object[] { msg });
                }
                else
                {
                    Application.Exit();
                }
            }
            else
            {
                richTextBox1.Text = msg + "\n" + richTextBox1.Text;

                int nRow = ((int)(richTextBox1.ClientSize.Height / richTextBox1.Font.GetHeight()));
                if (richTextBox1.Lines.Length > nRow)
                {
                    int iPos = 0;
                    int iIdx = -1;
                    for (int i = 0; i < nRow; i++)
                    {
                        if (iPos < richTextBox1.Text.Length)
                        {
                            iIdx = richTextBox1.Text.IndexOf('\n', iPos);
                            if (iIdx > -1)
                            {
                                iPos = iIdx + 1;
                            }
                        }
                    }
                    if (iPos > 0)
                    {
                        richTextBox1.Text = richTextBox1.Text.Remove(iPos - 1);
                    }
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Init();
        }
        private void MainFrm_Shown(object sender, EventArgs e)
        {
            this.tsGSMStatus.Text = szBroadcastStatus;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!smsObj.IsThreadAbort)
            {
                e.Cancel = true;
                bCanClose = true;
                smsObj.Dispose();
                tslbStatus.Text = "Please wait, Stop Thread Running...";
            }
            else
            {
                e.Cancel = false;
            }

        }
        private void Init()
        {
            try
            {
                Properties.Settings pf = new ZebraBroadcast.Properties.Settings();

                SMSQ = new CommonQeue(pf.Broadcast_SMSQ);
                System.Threading.ThreadStart smsThrd = new System.Threading.ThreadStart(StartThereadSMS);
                System.Threading.Thread thrd1 = new System.Threading.Thread(smsThrd);
                thrd1.Start();

                EMAILQ = new CommonQeue(pf.Broadcast_EMAILQ);
                System.Threading.ThreadStart emailThrd = new System.Threading.ThreadStart(StartThereadEmail);
                System.Threading.Thread thrd2 = new System.Threading.Thread(emailThrd);
                thrd2.Start();
            }
            catch (Exception ex)
            {
                WriteLogEvent(ex.Message);
            }
        }
        private void StartThereadSMS()
        {
            SMSQ.InitReceiveQMsg();
            SMSQ.OnReceiveMQ += new ReceiveMQ(SMSQ_OnReceiveMQ);
            SMSQ.BeginReceiveQMsg();
        }
        private void StartThereadEmail()
        {
            EMAILQ.InitReceiveQMsg();
            EMAILQ.OnReceiveMQ += new ReceiveMQ(EMAILQ_OnReceiveMQ);
            EMAILQ.BeginReceiveQMsg();
        }

        private void SMSQ_OnReceiveMQ(object owner, System.Xml.XmlReader xmlData, Q_BROADCASTTYPE qType)
        {
            XmlSerializer ser = new XmlSerializer(typeof(ZEBRABROADCASTMQINFO));
            ZEBRABROADCASTMQINFO msg = (ZEBRABROADCASTMQINFO)ser.Deserialize(xmlData);
            OnProcessSMS(msg, qType);
            SMSQ.BeginReceiveQMsg();

        }
        private void OnProcessSMS(ZEBRABROADCASTMQINFO msg, Q_BROADCASTTYPE qType)
        {
            try
            {
                String zoneLabel = "?";
                String routeLabel = "?";
                #region
                switch (msg.type)
                {
                    case (int)EVT_TYPE.GENERIC:
                        //MessageBox.Show("GENERIC sms");
                        string[] bMsg = msg.msg.Split(',');
                        if (bMsg == null || bMsg.Length < 1) return;
                        if (qType == Q_BROADCASTTYPE.SMS)
                        {
                            BroadcastMsg(msg.msg.Substring(bMsg[0].Length + 1), bMsg[0].Trim(), msg.veh_id.ToString("0").Trim(), msg.timestamp.Trim());
                            //BroadcastMsg(bMsg[1].Trim(), bMsg[0].Trim(), msg.veh_id.ToString("0").Trim(), msg.timestamp.Trim());
                        }
                        break;

                    case (int)EVT_TYPE.GENERIC2:
                        string[] bMsg2 = msg.msg.Split(',');
                        if (bMsg2 == null || bMsg2.Length < 1) return;
                        //string MSG, string TelNo, string VID, string TimeStamp
                        //*>W21621701,0890999031,181,2012-03-13 17:03:50
                        if (qType == Q_BROADCASTTYPE.SMS)
                        {
                            //BroadcastMsg2("*>W21621701", "+66890999031", "181", "2012-03-13 17:03:50");
                            BroadcastMsg2(bMsg2[1].Trim(), bMsg2[0].Trim(), msg.veh_id.ToString("0").Trim(), msg.timestamp.Trim());
                        }
                        break;

                    case (int)EVT_TYPE.IN_ZONE:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.OUT_ZONE:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.IN_ROUTE:
                        routeLabel = sql.GetRouteLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, routeLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.OUT_ROUTE:
                        routeLabel = sql.GetRouteLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, routeLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.NO_INZONE_CURFEW:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.NO_OUTZONE_CURFEW:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;
                    case (int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.WATCHDOG:
                        {
                            string[] bMsg3 = msg.msg.Split(',');
                            if (bMsg3 == null || bMsg3.Length < 1) return;

                            if (qType == Q_BROADCASTTYPE.SMS)
                            {
                                //Broadcast_WatchDog_SMS(bMsg3[1].Trim(), bMsg3[0].Trim(), msg.timestamp.Trim());
                                ///string zMsg, string zTelNo, string zTimestamp
                                ///MAP : 10007 Queue,0890999031,2/9/2012 11:09:30 AM

                                //send BlukSMS
                                Broadcast_SMS(bMsg3[0].Trim(), bMsg3[1].Trim(), bMsg3[2].Trim(), EVT_TYPE.WATCHDOG, SMSFORMAT.WatchDog, -1);

                                #region Test_phpMyadmin
                                string texttimestamp = Convert.ToString(bMsg3[2].Trim());
                                string[] timestampalert = texttimestamp.Split('/');
                                texttimestamp = timestampalert[2].Substring(0, 4) + "-" + timestampalert[0] + "-" + timestampalert[1] + " " + timestampalert[2].Substring(5, 8);
                                
                                //Code_Max
                                if (this.cbMAA.Checked)
                                {
                                    try
                                    {
                                        string post_data = "text=" + bMsg3[0].Trim() + "&time=" + texttimestamp + "&tel=" + bMsg3[1].Trim();
                                        string url = "http://10.0.10.29:5507/alert/MySQL/_push_29.php"; //max
                                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                                        request.Method = "POST";
                                        byte[] postBytes = Encoding.ASCII.GetBytes(post_data);
                                        request.ContentType = "application/x-www-form-urlencoded";
                                        request.ContentLength = postBytes.Length;
                                        Stream requestStream = request.GetRequestStream();
                                        requestStream.Write(postBytes, 0, postBytes.Length);
                                        requestStream.Close();
                                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                                        response.Close();
                                        PrintMsg("Finish!!! Send data to pushApp");
                                    }
                                    catch (Exception ex)
                                    {
                                        PrintMsg("Can't insert data to pushApp , Erroe " + ex.Message);
                                    }
                                }
                                #endregion

                            }
                        }
                        break;
                    case (int)EVT_TYPE.CALLCENTER:
                        {
                            string[] bMsg3 = msg.msg.Split(',');
                            if (bMsg3 == null || bMsg3.Length < 2) return;
                            Broadcast_SMS(bMsg3[0].Trim(), bMsg3[1].Trim(), msg.timestamp, EVT_TYPE.CALLCENTER, SMSFORMAT.TESTSYNTAX, int.Parse(bMsg3[2].Trim()));
                        }
                        break;
                    case (int)EVT_TYPE.OTA:
                        {
                            //string zMsg, string zTelNo, string zTimestamp
                            //Broadcast_SMS(string zTypeValue, string zTelNo, string zTimestamp, EVT_TYPE Event, SMSFORMAT format, int user_id)
                            string[] bMsg3 = msg.msg.Split('@');
                            if (bMsg3 == null || bMsg3.Length < 2) return;
                            // Broadcast_SMS(bMsg3[0].Trim(), "0890999031", DateTime.Now.ToString(), EVT_TYPE.OTA, SMSFORMAT.TESTSYNTAX, int.Parse(bMsg3[2].Trim()));
                            Broadcast_SMS(bMsg3[0].Trim(), bMsg3[1].Trim(), DateTime.Now.ToString(), EVT_TYPE.OTA, SMSFORMAT.TESTSYNTAX, int.Parse(bMsg3[2].Trim()));
                        }
                        break;

                    //case (int)EVT_TYPE.SPEEDING:
                    //    if (msg.msg == null || msg.msg.Length < 1) return;
                    //    BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                    //    break;

                    //case (int)EVT_TYPE.DISTANCE_OVER:
                    //    BroadcastEvent(msg.idx, msg.veh_id, msg.type, msg.msg, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                    //    break;

                    default:
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, msg.msg, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                WriteLogEvent("SMS : " + ex.Message);
            }
        }
        private void EMAILQ_OnReceiveMQ(object owner, System.Xml.XmlReader xmlData, Q_BROADCASTTYPE qType)
        {
            XmlSerializer ser = new XmlSerializer(typeof(ZEBRABROADCASTMQINFO));
            ZEBRABROADCASTMQINFO msg = (ZEBRABROADCASTMQINFO)ser.Deserialize(xmlData);
            OnProcessEmail(msg, qType);
            EMAILQ.BeginReceiveQMsg();

        
            
        }
        private void OnProcessEmail(ZEBRABROADCASTMQINFO msg, Q_BROADCASTTYPE qType)
        {
            try
            {
                String zoneLabel = "?";
                String routeLabel = "?";

                #region
                switch (msg.type)
                {
                    case (int)EVT_TYPE.GENERIC:
                        //MessageBox.Show("GENERIC email");
                        string[] bMsg = msg.msg.Split(',');
                        if (bMsg == null || bMsg.Length < 1) return;
                        if (qType == Q_BROADCASTTYPE.SMS)
                        {
                            BroadcastMsg(msg.msg.Substring(bMsg[0].Length+1), bMsg[0].Trim(), msg.veh_id.ToString("0").Trim(), msg.timestamp.Trim());
                        }
                        break;

                    case (int)EVT_TYPE.GENERIC2:
                        string[] bMsg2 = msg.msg.Split(',');
                        if (bMsg2 == null || bMsg2.Length < 1) return;
                        if (qType == Q_BROADCASTTYPE.SMS)
                        {
                            BroadcastMsg2(bMsg2[1].Trim(), bMsg2[0].Trim(), msg.veh_id.ToString("0").Trim(), msg.timestamp.Trim());
                        }
                        break;


                    case (int)EVT_TYPE.IN_ZONE:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.OUT_ZONE:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.IN_ROUTE:
                        routeLabel = sql.GetRouteLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, routeLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.OUT_ROUTE:
                        routeLabel = sql.GetRouteLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, routeLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.NO_INZONE_CURFEW:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.NO_OUTZONE_CURFEW:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    case (int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;
                    case (int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED:
                        zoneLabel = sql.GetZoneLabel(Convert.ToInt32(msg.msg));
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;
                    case (int)EVT_TYPE.DISTANCE_OVER:
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, zoneLabel, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;

                    default:
                        BroadcastEvent(msg.idx, msg.veh_id, msg.type, msg.msg, msg.timestamp, msg.location_t, msg.lat, msg.lon, msg.speed, qType, msg.msg);
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                WriteLogEvent("Email : " + ex.Message);
            }
        }
        private void BroadcastEvent(int idx, int VehID, int evt_type, string value, string timestamp, string location_t, double lat, double lon, int speed, Q_BROADCASTTYPE qType, string msg)
        {
            try
            {
                int tEvt_type = evt_type;

                if (evt_type == (int)EVT_TYPE.ENGINE_ON_IN_ZONE || evt_type == (int)EVT_TYPE.ENGINE_OFF_IN_ZONE || evt_type == (int)EVT_TYPE.ENGINE_ON_OUT_ZONE || evt_type == (int)EVT_TYPE.ENGINE_OFF_OUT_ZONE)
                {
                    tEvt_type = (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE;
                }
                //เว็บใหม่
                DataTable checknewweb = sql.Check_param_newweb(VehID, evt_type);
                if (checknewweb.Rows.Count > 0)
                {
                    DataTable typeTable = sql.GetBroadcastTypes_New(VehID, tEvt_type);
                    foreach (DataRow r in typeTable.Rows)
                    {
                        //á¡éàªç¤Êè§ email sms
                        int use_email = Convert.ToInt32((string)r["use_email"]);
                        int use_sms = Convert.ToInt32((string)r["use_sms"]);
                        int use_sms_broadcast = Convert.ToInt32((string)r["use_sms_broadcast"]);
                        if (use_email == 1 && (qType == Q_BROADCASTTYPE.EMAIL))
                        {
                            emailObj.SendEmail(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, msg);
                        }
                        if (use_sms == 1 && (qType == Q_BROADCASTTYPE.SMS))
                        {
                            smsObj.SendSMS(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, speed, msg);
                        }
                        if (use_sms_broadcast == 1 && (qType == Q_BROADCASTTYPE.SMS))
                        {
                            smsObj.SendBulkSMS(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, speed, msg);
                        }
                    }
                }
                else
                {
                    DataTable typeTable = sql.GetBroadcastTypes(VehID, tEvt_type);
                    foreach (DataRow r in typeTable.Rows)
                    {
                        string type = (string)r["type"];
                        string chk = (string)r["chk"];

                        switch (type)
                        {
                            case "EMAIL":
                                if (chk == "1" && (qType == Q_BROADCASTTYPE.EMAIL))
                                {
                                    emailObj.SendEmail(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, msg);
                                }
                                break;

                            case "SMS":
                                if (chk == "1" && (qType == Q_BROADCASTTYPE.SMS))
                                {
                                    smsObj.SendSMS(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, speed, msg);
                                }
                                break;

                            case "BULKSMS":
                                if (chk == "1" && (qType == Q_BROADCASTTYPE.SMS))
                                {
                                    smsObj.SendBulkSMS(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, speed, msg);
                                }
                                break;

                            default: continue;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                WriteLogEvent("BroadcastEvent : " + ex.Message);
            }
        }
        //äÁèãªé
        //private void BroadcastEvent(int idx, int VehID, int evt_type, string value, string timestamp, string location_t, double lat, double lon, int speed, Q_BROADCASTTYPE qType, string msg)
        //{
        //    try
        //    {
        //        int tEvt_type = evt_type;

        //        if (evt_type == (int)EVT_TYPE.ENGINE_ON_IN_ZONE || evt_type == (int)EVT_TYPE.ENGINE_OFF_IN_ZONE || evt_type == (int)EVT_TYPE.ENGINE_ON_OUT_ZONE || evt_type == (int)EVT_TYPE.ENGINE_OFF_OUT_ZONE)
        //        {
        //            tEvt_type = (int)EVT_TYPE.ENGINE_ON_OFF_IN_OUT_ZONE;
        //        }

        //        DataTable typeTable = sql.GetBroadcastTypes(VehID, tEvt_type);
        //        foreach (DataRow r in typeTable.Rows)
        //        {
        //            string type = (string)r["type"];
        //            string chk = (string)r["chk"];

        //            switch (type)
        //            {
        //                case "EMAIL":
        //                    if (chk == "1" && (qType == Q_BROADCASTTYPE.EMAIL))
        //                    {
        //                        emailObj.SendEmail(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, msg);
        //                    }
        //                    break;

        //                case "SMS":
        //                    if (chk == "1" && (qType == Q_BROADCASTTYPE.SMS))
        //                    {
        //                        smsObj.SendSMS(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, speed, msg);
        //                    }
        //                    break;

        //                case "BULKSMS":
        //                    if (chk == "1" && (qType == Q_BROADCASTTYPE.SMS))
        //                    {
        //                        smsObj.SendBulkSMS(idx, VehID, evt_type, value, timestamp, location_t, lat, lon, speed, msg);
        //                    }
        //                    break;

        //                default: continue;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLogEvent("BroadcastEvent : " + ex.Message);
        //    }
        //}
        private void BroadcastMsgx(string MSG, string TelNo)
        {
            try
            {
                smsObj.SendMsgSMS(TelNo, MSG);
            }
            catch { }
        }
        private void BroadcastMsg(string MSG, string TelNo, string VID, string TimeStamp)
        {
            try
            {
                smsObj.SendMsgSMS(TelNo, MSG);
                PROFILEINFO info = new PROFILEINFO();
                info.fleet_id = 0;
                info.staff_id = 0;
                info.veh_id = Convert.ToInt32(VID);
                info.timestamp = TimeStamp;
                info.evt_id = (int)EVT_TYPE.GENERIC;
                string msg = "SMS: " + info.timestamp + " : " + info.veh_reg + " : " + info.contact_name + " : " + TelNo + " Evt : " + "GENERIC";
                PrintMsg(msg);
                smsObj.RecordLog(info);
            }
            catch { }
        }
        private void BroadcastMsg2(string MSG, string TelNo, string VID, string TimeStamp)
        {
            try
            {
                string msg = "";
                //MessageBox.Show(TelNo + " : " + MSG);
                if (smsObj.SendMsgSMS2(TelNo, MSG))
                {
                    PROFILEINFO info = new PROFILEINFO();
                    info.fleet_id = 0;
                    info.staff_id = 0;
                    info.veh_id = Convert.ToInt32(VID);
                    info.timestamp = TimeStamp;
                    info.evt_id = (int)EVT_TYPE.GENERIC2;
                    msg = "SMS : " + info.timestamp + " : " + info.veh_reg + " : " + info.contact_name + " : " + TelNo + " Evt : " + EVT_TYPE.GENERIC2.ToString();
                    smsObj.RecordLog(info);
                }
                else
                {
                    msg = "Fail...Send SMS : " + TimeStamp + " : " + VID + " : " + "-" + " : " + TelNo + " Evt : " + EVT_TYPE.GENERIC2.ToString();
                }
                PrintMsg(msg);
            }
            catch
            {
            }
        }
        
        private void BroadcastTrueEvent(int idx, int VehID, EVT_TYPE evt_type, string value, string timestamp, string location_t, double lat, double lon, Q_BROADCASTTYPE qType)
        {
        //    try
        //    {
        //        SQLCls sql = new SQLCls(gDbHost);

        //        string[] m = value.Split(new char[] { ',' });
        //        int zone_id = Convert.ToInt32(m[0]);
        //        string zone_type = m[1];
        //        string zoneLabel = sql.GetZoneLabel(zone_id);

        //        DataTable vehTable = sql.GetTrueVehProfile(VehID);
        //        string reg = vehTable.Rows[0]["registration"] as string;

        //        ArrayList usrList = new ArrayList();

        //        DataTable usrTable = sql.GetTrueUserProfile(zone_id);
        //        foreach (DataRow r in usrTable.Rows)
        //        {
        //            TRUEUSERINFO info = new TRUEUSERINFO();
        //            info.name = (string)r["name"];
        //            info.mobile = (string)r["mobile"];
        //            info.email = (string)r["email"];
        //            usrList.Add(info);
        //        }

        //        DataTable typeTable = sql.GetTrueZoneProfile(zone_id);

        //        foreach (DataRow r in typeTable.Rows)
        //        {
        //            int type = (int)r["alert_type"];
        //            switch (type)
        //            {
        //                case 1:
        //                    if (qType == Q_BROADCASTTYPE.EMAIL)
        //                    {
        //                        System.Net.Mail.MailPriority mailType = System.Net.Mail.MailPriority.Normal;
        //                        if (zone_id == 1370)
        //                        {
        //                            mailType = System.Net.Mail.MailPriority.High;
        //                        }

        //                        // emailObj.SendTrueZone(VehID, usrList, reg, zoneLabel, zone_type, timestamp, location_t, lat, lon, mailType, (int)evt_type);
        //                    }
        //                    break;

        //                case 2:
        //                    if (qType == Q_BROADCASTTYPE.SMS)
        //                    {
        //                        foreach (TRUEUSERINFO info in usrList)
        //                        {
        //                            string msg = zone_type + zoneLabel + "," +
        //                                         reg + "," +
        //                                         location_t + "," +
        //                                         timestamp;

        //                            if (gTrueFleetSendZoneSMS && (info.mobile.Trim().Length > 0))
        //                            {
        //                                //smsObj.SendTrueMsgSMS(info.mobile.Trim(), msg);

        //                                PROFILEINFO smsInfo = new PROFILEINFO();
        //                                smsInfo.type = PROFILEINFO_TYPE.VALUE_TYPE;
        //                                smsInfo.value = msg;
        //                                smsInfo.tel_no = info.mobile.Trim();
        //                                smsInfo.veh_reg = VehID.ToString("0");
        //                                smsInfo.timestamp = timestamp;
        //                                smsInfo.contact_name = "?";
        //                                smsObj.SendTrueMsgSMS(smsInfo);

        //                                PROFILEINFO info2 = new PROFILEINFO();
        //                                info2.veh_id = VehID;
        //                                info2.contact_name = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).name;
        //                                info2.driver = "?";
        //                                info2.veh_reg = reg;
        //                                info2.timestamp = timestamp;
        //                                info2.lon = lon;
        //                                info2.lat = lat;
        //                                info2.tel_no = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).mobile;
        //                                info2.evt_id = (int)evt_type;

        //                                smsObj.RecordLog(info2);
        //                            }
        //                        }
        //                    }
        //                    break;

        //                default: continue;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLogEvent("Zebra Broadcast Error #2.5" + ex.Message);
        //    }
        }

        private void BroadcastTrueEvent2(int idx, int VehID, EVT_TYPE evt_type, string value, string timestamp, string location, double lat, double lon, Q_BROADCASTTYPE qType)
        {
        //    try
        //    {
        //        SQLCls sql = new SQLCls(gDbHost);

        //        DataTable vehTable = sql.GetTrueVehProfile(VehID);
        //        string reg = vehTable.Rows[0]["registration"] as string;

        //        ArrayList usrList = new ArrayList();
        //        DataTable usrTable = sql.GetTrueUserProfile();
        //        foreach (DataRow r in usrTable.Rows)
        //        {
        //            TRUEUSERINFO info = new TRUEUSERINFO();
        //            info.name = (string)r["name"];
        //            info.mobile = (string)r["mobile"];
        //            info.email = (string)r["email"];
        //            usrList.Add(info);
        //        }

        //        // Send Email
        //        if (qType == Q_BROADCASTTYPE.EMAIL)
        //        {
        //            System.Net.Mail.MailPriority mailType = System.Net.Mail.MailPriority.Normal;
        //            mailType = System.Net.Mail.MailPriority.Normal;

        //            emailObj.SendTrueMsg(VehID, usrList, reg, value, timestamp, location, lat, lon, mailType, (int)evt_type);
        //        }

        //        // Send SMS
        //        if (qType == Q_BROADCASTTYPE.SMS)
        //        {
        //            foreach (TRUEUSERINFO info in usrList)
        //            {
        //                string msg = value + " Alert:" + "," +
        //                             reg + "," +
        //                             location + "," +
        //                             timestamp;

        //                if (gTrueFleetSendIOSMS && info.mobile.Trim().Length > 0)
        //                {

        //                    PROFILEINFO smsInfo = new PROFILEINFO();
        //                    smsInfo.type = PROFILEINFO_TYPE.VALUE_TYPE;
        //                    smsInfo.value = msg;
        //                    smsInfo.tel_no = info.mobile.Trim();
        //                    smsInfo.veh_reg = VehID.ToString("0");
        //                    smsInfo.timestamp = timestamp;
        //                    smsInfo.contact_name = "?";
        //                    smsObj.SendTrueMsgSMS(smsInfo);

        //                    PROFILEINFO info2 = new PROFILEINFO();
        //                    info2.veh_id = VehID;
        //                    info2.contact_name = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).name;
        //                    info2.driver = "?";
        //                    info2.veh_reg = reg;
        //                    info2.timestamp = timestamp;
        //                    info2.lon = lon;
        //                    info2.lat = lat;
        //                    info2.tel_no = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).mobile;
        //                    info2.evt_id = (int)evt_type;

        //                    smsObj.RecordLog(info2);
        //                }
        //            }
        //        }
        //    }
        //    catch { }
        }
        
        private void BroadcastTrueZoneAlertEvent2(int idx, int VehID, EVT_TYPE evt_type, string value, string timestamp, string location, double lat, double lon, Q_BROADCASTTYPE qType)
        {
        //    try
        //    {
        //        SQLCls sql = new SQLCls(gDbHost);

        //        string[] m = value.Split(new char[] { ',' });
        //        int zone_id = Convert.ToInt32(m[0]);
        //        string zone_type = m[1];
        //        string zoneLabel = sql.GetZoneLabel(zone_id);

        //        DataTable vehTable = sql.GetTrueVehProfile2(VehID);
        //        string reg = vehTable.Rows[0]["registration"] as string;

        //        ArrayList usrList = new ArrayList();
        //        DataTable usrTable = sql.GetTrueUserProfile2(zone_id);
        //        foreach (DataRow r in usrTable.Rows)
        //        {
        //            TRUEUSERINFO2 info = new TRUEUSERINFO2();
        //            info.name = (string)r["name"];
        //            info.mobile = (string)r["mobile"];
        //            info.email = (string)r["email"];
        //            usrList.Add(info);
        //        }

        //        DataTable typeTable = sql.GetTrueZoneProfile2(zone_id);

        //        foreach (DataRow r in typeTable.Rows)
        //        {
        //            bool allow_emai = (bool)r["alert_email"];
        //            bool allow_sms = (bool)r["alert_sms"];

        //            if (allow_sms && (qType == Q_BROADCASTTYPE.SMS))
        //            {
        //                foreach (TRUEUSERINFO2 info in usrList)
        //                {
        //                    string msg = zone_type + zoneLabel + "," +
        //                                 reg + "," +
        //                                 location + "," +
        //                                 timestamp;

        //                    if (info.mobile.Trim().Length > 0)
        //                    {

        //                        PROFILEINFO smsInfo = new PROFILEINFO();
        //                        smsInfo.type = PROFILEINFO_TYPE.VALUE_TYPE;
        //                        smsInfo.value = msg;
        //                        smsInfo.tel_no = info.mobile.Trim();
        //                        smsInfo.veh_reg = VehID.ToString("0");
        //                        smsInfo.timestamp = timestamp;
        //                        smsInfo.contact_name = "?";
        //                        smsObj.SendTrueMsgSMS2(smsInfo);

        //                        PROFILEINFO info2 = new PROFILEINFO();
        //                        info2.veh_id = VehID;
        //                        info2.contact_name = ((ZebraBroadcast.MainFrm.TRUEUSERINFO2)usrList[0]).name;
        //                        info2.driver = "?";
        //                        info2.veh_reg = reg;
        //                        info2.timestamp = timestamp;
        //                        info2.lon = lon;
        //                        info2.lat = lat;
        //                        info2.tel_no = ((ZebraBroadcast.MainFrm.TRUEUSERINFO2)usrList[0]).mobile;
        //                        info2.evt_id = (int)evt_type;

        //                        smsObj.RecordLog(info2);
        //                    }
        //                }
        //            }

        //            if (allow_emai && (qType == Q_BROADCASTTYPE.EMAIL))
        //            {
        //                System.Net.Mail.MailPriority mailType = System.Net.Mail.MailPriority.Normal;
        //                emailObj.SendTrueZone2(VehID, usrList, reg, zoneLabel, zone_type, timestamp, location, lat, lon, mailType, (int)evt_type);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLogEvent("Zebra Broadcast Error #2.5" + ex.Message);
        //    }
        }
        
        private void testSMSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (smsObj == null)
            {
                return;
            }

            TestSMSFrm frm = new TestSMSFrm();
            DialogResult r = frm.ShowDialog();
            if (r == DialogResult.OK)
            {
                string telNo = frm.tbTelNo.Text.Trim();
                string msg = frm.tbMsg.Text.Trim();
                string pwd = frm.tbPwd.Text.Trim();
                if (pwd == "1q2w&pegasus")
                {
                    smsObj.TestSMS1(telNo, msg);
                }
            }
        }
        private void testSMS2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (smsObj == null)
            {
                return;
            }

            TestSMSFrm frm = new TestSMSFrm();
            DialogResult r = frm.ShowDialog();
            if (r == DialogResult.OK)
            {
                string telNo = frm.tbTelNo.Text.Trim();
                string msg = frm.tbMsg.Text.Trim();
                string pwd = frm.tbPwd.Text.Trim();
                if (pwd == "1q2w&pegasus")
                {
                    smsObj.TestSMS2(telNo, msg);
                }
            }
        }
        //public void WriteLogEvent(String msg)
        //{
        //    msg = DateTime.Now.ToString() + " : " + msg;
        //    String fileName = "ErrorEvent_" + DateTime.Now.ToShortDateString() + ".txt";
        //    if (File.Exists(fileName))
        //    {
        //        StreamWriter sr = File.AppendText(fileName);
        //        sr.WriteLine(msg);
        //        sr.Close();
        //        sr.Dispose();
        //    }
        //    else
        //    {
        //        StreamWriter sr = File.CreateText(fileName);
        //        sr.WriteLine(msg);
        //        sr.Close();
        //        sr.Dispose();
        //    }
        //}

        public static void WriteLogEvent(string msg)
        {
            try
            {
                //String logPath = Directory.GetCurrentDirectory() + "\\Log";
                String logPath = Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\LogError");
                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                }
                String fileName = logPath + "\\" + "Datalog_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";

                if (File.Exists(fileName))
                {
                    StreamWriter sr = File.AppendText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
                else
                {
                    StreamWriter sr = File.CreateText(fileName);
                    sr.WriteLine(msg);
                    sr.Close();
                    sr.Dispose();
                }
            }
            catch { }
        }

        private void testRemoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //WakeUpRemoteObj();
        }
        private bool WakeUpRemoteObj()
        {
            try
            {
                BinaryClientFormatterSinkProvider clientProvider2 = new BinaryClientFormatterSinkProvider();
                BinaryServerFormatterSinkProvider serverProvider2 = new BinaryServerFormatterSinkProvider();
                serverProvider2.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

                IDictionary props2 = new Hashtable();
                props2["port"] = 0;
                string s = System.Guid.NewGuid().ToString();
                props2["name"] = s;
                props2["typeFilterLevel"] = TypeFilterLevel.Full;
                TcpChannel chan2 = new TcpChannel(
                    props2, clientProvider2, serverProvider2);

                ChannelServices.RegisterChannel(chan2);
                Type typeofRI = typeof(BroadcastInterface);

                IPHostEntry localIP = Dns.GetHostByName(Dns.GetHostName());
                String connStr = String.Format("tcp://{0}:{1}/SMSObj", localIP.AddressList[0], gPortNo);

                BroadcastInterface obj = (BroadcastInterface)Activator.GetObject(
                    typeof(BroadcastInterface),
                    connStr);

                if (obj.IsAlive())
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Broadcast Error #3" + ex.Message);
            }
            return false;
        }
        private void tsSendSMS_Click(object sender, EventArgs e)
        {
            if (smsObj == null)
            {
                return;
            }

            TestSMSFrm frm = new TestSMSFrm();
            DialogResult r = frm.ShowDialog();
            if (r == DialogResult.OK)
            {
                string telNo = frm.tbTelNo.Text.Trim();
                string msg = frm.tbMsg.Text.Trim();
                string pwd = frm.tbPwd.Text.Trim();
                if (pwd == "1q2w&pegasus")
                {
                    try
                    {
                        PROFILEINFO info = new PROFILEINFO();
                        info.type = PROFILEINFO_TYPE.VALUE_TYPE;
                        info.value = msg;
                        info.tel_no = telNo;
                        info.veh_reg = "?";
                        info.timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        info.contact_name = "?";
                        smsObj.SendMsgSMS3(info);
                        MessageBox.Show("Complete");
                    }
                    catch
                    {
                        MessageBox.Show("Error");
                    }
                }
                else
                    MessageBox.Show("Password is incorrect.");
            }
        }
        private void testBulkSmsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (smsObj == null)
            {
                return;
            }
            TestSMSFrm frm = new TestSMSFrm();
            DialogResult r = frm.ShowDialog();
            if (r == DialogResult.OK)
            {
                string telNo = frm.tbTelNo.Text.Trim();
                string msg = frm.tbMsg.Text.Trim();
                string pwd = frm.tbPwd.Text.Trim();
                if (pwd == "1q2w&pegasus")
                {
                    PROFILEINFO info = new PROFILEINFO();
                    info.type = PROFILEINFO_TYPE.VALUE_TYPE;
                    info.value = msg;
                    if (telNo.StartsWith("+66"))
                    {
                        //info.tel_no = telNo.Remove(0, 3);
                        ///before +66891234567 
                        ///after 0891234567
                        info.tel_no = telNo.Replace("+66", "0");
                    }
                    info.veh_reg = "?";
                    info.timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    info.contact_name = "?";
                    info.format_send = (int)SMSFORMAT.TESTSYNTAX;
                    smsObj.SendTestBulkSMS(info);
                }
                else
                    MessageBox.Show("Password is incorrect.");
            }
        }
        private void testEmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            testEmailFrm frmEmail = new testEmailFrm();
            DialogResult r = frmEmail.ShowDialog();
            ArrayList contactList = new ArrayList();

            if (r == DialogResult.OK)
            {
                string to = frmEmail.tbTo.Text.Trim();
                string subject = frmEmail.tbSubject.Text;
                string msg = frmEmail.tbMsg.Text.Trim();

                PROFILEINFO info = new PROFILEINFO();
                info.veh_id = -1;
                info.fleet_id = -1;
                info.fleet_name = "-";
                info.veh_reg = "-";
                info.staff_id = -1;
                info.driver = "";
                info.max_speed = -1;
                info.value = msg;
                info.msg = msg;
                info.tel_no = "";
                info.email = to;
                info.timestamp = DateTime.Now.ToString();
                info.location = "";
                info.lat = 0.0;
                info.lon = 0.0;
                info.evt_id = -1;
                info.evt_desc = "-";
                info.subject = subject;

                contactList.Add(info);

                MessageBox.Show(emailObj.TestSendMail(contactList), "Send Email", MessageBoxButtons.OK);
            }

            frmEmail.Dispose();
        }
        private void Broadcast_SMS(string zTypeValue, string zTelNo, string zTimestamp, EVT_TYPE Event, SMSFORMAT format, int user_id)
        {
            if (zTelNo.Trim().Length > 0)
            {
                PROFILEINFO info = new PROFILEINFO();

                if (Event == EVT_TYPE.CALLCENTER || Event == EVT_TYPE.OTA)
                {
                    info.staff_id = user_id;
                }
                else
                {
                    info.staff_id = -1;
                }

                info.veh_id = -1;
                info.fleet_id = -1;
                info.fleet_name = "-";
                info.veh_reg = "-";
                info.max_speed = -1;
                info.value = zTypeValue;//MAP : 10007 Queue,0890999031,2/9/2012 11:09:30 AM
                info.tel_no = zTelNo;
                info.email = "-";
                info.subject = "-";
                info.timestamp = zTimestamp;
                info.location = "-";
                info.lat = 0.0;
                info.lon = 0.0;
                info.driver = "-";
                info.contact_name = "-";
                info.evt_id = (int)Event;
                info.speed = -1;
                info.msg = zTypeValue;
                info.format_send = (int)format;
                info.evt_desc = Event.ToString();
                info.type = PROFILEINFO_TYPE.ALERT;
                smsObj.SendTestBulkSMS(info);
            }
        }
        private void BroadcastTrueZoneing(int idx, int VehID, EVT_TYPE evt_type, string msg, string timestamp, string location, double lat, double lon, int speed, Q_BROADCASTTYPE qType)
        {
            try
            {
                SQLCls sql = new SQLCls(gDbHost);

                //1.Check Veh_id in True
                DataTable dtVehInGroup = sql.True2GetTrueCorpVehicle(VehID);
                if (dtVehInGroup.Rows.Count < 1) { return; }

                //2.Get Profile 
                DataTable vehTable = sql.True2GetTrueVehProfile(VehID);
                string reg = vehTable.Rows[0]["registration"] as string;

                foreach (DataRow dr in vehTable.Rows)
                {
                    //zone
                    string[] value = msg.Split(',');
                    //3.Send To User 
                    DataTable dtProfile = sql.True2GetSendProfile((int)dr["group_id"], (int)evt_type, idx, (int)qType, int.Parse(value[2]));
                    foreach (DataRow dr2 in dtProfile.Rows)
                    {
                        ArrayList usrList = new ArrayList();

                        switch ((int)qType)
                        {
                            case (int)BROADCASTLOGTYPE.EMAIL:
                                {
                                    string user_id = (string)dr2["user_id"];
                                    foreach (string uid in user_id.Split(','))
                                    {
                                        DataTable dtUserInfo = sql.True2GetUserProfile(int.Parse(uid.Trim()));
                                        if (dtUserInfo.Rows.Count > 0)
                                        {
                                            TRUEUSERINFO info = new TRUEUSERINFO();
                                            info.name = (string)dtUserInfo.Rows[0]["name"];
                                            info.mobile = (string)dtUserInfo.Rows[0]["tel_no"];
                                            info.email = (string)dtUserInfo.Rows[0]["email"];
                                            usrList.Add(info);
                                        }
                                    }

                                    //Send E-Mail
                                    if (usrList.Count > 0)
                                    {
                                        emailObj.SendTrueZone(VehID, usrList, reg, value[1], timestamp, location, lat, lon, (int)evt_type);
                                    }
                                }
                                break;
                            case (int)BROADCASTLOGTYPE.SMS:
                                {
                                    string user_id = (string)dr2["user_id"];
                                    foreach (string uid in user_id.Split(','))
                                    {
                                        DataTable dtUserInfo = sql.True2GetUserProfile(int.Parse(uid.Trim()));
                                        if (dtUserInfo.Rows.Count > 0)
                                        {
                                            TRUEUSERINFO info = new TRUEUSERINFO();
                                            info.name = (string)dtUserInfo.Rows[0]["name"];
                                            info.mobile = (string)dtUserInfo.Rows[0]["tel_no"];
                                            info.email = (string)dtUserInfo.Rows[0]["email"];
                                            usrList.Add(info);
                                        }
                                    }

                                    //Send SMS
                                    if (usrList.Count > 0)
                                    {
                                        string zoneLabel = sql.GetZoneLabel(Convert.ToInt32(value[2]));
                                        smsObj.True2BulkSmsSendToContact(VehID, usrList, reg, value[0].ToString(), timestamp, location, lat, lon, evt_type, speed, zoneLabel);
                                    }
                                }
                                break;
                            default: break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Broadcast Error #2.5" + ex.Message);
            }
        }
        private void BroadcastTrueIOStatus(int idx, int VehID, EVT_TYPE evt_type, string msg, string timestamp, string location, double lat, double lon, int speed, Q_BROADCASTTYPE qType)
        {
            try
            {
                SQLCls sql = new SQLCls(gDbHost);

                //1.Check Veh_id in True
                DataTable dtVehInGroup = sql.True2GetTrueCorpVehicle(VehID);
                if (dtVehInGroup.Rows.Count < 1) { return; }

                //2.Get Profile 
                DataTable vehTable = sql.True2GetTrueVehProfile(VehID);
                string reg = vehTable.Rows[0]["registration"] as string;

                foreach (DataRow dr in vehTable.Rows)
                {
                    string[] value = msg.Split(',');

                    //3.Send To User 
                    DataTable dtProfile = sql.True2GetSendProfile((int)dr["group_id"], (int)evt_type, idx, (int)qType, -1);
                    foreach (DataRow dr2 in dtProfile.Rows)
                    {
                        ArrayList usrList = new ArrayList();

                        switch ((int)qType)
                        {
                            case (int)BROADCASTLOGTYPE.EMAIL:
                                {
                                    string user_id = (string)dr2["user_id"];
                                    foreach (string uid in user_id.Split(','))
                                    {
                                        DataTable dtUserInfo = sql.True2GetUserProfile(int.Parse(uid.Trim()));
                                        if (dtUserInfo.Rows.Count > 0)
                                        {
                                            TRUEUSERINFO info = new TRUEUSERINFO();
                                            info.name = (string)dtUserInfo.Rows[0]["name"];
                                            info.mobile = (string)dtUserInfo.Rows[0]["tel_no"];
                                            info.email = (string)dtUserInfo.Rows[0]["email"];
                                            usrList.Add(info);
                                        }
                                    }

                                    //Send E-Mail
                                    if (usrList.Count > 0)
                                    {
                                        emailObj.SendTrueZone(VehID, usrList, reg, value[1], timestamp, location, lat, lon, (int)evt_type);
                                    }
                                }
                                break;
                            case (int)BROADCASTLOGTYPE.SMS:
                                {
                                    string user_id = (string)dr2["user_id"];
                                    foreach (string uid in user_id.Split(','))
                                    {
                                        DataTable dtUserInfo = sql.True2GetUserProfile(int.Parse(uid.Trim()));
                                        if (dtUserInfo.Rows.Count > 0)
                                        {
                                            TRUEUSERINFO info = new TRUEUSERINFO();
                                            info.name = (string)dtUserInfo.Rows[0]["name"];
                                            info.mobile = (string)dtUserInfo.Rows[0]["tel_no"];
                                            info.email = (string)dtUserInfo.Rows[0]["email"];
                                            usrList.Add(info);
                                        }
                                    }

                                    //Send SMS
                                    if (usrList.Count > 0)
                                    {
                                        smsObj.True2BulkSmsSendToContact(VehID, usrList, reg, value[0], timestamp, location, lat, lon, evt_type, speed, "IO EVENT");
                                    }
                                }
                                break;
                            default: break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("Zebra Broadcast Error #2.5" + ex.Message);
            }
        }

        private void mySQLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MySQL frmMySQL = new MySQL();
            DialogResult r = frmMySQL.ShowDialog();
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     
        //tel : 09 Edit in _SMSCls.cs_ in _GetTelNo_ 
        
    }
}