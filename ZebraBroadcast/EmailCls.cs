using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ZebraCommonInterface;
using System.Collections;
using System.IO;

namespace ZebraBroadcast
{
    public enum PROFILEINFO_TYPE { ORIGINAL = 0, VALUE_TYPE = 1, ALERT = 2 }

    public enum RESP_STATUS { OK, ERR }

    public enum ALERT_TYPE { EMAIL, SMS, BULK_SMS }

    public enum SMSFORMAT { NORMAL = 0, POWERLINE = 1, GPSPLUG = 2, IOPORT = 3,ENGINE_ON_IN_PROHITBIT_TIME = 4,SPEEDING = 5, TRUEFORMATZONE = 28, TRUEFORMATIO = 29, TESTSYNTAX = 30, WatchDog = 999 }

    public struct PROFILEINFO
    {
        public int veh_id;
        public int fleet_id;
        public string fleet_name;
        public string veh_reg;
        public int max_speed;
        public string value;
        public string tel_no;
        public string email;
        public string subject;
        public string timestamp;
        public string location;
        public double lat;
        public double lon;
        public int staff_id;
        public string driver;
        public string contact_name;
        public int evt_id;
        public int speed;
        public string evt_desc;
        public string msg;
        public int format_send;
        public string noday;
        public string start_time;
        public string stop_time;
        public ALERT_TYPE alert_type;
        public PROFILEINFO_TYPE type;
        public SERVER_RESP srvResp;
    }

    public struct SERVER_RESP
    {
        public RESP_STATUS STATUS;
        public string DETAIL;
        public Int64[] SMID;
        public int resendCnt;
    }

    public delegate void SendEmailCompletedHandler(object sender, PROFILEINFO info) ;

    class EmailCls
    {
        public event SendEmailCompletedHandler OnSendEmailCompleted;
        SQLCls gSql;
        public EmailCls(String dbHost)
        {
            gSql = new SQLCls(dbHost);
        }

        public void SendEmail(int idx, int VehID, int evt_type, string value, string timestamp, string location, double lat, double lon, string msg)
        {
            try
            {
                bool stataus = true;
                DataTable dtProfile = new DataTable();
                DataTable checknewweb = new DataTable();
                ArrayList contactList = new ArrayList();

                if (evt_type == (int)EVT_TYPE.IN_ZONE || evt_type == (int)EVT_TYPE.OUT_ZONE || evt_type == (int)EVT_TYPE.IN_ROUTE || evt_type == (int)EVT_TYPE.OUT_ROUTE)
                {

                    //�������
                    checknewweb = gSql.Check_param_newweb(VehID, evt_type);
                    if (checknewweb.Rows.Count > 0)
                    {
                        dtProfile = gSql.GetFeatureProfile_New_Email(idx, VehID, evt_type, Convert.ToInt32(msg));
                    }
                    else
                    {
                        dtProfile = gSql.GetFeatureProfile(idx, VehID, evt_type, Convert.ToInt32(msg));
                    }                
                }
                else if (evt_type == (int)EVT_TYPE.SPEEDING)
                {

                    string strSpeedValue = value.Substring(13, 3);
                    strSpeedValue.Trim();
                    if ((Convert.ToInt16(strSpeedValue)) < 175)
                    {
                        //�������
                        checknewweb = gSql.Check_param_newweb(VehID, evt_type);
                        if (checknewweb.Rows.Count > 0)
                        {
                            dtProfile = gSql.GetFeatureProfileByDay_New_Email(VehID, evt_type, (int)DateTime.Now.DayOfWeek + 1);
                        }
                        else
                        {
                            dtProfile = gSql.GetFeatureProfileByDay(VehID, evt_type, (int)DateTime.Now.DayOfWeek + 1);
                        }
                    
                    }
                    else
                        stataus = false;
                }
                else
                {
                    //�������
                    checknewweb = gSql.Check_param_newweb(VehID, evt_type);
                    if (checknewweb.Rows.Count > 0)
                    {
                        dtProfile = gSql.GetFeatureProfile_New_Email(idx, VehID, evt_type);
                    }
                    else
                    {
                        dtProfile = gSql.GetFeatureProfile(idx, VehID, evt_type);
                    }
                }

                if (stataus)
                {

                    foreach (DataRow r in dtProfile.Rows)
                    {
                        PROFILEINFO info = new PROFILEINFO();
                        info.alert_type = ALERT_TYPE.EMAIL;
                        info.veh_id = (int)r["veh_id"];
                        info.fleet_id = (int)r["fleet_id"];
                        info.fleet_name = (string)r["fleet_desc"];
                        info.veh_reg = (string)r["registration"];
                        info.staff_id = (int)r["staff_id"];
                        info.driver = (string)r["driver"];
                        info.max_speed = (int)r["max_speed"];
                        info.value = value;
                        info.tel_no = (string)r["tel_no"];
                        info.email = (string)r["email"];
                        info.timestamp = timestamp;
                        info.location = location;
                        info.lat = lat;
                        info.lon = lon;
                        info.evt_id = (int)evt_type;
                        info.evt_desc = evt_type.ToString();
                        string evt_desc = (string)r["evt_desc"];

                        #region
                        switch (evt_type)
                        {
                            case (int)EVT_TYPE.SPEEDING:
                                info.subject = "Speeding Alert";
                                break;
                            case (int)EVT_TYPE.IN_ZONE:
                                info.subject = "In Zone Alert";
                                break;
                            case (int)EVT_TYPE.OUT_ZONE:
                                info.subject = "Out Zone Alert";
                                break;
                            case (int)EVT_TYPE.IN_ROUTE:
                                info.subject = "In Route Alert";
                                break;
                            case (int)EVT_TYPE.OUT_ROUTE:
                                info.subject = "Out Route Alert";
                                break;

                            case (int)EVT_TYPE.NO_INZONE_CURFEW:
                                info.subject = "In Zone In Curfew";
                                break;

                            case (int)EVT_TYPE.NO_OUTZONE_CURFEW:
                                info.subject = "Out Zone In Curfew";
                                break;

                            case (int)EVT_TYPE.ENGINE_ON_IN_PROHITBIT_TIME:
                                info.subject = "Engine On in Curfew";
                                break;

                            case (int)EVT_TYPE.TEMPERATURE:
                                info.subject = "Temperature";
                                break;

                            case (int)EVT_TYPE.IN_ZONE_CURFEW:
                                info.subject = "In Zone In Curfew";
                                break;

                            case (int)EVT_TYPE.OUT_ZONE_CURFEW:
                                info.subject = "Out Zone In Curfew";
                                break;
                            ///////////////////////////////////////////////////////////////

                            case (int)EVT_TYPE.EVENT_GPS_DISCONNECTED:
                                info.subject = "GPS IS UNPLUGED";
                                break;

                            case (int)EVT_TYPE.EVENT_GPS_CONNECTED:
                                info.subject = "GPS PLUG IN";
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED1_ON:
                                info.subject = "INPUT USER DEFINE 1 ON";
                                if (evt_desc != "?") { info.subject = evt_desc; }
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED1_OFF:
                                info.subject = "INPUT USER DEFINE 1 OFF";
                                if (evt_desc != "?") { info.subject = evt_desc; }
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED2_ON:
                                info.subject = "INPUT USER DEFINE 2 ON";
                                if (evt_desc != "?") { info.subject = evt_desc; }
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED2_OFF:
                                info.subject = "INPUT USER DEFINE 2 OFF";
                                if (evt_desc != "?") { info.subject = evt_desc; }
                                break;

                            case (int)EVT_TYPE.EVENT_IGNITION_LINE_ON:
                                info.subject = "IGNITION LINE ON";
                                break;

                            case (int)EVT_TYPE.EVENT_IGNITION_LINE_OFF:
                                info.subject = "IGNITION LINE OFF";
                                break;

                            case (int)EVT_TYPE.EVENT_POWER_LINE_DISCONNECTED:
                                info.subject = "POWER LINE IS UNPLUGED";
                                break;

                            case (int)EVT_TYPE.EVENT_POWER_LINE_CONNECTED:
                                info.subject = "POWER LINE PLUG IN";
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_ON:
                                info.subject = "OUTPUT USER DEFINE 1 ON";
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED1_OUT_OFF:
                                info.subject = "OUTPUT USER DEFINE 1 OFF";
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_ON:
                                info.subject = "OUTPUT USER DEFINE 2 ON";
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED2_OUT_OFF:
                                info.subject = "OUTPUT USER DEFINE 2 OFF";
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED3_OUT_ON:
                                info.subject = "OUTPUT USER DEFINE 3 ON";
                                break;

                            case (int)EVT_TYPE.EVENT_USER_DEFINED3_OUT_OFF:
                                info.subject = "OUTPUT USER DEFINE 3 OFF";
                                break;

                            ////////////////////////////////////////////////////////////////////

                            case (int)EVT_TYPE.ENGINE_ON_IN_ZONE:
                                info.subject = "ENGINE ON INZONE";
                                break;
                            case (int)EVT_TYPE.ENGINE_OFF_IN_ZONE:
                                info.subject = "ENGINE OFF INZONE";
                                break;
                            case (int)EVT_TYPE.ENGINE_ON_OUT_ZONE:
                                info.subject = "ENGINE ON OUTZONE";
                                break;
                            case (int)EVT_TYPE.ENGINE_OFF_OUT_ZONE:
                                info.subject = "ENGINE OFF OUTZONE";
                                break;
                            case (int)EVT_TYPE.NO_INZONE_CURFEW_WITH_SPEED:
                                info.subject = "In Zone Curfew With Speed";
                                break;
                            case (int)EVT_TYPE.NO_OUTZONE_CURFEW_WITH_SPEED:
                                info.subject = "Out Zone Curfew With Speed";
                                break;

                            case (int)EVT_TYPE.EXTERNAL_INPUT01_ON:
                                info.subject = "EXTERNAL INPUT01 ON";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT01_OFF:
                                info.subject = "EXTERNAL INPUT01 OFF";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT02_ON:
                                info.subject = "EXTERNAL INPUT02 ON";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT02_OFF:
                                info.subject = "EXTERNAL INPUT02 OFF";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT03_ON:
                                info.subject = "EXTERNAL INPUT03 ON";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT03_OFF:
                                info.subject = "EXTERNAL INPUT03 OFF";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT04_ON:
                                info.subject = "EXTERNAL INPUT04 ON";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT04_OFF:
                                info.subject = "EXTERNAL INPUT04 OFF";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT05_ON:
                                info.subject = "EXTERNAL INPUT05 ON";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT05_OFF:
                                info.subject = "EXTERNAL INPUT05 OFF";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT06_ON:
                                info.subject = "EXTERNAL INPUT06 ON";
                                break;
                            case (int)EVT_TYPE.EXTERNAL_INPUT06_OFF:
                                info.subject = "EXTERNAL INPUT06 OFF";
                                break;

                            case (int)EVT_TYPE.FUEL_OIL_ON:
                                info.subject = "FUEL OIL ON";
                                break;
                            case (int)EVT_TYPE.FUEL_OIL_OFF:
                                info.subject = "FUEL OIL OFF";
                                break;
                            case (int)EVT_TYPE.FUEL_GAS_ON:
                                info.subject = "FUEL GAS ON";
                                break;
                            case (int)EVT_TYPE.FUEL_GAS_OFF:
                                info.subject = "FUEL GAS OFF";
                                break;
                            case (int)EVT_TYPE.SOS:
                                info.subject = "SOS";
                                break;
                            case (int)EVT_TYPE.BUZZER_ON:
                                info.subject = "BUZZER ON";
                                break;
                            case (int)EVT_TYPE.BUZZER_OFF:
                                info.subject = "BUZZER OFF";
                                break;
                            case (int)EVT_TYPE.LED_ON:
                                info.subject = "LED ON";
                                break;
                            case (int)EVT_TYPE.LED_OFF:
                                info.subject = "LED OFF";
                                break;
                            case (int)EVT_TYPE.ENGINE_CONTROL_ON:
                                info.subject = "ENGINE CONTROL ON";
                                break;
                            case (int)EVT_TYPE.ENGINE_CONTROL_OFF:
                                info.subject = "ENGINE CONTROL OFF";
                                break;

                            case (int)EVT_TYPE.VALID_CARD:
                                info.subject = "VALID CARD ";
                                break;
                            case (int)EVT_TYPE.INVALID_CARD:
                                info.subject = "INVALID CARD ";
                                break;
                            case (int)EVT_TYPE.NO_CARD:
                                info.subject = "NO CARD ";
                                break;
                            case (int)EVT_TYPE.CARD_DATA_ERROR:
                                info.subject = "CARD DATA ERROR ";
                                break;

                            default:
                                info.subject = "";
                                break;
                        }
                        #endregion
                        contactList.Add(info);
                    }

                    Send(contactList);
                }
            }
            catch (Exception ex)
            {
                WriteLogEvent("SendEmail : " +ex.Message + " Event:" + evt_type.ToString());
            }
        }

        private void Send(ArrayList contactList)
        {
            if (contactList.Count < 1) { return; }
            try
            {
                ZebraBroadcast.Properties.Settings pf = new ZebraBroadcast.Properties.Settings();
                PROFILEINFO info = (PROFILEINFO)contactList[0];

                String mailFrom = pf.mailFrom.Trim();
                String mailTo = info.email;
                String smtpHost = pf.smtpHost.Trim();
                String mailSubject = info.subject + " (" + info.veh_reg + ")";
                String mailBody =
                    info.subject + " : " + info.value + "\n\r" +
                    "Fleet Name : " + info.fleet_name + "\n\r" +
                    "The vehicle registration : " + info.veh_reg + "\n\r" +
                    "Driver Name : " + info.driver + "\n\r" +
                    "Time : " + info.timestamp + "\n\r" +
                    "Location : " + info.location + "\n\r" +
                    "Latitude : " + Math.Round(info.lat, 6).ToString() + "\n\r" +
                    "Longitude : " + Math.Round(info.lon, 6).ToString();

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                String to = mailTo;
                String from = mailFrom;

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(from, to);
                msg.Body = mailBody;
                msg.Subject = mailSubject;

                if (contactList.Count > 1)
                {
                    foreach (PROFILEINFO info2 in contactList)
                    {
                        if (info2.email.Trim().Length > 0)
                        {
                            try
                            {
                                System.Net.Mail.MailAddress cc = new System.Net.Mail.MailAddress(info2.email);
                                msg.CC.Add(cc);
                            }
                            catch { }
                        }
                    }
                }

                smtp.Host = smtpHost;
                smtp.Credentials = new System.Net.NetworkCredential(@"gtt_robot@k-trak.com", "1q2wgtt_robot");
                smtp.Send(msg);

                if (OnSendEmailCompleted != null)
                {
                    OnSendEmailCompleted(this, info);
                }

                gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.EMAIL, info);
            }
            catch (Exception e)
            {
                WriteLogEvent("EmailSend : " + e.Message);
            }
        }

        public void SendTrueZone(int veh_id, ArrayList usrList, string reg, string Msg , string timestamp, string location, double lat, double lon, int evt_id)
        {
            try
            {
                ZebraBroadcast.Properties.Settings pf = new ZebraBroadcast.Properties.Settings();

                String subject = reg + " " + Msg;
                String mailFrom = pf.mailFrom.Trim();
                String mailTo = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).email;
                String smtpHost = pf.smtpHost.Trim();
                String mailSubject = subject;
                String mailBody =
                    "The vehicle registration :" + reg + "\n\r" +
                    "Time : " + timestamp + "\n\r" +
                    "Location : " + location + "\n\r" +
                    "Latitude : " + Math.Round(lat, 6).ToString() + "\n\r" +
                    "Longitude : " + Math.Round(lon, 6).ToString();
               
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                String to = mailTo;
                String from = mailFrom;

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(from, to);
                msg.Body = mailBody;
                msg.Subject = mailSubject;

                if (usrList.Count > 1)
                {
                    for (int i = 1; i < usrList.Count; i++)
                    {
                        if (((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[i]).email.Trim().Length > 0)
                        {
                            try
                            {
                                System.Net.Mail.MailAddress cc = new System.Net.Mail.MailAddress(((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[i]).email);
                                msg.CC.Add(cc);
                            }
                            catch { }
                        }
                    }
                }

                smtp.Host = smtpHost;
                smtp.Credentials = new System.Net.NetworkCredential(@"gtt_robot@k-trak.com", "1q2wgtt_robot");
                smtp.Send(msg);

                PROFILEINFO info = new PROFILEINFO();
                info.alert_type = ALERT_TYPE.EMAIL;
                info.veh_id = veh_id;
                info.contact_name = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).name;
                info.driver = "?";
                info.veh_reg = reg;
                info.timestamp = timestamp;
                info.lon = lon;
                info.lat = lat;
                info.tel_no = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).mobile;
                info.evt_id = evt_id;
                info.evt_desc = evt_id == (int)EVT_TYPE.TRUE_ZONEING ? EVT_TYPE.TRUE_ZONEING.ToString() : EVT_TYPE.TRUE_IOSTATUS.ToString();
                if (OnSendEmailCompleted != null)
                {
                    OnSendEmailCompleted(this, info);
                }

                gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.EMAIL, info);
            }
            catch (Exception e)
            {
                //GlobalClass.WriteLogEvent("Errror #2 : " + e.Message);
            }
        }
        public void SendTrueMsg(int veh_id, ArrayList usrList, string reg, string value, string timestamp, string location, double lat, double lon, System.Net.Mail.MailPriority mailType, int evt_id)
        {
            try
            {
                ZebraBroadcast.Properties.Settings pf = new ZebraBroadcast.Properties.Settings();

                String subject = reg + " " + value;
                String mailFrom = pf.mailFrom.Trim();
                String mailTo = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).email;
                String smtpHost = pf.smtpHost.Trim();
                String mailSubject = subject;
                String mailBody =
                    "The vehicle registration :" + reg + "\n\r" +
                    "Time : " + timestamp + "\n\r" +
                    "Location : " + location + "\n\r" +
                    "Latitude : " + Math.Round(lat, 6).ToString() + "\n\r" +
                    "Longitude : " + Math.Round(lon, 6).ToString();

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                String to = mailTo;
                String from = mailFrom;

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(from, to);
                msg.Body = mailBody;
                msg.Subject = mailSubject;
                msg.Priority = mailType;

                if (usrList.Count > 1)
                {
                    for (int i = 1; i < usrList.Count; i++)
                    {
                        if (((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[i]).email.Trim().Length > 0)
                        {
                            try
                            {
                                System.Net.Mail.MailAddress cc = new System.Net.Mail.MailAddress(((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[i]).email);
                                msg.CC.Add(cc);
                            }
                            catch { }
                        }
                    }
                }

                smtp.Host = smtpHost;
                smtp.Credentials = new System.Net.NetworkCredential(@"gtt_robot@k-trak.com", "1q2wgtt_robot");
                smtp.Send(msg);

                PROFILEINFO info = new PROFILEINFO();
                info.alert_type = ALERT_TYPE.EMAIL;
                info.veh_id = veh_id;
                info.contact_name = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).name;
                info.driver = "?";
                info.veh_reg = reg;
                info.timestamp = timestamp;
                info.lon = lon;
                info.lat = lat;
                info.tel_no = ((ZebraBroadcast.MainFrm.TRUEUSERINFO)usrList[0]).mobile;
                info.evt_id = evt_id;

                if (OnSendEmailCompleted != null)
                {
                    OnSendEmailCompleted(this, info);
                }

                gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.EMAIL, info);
            }
            catch (Exception e)
            {
                //GlobalClass.WriteLogEvent("Errror #3 : " + e.Message);
            }
        }

        public void SendTestZone(int veh_id, ArrayList usrList, string reg, string zone_label, string zone_type, string timestamp, string location, double lat, double lon, System.Net.Mail.MailPriority mailType, int evt_id)
        {
            try
            {
                ZebraBroadcast.Properties.Settings pf = new ZebraBroadcast.Properties.Settings();

                String subject = reg + " (" + zone_type + ") " + zone_label;
                String mailFrom = pf.mailFrom.Trim();
                String mailTo = ((ZebraBroadcast.MainFrm.TESTUSERINFO)usrList[0]).email;
                String smtpHost = pf.smtpHost.Trim();
                String mailSubject = subject;
                String mailBody =
                    "The vehicle registration :" + reg + "\n\r" +
                    "Time : " + timestamp + "\n\r" +
                    "Location : " + location + "\n\r" +
                    "Latitude : " + Math.Round(lat, 6).ToString() + "\n\r" +
                    "Longitude : " + Math.Round(lon, 6).ToString();

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                String to = mailTo;
                String from = mailFrom;

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(from, to);
                msg.Body = mailBody;
                msg.Subject = mailSubject;
                msg.Priority = mailType;

                if (usrList.Count > 1)
                {
                    for (int i = 1; i < usrList.Count; i++)
                    {
                        if (((ZebraBroadcast.MainFrm.TESTUSERINFO)usrList[i]).email.Trim().Length > 0)
                        {
                            try
                            {
                                System.Net.Mail.MailAddress cc = new System.Net.Mail.MailAddress(((ZebraBroadcast.MainFrm.TESTUSERINFO)usrList[i]).email);
                                msg.CC.Add(cc);
                            }
                            catch { }
                        }
                    }
                }

                smtp.Host = smtpHost;
                smtp.Credentials = new System.Net.NetworkCredential(@"gtt_robot@k-trak.com", "1q2wgtt_robot");
                smtp.Send(msg);

                PROFILEINFO info = new PROFILEINFO();
                info.alert_type = ALERT_TYPE.EMAIL;
                info.veh_id = veh_id;
                info.contact_name = ((ZebraBroadcast.MainFrm.TESTUSERINFO)usrList[0]).name;
                info.driver = "?";
                info.veh_reg = reg;
                info.timestamp = timestamp;
                info.lon = lon;
                info.lat = lat;
                info.tel_no = ((ZebraBroadcast.MainFrm.TESTUSERINFO)usrList[0]).mobile;
                info.evt_id = evt_id;

                if (OnSendEmailCompleted != null)
                {
                    OnSendEmailCompleted(this, info);
                }

                gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.EMAIL, info);
            }
            catch (Exception e)
            {
                //GlobalClass.WriteLogEvent("Errror #2 : " + e.Message);
            }
        }

        public void SendTrueZone2(int veh_id, ArrayList usrList, string reg, string zone_label, string zone_type, string timestamp, string location, double lat, double lon, System.Net.Mail.MailPriority mailType, int evt_id)
        {
            try
            {
                ZebraBroadcast.Properties.Settings pf = new ZebraBroadcast.Properties.Settings();

                if (usrList.Count > 1)
                {
                    for (int i = 0; i < usrList.Count; i++)
                    {
                        if (((ZebraBroadcast.MainFrm.TRUEUSERINFO2)usrList[i]).email.Trim().Length > 0)
                        {
                            try
                            {
                                String subject = reg + " (" + zone_type + ") " + zone_label;
                                String mailFrom = pf.mailFrom.Trim();
                                String mailTo = ((ZebraBroadcast.MainFrm.TRUEUSERINFO2)usrList[i]).email;
                                String smtpHost = pf.smtpHost.Trim();
                                String mailSubject = subject;
                                String mailBody =
                                    "The vehicle registration :" + reg + "\n\r" +
                                    "Time : " + timestamp + "\n\r" +
                                    "Location : " + location + "\n\r" +
                                    "Latitude : " + Math.Round(lat, 6).ToString() + "\n\r" +
                                    "Longitude : " + Math.Round(lon, 6).ToString();

                                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                                String to = mailTo;
                                String from = mailFrom;

                                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(from, to);
                                msg.Body = mailBody;
                                msg.Subject = mailSubject;
                                msg.Priority = mailType;

                                smtp.Host = smtpHost;
                                smtp.Credentials = new System.Net.NetworkCredential(@"gtt_robot@k-trak.com", "1q2wgtt_robot");
                                smtp.Send(msg);

                                PROFILEINFO info = new PROFILEINFO();
                                info.alert_type = ALERT_TYPE.EMAIL;
                                info.veh_id = veh_id;
                                info.contact_name = ((ZebraBroadcast.MainFrm.TRUEUSERINFO2)usrList[i]).name;
                                info.driver = "?";
                                info.veh_reg = reg;
                                info.timestamp = timestamp;
                                info.lon = lon;
                                info.lat = lat;
                                info.tel_no = ((ZebraBroadcast.MainFrm.TRUEUSERINFO2)usrList[i]).mobile;
                                info.evt_id = evt_id;

                                if (OnSendEmailCompleted != null)
                                {
                                    OnSendEmailCompleted(this, info);
                                }

                                gSql.InsertNewBroadcastLog(BROADCASTLOGTYPE.EMAIL, info);
                            }
                            catch { }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //GlobalClass.WriteLogEvent("Errror #2 : " + e.Message);
            }
        }

        public string TestSendMail(ArrayList contactList)
        {
            if (contactList.Count < 1) { return "Send Fail.."; }
            try
            {
                ZebraBroadcast.Properties.Settings pf = new ZebraBroadcast.Properties.Settings();
                PROFILEINFO info = (PROFILEINFO)contactList[0];

                String mailFrom = pf.mailFrom.Trim();
                String mailTo = info.email;
                String smtpHost = pf.smtpHost.Trim();
                String mailSubject = info.subject;
                String mailBody = info.msg;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                String to = mailTo;
                String from = mailFrom;

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(from, to);
                msg.Body = mailBody;
                msg.Subject = mailSubject;

                if (contactList.Count > 1)
                {
                    foreach (PROFILEINFO info2 in contactList)
                    {
                        if (info2.email.Trim().Length > 0)
                        {
                            try
                            {
                                System.Net.Mail.MailAddress cc = new System.Net.Mail.MailAddress(info2.email);
                                msg.CC.Add(cc);
                            }
                            catch { }
                        }
                    }
                }

                smtp.Host = smtpHost;
                smtp.Credentials = new System.Net.NetworkCredential(@"gtt_robot", "1q2wgtt_robot");
                smtp.Send(msg);

                if (OnSendEmailCompleted != null)
                {
                    OnSendEmailCompleted(this, info);
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return "Send Success";
               
        }

        public void WriteLogEvent(String msg)
        {
            msg = DateTime.Now.ToString() + " : " + msg;
            String fileName = "ErrorEvent_" + DateTime.Now.ToShortDateString() + ".txt";
            if (File.Exists(fileName))
            {
                StreamWriter sr = File.AppendText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
            else
            {
                StreamWriter sr = File.CreateText(fileName);
                sr.WriteLine(msg);
                sr.Close();
                sr.Dispose();
            }
        }

    }
}