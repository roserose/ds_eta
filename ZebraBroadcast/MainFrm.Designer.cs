namespace ZebraBroadcast
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsGSMStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testSMSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testSMS2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSendSMS = new System.Windows.Forms.ToolStripMenuItem();
            this.testRemoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testBulkSmsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testEmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mySQLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.cbMAA = new System.Windows.Forms.CheckBox();
            this.btExit = new System.Windows.Forms.Button();
            this.lbl_SystemQ = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ZebraBroadcast.Properties.Resources.broadcast;
            this.pictureBox1.Location = new System.Drawing.Point(12, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(115, 284);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.tslbStatus,
            this.toolStripStatusLabel3,
            this.tsGSMStatus,
            this.lbl_SystemQ});
            this.statusStrip1.Location = new System.Drawing.Point(0, 316);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(609, 22);
            this.statusStrip1.TabIndex = 1;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(45, 17);
            this.toolStripStatusLabel1.Text = "Status :";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // tslbStatus
            // 
            this.tslbStatus.Name = "tslbStatus";
            this.tslbStatus.Size = new System.Drawing.Size(12, 17);
            this.tslbStatus.Text = "?";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(119, 17);
            this.toolStripStatusLabel3.Text = "GSM Module Staus  : ";
            // 
            // tsGSMStatus
            // 
            this.tsGSMStatus.Name = "tsGSMStatus";
            this.tsGSMStatus.Size = new System.Drawing.Size(12, 17);
            this.tsGSMStatus.Text = "-";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Location = new System.Drawing.Point(133, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(472, 286);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "The Last Messages";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ControlText;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.richTextBox1.ForeColor = System.Drawing.Color.Lime;
            this.richTextBox1.Location = new System.Drawing.Point(6, 16);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(460, 264);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(609, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testSMSToolStripMenuItem,
            this.testSMS2ToolStripMenuItem,
            this.tsSendSMS,
            this.testRemoteToolStripMenuItem,
            this.testBulkSmsToolStripMenuItem,
            this.testEmailToolStripMenuItem,
            this.mySQLToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // testSMSToolStripMenuItem
            // 
            this.testSMSToolStripMenuItem.Enabled = false;
            this.testSMSToolStripMenuItem.Name = "testSMSToolStripMenuItem";
            this.testSMSToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.testSMSToolStripMenuItem.Text = "Test &SMS1";
            this.testSMSToolStripMenuItem.Click += new System.EventHandler(this.testSMSToolStripMenuItem_Click);
            // 
            // testSMS2ToolStripMenuItem
            // 
            this.testSMS2ToolStripMenuItem.Enabled = false;
            this.testSMS2ToolStripMenuItem.Name = "testSMS2ToolStripMenuItem";
            this.testSMS2ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.testSMS2ToolStripMenuItem.Text = "Test SMS2";
            this.testSMS2ToolStripMenuItem.Click += new System.EventHandler(this.testSMS2ToolStripMenuItem_Click);
            // 
            // tsSendSMS
            // 
            this.tsSendSMS.Name = "tsSendSMS";
            this.tsSendSMS.Size = new System.Drawing.Size(146, 22);
            this.tsSendSMS.Text = "Send SMS";
            this.tsSendSMS.Click += new System.EventHandler(this.tsSendSMS_Click);
            // 
            // testRemoteToolStripMenuItem
            // 
            this.testRemoteToolStripMenuItem.Name = "testRemoteToolStripMenuItem";
            this.testRemoteToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.testRemoteToolStripMenuItem.Text = "Test Remote";
            this.testRemoteToolStripMenuItem.Click += new System.EventHandler(this.testRemoteToolStripMenuItem_Click);
            // 
            // testBulkSmsToolStripMenuItem
            // 
            this.testBulkSmsToolStripMenuItem.Name = "testBulkSmsToolStripMenuItem";
            this.testBulkSmsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.testBulkSmsToolStripMenuItem.Text = "Test Bulk Sms";
            this.testBulkSmsToolStripMenuItem.Click += new System.EventHandler(this.testBulkSmsToolStripMenuItem_Click);
            // 
            // testEmailToolStripMenuItem
            // 
            this.testEmailToolStripMenuItem.Name = "testEmailToolStripMenuItem";
            this.testEmailToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.testEmailToolStripMenuItem.Text = "Test Email";
            this.testEmailToolStripMenuItem.Click += new System.EventHandler(this.testEmailToolStripMenuItem_Click);
            // 
            // mySQLToolStripMenuItem
            // 
            this.mySQLToolStripMenuItem.Name = "mySQLToolStripMenuItem";
            this.mySQLToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.mySQLToolStripMenuItem.Text = "MySQL";
            this.mySQLToolStripMenuItem.Click += new System.EventHandler(this.mySQLToolStripMenuItem_Click);
            // 
            // cbMAA
            // 
            this.cbMAA.AutoSize = true;
            this.cbMAA.Location = new System.Drawing.Point(421, 12);
            this.cbMAA.Name = "cbMAA";
            this.cbMAA.Size = new System.Drawing.Size(103, 17);
            this.cbMAA.TabIndex = 4;
            this.cbMAA.Text = "Mobile Alert App";
            this.cbMAA.UseVisualStyleBackColor = true;
            // 
            // btExit
            // 
            this.btExit.Location = new System.Drawing.Point(530, 8);
            this.btExit.Name = "btExit";
            this.btExit.Size = new System.Drawing.Size(75, 23);
            this.btExit.TabIndex = 5;
            this.btExit.Text = "Exit";
            this.btExit.UseVisualStyleBackColor = true;
            this.btExit.Click += new System.EventHandler(this.btExit_Click);
            // 
            // lbl_SystemQ
            // 
            this.lbl_SystemQ.Name = "lbl_SystemQ";
            this.lbl_SystemQ.Size = new System.Drawing.Size(96, 17);
            this.lbl_SystemQ.Text = "SystemsQueue: -";
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 338);
            this.Controls.Add(this.btExit);
            this.Controls.Add(this.cbMAA);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zebra Broadcast V2.0 (Build 2013/06/14)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.MainFrm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tslbStatus;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testSMSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRemoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testSMS2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsSendSMS;
        private System.Windows.Forms.ToolStripMenuItem testBulkSmsToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel tsGSMStatus;
        private System.Windows.Forms.ToolStripMenuItem testEmailToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStripMenuItem mySQLToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbMAA;
        private System.Windows.Forms.Button btExit;
        private System.Windows.Forms.ToolStripStatusLabel lbl_SystemQ;
    }
}

