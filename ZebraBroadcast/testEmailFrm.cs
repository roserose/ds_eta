using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ZebraBroadcast
{
    public partial class testEmailFrm : Form
    {
        public testEmailFrm()
        {
            InitializeComponent();
        }

        private void btSend_Click(object sender, EventArgs e)
        {

        }

        private void testEmailFrm_Load(object sender, EventArgs e)
        {
            this.tbTo.Text = "@k-trak.com";
            this.tbMsg.Text = "Test Message" + "\r\nTime : " + DateTime.Now.ToString();
            this.tbSubject.Text = "Test Email " + DateTime.Now.ToString(); 
        }
    }
}